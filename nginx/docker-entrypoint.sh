#!/bin/sh

if [[ -z $1 ]]; then
    echo 'ERROR: No input file given.'
    exit 1
fi

if [[ -z $2 ]]; then
    echo 'ERROR: No target file given.'
    exit 1
fi
# exec cat $1
# Substitute all environment variables defined in the file given as argument
envsubst '\$ENVIRONMENT_TYPE \$SCHEME_URL' < $1 > $2
# For multiple vars separate them with a space
# exec cat $2

# Execute nginx
# exec ${@:56}
exec nginx

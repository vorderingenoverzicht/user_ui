import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';
import 'package:url_launcher_platform_interface/url_launcher_platform_interface.dart';
import 'package:user_ui/robots/app_robot.dart';
import 'package:user_ui/robots/home_robot.dart';
import 'package:user_ui/robots/onboarding_robot.dart';

class MockUrlLauncher extends Mock
    with MockPlatformInterfaceMixin
    implements UrlLauncherPlatform {}

void main() {
  final binding = IntegrationTestWidgetsFlutterBinding.ensureInitialized();
  binding.framePolicy = LiveTestWidgetsFlutterBindingFramePolicy.fullyLive;

  group(
    'Onboarding scenario',
    () {
      testWidgets(
        'Happy flow until home screen',
        (tester) async {
          // Setup
          UrlLauncherPlatform.instance = setupMockUrlLauncher();

          // Create robots
          OnboardingRobot onboardingRobot = OnboardingRobot(tester);
          HomeRobot homeRobot = HomeRobot(tester);
          AppRobot appRobot = AppRobot(tester);

          await appRobot.launchTheApp();
          await onboardingRobot.assertThatUserIsOnThePropositionScreen();
          await onboardingRobot.navigateToTheDataUseScreen();
          await onboardingRobot.assertThatUserIsOnTheDataUseScreen();
          await onboardingRobot.navigateToTheActivationInfoScreen();
          await onboardingRobot.assertThatUserIsOnTheActivationInfoScreen();
          await onboardingRobot.navigateToStartActivationScreen();
          await onboardingRobot
              .assertThatUserIsOnTheStartActivationInfoScreen();
          await onboardingRobot.navigateToCompleteActivationScreen();
          await homeRobot.assertThatUserIsOnTheHomeScreen();
        },
      );
    },
  );
}

MockUrlLauncher setupMockUrlLauncher() {
  final mock = MockUrlLauncher();
  registerFallbackValue(const LaunchOptions());

  when(() => mock.launchUrl(any(), any())).thenAnswer((_) async => true);
  when(() => mock.canLaunch(any())).thenAnswer((_) async => true);

  return mock;
}

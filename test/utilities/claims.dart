class Claims {
  final String appPublicKey;
  final String appManagerOin;
  final String appManagerPublicKey;
  final String givenName;
  final String familyName;
  final String birthdate;
  final String bsn;
  final String scope;
  final int leewayNotBeforeMinutes;
  final int ucCertificateExpirationMinutes;
  late int iat;
  late int nbf;
  late int exp;
  // Default values based on the original variable
  Claims({
    this.appPublicKey = 'default_app_public_key',
    this.appManagerOin = '00000001001172773000',
    this.appManagerPublicKey = 'default_app_manager_public_key',
    this.givenName = 'John',
    this.familyName = 'Doe',
    this.birthdate = 'birthdate',
    this.bsn = 'registration_bsn_here',
    this.scope = 'nl.vorijk.oauth_scope.blauwe_knop',
    this.leewayNotBeforeMinutes = -2,
    this.ucCertificateExpirationMinutes = 60,
  }) {
    iat = DateTime.now().toUtc().millisecondsSinceEpoch ~/ 1000;
    nbf = DateTime.now()
            .add(Duration(minutes: leewayNotBeforeMinutes))
            .toUtc()
            .millisecondsSinceEpoch ~/
        1000;
    exp = DateTime.now()
            .add(Duration(minutes: ucCertificateExpirationMinutes))
            .toUtc()
            .millisecondsSinceEpoch ~/
        1000;
  }

  // CopyWith function remains unchanged
  Claims copyWith({
    String? appPublicKey,
    String? appManagerOin,
    String? appManagerPublicKey,
    String? givenName,
    String? familyName,
    String? birthdate,
    String? bsn,
    String? scope,
    int? leewayNotBeforeMinutes,
    int? ucCertificateExpirationMinutes,
  }) {
    return Claims(
      appPublicKey: appPublicKey ?? this.appPublicKey,
      appManagerOin: appManagerOin ?? this.appManagerOin,
      appManagerPublicKey: appManagerPublicKey ?? this.appManagerPublicKey,
      givenName: givenName ?? this.givenName,
      familyName: familyName ?? this.familyName,
      birthdate: birthdate ?? this.birthdate,
      bsn: bsn ?? this.bsn,
      scope: scope ?? this.scope,
      leewayNotBeforeMinutes:
          leewayNotBeforeMinutes ?? this.leewayNotBeforeMinutes,
      ucCertificateExpirationMinutes:
          ucCertificateExpirationMinutes ?? this.ucCertificateExpirationMinutes,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'app_public_key': appPublicKey,
      'app_manager_oin': appManagerOin,
      'app_manager_public_key': appManagerPublicKey,
      'given_name': givenName,
      'family_name': familyName,
      'birthdate': birthdate,
      'bsn': bsn,
      'scope': scope,
      'iat': iat,
      'nbf': nbf,
      'exp': exp,
    };
  }
}

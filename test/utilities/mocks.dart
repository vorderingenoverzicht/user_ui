import 'package:fcid_library/fcid_library.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:mocktail/mocktail.dart';
import 'package:user_ui/clients/scheme/scheme_client.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client.dart';
import 'package:user_ui/clients/session/session_client.dart';
import 'package:user_ui/entities/app_manager_selection.dart';
import 'package:user_ui/entities/app_session.dart';
import 'package:user_ui/entities/scheme_app_manager.dart' as app_manager_entity;
import 'package:user_ui/entities/scheme_document_type.dart' as document_entity;
import 'package:user_ui/entities/user_settings.dart';
import 'package:user_ui/repositories/app_identity/app_root_keypair.dart';
import 'package:user_ui/repositories/app_identity/app_session_repository.dart';
import 'package:user_ui/repositories/app_manager_selection/app_manager_selection_repository.dart';
import 'package:user_ui/repositories/certificate/certificate_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_inbox/financial_claims_information_inbox_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_storage/financial_claims_information_storage_repository.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/organization_selection/organization_selection_repository.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';
import 'package:user_ui/repositories/scheme_app_manager/fetch_scheme_app_manager_notifier.dart';
import 'package:user_ui/repositories/scheme_app_manager/scheme_app_manager_repository.dart';
import 'package:user_ui/repositories/scheme_document_type/scheme_document_type_repository.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_organization_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';

class MockSessionClient extends Mock implements SessionClient {}

class MockSchemeClient extends Mock implements SchemeClient {}

class MockServiceDiscoveryClient extends Mock
    implements ServiceDiscoveryClient {}

// REPOSITORIES
class MockFetchSchemeAppManager extends Mock implements FetchSchemeAppManager {}

class MockCertificateRepository extends Mock implements CertificateRepository {}

class MockAppRootKeypair extends Mock implements AppRootKeypair {}

class MockLoggingHelper extends Mock implements LoggingHelper {}

class MockOrganizationSelectionRepository extends Mock
    implements OrganizationSelectionRepository {}

class MockFinancialClaimsInformationConfigurationRepository extends Mock
    implements FinancialClaimsInformationConfigurationRepository {}

class MockFinancialClaimsInformationRequestRepository extends Mock
    implements FinancialClaimsInformationRequestRepository {}

class MockFinancialClaimsInformationInboxRepository extends Mock
    implements FinancialClaimsInformationInboxRepository {}

class MockFinancialClaimsInformationStorageRepository extends Mock
    implements FinancialClaimsInformationStorageRepository {}

class MockUserSettingsRepository extends StateNotifier<UserSettings>
    with Mock
    implements UserSettingsRepository {
  MockUserSettingsRepository(super.state);
}

class MockAppManagerSelectionRepository extends Mock
    implements AppManagerSelectionRepository {}

class MockRegistrationRepository extends Mock
    implements RegistrationRepository {}

class MockAppSessionRepository
    extends AutoDisposeAsyncNotifier<List<AppSession>>
    with Mock
    implements AppSessionRepository {}

class MockDriftSchemeOrganizationRepository
    extends StateNotifier<List<SchemeOrganization>>
    with Mock
    implements SchemeOrganizationRepository {
  MockDriftSchemeOrganizationRepository(super.state);
}

class MockDriftSchemeAppManagerRepository
    extends StateNotifier<List<app_manager_entity.SchemeAppManager>>
    with Mock
    implements SchemeAppManagerRepository {
  MockDriftSchemeAppManagerRepository(super.state);
}

class MockDriftSchemeDocumentTypeRepository
    extends StateNotifier<List<document_entity.SchemeDocumentType>>
    with Mock
    implements SchemeDocumentTypeRepository {
  MockDriftSchemeDocumentTypeRepository(super.state);
}

class MockDriftAppManagerSelectionRepository
    extends StateNotifier<AppManagerSelection?>
    with Mock
    implements AppManagerSelectionRepository {
  MockDriftAppManagerSelectionRepository(super.state);
}

class FakeSchemeOrganizationEntity extends Fake implements SchemeOrganization {}

class FakeSchemeDocumentEntity extends Fake
    implements document_entity.SchemeDocumentType {}

class FakeSchemeAppManagerEntity extends Fake
    implements app_manager_entity.SchemeAppManager {}

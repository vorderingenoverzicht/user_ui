import 'package:fcid_library/fcid_v4.dart';
import 'package:fcid_library/fcid_library.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('FinancialClaimsInformationDocumentV4', () {
    test('parses JSON correctly', () {
      final financialClaimsInfoJson = {
        "type": "FINANCIAL_CLAIMS_INFORMATION_DOCUMENT_V4",
        "version": "4",
        "body": {
          "aangeleverd_door": "00000004000000044000",
          "document_datumtijd": "2024-07-18T10:07:16.067219+02:00",
          "bsn": "814859094",
          "beschikbaarheid": {
            "CJIB_VERKEERSBOETE": "NIET_BESCHIKBAAR",
            "CJIB_KOSTEN_EERSTE_AANMANING": "BESCHIKBAAR",
            "CJIB_ADMINISTRATIEKOSTEN": "BESCHIKBAAR",
          },
          "financiele_zaken": [
            {
              "zaakkenmerk": "81029991234567891",
              "bsn": "814859094",
              "totaal_financieel_verplicht": 16500,
              "totaal_financieel_vereffend": 0,
              "saldo": 16500,
              "saldo_datumtijd": "2023-05-11T00:00:00Z",
              "gebeurtenissen": [
                {
                  "datumtijd_opgelegd": "2023-03-31T10:25:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "beschikkingsnummer": "81029991234567891-123",
                  "bsn": "814859094",
                  "primaire_verplichting": true,
                  "type": "CJIB_WAHV",
                  "categorie": "Algemeen",
                  "bedrag": 10400,
                  "omschrijving":
                      "Kenteken: a-000-aa; 11 km te hard buiten de bebouwde kom op 31 maart 2023",
                  "juridische_grondslag_omschrijving":
                      "Boete voor te hard rijden Artikel 21 (RVV 1990)",
                  "juridische_grondslag_bron":
                      "http://wetten.overheid.nl/jci1.3:c:BWBR0004825\u0026hoofdstuk=II\u0026paragraaf=8\u0026artikel=21\u0026z=2023-01-01\u0026g=2023-01-01",
                  "opgelegd_door": "00000004000000044000",
                  "uitgevoerd_door": "00000004000000044000",
                  "gebeurtenis_type": "FinancieleVerplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-1",
                  "datumtijd_gebeurtenis": "2023-03-31T10:25:00Z"
                },
                {
                  "datumtijd_opgelegd": "2023-04-01T00:00:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "beschikkingsnummer": "81029991234567891-123",
                  "bsn": "814859094",
                  "primaire_verplichting": false,
                  "type": "CJIB_ADMINISTRATIEKOSTEN",
                  "categorie": "Administratiekosten",
                  "bedrag": 900,
                  "omschrijving": "Administratiekosten",
                  "juridische_grondslag_omschrijving":
                      "CJIB mag op grond van Artikel 63d van het Organisatiebesluit Ministerie van Justitie en Veiligheid administratiekosten in rekening brengen",
                  "juridische_grondslag_bron":
                      "http://wetten.overheid.nl/BWBR0045971/2021-12-03/0",
                  "opgelegd_door": "00000004000000044000",
                  "uitgevoerd_door": "00000004000000044000",
                  "gebeurtenis_type": "FinancieleVerplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-2",
                  "datumtijd_gebeurtenis": "2023-04-01T00:00:00Z"
                },
                {
                  "datumtijd_opgelegd": "2023-04-02T00:00:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "bsn": "814859094",
                  "bedrag": 11300,
                  "type": "Aanschrijving",
                  "betaalwijze": "Handmatig",
                  "te_betalen_aan": "00000004000000044000",
                  "rekeningnummer": "GB33BUKB20201555555555",
                  "rekeningnummer_tenaamstelling": "CJIB",
                  "betalingskenmerk": "81029991234567891-1",
                  "vervaldatum": "2023-05-02T00:00:00Z",
                  "gebeurtenis_type": "BetalingsverplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-3",
                  "datumtijd_gebeurtenis": "2023-04-02T00:00:00Z"
                },
                {
                  "ingetrokken_gebeurtenis_kenmerk": "81029991234567891-3",
                  "gebeurtenis_type": "BetalingsverplichtingIngetrokken",
                  "gebeurtenis_kenmerk": "81029991234567891-5",
                  "datumtijd_gebeurtenis": "2023-05-05T00:00:00Z",
                  "datumtijd_ingetrokken": "2023-04-05T00:00:00Z"
                },
                {
                  "datumtijd_opgelegd": "2023-05-11T00:00:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "beschikkingsnummer": "81029991234567891-123",
                  "bsn": "814859094",
                  "primaire_verplichting": false,
                  "type": "CJIB_KOSTEN_EERSTE_AANMANING",
                  "categorie": "Algemeen",
                  "bedrag": 5200,
                  "omschrijving": "Kosten eerste aanmaning",
                  "juridische_grondslag_omschrijving":
                      "CJIB mag op grond van Artikel 63d van het Organisatiebesluit Ministerie van Justitie en Veiligheid aanmaningskosten in rekening brengen",
                  "juridische_grondslag_bron":
                      "http://wetten.overheid.nl/BWBR0045971/2021-12-03/0",
                  "opgelegd_door": "00000004000000044000",
                  "uitgevoerd_door": "00000004000000044000",
                  "gebeurtenis_type": "FinancieleVerplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-4",
                  "datumtijd_gebeurtenis": "2023-05-11T00:00:00Z"
                },
                {
                  "datumtijd_opgelegd": "2023-05-11T00:00:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "bsn": "814859094",
                  "bedrag": 16500,
                  "type": "Aanmaning",
                  "betaalwijze": "Handmatig",
                  "te_betalen_aan": "00000004000000044000",
                  "rekeningnummer": "GB33BUKB20201555555555",
                  "rekeningnummer_tenaamstelling": "CJIB",
                  "betalingskenmerk": "81029991234567891-2",
                  "vervaldatum": "2023-05-31T00:00:00Z",
                  "gebeurtenis_type": "BetalingsverplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-6",
                  "datumtijd_gebeurtenis": "2023-05-11T00:00:00Z"
                }
              ],
              "achterstanden": ["81029991234567891-6"],
              "contact_opties": [
                {
                  "contact_vorm": "telefoon",
                  "url": "tel:+31612345678",
                  "naam": "Telefoon",
                  "omschrijving": "Nieuwe omschrijving voor telefoon",
                  "prioriteit": 1
                },
                {
                  "contact_vorm": "e_mail",
                  "url":
                      "mailto:nieuwemail@example.com?subject=Nieuw onderwerp",
                  "naam": "E-mail",
                  "omschrijving": "Nieuwe omschrijving voor e-mail",
                  "prioriteit": 2
                },
                {
                  "contact_vorm": "persoonlijke_website",
                  "url": "https://www.example.nl/persoonlijk",
                  "naam": "Persoonlijke website",
                  "omschrijving":
                      "Nieuwe omschrijving voor persoonlijke website",
                  "prioriteit": 3
                },
                {
                  "contact_vorm": "publieke_website",
                  "url": "https://www.example.nl/publiek",
                  "naam": "Website",
                  "omschrijving": "Nieuwe omschrijving voor publieke website",
                  "prioriteit": 4
                }
              ]
            }
          ]
        }
      };

      final document =
          FinancialClaimsInformationDocument.fromJson(financialClaimsInfoJson);

      expect(document.type, equals('FINANCIAL_CLAIMS_INFORMATION_DOCUMENT_V4'));
      expect(document.version, equals('4'));

      expect(document.body.beschikbaarheid?.length, equals(3));
      expect(document.body.beschikbaarheid?['CJIB_VERKEERSBOETE'],
          equals(Beschikbaarheidsstatus.nietBeschikbaar));
      expect(document.body.beschikbaarheid?['CJIB_KOSTEN_EERSTE_AANMANING'],
          equals(Beschikbaarheidsstatus.beschikbaar));
      expect(document.body.beschikbaarheid?['CJIB_ADMINISTRATIEKOSTEN'],
          equals(Beschikbaarheidsstatus.beschikbaar));

      final financieleZaak = document.body.financieleZaken.first;

      expect(financieleZaak.contactOpties?.length, equals(4));
      expect(financieleZaak.contactOpties?[0].contactVorm,
          equals(ContactVorm.telefoon));
      expect(financieleZaak.contactOpties?[0].url, equals('tel:+31612345678'));

      expect(financieleZaak.totaalFinancieelVerplicht, equals(16500));
      expect(financieleZaak.totaalFinancieelVereffend, equals(0));
    });
  });
}

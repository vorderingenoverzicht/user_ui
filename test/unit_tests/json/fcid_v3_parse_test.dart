import 'package:fcid_library/fcid_v3.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('FinancialClaimsInformationDocumentV4', () {
    test('parses JSON correctly', () {
      final financialClaimsInfoJson = {
        "type": "FINANCIAL_CLAIMS_INFORMATION_DOCUMENT_V3",
        "version": "3",
        "body": {
          "aangeleverd_door": "00000002003214394002",
          "document_datumtijd": "2024-07-08T16:01:14.48799+02:00",
          "bsn": "814859094",
          "financiele_zaken": [
            {
              "zaakkenmerk": "814859094-2022",
              "bsn": "814859094",
              "totaal_opgelegd": 120000,
              "totaal_reeds_betaald": 0,
              "saldo": 120000,
              "saldo_datumtijd": "2023-04-01T00:00:00Z",
              "gebeurtenissen": [
                {
                  "datumtijd_opgelegd": "2023-04-01T00:00:00Z",
                  "zaakkenmerk": "814859094-2022",
                  "beschikkingsnummer": "814859094-2022-123",
                  "bsn": "814859094",
                  "primaire_verplichting": true,
                  "categorie": "Algemeen",
                  "bedrag": 120000,
                  "omschrijving": "Inkomensheffing over het jaar 2022.",
                  "juridische_grondslag_omschrijving":
                      "Wet inkomstenbelasting 2001",
                  "juridische_grondslag_bron":
                      "https://wetten.overheid.nl/jci1.3:c:BWBR0011353\u0026z=2023-01-01\u0026g=2023-01-01",
                  "opgelegd_door": "00000002003214394002",
                  "uitgevoerd_door": "00000002003214394002",
                  "type": "BD_INKOMENSHEFFING",
                  "gebeurtenis_type": "FinancieleVerplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "814859094-2022-1",
                  "datumtijd_gebeurtenis": "2023-03-31T10:25:00Z"
                },
                {
                  "datumtijd_opgelegd": "2023-04-01T00:00:00Z",
                  "zaakkenmerk": "814859094-2022",
                  "bsn": "814859094",
                  "bedrag": 16500,
                  "betaalwijze": "Handmatig",
                  "te_betalen_aan": "00000002003214394002",
                  "rekeningnummer": "GB33BUKB20201555555555",
                  "rekeningnummer_tenaamstelling": "Belastingdienst",
                  "betalingskenmerk": "2617374637287631",
                  "vervaldatum": "2023-07-31T00:00:00Z",
                  "gebeurtenis_type": "BetalingsverplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "814859094-2022-2",
                  "datumtijd_gebeurtenis": "2023-04-01T00:00:00Z"
                }
              ],
              "achterstanden": null
            }
          ]
        }
      };

      final document =
          FinancialClaimsInformationDocument.fromJson(financialClaimsInfoJson);

      expect(document.type, equals('FINANCIAL_CLAIMS_INFORMATION_DOCUMENT_V3'));
      expect(document.version, equals('3'));

      final financieleZaak = document.body.financieleZaken.first;

      expect(financieleZaak.totaalOpgelegd, equals(120000));
      expect(financieleZaak.totaalReedsBetaald, equals(0));
    });
  });
}

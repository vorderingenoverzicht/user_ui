import 'dart:convert';

import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/app_manager/models/registration_expired_exception.dart';
import 'package:user_ui/entities/financial_claims_information_configuration.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/entities/user_settings.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/registration/latest_usable_registration_notifier.dart';
import 'package:user_ui/repositories/registration/mocks/latest_usable_registration_mock_notifier.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/usecases/helpers/fetch_and_store_certificate_helper_delegate.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/fetch_new_certificate.dart';

import '../../../utilities/create_container.dart';
import '../../../utilities/mocks.dart';
import '../../../utilities/test_helpers.dart';

void main() {
  late MockDriftSchemeOrganizationRepository
      mockDriftSchemeOrganizationRepository;
  late MockFinancialClaimsInformationConfigurationRepository
      mockFinancialClaimsInformationConfigurationRepository;
  late MockFinancialClaimsInformationRequestRepository
      mockFinancialClaimsInformationRequestRepository;
  late MockUserSettingsRepository mockUserSettingsRepository;
  late MockLoggingHelper mockLoggingHelper;

  const financialClaimsInformationConfiguration =
      FinancialClaimsInformationConfiguration(
    id: 1,
    oin: 'oin',
    document: 'document',
    documentSignature: 'documentSignature',
    envelope: 'envelope',
    encryptedEnvelope: 'encryptedEnvelope',
    configuration: 'configuration',
    expired: false,
  );

  final financialClaimsInformationRequest = FinancialClaimsInformationRequest(
    id: 1,
    oin: 'oin',
    dateTimeRequested: DateTime.now(),
    status: FinancialClaimsInformationRequestStatus.certificateInvalid,
    lock: true,
  );

  final sut = fetchNewCertificateProvider(financialClaimsInformationRequest);

  const userSettings = UserSettings(
    hasSeenOnboarding: true,
    autoSelectAllOrganizations: true,
    hasCompletedRegistration: true,
    hasCompletedOrganizationSelection: true,
    delay: false,
    showDevelopmentOverlay: false,
    showCriticalErrorAlerts: false,
    pincode: '1234',
    betalingsregelingRijkFeature: false,
  );

  setUp(() {
    mockDriftSchemeOrganizationRepository =
        MockDriftSchemeOrganizationRepository([]);
    mockFinancialClaimsInformationConfigurationRepository =
        MockFinancialClaimsInformationConfigurationRepository();
    mockFinancialClaimsInformationRequestRepository =
        MockFinancialClaimsInformationRequestRepository();
    mockUserSettingsRepository = MockUserSettingsRepository(userSettings);
    mockLoggingHelper = MockLoggingHelper();

    registerFallbackValue(const AsyncData(''));
    registerFallbackValue(financialClaimsInformationRequest);
    registerFallbackValue(financialClaimsInformationConfiguration);
    registerFallbackValue(DeviceEvent.uu_10);

    when(
      () => mockUserSettingsRepository.userSettings,
    ).thenAnswer(
      (_) => Future.value(const UserSettings(
        hasSeenOnboarding: true,
        autoSelectAllOrganizations: true,
        hasCompletedRegistration: true,
        hasCompletedOrganizationSelection: true,
        delay: false,
        showDevelopmentOverlay: false,
        showCriticalErrorAlerts: false,
        pincode: '1234',
        betalingsregelingRijkFeature: false,
      )),
    );

    const document = Document(type: DocumentType(name: 'document'));
    final jsonDocument = json.encode(document.toJson());
    final jsonEnvelope = json.encode(
        Envelope(document: document, documentSignature: 'documentSignature')
            .toJson());

    when(
      () => mockFinancialClaimsInformationConfigurationRepository
          .getUnexpiredConfigurationForOin(any()),
    ).thenAnswer(
      (_) => Future.value(financialClaimsInformationConfiguration.copyWith(
          document: jsonDocument, envelope: jsonEnvelope)),
    );

    when(
      () => mockFinancialClaimsInformationConfigurationRepository.update(any()),
    ).thenAnswer(
      (_) => Future.value(),
    );

    when(
      () => mockFinancialClaimsInformationRequestRepository.update(any()),
    ).thenAnswer(
      (_) => Future.value(),
    );

    const schemeOrganization = SchemeOrganization(
      id: 1,
      oin: 'oin',
      name: 'name',
      publicKey: 'publicKey',
      discoveryUrl: 'discoveryUrl',
      available: true,
    );

    when(
      () => mockDriftSchemeOrganizationRepository.organizations,
    ).thenAnswer(
      (_) => Future.value([schemeOrganization]),
    );
  });

  group('Fetch certificate', () {
    WidgetsFlutterBinding.ensureInitialized();

    test('Should set state to envelopCreated', () async {
      final container = createContainer(
        overrides: [
          userSettingsRepositoryProvider
              .overrideWith((ref) => mockUserSettingsRepository),
          financialClaimsInformationRequestRepositoryProvider.overrideWith(
              (ref) => mockFinancialClaimsInformationRequestRepository),
          financialClaimsInformationConfigurationRepositoryProvider
              .overrideWith((ref) =>
                  mockFinancialClaimsInformationConfigurationRepository),
          userSettingsRepositoryProvider
              .overrideWith((ref) => mockUserSettingsRepository),
          financialClaimsInformationConfigurationRepositoryProvider
              .overrideWith((ref) =>
                  mockFinancialClaimsInformationConfigurationRepository),
          latestUsableRegistrationProvider
              .overrideWith(() => LatestUsableRegistrationMock()),
          loggingProvider.overrideWith((ref) => mockLoggingHelper),
          fetchAndStoreCertificateHelperDelegateProvider.overrideWith(
            (ref) => (
              ref,
              registration,
            ) async {
              return Future.value();
            },
          ),
        ],
      );

      await handleSuccesTests(
        container: container,
        sut: sut,
        callSut: () async {
          await container.read(sut.future);
        },
      );

      final financialClaimsInformationRequestUpdate =
          financialClaimsInformationRequest.copyWith(
        status: FinancialClaimsInformationRequestStatus.envelopeCreated,
      );

      verify(() => mockFinancialClaimsInformationRequestRepository
          .update(financialClaimsInformationRequestUpdate)).called(1);
    });

    test('Should throw when registration is expired', () async {
      final container = createContainer(
        overrides: [
          userSettingsRepositoryProvider
              .overrideWith((ref) => mockUserSettingsRepository),
          financialClaimsInformationRequestRepositoryProvider.overrideWith(
              (ref) => mockFinancialClaimsInformationRequestRepository),
          financialClaimsInformationConfigurationRepositoryProvider
              .overrideWith((ref) =>
                  mockFinancialClaimsInformationConfigurationRepository),
          userSettingsRepositoryProvider
              .overrideWith((ref) => mockUserSettingsRepository),
          financialClaimsInformationConfigurationRepositoryProvider
              .overrideWith((ref) =>
                  mockFinancialClaimsInformationConfigurationRepository),
          latestUsableRegistrationProvider
              .overrideWith(() => LatestUsableRegistrationMock()),
          loggingProvider.overrideWith((ref) => mockLoggingHelper),
          fetchAndStoreCertificateHelperDelegateProvider.overrideWith(
            (ref) => (
              ref,
              registration,
            ) async {
              throw RegistrationExpiredException('expired');
            },
          ),
        ],
      );

      await handleSuccesTests(
        container: container,
        sut: sut,
        callSut: () async {
          await container.read(sut.future);
        },
      );

      verify(() => mockLoggingHelper.addLog(DeviceEvent.uu_80,
          "registration expired when fetching new certificate")).called(1);
    });
  });
}

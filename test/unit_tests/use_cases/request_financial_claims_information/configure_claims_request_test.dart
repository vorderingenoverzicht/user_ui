import 'dart:convert';

import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/citizen_financial_claim/citizen_financial_claim_client_live.dart';
import 'package:user_ui/clients/citizen_financial_claim/mocks/citizen_financial_claim_client_invalid_mock.dart';
import 'package:user_ui/clients/citizen_financial_claim/mocks/citizen_financial_claim_client_mock.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_mock.dart';
import 'package:user_ui/entities/app_session.dart';
import 'package:user_ui/entities/financial_claims_information_configuration.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/entities/user_settings.dart';
import 'package:user_ui/repositories/app_identity/latest_app_session_delegate.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_organization_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/configure_claims_request.dart';

import '../../../utilities/create_container.dart';
import '../../../utilities/mocks.dart';
import '../../../utilities/test_helpers.dart';

void main() {
  late MockLoggingHelper mockLoggingHelper;
  late MockDriftSchemeOrganizationRepository
      mockDriftSchemeOrganizationRepository;
  late MockFinancialClaimsInformationConfigurationRepository
      mockFinancialClaimsInformationConfigurationRepository;
  late MockFinancialClaimsInformationRequestRepository
      mockFinancialClaimsInformationRequestRepository;
  late MockUserSettingsRepository mockUserSettingsRepository;

  const financialClaimsInformationConfiguration =
      FinancialClaimsInformationConfiguration(
    id: 1,
    oin: 'oin',
    document: 'document',
    documentSignature: 'documentSignature',
    envelope: 'envelope',
    encryptedEnvelope: 'encryptedEnvelope',
    configuration: 'configuration',
    expired: false,
  );

  final financialClaimsInformationRequest = FinancialClaimsInformationRequest(
    id: 1,
    oin: 'oin',
    dateTimeRequested: DateTime.now(),
    status: FinancialClaimsInformationRequestStatus.sessionCreated,
    lock: true,
  );

  final sut = configureClaimsRequestProvider(financialClaimsInformationRequest);

  const userSettings = UserSettings(
    hasSeenOnboarding: true,
    autoSelectAllOrganizations: true,
    hasCompletedRegistration: true,
    hasCompletedOrganizationSelection: true,
    delay: false,
    showDevelopmentOverlay: false,
    showCriticalErrorAlerts: false,
    pincode: '1234',
    betalingsregelingRijkFeature: false,
  );

  setUp(() {
    mockLoggingHelper = MockLoggingHelper();
    mockDriftSchemeOrganizationRepository =
        MockDriftSchemeOrganizationRepository([]);
    mockFinancialClaimsInformationConfigurationRepository =
        MockFinancialClaimsInformationConfigurationRepository();
    mockFinancialClaimsInformationRequestRepository =
        MockFinancialClaimsInformationRequestRepository();
    mockUserSettingsRepository = MockUserSettingsRepository(userSettings);

    registerFallbackValue(const AsyncData(''));
    registerFallbackValue(financialClaimsInformationRequest);
    registerFallbackValue(financialClaimsInformationConfiguration);
    registerFallbackValue(DeviceEvent.uu_10);

    when(
      () => mockUserSettingsRepository.userSettings,
    ).thenAnswer(
      (_) => Future.value(const UserSettings(
        hasSeenOnboarding: true,
        autoSelectAllOrganizations: true,
        hasCompletedRegistration: true,
        hasCompletedOrganizationSelection: true,
        delay: false,
        showDevelopmentOverlay: false,
        showCriticalErrorAlerts: false,
        pincode: '1234',
        betalingsregelingRijkFeature: false,
      )),
    );

    when(
      () => mockFinancialClaimsInformationConfigurationRepository.update(any()),
    ).thenAnswer(
      (_) => Future.value(),
    );

    when(
      () => mockFinancialClaimsInformationRequestRepository.update(any()),
    ).thenAnswer(
      (_) => Future.value(),
    );
    const schemeOrganization = SchemeOrganization(
      id: 1,
      oin: 'oin',
      name: 'name',
      publicKey: 'publicKey',
      discoveryUrl: 'discoveryUrl',
      available: true,
    );

    when(
      () => mockDriftSchemeOrganizationRepository.organizations,
    ).thenAnswer(
      (_) => Future.value([schemeOrganization]),
    );
  });

  group('Create configuration request', () {
    WidgetsFlutterBinding.ensureInitialized();
    test('Should throw when decryption fails', () async {
      const document = Document(type: DocumentType(name: 'document'));
      final jsonDocument = json.encode(document.toJson());
      final jsonEnvelope = json.encode(
          Envelope(document: document, documentSignature: 'documentSignature')
              .toJson());

      when(
        () => mockFinancialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(any()),
      ).thenAnswer(
        (_) => Future.value(financialClaimsInformationConfiguration.copyWith(
            document: jsonDocument, envelope: jsonEnvelope)),
      );

      final container = createContainer(
        overrides: [
          loggingProvider.overrideWith((ref) => mockLoggingHelper),
          citizenFinancialClaimClientProvider
              .overrideWith((ref) => CitizenFinancialClaimClientInvalidMock()),
          serviceDiscoveryClientProvider
              .overrideWith((ref) => ServiceDiscoveryClientMock()),
          schemeOrganizationRepositoryProvider
              .overrideWith((ref) => mockDriftSchemeOrganizationRepository),
          financialClaimsInformationConfigurationRepositoryProvider
              .overrideWith((ref) =>
                  mockFinancialClaimsInformationConfigurationRepository),
          financialClaimsInformationRequestRepositoryProvider.overrideWith(
              (ref) => mockFinancialClaimsInformationRequestRepository),
          userSettingsRepositoryProvider
              .overrideWith((ref) => mockUserSettingsRepository),
          latestAppSessionDelegateProvider.overrideWith(
            (ref) => (
              ref,
              oin,
            ) async {
              final keys = jsonDecode(
                  await rootBundle.loadString('assets/mock_app_keys.json'));
              final aesKey = keys["aesKey"];
              return AppSession(id: 1, key: aesKey, token: 'token', oin: oin);
            },
          ),
        ],
      );

      final matcherFailureText = Exception(
        "failed to configure claims request",
      );

      await handleErrorTests(
        container: container,
        sut: sut,
        callSut: () async {
          try {
            await container.read(sut.future);
          } on Exception catch (e) {
            expect(e.toString(), matcherFailureText.toString());
          }
        },
      );
    });

    test('Should throw when envelope not found', () async {
      when(
        () => mockFinancialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(any()),
      ).thenAnswer(
        (_) => Future.value(financialClaimsInformationConfiguration.copyWith(
            envelope: null, encryptedEnvelope: null)),
      );

      final container = createContainer(
        overrides: [
          loggingProvider.overrideWith((ref) => mockLoggingHelper),
          schemeOrganizationRepositoryProvider
              .overrideWith((ref) => mockDriftSchemeOrganizationRepository),
          financialClaimsInformationConfigurationRepositoryProvider
              .overrideWith((ref) =>
                  mockFinancialClaimsInformationConfigurationRepository),
          financialClaimsInformationRequestRepositoryProvider.overrideWith(
              (ref) => mockFinancialClaimsInformationRequestRepository),
          userSettingsRepositoryProvider
              .overrideWith((ref) => mockUserSettingsRepository),
          latestAppSessionDelegateProvider.overrideWith(
            (ref) => (
              ref,
              oin,
            ) async {
              final keys = jsonDecode(
                  await rootBundle.loadString('assets/mock_app_keys.json'));
              final aesKey = keys["aesKey"];
              return AppSession(id: 1, key: aesKey, token: 'token', oin: oin);
            },
          ),
        ],
      );

      final matcherFailureText = Exception(
        "encryptedEnvelope not found",
      );

      await handleErrorTests(
        container: container,
        sut: sut,
        callSut: () async {
          try {
            await container.read(sut.future);
          } on Exception catch (e) {
            expect(e.toString(), matcherFailureText.toString());
          }
        },
      );
    });

    test('Should throw when finanncial claims configuration not found',
        () async {
      when(
        () => mockFinancialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(any()),
      ).thenAnswer(
        (_) => Future.value(null),
      );

      final container = createContainer(
        overrides: [
          loggingProvider.overrideWith((ref) => mockLoggingHelper),
          schemeOrganizationRepositoryProvider
              .overrideWith((ref) => mockDriftSchemeOrganizationRepository),
          financialClaimsInformationConfigurationRepositoryProvider
              .overrideWith((ref) =>
                  mockFinancialClaimsInformationConfigurationRepository),
          financialClaimsInformationRequestRepositoryProvider.overrideWith(
              (ref) => mockFinancialClaimsInformationRequestRepository),
          userSettingsRepositoryProvider
              .overrideWith((ref) => mockUserSettingsRepository),
          latestAppSessionDelegateProvider.overrideWith(
            (ref) => (
              ref,
              oin,
            ) async {
              final keys = jsonDecode(
                  await rootBundle.loadString('assets/mock_app_keys.json'));
              final aesKey = keys["aesKey"];
              return AppSession(id: 1, key: aesKey, token: 'token', oin: oin);
            },
          ),
        ],
      );

      final matcherFailureText = Exception(
        "financial claims configuration not found",
      );

      await handleErrorTests(
        container: container,
        sut: sut,
        callSut: () async {
          try {
            await container.read(sut.future);
          } on Exception catch (e) {
            expect(e.toString(), matcherFailureText.toString());
          }
        },
      );
    });

    test('Should throw when failed to decrypt configuration', () async {
      const document = Document(type: DocumentType(name: 'document'));
      final jsonDocument = json.encode(document.toJson());
      final jsonEnvelope = json.encode(
          Envelope(document: document, documentSignature: 'documentSignature')
              .toJson());

      when(
        () => mockFinancialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(any()),
      ).thenAnswer(
        (_) => Future.value(financialClaimsInformationConfiguration.copyWith(
            document: jsonDocument, envelope: jsonEnvelope)),
      );

      final container = createContainer(
        overrides: [
          loggingProvider.overrideWith((ref) => mockLoggingHelper),
          citizenFinancialClaimClientProvider
              .overrideWith((ref) => CitizenFinancialClaimClientMock()),
          serviceDiscoveryClientProvider
              .overrideWith((ref) => ServiceDiscoveryClientMock()),
          schemeOrganizationRepositoryProvider
              .overrideWith((ref) => mockDriftSchemeOrganizationRepository),
          financialClaimsInformationConfigurationRepositoryProvider
              .overrideWith((ref) =>
                  mockFinancialClaimsInformationConfigurationRepository),
          financialClaimsInformationRequestRepositoryProvider.overrideWith(
              (ref) => mockFinancialClaimsInformationRequestRepository),
          userSettingsRepositoryProvider
              .overrideWith((ref) => mockUserSettingsRepository),
          latestAppSessionDelegateProvider.overrideWith(
            (ref) => (
              ref,
              oin,
            ) async {
              return const AppSession(
                  id: 1, key: '1234', token: 'token', oin: 'oid');
            },
          ),
        ],
      );

      final matcherFailureText = Exception(
        "decrypt encrypted configuration failed",
      );

      await handleErrorTests(
        container: container,
        sut: sut,
        callSut: () async {
          try {
            await container.read(sut.future);
          } on Exception catch (e) {
            expect(e.toString(), matcherFailureText.toString());
          }
        },
      );
    });
  });
}

import 'dart:convert';

import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/app_session.dart';
import 'package:user_ui/entities/financial_claims_information_configuration.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/entities/user_settings.dart';
import 'package:user_ui/repositories/app_identity/latest_app_session_delegate.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_received/financial_claims_information_received_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_received/mocks/v4/financial_claims_information_received_repository_mock.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_organization_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/copy_financial_claims_information_to_inbox.dart';

import '../../../utilities/create_container.dart';
import '../../../utilities/mocks.dart';
import '../../../utilities/test_helpers.dart';

void main() {
  late MockLoggingHelper mockLoggingHelper;
  late MockDriftSchemeOrganizationRepository
      mockDriftSchemeOrganizationRepository;
  late MockFinancialClaimsInformationConfigurationRepository
      mockFinancialClaimsInformationConfigurationRepository;
  late MockFinancialClaimsInformationRequestRepository
      mockFinancialClaimsInformationRequestRepository;
  late MockFinancialClaimsInformationInboxRepository
      mockFinancialClaimsInformationInboxRepository;
  late MockUserSettingsRepository mockUserSettingsRepository;

  const financialClaimsInformationConfiguration =
      FinancialClaimsInformationConfiguration(
    id: 1,
    oin: 'oin',
    document: 'document',
    documentSignature: 'documentSignature',
    envelope: 'envelope',
    encryptedEnvelope: 'encryptedEnvelope',
    configuration: 'configuration',
    expired: false,
  );

  final financialClaimsInformationRequest = FinancialClaimsInformationRequest(
    id: 1,
    oin: 'oin',
    dateTimeRequested: DateTime.now(),
    status: FinancialClaimsInformationRequestStatus
        .financialClaimsInformationChanged,
    lock: true,
  );

  final sut = copyFinancialClaimsInformationToInboxProvider(
      financialClaimsInformationRequest);

  const userSettings = UserSettings(
    hasSeenOnboarding: true,
    autoSelectAllOrganizations: true,
    hasCompletedRegistration: true,
    hasCompletedOrganizationSelection: true,
    delay: false,
    showDevelopmentOverlay: false,
    showCriticalErrorAlerts: false,
    pincode: '1234',
    betalingsregelingRijkFeature: false,
  );

  setUp(() {
    mockLoggingHelper = MockLoggingHelper();
    mockDriftSchemeOrganizationRepository =
        MockDriftSchemeOrganizationRepository([]);
    mockFinancialClaimsInformationConfigurationRepository =
        MockFinancialClaimsInformationConfigurationRepository();
    mockFinancialClaimsInformationRequestRepository =
        MockFinancialClaimsInformationRequestRepository();
    mockFinancialClaimsInformationInboxRepository =
        MockFinancialClaimsInformationInboxRepository();
    mockUserSettingsRepository = MockUserSettingsRepository(userSettings);

    registerFallbackValue(const AsyncData(''));
    registerFallbackValue(financialClaimsInformationRequest);
    registerFallbackValue(financialClaimsInformationConfiguration);
    registerFallbackValue(DeviceEvent.uu_10);

    const document = Document(type: DocumentType(name: 'document'));
    final jsonDocument = json.encode(document.toJson());
    final jsonEnvelope = json.encode(
        Envelope(document: document, documentSignature: 'documentSignature')
            .toJson());
    const encryptedEnvelope = 'OIxi2bQPXRSNBTHWEBn4fQ==';

    when(
      () => mockFinancialClaimsInformationConfigurationRepository
          .getUnexpiredConfigurationForOin(any()),
    ).thenAnswer(
      (_) => Future.value(financialClaimsInformationConfiguration.copyWith(
          document: jsonDocument,
          envelope: jsonEnvelope,
          encryptedEnvelope: encryptedEnvelope)),
    );

    when(
      () => mockFinancialClaimsInformationConfigurationRepository.update(any()),
    ).thenAnswer(
      (_) => Future.value(),
    );

    when(
      () => mockFinancialClaimsInformationRequestRepository.update(any()),
    ).thenAnswer(
      (_) => Future.value(),
    );

    when(
      () => mockFinancialClaimsInformationInboxRepository.add(
        oin: any(named: 'oin'),
        dateTimeReceived: any(named: 'dateTimeReceived'),
        dateTimeRequested: any(named: 'dateTimeRequested'),
        financialClaimsInformationDocument:
            any(named: 'financialClaimsInformationDocument'),
      ),
    ).thenAnswer(
      (_) => Future.value(),
    );

    const schemeOrganization = SchemeOrganization(
      id: 1,
      oin: 'oin',
      name: 'name',
      publicKey: 'publicKey',
      discoveryUrl: 'discoveryUrl',
      available: true,
    );

    when(
      () => mockDriftSchemeOrganizationRepository.organizations,
    ).thenAnswer(
      (_) => Future.value([schemeOrganization]),
    );
  });

  group('Copy financial claims to inbox', () {
    WidgetsFlutterBinding.ensureInitialized();
    test('Should set state to financialClaimsInformationCopiedToInbox',
        () async {
      final container = createContainer(
        overrides: [
          loggingProvider.overrideWith((ref) => mockLoggingHelper),
          userSettingsRepositoryProvider
              .overrideWith((ref) => mockUserSettingsRepository),
          financialClaimsInformationRequestRepositoryProvider.overrideWith(
              (ref) => mockFinancialClaimsInformationRequestRepository),
          financialClaimsInformationConfigurationRepositoryProvider
              .overrideWith((ref) =>
                  mockFinancialClaimsInformationConfigurationRepository),
          latestAppSessionDelegateProvider.overrideWith(
            (ref) => (
              ref,
              oin,
            ) async {
              final keys = jsonDecode(
                  await rootBundle.loadString('assets/mock_app_keys.json'));
              final aesKey = keys["aesKey"];
              return AppSession(id: 1, key: aesKey, token: 'token', oin: oin);
            },
          ),
          financialClaimsInformationReceivedRepositoryProvider.overrideWith(
            () => FinancialClaimsInformationReceivedRepositoryMock(),
          ),
          schemeOrganizationRepositoryProvider
              .overrideWith((ref) => mockDriftSchemeOrganizationRepository),
        ],
      );

      await handleSuccesTests(
        container: container,
        sut: sut,
        callSut: () async {
          await container.read(sut.future);
        },
      );

      final financialClaimsInformationRequestUpdate =
          financialClaimsInformationRequest.copyWith(
        status: FinancialClaimsInformationRequestStatus
            .financialClaimsInformationCopiedToInbox,
      );

      verify(() => mockFinancialClaimsInformationRequestRepository
          .update(financialClaimsInformationRequestUpdate)).called(1);
    });

    //TODO: Add remaining failure tests
  });
}

import 'dart:convert';

import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_mock.dart';
import 'package:user_ui/entities/financial_claims_information_configuration.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_organization_repository.dart';
import 'package:user_ui/usecases/helpers/start_session_delegate.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/create_session_for_source_organization.dart';

import '../../../utilities/create_container.dart';
import '../../../utilities/mocks.dart';
import '../../../utilities/test_helpers.dart';

void main() {
  late MockLoggingHelper mockLoggingHelper;
  late MockDriftSchemeOrganizationRepository
      mockDriftSchemeOrganizationRepository;
  late MockFinancialClaimsInformationConfigurationRepository
      mockFinancialClaimsInformationConfigurationRepository;
  late MockFinancialClaimsInformationRequestRepository
      mockFinancialClaimsInformationRequestRepository;

  const financialClaimsInformationConfiguration =
      FinancialClaimsInformationConfiguration(
    id: 1,
    oin: 'oin',
    document: 'document',
    documentSignature: 'documentSignature',
    envelope: 'envelope',
    encryptedEnvelope: 'encryptedEnvelope',
    configuration: 'configuration',
    expired: false,
  );

  final financialClaimsInformationRequest = FinancialClaimsInformationRequest(
    id: 1,
    oin: 'oin',
    dateTimeRequested: DateTime.now(),
    status: FinancialClaimsInformationRequestStatus.envelopeCreated,
    lock: true,
  );

  final sut = createSessionForSourceOrganizationProvider(
      financialClaimsInformationRequest);

  setUp(() {
    mockLoggingHelper = MockLoggingHelper();
    mockDriftSchemeOrganizationRepository =
        MockDriftSchemeOrganizationRepository([]);
    mockFinancialClaimsInformationConfigurationRepository =
        MockFinancialClaimsInformationConfigurationRepository();
    mockFinancialClaimsInformationRequestRepository =
        MockFinancialClaimsInformationRequestRepository();

    registerFallbackValue(const AsyncData(''));
    registerFallbackValue(financialClaimsInformationRequest);
    registerFallbackValue(financialClaimsInformationConfiguration);
    registerFallbackValue(DeviceEvent.uu_10);

    when(
      () => mockFinancialClaimsInformationConfigurationRepository.update(any()),
    ).thenAnswer(
      (_) => Future.value(),
    );

    when(
      () => mockFinancialClaimsInformationRequestRepository.update(any()),
    ).thenAnswer(
      (_) => Future.value(),
    );

    const schemeOrganization = SchemeOrganization(
      id: 1,
      oin: 'oin',
      name: 'name',
      publicKey: 'publicKey',
      discoveryUrl: 'discoveryUrl',
      available: true,
    );

    when(
      () => mockDriftSchemeOrganizationRepository.organizations,
    ).thenAnswer(
      (_) => Future.value([schemeOrganization]),
    );
  });

  group('Create session for source organizations', () {
    WidgetsFlutterBinding.ensureInitialized();
    test('Should set state to sessionCreated', () async {
      const document = Document(type: DocumentType(name: 'document'));
      final jsonDocument = json.encode(document.toJson());
      final jsonEnvelope = json.encode(
          Envelope(document: document, documentSignature: 'documentSignature')
              .toJson());

      when(
        () => mockFinancialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(any()),
      ).thenAnswer(
        (_) => Future.value(financialClaimsInformationConfiguration.copyWith(
            document: jsonDocument, envelope: jsonEnvelope)),
      );

      final container = createContainer(
        overrides: [
          loggingProvider.overrideWith((ref) => mockLoggingHelper),
          financialClaimsInformationRequestRepositoryProvider.overrideWith(
              (ref) => mockFinancialClaimsInformationRequestRepository),
          financialClaimsInformationConfigurationRepositoryProvider
              .overrideWith((ref) =>
                  mockFinancialClaimsInformationConfigurationRepository),
          schemeOrganizationRepositoryProvider
              .overrideWith((ref) => mockDriftSchemeOrganizationRepository),
          serviceDiscoveryClientProvider
              .overrideWith((ref) => ServiceDiscoveryClientMock()),
          startSessionDelegateProvider.overrideWith(
            (ref) => (
              ref,
              sessionApiUrl,
              rootOrganizationEcPublicKey,
              organizationOin,
              contactType,
            ) async {
              return Future.value('');
            },
          ),
        ],
      );

      await handleSuccesTests(
        container: container,
        sut: sut,
        callSut: () async {
          await container.read(sut.future);
        },
      );

      final financialClaimsInformationRequestUpdate =
          financialClaimsInformationRequest.copyWith(
        status: FinancialClaimsInformationRequestStatus.sessionCreated,
      );

      verify(() => mockFinancialClaimsInformationRequestRepository
          .update(financialClaimsInformationRequestUpdate)).called(1);
    });

    test('Should set state to certificateInvalid', () async {
      const document = Document(type: DocumentType(name: 'document'));
      final jsonDocument = json.encode(document.toJson());
      final jsonEnvelope = json.encode(
          Envelope(document: document, documentSignature: 'documentSignature')
              .toJson());

      when(
        () => mockFinancialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(any()),
      ).thenAnswer(
        (_) => Future.value(financialClaimsInformationConfiguration.copyWith(
            document: jsonDocument, envelope: jsonEnvelope)),
      );

      final container = createContainer(
        overrides: [
          loggingProvider.overrideWith((ref) => mockLoggingHelper),
          financialClaimsInformationRequestRepositoryProvider.overrideWith(
              (ref) => mockFinancialClaimsInformationRequestRepository),
          financialClaimsInformationConfigurationRepositoryProvider
              .overrideWith((ref) =>
                  mockFinancialClaimsInformationConfigurationRepository),
          schemeOrganizationRepositoryProvider
              .overrideWith((ref) => mockDriftSchemeOrganizationRepository),
          serviceDiscoveryClientProvider
              .overrideWith((ref) => ServiceDiscoveryClientMock()),
          startSessionDelegateProvider.overrideWith(
            (ref) => (
              ref,
              sessionApiUrl,
              rootOrganizationEcPublicKey,
              organizationOin,
              contactType,
            ) async {
              throw AppCertificateExpiredException('expired');
            },
          ),
        ],
      );

      await handleSuccesTests(
        container: container,
        sut: sut,
        callSut: () async {
          await container.read(sut.future);
        },
      );

      final financialClaimsInformationRequestUpdate =
          financialClaimsInformationRequest.copyWith(
        status: FinancialClaimsInformationRequestStatus.certificateInvalid,
      );

      verify(() => mockFinancialClaimsInformationRequestRepository
          .update(financialClaimsInformationRequestUpdate)).called(1);
    });

    test('Should throw when financial claims configuration not found',
        () async {
      when(
        () => mockFinancialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(any()),
      ).thenAnswer(
        (_) => Future.value(null),
      );
      final container = createContainer(
        overrides: [
          loggingProvider.overrideWith((ref) => mockLoggingHelper),
          financialClaimsInformationRequestRepositoryProvider.overrideWith(
              (ref) => mockFinancialClaimsInformationRequestRepository),
          financialClaimsInformationConfigurationRepositoryProvider
              .overrideWith((ref) =>
                  mockFinancialClaimsInformationConfigurationRepository),
          schemeOrganizationRepositoryProvider
              .overrideWith((ref) => mockDriftSchemeOrganizationRepository),
          serviceDiscoveryClientProvider
              .overrideWith((ref) => ServiceDiscoveryClientMock()),
          startSessionDelegateProvider.overrideWith(
            (ref) => (
              ref,
              sessionApiUrl,
              rootOrganizationEcPublicKey,
              organizationOin,
              contactType,
            ) async {
              return Future.value('');
            },
          ),
        ],
      );

      final matcherFailureText = Exception(
        "financial claims configuration not found",
      );

      await handleErrorTests(
        container: container,
        sut: sut,
        callSut: () async {
          try {
            await container.read(sut.future);
          } on Exception catch (e) {
            expect(e.toString(), matcherFailureText.toString());
          }
        },
      );
    });
  });
}

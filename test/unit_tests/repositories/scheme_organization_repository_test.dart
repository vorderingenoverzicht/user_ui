import 'package:drift/native.dart';
import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:user_ui/database/encrypted_database.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_organization_repository.dart';

import '../../utilities/create_container.dart';

void main() {
  const stubOrganization = SchemeOrganization(
    id: 1,
    oin: '1',
    name: 'name1',
    discoveryUrl: 'discoveryUrl',
    publicKey: 'publicKey',
    available: true,
  );

  late EncryptedDatabase appDatabase;

  // Opens the database for each test.
  setUp(
    () {
      WidgetsFlutterBinding.ensureInitialized();
      // Creates an in memory database
      appDatabase = EncryptedDatabase.forTesting(NativeDatabase.memory());
    },
  );

  // Closes the database after each test.
  tearDown(
    () async {
      await appDatabase.close();
    },
  );

  test(
    'Should get organization from database table',
    () async {
      appDatabase.into(appDatabase.schemeOrganizationTable).insert(
            SchemeOrganizationTableCompanion.insert(
              oin: stubOrganization.oin,
              name: stubOrganization.name,
              publicKey: stubOrganization.publicKey,
              discoveryUrl: stubOrganization.discoveryUrl,
              available: stubOrganization.available,
            ),
          );

      final container = createContainer(
        overrides: [
          encryptedDatabaseProvider.overrideWithValue(appDatabase),
        ],
      );

      final schemeOrganizationRepository =
          container.read(schemeOrganizationRepositoryProvider.notifier);

      final organizations = await schemeOrganizationRepository.organizations;

      expect(organizations.length, 1);
      expect(
        organizations.any(
          (SchemeOrganization organization) =>
              organization.oin == stubOrganization.oin &&
              organization.available == stubOrganization.available &&
              organization.discoveryUrl == stubOrganization.discoveryUrl &&
              organization.name == stubOrganization.name &&
              organization.publicKey == stubOrganization.publicKey,
        ),
        true,
      );
      expect(organizations, schemeOrganizationRepository.state);
    },
  );

  test('Should add new organization in database table', () async {
    final container = createContainer(
      overrides: [
        encryptedDatabaseProvider.overrideWithValue(appDatabase),
      ],
    );

    final schemeOrganizationRepository =
        container.read(schemeOrganizationRepositoryProvider.notifier);

    await schemeOrganizationRepository.add(
      oin: stubOrganization.oin,
      name: stubOrganization.name,
      publicKey: stubOrganization.publicKey,
      discoveryUrl: stubOrganization.discoveryUrl,
      available: stubOrganization.available,
    );

    final organizations = await schemeOrganizationRepository.organizations;

    expect(organizations.length, 1);
    expect(
      organizations.any(
        (SchemeOrganization organization) =>
            organization.oin == stubOrganization.oin &&
            organization.available == stubOrganization.available &&
            organization.discoveryUrl == stubOrganization.discoveryUrl &&
            organization.name == stubOrganization.name &&
            organization.publicKey == stubOrganization.publicKey,
      ),
      true,
    );
    expect(organizations, schemeOrganizationRepository.state);
  });

  group(
    'Organization update tests',
    () {
      const stubOrganization2 = SchemeOrganization(
        id: 1,
        oin: '2',
        name: 'name2',
        discoveryUrl: 'discoveryUrl2',
        publicKey: 'publicKey2',
        available: false,
      );

      test(
        'Should update organization in database table',
        () async {
          appDatabase.into(appDatabase.schemeOrganizationTable).insert(
                SchemeOrganizationTableCompanion.insert(
                  oin: stubOrganization.oin,
                  name: stubOrganization.name,
                  publicKey: stubOrganization.publicKey,
                  discoveryUrl: stubOrganization.discoveryUrl,
                  available: stubOrganization.available,
                ),
              );

          final container = createContainer(
            overrides: [
              encryptedDatabaseProvider.overrideWithValue(appDatabase),
            ],
          );

          final schemeOrganizationRepository =
              container.read(schemeOrganizationRepositoryProvider.notifier);

          await schemeOrganizationRepository.update(stubOrganization2);

          final organizations =
              await schemeOrganizationRepository.organizations;

          expect(organizations.length, 1);
          expect(
            organizations.any(
              (SchemeOrganization organization) =>
                  organization.oin == stubOrganization2.oin &&
                  organization.available == stubOrganization2.available &&
                  organization.discoveryUrl == stubOrganization2.discoveryUrl &&
                  organization.name == stubOrganization2.name &&
                  organization.publicKey == stubOrganization2.publicKey,
            ),
            true,
          );
          expect(organizations, schemeOrganizationRepository.state);
        },
      );

      test(
        'Should throw Exception for no organization found',
        () async {
          final matchingError = StateError("No element");

          final container = createContainer(
            overrides: [
              encryptedDatabaseProvider.overrideWithValue(appDatabase),
            ],
          );

          final schemeOrganizationRepository =
              container.read(schemeOrganizationRepositoryProvider.notifier);

          try {
            await schemeOrganizationRepository.update(stubOrganization2);
          } catch (error) {
            expect(error.toString(), matchingError.toString());
            expect(error.runtimeType, matchingError.runtimeType);
          }
        },
      );
    },
  );

  test(
    'Should clear database table',
    () async {
      appDatabase.into(appDatabase.schemeOrganizationTable).insert(
            SchemeOrganizationTableCompanion.insert(
              oin: stubOrganization.oin,
              name: stubOrganization.name,
              publicKey: stubOrganization.publicKey,
              discoveryUrl: stubOrganization.discoveryUrl,
              available: stubOrganization.available,
            ),
          );

      final container = createContainer(
        overrides: [
          encryptedDatabaseProvider.overrideWithValue(appDatabase),
        ],
      );

      final schemeOrganizationRepository =
          container.read(schemeOrganizationRepositoryProvider.notifier);

      await schemeOrganizationRepository.clear();

      final organizations = await schemeOrganizationRepository.organizations;

      expect(organizations.isEmpty, true);
      expect(organizations, schemeOrganizationRepository.state);
    },
  );
}

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:user_ui/theme/responsive_grid.dart';
import 'package:user_ui/theme/theme_constants.dart';

class ThemeManager {
  static const String fontMontserrat = 'Montserrat';
  static const String fontSFProTR = 'SF Pro Text';
  static const Color colorBlack = Color(0xff2b2b2b);
  static const Color colorWhite = Color(0xffffffff);
  static const Color colorGreyHighest = Color(0xff757575);
  static const Color colorGreyLowest = Color(0xfff2f2f7);

  static setBreakpoints() =>
      ResponsiveGridBreakpoints.value = ResponsiveGridBreakpoints(
        xs: 330,
        sm: 560,
        md: 768,
        lg: 992,
        xl: 1200,
      );

  static light(bool isSmallScreenSize) => ThemeData(
        useMaterial3: false,
        textTheme: isSmallScreenSize ? textThemeSmall : textThemeDefault,
        cardTheme: CardTheme(
          color: colorWhite,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(RadiusSize.large.value),
          ),
          elevation: 20.0,
        ),
        colorScheme: const ColorScheme(
          brightness: Brightness.light,
          primary: Color(0xff0047ff),
          onPrimary: colorWhite,
          primaryContainer: Color(0xffa8c1ff),
          onPrimaryContainer: colorBlack,
          secondary: Color(0xff76d2b6),
          onSecondary: colorWhite,
          tertiary: Color(0xFF5616FF),
          onTertiary: colorWhite,
          secondaryContainer: Color(0xff6abda3),
          onSecondaryContainer: colorBlack,
          error: Color(0xffFF0000),
          onError: colorWhite,
          surface: colorWhite,
          onSurface: colorBlack,
          surfaceContainerLowest: colorGreyLowest,
          surfaceContainerLow: Color(0xffE3E3ED),
          surfaceContainer: Color(0xffABABB6),
          surfaceContainerHigh: colorGreyHighest,
          surfaceContainerHighest: Color(0xFF323232),
          onSurfaceVariant: colorBlack,
          onInverseSurface: colorWhite,
          outline: colorGreyHighest,
          outlineVariant: colorGreyLowest,
        ),
      );

  //TODO: Add Dark Theme

  static const textThemeDefault = TextTheme(
    displayLarge: TextStyle(
      color: colorBlack,
      fontFamily: fontMontserrat,
      fontWeight: FontWeight.w700,
      fontSize: 26,
      height: 32 / 26,
    ),
    displayMedium: TextStyle(
      color: colorBlack,
      fontFamily: fontMontserrat,
      fontWeight: FontWeight.w700,
      fontSize: 20,
      height: 28 / 20,
    ),
    displaySmall: TextStyle(
      color: colorBlack,
      fontFamily: fontMontserrat,
      fontWeight: FontWeight.w700,
      fontSize: 18,
      height: 21 / 18,
    ),
    headlineMedium: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w600,
      fontSize: 17,
      height: 22 / 17,
    ),
    bodyLarge: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w500,
      fontSize: 17,
      height: 22 / 17,
    ),
    bodyMedium: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w500,
      fontSize: 16,
      height: 20 / 16,
    ),
    bodySmall: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w500,
      fontSize: 14,
      height: 18 / 14,
    ),
    titleLarge: TextStyle(
      color: colorGreyHighest,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w400,
      fontSize: 17,
      height: 22 / 17,
    ),
    titleMedium: TextStyle(
      color: colorGreyHighest,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w400,
      fontSize: 15,
      height: 18 / 15,
    ),
    titleSmall: TextStyle(
      color: colorGreyHighest,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w400,
      fontSize: 13,
      height: 18 / 13,
    ),
    labelLarge: TextStyle(
      fontFamily: fontMontserrat,
    ),
  );

  static const textThemeSmall = TextTheme(
    displayLarge: TextStyle(
      color: colorBlack,
      fontFamily: fontMontserrat,
      fontWeight: FontWeight.w700,
      fontSize: 22,
      height: 24 / 22,
    ),
    displayMedium: TextStyle(
      color: colorBlack,
      fontFamily: fontMontserrat,
      fontWeight: FontWeight.w700,
      fontSize: 16,
      height: 20 / 16,
    ),
    displaySmall: TextStyle(
      color: colorBlack,
      fontFamily: fontMontserrat,
      fontWeight: FontWeight.w700,
      fontSize: 14,
      height: 17 / 14,
    ),
    headlineMedium: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w600,
      fontSize: 17,
      height: 22 / 17,
    ),
    bodyLarge: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w500,
      fontSize: 14,
      height: 16 / 14,
    ),
    bodyMedium: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w500,
      fontSize: 12,
      height: 16 / 12,
    ),
    bodySmall: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w500,
      fontSize: 10,
      height: 14 / 10,
    ),
    titleLarge: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w400,
      fontSize: 14,
      height: 17 / 14,
    ),
    titleMedium: TextStyle(
      color: colorGreyHighest,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w400,
      fontSize: 10,
      height: 14 / 12,
    ),
    titleSmall: TextStyle(
      color: colorGreyHighest,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w400,
      fontSize: 10,
      height: 12 / 10,
    ),
    labelLarge: TextStyle(
      fontFamily: fontMontserrat,
    ),
  );
}

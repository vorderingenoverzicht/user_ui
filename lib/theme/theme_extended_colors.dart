import 'dart:ui';

class ThemeExtendedColors {
  static const Color iconGreen = Color(0xFF23cc52);
  static const Color tipSurfaceContainer = Color(0xFFFFB612);
  static const Color iconOrange = Color(0xffFDEAE3);
  static const Color severityUnknown = Color.fromARGB(255, 163, 163, 163);
  static const Color severityInfo = Color(0xffa8c1ff);
  static const Color severityLow = Color(0xffffff00);
  static const Color severityMedium = Color(0xFFFFB612);
  static const Color severityHigh = Color(0xffFF0000);
}

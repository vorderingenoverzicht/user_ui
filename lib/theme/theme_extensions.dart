import 'package:flutter/material.dart';
import 'package:user_ui/theme/theme_extended_colors.dart';

extension ThemeExtensions<T> on BuildContext {
  ThemeData get theme => Theme.of(this);
  ColorScheme get colorScheme => Theme.of(this).colorScheme;
  ThemeExtendedColors get extendedColors =>
      Theme.of(this).extension<ThemeExtendedColors>()!;
  TextTheme get textTheme => Theme.of(this).textTheme;
}

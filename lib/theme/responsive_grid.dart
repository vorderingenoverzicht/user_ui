import 'package:flutter/material.dart';

//
// responsive grid layout
//

class ResponsiveGridBreakpoints {
  final double xs;
  final double sm;
  final double md;
  final double lg;
  final double xl;

  ResponsiveGridBreakpoints({
    this.xs = 576,
    this.sm = 768,
    this.md = 992,
    this.lg = 1200,
    this.xl = double.infinity,
  });

  static ResponsiveGridBreakpoints value = ResponsiveGridBreakpoints();
}

enum _GridTier { xs, sm, md, lg, xl }

_GridTier _currentSize(BuildContext context) {
  final breakpoints = ResponsiveGridBreakpoints.value;
  final width = MediaQuery.of(context).size.width;

  if (width < breakpoints.xs) {
    return _GridTier.xs;
  } else if (width < breakpoints.sm) {
    return _GridTier.sm;
  } else if (width < breakpoints.md) {
    return _GridTier.md;
  } else if (width < breakpoints.lg) {
    return _GridTier.lg;
  } else {
    // width >= 1200
    return _GridTier.xl;
  }
}

class ResponsiveGridGutters {
  final double xs;
  final double sm;
  final double md;
  final double lg;
  final double xl;

  ResponsiveGridGutters({
    this.xs = 8,
    this.sm = 16,
    this.md = 20,
    this.lg = 24,
    this.xl = 28,
  });

  static ResponsiveGridGutters value = ResponsiveGridGutters();
}

class ResponsiveGridRow extends StatelessWidget {
  final List<ResponsiveGridCol> children;
  final CrossAxisAlignment crossAxisAlignment;
  final int rowSegments;
  final double margin;

  const ResponsiveGridRow({
    required this.children,
    this.crossAxisAlignment = CrossAxisAlignment.start,
    this.rowSegments = 12,
    this.margin = 16.0,
    super.key,
  });

  double get _totalMarginWidth => margin * 2;

  double _getTotalGutterWidth(double gutter) => (rowSegments - 1) * gutter;

  double _totalWidthWithoutGutterAndMargin(double totalWidth, double gutter) {
    return totalWidth - _getTotalGutterWidth(gutter) - _totalMarginWidth;
  }

  double _columnWidth(double totalAvailableWidth, double gutter) {
    return _totalWidthWithoutGutterAndMargin(totalAvailableWidth, gutter) /
        rowSegments;
  }

  double _columnSpanWidth(int columnSpan, double columnWidth, double gutter) {
    if (columnSpan == 0) {
      return 0;
    }
    return (columnWidth * columnSpan) + (gutter * (columnSpan - 1));
  }

  @override
  Widget build(BuildContext context) {
    final double gutter = responsiveValue(
      context,
      xs: ResponsiveGridGutters.value.xs,
      sm: ResponsiveGridGutters.value.sm,
      md: ResponsiveGridGutters.value.md,
      lg: ResponsiveGridGutters.value.lg,
      xl: ResponsiveGridGutters.value.xl,
    );

    return LayoutBuilder(
      builder: (context, constraints) {
        final rows = <Widget>[];
        var cols = <Widget>[];

        double accumulatedWidth = 0;

        final double colWidth = _columnWidth(constraints.maxWidth, gutter);

        final double maxRowWidth = ((rowSegments * colWidth) +
            _getTotalGutterWidth(gutter) +
            _totalMarginWidth);

        // Add first margin
        cols.add(ResponsiveGridMargin(width: margin));
        accumulatedWidth += margin;

        for (int i = 0; i < children.length; i++) {
          final int colSpan = children[i].currentConfig(context);

          final double colSpanWidth =
              _columnSpanWidth(colSpan, colWidth, gutter);

          final double predictedAccumulatedWidth = double.parse(
              (accumulatedWidth + colSpanWidth + margin + gutter)
                  .toStringAsFixed(5));

          /// Only add gutter IF
          /// Previous is a Column AND
          /// Predicted accumulatedWidth will fit the row
          if (cols.last.runtimeType == ConstrainedBox &&
              predictedAccumulatedWidth <= maxRowWidth) {
            cols.add(ResponsiveGridGutter(width: gutter));
            accumulatedWidth += gutter;
          }

          /// If the sum of the new span and the already occupied width is greater than the max row width,
          /// then finish up this row with an end margin and start a new row by adding a margin.
          if (accumulatedWidth + colSpanWidth > maxRowWidth) {
            // Finish up first row
            cols.add(ResponsiveGridMargin(width: margin));
            rows.add(Row(
              crossAxisAlignment: crossAxisAlignment,
              children: cols,
            ));

            // Clear and start new row
            cols = [];
            cols.add(ResponsiveGridMargin(width: margin));
            accumulatedWidth = margin;
          }

          // Add Column (aka content spread over a column span)
          if (colSpanWidth > 0) {
            cols.add(ConstrainedBox(
              constraints: BoxConstraints(minWidth: 0, maxWidth: colSpanWidth),
              child: children[i],
            ));

            accumulatedWidth += colSpanWidth;
          }
        }

        // Add columns to Row
        if (accumulatedWidth >= 0) {
          cols.add(ResponsiveGridMargin(width: margin));
          accumulatedWidth += margin;

          rows.add(Row(
            crossAxisAlignment: crossAxisAlignment,
            children: cols,
          ));
        }

        return Column(
          children: rows,
        );
      },
    );
  }
}

class ResponsiveGridCol extends StatelessWidget {
  final _config = [0, 0, 0, 0, 0];
  final Widget child;

  ResponsiveGridCol({
    int xs = 12,
    int? sm,
    int? md,
    int? lg,
    int? xl,
    required this.child,
    super.key,
  }) {
    _config[_GridTier.xs.index] = xs;
    _config[_GridTier.sm.index] = sm ?? _config[_GridTier.xs.index];
    _config[_GridTier.md.index] = md ?? _config[_GridTier.sm.index];
    _config[_GridTier.lg.index] = lg ?? _config[_GridTier.md.index];
    _config[_GridTier.xl.index] = xl ?? _config[_GridTier.lg.index];
  }

  int currentConfig(BuildContext context) {
    return _config[_currentSize(context).index];
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final configFlex = currentConfig(context);
        return configFlex == 0 ? Container() : child;
      },
    );
  }
}

// Widget representing gutters between columns in a responsive grid.
class ResponsiveGridGutter extends StatelessWidget {
  // Width of the gutter.
  final double width;

  const ResponsiveGridGutter({required this.width, super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(width: width);
  }
}

// Widget representing margins around columns in a responsive grid.
class ResponsiveGridMargin extends StatelessWidget {
  // Width of the margin.
  final double width;

  const ResponsiveGridMargin({required this.width, super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(width: width);
  }
}

//
// Utilities
//

/// a widget for certain tier applies also for larger tiers unless overridden, so you must set xs at least
class ResponsiveWidget extends StatelessWidget {
  final Widget? sm, md, lg, xl;
  final Widget xs;

  const ResponsiveWidget(
      {super.key, this.lg, this.md, this.sm, this.xl, required this.xs});

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    if (w >= ResponsiveGridBreakpoints.value.lg && xl != null) {
      return xl!;
    }
    if (w >= ResponsiveGridBreakpoints.value.md && lg != null) {
      return lg!;
    }
    if (w >= ResponsiveGridBreakpoints.value.sm && md != null) {
      return md!;
    }
    if (w >= ResponsiveGridBreakpoints.value.xs && sm != null) {
      return sm!;
    }
    return xs;
  }
}

/// a builder for certain tier applies also for larger tiers, so you must set xs at least
class ResponsiveBuilder extends StatelessWidget {
  final Widget child;
  final Function(BuildContext context, Widget child)? sm, md, lg, xl;
  final Function(BuildContext context, Widget child) xs;

  const ResponsiveBuilder({
    super.key,
    required this.child,
    required this.xs,
    this.sm,
    this.md,
    this.lg,
    this.xl,
  });

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    if (w >= ResponsiveGridBreakpoints.value.lg && xl != null) {
      return xl!(context, child);
    }
    if (w >= ResponsiveGridBreakpoints.value.md && lg != null) {
      return lg!(context, child);
    }
    if (w >= ResponsiveGridBreakpoints.value.sm && md != null) {
      return md!(context, child);
    }
    if (w >= ResponsiveGridBreakpoints.value.xs && sm != null) {
      return sm!(context, child);
    }
    return xs(context, child);
  }
}

/// a builder for certain tier applies also for larger tiers, so you must set xs at least
class ResponsiveLayoutBuilder extends StatelessWidget {
  final List<Widget> children;
  final Widget Function(BuildContext context, List<Widget> children)? sm,
      md,
      lg,
      xl;
  final Widget Function(BuildContext context, List<Widget> children) xs;

  const ResponsiveLayoutBuilder({
    super.key,
    required this.children,
    required this.xs,
    this.sm,
    this.md,
    this.lg,
    this.xl,
  });

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    if (w >= ResponsiveGridBreakpoints.value.lg && xl != null) {
      return xl!(context, children);
    }
    if (w >= ResponsiveGridBreakpoints.value.md && lg != null) {
      return lg!(context, children);
    }
    if (w >= ResponsiveGridBreakpoints.value.sm && md != null) {
      return md!(context, children);
    }
    if (w >= ResponsiveGridBreakpoints.value.xs && sm != null) {
      return sm!(context, children);
    }
    return xs(context, children);
  }
}

/// a value for certain tier applies also for larger tiers unless overridden, so you must set xs at least
T responsiveValue<T>(BuildContext context,
    {required T xs, T? sm, T? md, T? lg, T? xl}) {
  var w = MediaQuery.of(context).size.width;
  if (w >= ResponsiveGridBreakpoints.value.lg && xl != null) {
    return xl;
  }
  if (w >= ResponsiveGridBreakpoints.value.md && lg != null) {
    return lg;
  }
  if (w >= ResponsiveGridBreakpoints.value.sm && md != null) {
    return md;
  }
  if (w >= ResponsiveGridBreakpoints.value.xs && sm != null) {
    return sm;
  }
  return xs;
}

// local responsive

class ResponsiveWidgetConfig {
  final double upToWidth;
  final Widget child;

  ResponsiveWidgetConfig({required this.upToWidth, required this.child});
}

class ResponsiveBuilderConfig {
  final double upToWidth;
  final Widget Function(BuildContext context, Widget child) builder;

  ResponsiveBuilderConfig({required this.upToWidth, required this.builder});
}

class ResponsiveLocalWidget extends StatelessWidget {
  final List<ResponsiveWidgetConfig> configs;

  /// a widget that changes according to its own width,
  /// the configs are assumed to be provided in ascending order
  ResponsiveLocalWidget({super.key, required this.configs}) {
    assert(configs.isNotEmpty);
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.hasInfiniteWidth) {
        debugPrint(
          "ResponsiveGrid: Warning, You're using the ResponsiveLocalWidget inside an unbounded width parent, "
          "Your configuration is useless",
        );
      }

      for (var config in configs) {
        if (constraints.maxWidth <= config.upToWidth) {
          return config.child;
        }
      }
      //
      return configs.last.child;
    });
  }
}

class ResponsiveLocalBuilder extends StatelessWidget {
  final List<ResponsiveBuilderConfig> configs;
  final Widget child;

  /// a widget that changes according to its own width,
  /// the configs are assumed to be provided in ascending order
  ResponsiveLocalBuilder(
      {super.key, required this.configs, required this.child}) {
    assert(configs.isNotEmpty);
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.hasInfiniteWidth) {
        debugPrint(
          "ResponsiveGrid: Warning, You're using the ResponsiveLocalBuilder inside an unbounded width parent, "
          "Your configuration is useless",
        );
      }

      for (var config in configs) {
        if (constraints.maxWidth <= config.upToWidth) {
          return config.builder(context, child);
        }
      }
      //
      return configs.last.builder(context, child);
    });
  }
}

class ResponsiveLayoutBuilderConfig {
  final double upToWidth;
  final Widget Function(BuildContext context, List<Widget> children) builder;

  ResponsiveLayoutBuilderConfig(
      {required this.upToWidth, required this.builder});
}

class ResponsiveLocalLayoutBuilder extends StatelessWidget {
  final List<ResponsiveLayoutBuilderConfig> configs;
  final List<Widget> children;

  /// a widget that changes according to its own width,
  /// the configs are assumed to be provided in ascending order
  ResponsiveLocalLayoutBuilder(
      {super.key, required this.configs, required this.children}) {
    assert(configs.isNotEmpty);
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.hasInfiniteWidth) {
        debugPrint(
          "ResponsiveGrid: Warning, You're using the ResponsiveLocalLayoutBuilder inside an unbounded width parent, "
          "Your configuration is useless",
        );
      }

      for (var config in configs) {
        if (constraints.maxWidth <= config.upToWidth) {
          return config.builder(context, children);
        }
      }
      //
      return configs.last.builder(context, children);
    });
  }
}

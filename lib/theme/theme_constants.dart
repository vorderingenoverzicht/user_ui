enum Spacing {
  /// 2 dp
  tiny(2),

  /// 4 dp
  small(4),

  /// 8 dp
  medium(8),

  /// 16 dp
  large(16),

  /// 32 dp
  xlarge(32),

  /// 48 dp
  xxlarge(48),

  /// 96 dp
  xxxlarge(96);

  const Spacing(this.value);
  final double value;
}

enum RadiusSize {
  /// 4 dp
  small(4),

  /// 8 dp
  medium(8),

  /// 16 dp
  large(16),

  /// 24 dp
  xlarge(24);

  const RadiusSize(this.value);
  final double value;
}

enum IconSize {
  /// 12 dp
  xsmall(12),

  /// 16 dp
  small(16),

  /// 20 dp
  medium(20),

  /// 24 dp
  large(24),

  /// 32 dp
  xlarge(32),

  /// 64 dp
  xxlarge(64),

  /// 96 dp
  xxxlarge(96);

  const IconSize(this.value);
  final double value;
}

const double maxContentWidth = 1280;
const double minTapSize = 44;

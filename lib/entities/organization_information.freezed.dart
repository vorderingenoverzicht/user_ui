// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'organization_information.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

OrganizationInformation _$OrganizationInformationFromJson(
    Map<String, dynamic> json) {
  return _OrganizationInformation.fromJson(json);
}

/// @nodoc
mixin _$OrganizationInformation {
  String get oin => throw _privateConstructorUsedError;
  String get organization => throw _privateConstructorUsedError;
  String get introText => throw _privateConstructorUsedError;
  List<String> get availableClaims => throw _privateConstructorUsedError;
  List<String> get nonAvailableClaims => throw _privateConstructorUsedError;

  /// Serializes this OrganizationInformation to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of OrganizationInformation
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $OrganizationInformationCopyWith<OrganizationInformation> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OrganizationInformationCopyWith<$Res> {
  factory $OrganizationInformationCopyWith(OrganizationInformation value,
          $Res Function(OrganizationInformation) then) =
      _$OrganizationInformationCopyWithImpl<$Res, OrganizationInformation>;
  @useResult
  $Res call(
      {String oin,
      String organization,
      String introText,
      List<String> availableClaims,
      List<String> nonAvailableClaims});
}

/// @nodoc
class _$OrganizationInformationCopyWithImpl<$Res,
        $Val extends OrganizationInformation>
    implements $OrganizationInformationCopyWith<$Res> {
  _$OrganizationInformationCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of OrganizationInformation
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? oin = null,
    Object? organization = null,
    Object? introText = null,
    Object? availableClaims = null,
    Object? nonAvailableClaims = null,
  }) {
    return _then(_value.copyWith(
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      organization: null == organization
          ? _value.organization
          : organization // ignore: cast_nullable_to_non_nullable
              as String,
      introText: null == introText
          ? _value.introText
          : introText // ignore: cast_nullable_to_non_nullable
              as String,
      availableClaims: null == availableClaims
          ? _value.availableClaims
          : availableClaims // ignore: cast_nullable_to_non_nullable
              as List<String>,
      nonAvailableClaims: null == nonAvailableClaims
          ? _value.nonAvailableClaims
          : nonAvailableClaims // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$OrganizationInformationImplCopyWith<$Res>
    implements $OrganizationInformationCopyWith<$Res> {
  factory _$$OrganizationInformationImplCopyWith(
          _$OrganizationInformationImpl value,
          $Res Function(_$OrganizationInformationImpl) then) =
      __$$OrganizationInformationImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String oin,
      String organization,
      String introText,
      List<String> availableClaims,
      List<String> nonAvailableClaims});
}

/// @nodoc
class __$$OrganizationInformationImplCopyWithImpl<$Res>
    extends _$OrganizationInformationCopyWithImpl<$Res,
        _$OrganizationInformationImpl>
    implements _$$OrganizationInformationImplCopyWith<$Res> {
  __$$OrganizationInformationImplCopyWithImpl(
      _$OrganizationInformationImpl _value,
      $Res Function(_$OrganizationInformationImpl) _then)
      : super(_value, _then);

  /// Create a copy of OrganizationInformation
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? oin = null,
    Object? organization = null,
    Object? introText = null,
    Object? availableClaims = null,
    Object? nonAvailableClaims = null,
  }) {
    return _then(_$OrganizationInformationImpl(
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      organization: null == organization
          ? _value.organization
          : organization // ignore: cast_nullable_to_non_nullable
              as String,
      introText: null == introText
          ? _value.introText
          : introText // ignore: cast_nullable_to_non_nullable
              as String,
      availableClaims: null == availableClaims
          ? _value._availableClaims
          : availableClaims // ignore: cast_nullable_to_non_nullable
              as List<String>,
      nonAvailableClaims: null == nonAvailableClaims
          ? _value._nonAvailableClaims
          : nonAvailableClaims // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$OrganizationInformationImpl implements _OrganizationInformation {
  const _$OrganizationInformationImpl(
      {required this.oin,
      required this.organization,
      required this.introText,
      required final List<String> availableClaims,
      required final List<String> nonAvailableClaims})
      : _availableClaims = availableClaims,
        _nonAvailableClaims = nonAvailableClaims;

  factory _$OrganizationInformationImpl.fromJson(Map<String, dynamic> json) =>
      _$$OrganizationInformationImplFromJson(json);

  @override
  final String oin;
  @override
  final String organization;
  @override
  final String introText;
  final List<String> _availableClaims;
  @override
  List<String> get availableClaims {
    if (_availableClaims is EqualUnmodifiableListView) return _availableClaims;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_availableClaims);
  }

  final List<String> _nonAvailableClaims;
  @override
  List<String> get nonAvailableClaims {
    if (_nonAvailableClaims is EqualUnmodifiableListView)
      return _nonAvailableClaims;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_nonAvailableClaims);
  }

  @override
  String toString() {
    return 'OrganizationInformation(oin: $oin, organization: $organization, introText: $introText, availableClaims: $availableClaims, nonAvailableClaims: $nonAvailableClaims)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$OrganizationInformationImpl &&
            (identical(other.oin, oin) || other.oin == oin) &&
            (identical(other.organization, organization) ||
                other.organization == organization) &&
            (identical(other.introText, introText) ||
                other.introText == introText) &&
            const DeepCollectionEquality()
                .equals(other._availableClaims, _availableClaims) &&
            const DeepCollectionEquality()
                .equals(other._nonAvailableClaims, _nonAvailableClaims));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      oin,
      organization,
      introText,
      const DeepCollectionEquality().hash(_availableClaims),
      const DeepCollectionEquality().hash(_nonAvailableClaims));

  /// Create a copy of OrganizationInformation
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$OrganizationInformationImplCopyWith<_$OrganizationInformationImpl>
      get copyWith => __$$OrganizationInformationImplCopyWithImpl<
          _$OrganizationInformationImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$OrganizationInformationImplToJson(
      this,
    );
  }
}

abstract class _OrganizationInformation implements OrganizationInformation {
  const factory _OrganizationInformation(
          {required final String oin,
          required final String organization,
          required final String introText,
          required final List<String> availableClaims,
          required final List<String> nonAvailableClaims}) =
      _$OrganizationInformationImpl;

  factory _OrganizationInformation.fromJson(Map<String, dynamic> json) =
      _$OrganizationInformationImpl.fromJson;

  @override
  String get oin;
  @override
  String get organization;
  @override
  String get introText;
  @override
  List<String> get availableClaims;
  @override
  List<String> get nonAvailableClaims;

  /// Create a copy of OrganizationInformation
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$OrganizationInformationImplCopyWith<_$OrganizationInformationImpl>
      get copyWith => throw _privateConstructorUsedError;
}

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'pincode_error.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$PincodeError {
  String get title => throw _privateConstructorUsedError;
  String get message => throw _privateConstructorUsedError;

  /// Create a copy of PincodeError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $PincodeErrorCopyWith<PincodeError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PincodeErrorCopyWith<$Res> {
  factory $PincodeErrorCopyWith(
          PincodeError value, $Res Function(PincodeError) then) =
      _$PincodeErrorCopyWithImpl<$Res, PincodeError>;
  @useResult
  $Res call({String title, String message});
}

/// @nodoc
class _$PincodeErrorCopyWithImpl<$Res, $Val extends PincodeError>
    implements $PincodeErrorCopyWith<$Res> {
  _$PincodeErrorCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of PincodeError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? message = null,
  }) {
    return _then(_value.copyWith(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$PincodeErrorImplCopyWith<$Res>
    implements $PincodeErrorCopyWith<$Res> {
  factory _$$PincodeErrorImplCopyWith(
          _$PincodeErrorImpl value, $Res Function(_$PincodeErrorImpl) then) =
      __$$PincodeErrorImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String title, String message});
}

/// @nodoc
class __$$PincodeErrorImplCopyWithImpl<$Res>
    extends _$PincodeErrorCopyWithImpl<$Res, _$PincodeErrorImpl>
    implements _$$PincodeErrorImplCopyWith<$Res> {
  __$$PincodeErrorImplCopyWithImpl(
      _$PincodeErrorImpl _value, $Res Function(_$PincodeErrorImpl) _then)
      : super(_value, _then);

  /// Create a copy of PincodeError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? message = null,
  }) {
    return _then(_$PincodeErrorImpl(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$PincodeErrorImpl implements _PincodeError {
  const _$PincodeErrorImpl({required this.title, required this.message});

  @override
  final String title;
  @override
  final String message;

  @override
  String toString() {
    return 'PincodeError(title: $title, message: $message)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PincodeErrorImpl &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, title, message);

  /// Create a copy of PincodeError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$PincodeErrorImplCopyWith<_$PincodeErrorImpl> get copyWith =>
      __$$PincodeErrorImplCopyWithImpl<_$PincodeErrorImpl>(this, _$identity);
}

abstract class _PincodeError implements PincodeError {
  const factory _PincodeError(
      {required final String title,
      required final String message}) = _$PincodeErrorImpl;

  @override
  String get title;
  @override
  String get message;

  /// Create a copy of PincodeError
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$PincodeErrorImplCopyWith<_$PincodeErrorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

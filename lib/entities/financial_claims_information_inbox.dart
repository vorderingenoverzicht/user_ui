import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'financial_claims_information_inbox.freezed.dart';

@freezed
class FinancialClaimsInformationInbox with _$FinancialClaimsInformationInbox {
  const factory FinancialClaimsInformationInbox({
    required int id,
    required String oin,
    required DateTime dateTimeRequested,
    required DateTime dateTimeReceived,
    required String financialClaimsInformationDocument,
    required bool copiedToStorage,
  }) = _FinancialClaimsInformationInbox;
}

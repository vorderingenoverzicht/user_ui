import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'log_record.freezed.dart';

@freezed
class LogRecord with _$LogRecord {
  const factory LogRecord({
    int? id,
    required DateTime timestamp,
    required String host,
    required int cef,
    required String deviceVendor,
    required String deviceProduct,
    required String deviceVersion,
    required String deviceEventClassId,
    required String name,
    required int severity,
    required String flexString1Label,
    required String flexString1,
    required String flexString2Label,
    required String flexString2,
    required String act,
    required String app,
    required String request,
    required String requestMethod,
  }) = _LogRecord;
}

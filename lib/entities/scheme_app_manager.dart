import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'scheme_app_manager.freezed.dart';

@freezed
class SchemeAppManager with _$SchemeAppManager {
  const factory SchemeAppManager({
    required int id,
    required String oin,
    required String name,
    required String publicKey,
    required String discoveryUrl,
    required bool available,
  }) = _SchemeAppManager;
}

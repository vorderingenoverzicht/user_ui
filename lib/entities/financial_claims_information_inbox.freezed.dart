// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'financial_claims_information_inbox.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$FinancialClaimsInformationInbox {
  int get id => throw _privateConstructorUsedError;
  String get oin => throw _privateConstructorUsedError;
  DateTime get dateTimeRequested => throw _privateConstructorUsedError;
  DateTime get dateTimeReceived => throw _privateConstructorUsedError;
  String get financialClaimsInformationDocument =>
      throw _privateConstructorUsedError;
  bool get copiedToStorage => throw _privateConstructorUsedError;

  /// Create a copy of FinancialClaimsInformationInbox
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $FinancialClaimsInformationInboxCopyWith<FinancialClaimsInformationInbox>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FinancialClaimsInformationInboxCopyWith<$Res> {
  factory $FinancialClaimsInformationInboxCopyWith(
          FinancialClaimsInformationInbox value,
          $Res Function(FinancialClaimsInformationInbox) then) =
      _$FinancialClaimsInformationInboxCopyWithImpl<$Res,
          FinancialClaimsInformationInbox>;
  @useResult
  $Res call(
      {int id,
      String oin,
      DateTime dateTimeRequested,
      DateTime dateTimeReceived,
      String financialClaimsInformationDocument,
      bool copiedToStorage});
}

/// @nodoc
class _$FinancialClaimsInformationInboxCopyWithImpl<$Res,
        $Val extends FinancialClaimsInformationInbox>
    implements $FinancialClaimsInformationInboxCopyWith<$Res> {
  _$FinancialClaimsInformationInboxCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of FinancialClaimsInformationInbox
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? oin = null,
    Object? dateTimeRequested = null,
    Object? dateTimeReceived = null,
    Object? financialClaimsInformationDocument = null,
    Object? copiedToStorage = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      dateTimeRequested: null == dateTimeRequested
          ? _value.dateTimeRequested
          : dateTimeRequested // ignore: cast_nullable_to_non_nullable
              as DateTime,
      dateTimeReceived: null == dateTimeReceived
          ? _value.dateTimeReceived
          : dateTimeReceived // ignore: cast_nullable_to_non_nullable
              as DateTime,
      financialClaimsInformationDocument: null ==
              financialClaimsInformationDocument
          ? _value.financialClaimsInformationDocument
          : financialClaimsInformationDocument // ignore: cast_nullable_to_non_nullable
              as String,
      copiedToStorage: null == copiedToStorage
          ? _value.copiedToStorage
          : copiedToStorage // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FinancialClaimsInformationInboxImplCopyWith<$Res>
    implements $FinancialClaimsInformationInboxCopyWith<$Res> {
  factory _$$FinancialClaimsInformationInboxImplCopyWith(
          _$FinancialClaimsInformationInboxImpl value,
          $Res Function(_$FinancialClaimsInformationInboxImpl) then) =
      __$$FinancialClaimsInformationInboxImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String oin,
      DateTime dateTimeRequested,
      DateTime dateTimeReceived,
      String financialClaimsInformationDocument,
      bool copiedToStorage});
}

/// @nodoc
class __$$FinancialClaimsInformationInboxImplCopyWithImpl<$Res>
    extends _$FinancialClaimsInformationInboxCopyWithImpl<$Res,
        _$FinancialClaimsInformationInboxImpl>
    implements _$$FinancialClaimsInformationInboxImplCopyWith<$Res> {
  __$$FinancialClaimsInformationInboxImplCopyWithImpl(
      _$FinancialClaimsInformationInboxImpl _value,
      $Res Function(_$FinancialClaimsInformationInboxImpl) _then)
      : super(_value, _then);

  /// Create a copy of FinancialClaimsInformationInbox
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? oin = null,
    Object? dateTimeRequested = null,
    Object? dateTimeReceived = null,
    Object? financialClaimsInformationDocument = null,
    Object? copiedToStorage = null,
  }) {
    return _then(_$FinancialClaimsInformationInboxImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      dateTimeRequested: null == dateTimeRequested
          ? _value.dateTimeRequested
          : dateTimeRequested // ignore: cast_nullable_to_non_nullable
              as DateTime,
      dateTimeReceived: null == dateTimeReceived
          ? _value.dateTimeReceived
          : dateTimeReceived // ignore: cast_nullable_to_non_nullable
              as DateTime,
      financialClaimsInformationDocument: null ==
              financialClaimsInformationDocument
          ? _value.financialClaimsInformationDocument
          : financialClaimsInformationDocument // ignore: cast_nullable_to_non_nullable
              as String,
      copiedToStorage: null == copiedToStorage
          ? _value.copiedToStorage
          : copiedToStorage // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$FinancialClaimsInformationInboxImpl
    with DiagnosticableTreeMixin
    implements _FinancialClaimsInformationInbox {
  const _$FinancialClaimsInformationInboxImpl(
      {required this.id,
      required this.oin,
      required this.dateTimeRequested,
      required this.dateTimeReceived,
      required this.financialClaimsInformationDocument,
      required this.copiedToStorage});

  @override
  final int id;
  @override
  final String oin;
  @override
  final DateTime dateTimeRequested;
  @override
  final DateTime dateTimeReceived;
  @override
  final String financialClaimsInformationDocument;
  @override
  final bool copiedToStorage;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FinancialClaimsInformationInbox(id: $id, oin: $oin, dateTimeRequested: $dateTimeRequested, dateTimeReceived: $dateTimeReceived, financialClaimsInformationDocument: $financialClaimsInformationDocument, copiedToStorage: $copiedToStorage)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'FinancialClaimsInformationInbox'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('oin', oin))
      ..add(DiagnosticsProperty('dateTimeRequested', dateTimeRequested))
      ..add(DiagnosticsProperty('dateTimeReceived', dateTimeReceived))
      ..add(DiagnosticsProperty('financialClaimsInformationDocument',
          financialClaimsInformationDocument))
      ..add(DiagnosticsProperty('copiedToStorage', copiedToStorage));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FinancialClaimsInformationInboxImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.oin, oin) || other.oin == oin) &&
            (identical(other.dateTimeRequested, dateTimeRequested) ||
                other.dateTimeRequested == dateTimeRequested) &&
            (identical(other.dateTimeReceived, dateTimeReceived) ||
                other.dateTimeReceived == dateTimeReceived) &&
            (identical(other.financialClaimsInformationDocument,
                    financialClaimsInformationDocument) ||
                other.financialClaimsInformationDocument ==
                    financialClaimsInformationDocument) &&
            (identical(other.copiedToStorage, copiedToStorage) ||
                other.copiedToStorage == copiedToStorage));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, oin, dateTimeRequested,
      dateTimeReceived, financialClaimsInformationDocument, copiedToStorage);

  /// Create a copy of FinancialClaimsInformationInbox
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$FinancialClaimsInformationInboxImplCopyWith<
          _$FinancialClaimsInformationInboxImpl>
      get copyWith => __$$FinancialClaimsInformationInboxImplCopyWithImpl<
          _$FinancialClaimsInformationInboxImpl>(this, _$identity);
}

abstract class _FinancialClaimsInformationInbox
    implements FinancialClaimsInformationInbox {
  const factory _FinancialClaimsInformationInbox(
          {required final int id,
          required final String oin,
          required final DateTime dateTimeRequested,
          required final DateTime dateTimeReceived,
          required final String financialClaimsInformationDocument,
          required final bool copiedToStorage}) =
      _$FinancialClaimsInformationInboxImpl;

  @override
  int get id;
  @override
  String get oin;
  @override
  DateTime get dateTimeRequested;
  @override
  DateTime get dateTimeReceived;
  @override
  String get financialClaimsInformationDocument;
  @override
  bool get copiedToStorage;

  /// Create a copy of FinancialClaimsInformationInbox
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$FinancialClaimsInformationInboxImplCopyWith<
          _$FinancialClaimsInformationInboxImpl>
      get copyWith => throw _privateConstructorUsedError;
}

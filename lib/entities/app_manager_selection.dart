import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'app_manager_selection.freezed.dart';

@freezed
class AppManagerSelection with _$AppManagerSelection {
  const factory AppManagerSelection({
    required int id,
    required String oin,
  }) = _AppManagerSelection;
}

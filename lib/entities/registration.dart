// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'registration.freezed.dart';

@freezed
class Registration with _$Registration {
  const factory Registration({
    required int id,
    required DateTime dateTimeStarted,
    required String appPublicKey,
    required String appManagerOin,
    required String appManagerPublicKey,
    required String registrationToken,
    required DateTime? dateTimeCompleted,
    required String? givenName,
    required bool expired,
    required bool revoked,
  }) = _Registration;
}

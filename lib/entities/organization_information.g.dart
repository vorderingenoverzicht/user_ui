// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'organization_information.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$OrganizationInformationImpl _$$OrganizationInformationImplFromJson(
        Map<String, dynamic> json) =>
    _$OrganizationInformationImpl(
      oin: json['oin'] as String,
      organization: json['organization'] as String,
      introText: json['introText'] as String,
      availableClaims: (json['availableClaims'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      nonAvailableClaims: (json['nonAvailableClaims'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$$OrganizationInformationImplToJson(
        _$OrganizationInformationImpl instance) =>
    <String, dynamic>{
      'oin': instance.oin,
      'organization': instance.organization,
      'introText': instance.introText,
      'availableClaims': instance.availableClaims,
      'nonAvailableClaims': instance.nonAvailableClaims,
    };

import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_settings.freezed.dart';

@freezed
class UserSettings with _$UserSettings {
  const factory UserSettings({
    required bool hasSeenOnboarding,
    required bool autoSelectAllOrganizations,
    required bool hasCompletedRegistration,
    required bool hasCompletedOrganizationSelection,
    required bool delay,
    required bool showDevelopmentOverlay,
    required bool showCriticalErrorAlerts,
    required String pincode,
    required bool betalingsregelingRijkFeature,
  }) = _UserSettings;
}

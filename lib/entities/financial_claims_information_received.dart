import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'financial_claims_information_received.freezed.dart';

@freezed
class FinancialClaimsInformationReceived
    with _$FinancialClaimsInformationReceived {
  const factory FinancialClaimsInformationReceived({
    required int id,
    required String oin,
    required DateTime dateTimeRequested,
    required DateTime dateTimeReceived,
    required String encryptedFinancialClaimsInformationDocument,
    required String? financialClaimsInformationDocument,
    required bool copiedToInbox,
    required bool? changed,
  }) = _FinancialClaimsInformationReceived;
}

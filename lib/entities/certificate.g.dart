// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'certificate.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$CertificateImpl _$$CertificateImplFromJson(Map<String, dynamic> json) =>
    _$CertificateImpl(
      id: (json['id'] as num).toInt(),
      type: $enumDecode(_$CertificateTypeEnumMap, json['type']),
      value: const Uint8ListJsonConverter().fromJson(json['value'] as String),
      bsn: json['bsn'] as String,
      givenName: json['givenName'] as String,
      expiresAt: DateTime.parse(json['expiresAt'] as String),
      deemedExpiredBySourceOrganization:
          json['deemedExpiredBySourceOrganization'] as bool,
      scope: json['scope'] as String,
    );

Map<String, dynamic> _$$CertificateImplToJson(_$CertificateImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': _$CertificateTypeEnumMap[instance.type]!,
      'value': const Uint8ListJsonConverter().toJson(instance.value),
      'bsn': instance.bsn,
      'givenName': instance.givenName,
      'expiresAt': instance.expiresAt.toIso8601String(),
      'deemedExpiredBySourceOrganization':
          instance.deemedExpiredBySourceOrganization,
      'scope': instance.scope,
    };

const _$CertificateTypeEnumMap = {
  CertificateType.appManagerJWTCertificate: 'appManagerJWTCertificate',
  CertificateType.certificateTypeNone: 'certificateTypeNone',
};

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

enum FinancialClaimsInformationRequestStatus {
  queue("queue"),
  configurationCreated("configuration_created"),
  documentCreated("document_created"),
  documentSigned("document_signed"),
  envelopeCreated("envelope_created"),
  sessionCreated("create_session"),
  certificateInvalid("certificate_invalid"),
  appManagerSessionInvalid("app_manager_session_invalid"),
  envelopeEncrypted("envelope_encrypted"),
  encryptedEnvelopeMailed("encrypted_envelope_mailed"),
  financialClaimsInformationReceived("financial_claims_information_received"),
  financialClaimsInformationDecrypted("financial_claims_information_decrypted"),
  financialClaimsInformationChanged("financial_claims_information_changed"),
  financialClaimsInformationUnchanged("financial_claims_information_unchanged"),
  financialClaimsInformationCopiedToInbox(
      "financial_claims_information_copied_to_inbox"),
  paymentPlanRulesFetchedAndStored("payment_plan_rules_fetched_and_stored"),
  paymentPlanRulesProcessed("payment_plan_rules_processed"),
  cancelled("cancelled");

  const FinancialClaimsInformationRequestStatus(this.value);

  final String value;
}

final financialClaimsInformationRequestEndStatusList = [
  FinancialClaimsInformationRequestStatus.paymentPlanRulesProcessed,
  FinancialClaimsInformationRequestStatus.cancelled,
];

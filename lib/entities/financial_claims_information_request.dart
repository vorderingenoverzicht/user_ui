import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';

part 'financial_claims_information_request.freezed.dart';

@freezed
class FinancialClaimsInformationRequest
    with _$FinancialClaimsInformationRequest {
  const factory FinancialClaimsInformationRequest({
    required int id,
    required String oin,
    required DateTime dateTimeRequested,
    required FinancialClaimsInformationRequestStatus status,
    required bool lock,
  }) = _FinancialClaimsInformationRequest;
}

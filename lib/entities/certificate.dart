// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:typed_data';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:user_ui/entities/certificate_type.dart';
import 'package:user_ui/utils/uint8_list_json_converter.dart';

part 'certificate.freezed.dart';
part 'certificate.g.dart';

@freezed
class Certificate with _$Certificate {
  const factory Certificate({
    required int id,
    required CertificateType type,
    @Uint8ListJsonConverter() required Uint8List value,
    required String bsn,
    required String givenName,
    required DateTime expiresAt,
    required bool deemedExpiredBySourceOrganization,
    required String scope,
  }) = _Certificate;

  factory Certificate.fromJson(Map<String, dynamic> json) =>
      _$CertificateFromJson(json);
}

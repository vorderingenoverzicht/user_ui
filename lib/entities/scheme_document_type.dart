import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'scheme_document_type.freezed.dart';

@freezed
class SchemeDocumentType with _$SchemeDocumentType {
  const factory SchemeDocumentType({
    required int id,
    required String name,
    required bool available,
  }) = _SchemeDocumentType;
}

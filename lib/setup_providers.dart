// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL
import 'package:shared_preferences/shared_preferences.dart';
import 'package:user_ui/env_provider.dart';
import 'package:fcid_library/fcid_library.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/payment_plan/payment_plan_group_repository.dart';
import 'package:user_ui/repositories/payment_plan/payment_plan_rule_repository.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_organization_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_storage/financial_claims_information_storage_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/utils/shared_preferences_provider.dart';

Future<ProviderContainer> setupProviders(Environment environment) async {
  final sharedPreferences = await SharedPreferences.getInstance();

  return ProviderContainer(
    overrides: [
      sharedPreferencesProvider.overrideWithValue(sharedPreferences),
      loggingDelegateProvider.overrideWith(
        (ref) => (ref, eventClassId, eventName, severityLevel, message) async {
          ref.read(loggingProvider).addFcidWidgetsLog(
              eventClassId, eventName, severityLevel, message);
        },
      ),
      isPaymentPlanEnabledProvider.overrideWith(
        (ref) => ref
            .watch(userSettingsRepositoryProvider)
            .betalingsregelingRijkFeature,
      ),
      getPaymentPlanGroupsProvider.overrideWith(
        (ref) => ref.watch(paymentPlanGroupRepositoryProvider),
      ),
      getPaymentPlanRulesProvider.overrideWith(
        (ref) => ref.watch(paymentPlanRuleRepositoryProvider),
      ),
      getFinancialClaimInformationDocumentListProvider.overrideWith(
        (ref) => ref.watch(financialClaimsInformationStorageRepositoryProvider),
      ),
      getSchemeOrganizationsProvider.overrideWith(
        (ref) => ref.watch(schemeOrganizationRepositoryProvider),
      ),
      getFinancialClaimInformationDocumentDelegateProvider.overrideWith(
        (ref) => (ref, id) async {
          final financialClaimsInformationStorageRepository = ref.watch(
              financialClaimsInformationStorageRepositoryProvider.notifier);

          var financialClaimsInformationStorage =
              await financialClaimsInformationStorageRepository.getById(id);

          return financialClaimsInformationStorage;
        },
      ),
      environmentProvider.overrideWithValue(environment),
    ],
  );
}

import 'package:drift/drift.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/app_identity/models/app_key_pair.dart';
import 'package:user_ui/repositories/app_identity/models/app_session_key.dart';
import 'package:user_ui/repositories/app_manager_selection/models/app_manager_selection.dart';
import 'package:user_ui/repositories/certificate/models/certificate.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/models/financial_claims_information_configuration.dart';
import 'package:user_ui/repositories/financial_claims_information_inbox/models/financial_claims_information_inbox.dart';
import 'package:user_ui/repositories/financial_claims_information_received/models/financial_claims_information_received.dart';
import 'package:user_ui/repositories/financial_claims_information_request/models/financial_claims_information_request.dart';
import 'package:user_ui/repositories/financial_claims_information_storage/models/financial_claims_information_storage.dart';
import 'package:user_ui/repositories/logging/models/log_record.dart';
import 'package:user_ui/repositories/organization_selection/models/organization_selection.dart';
import 'package:user_ui/repositories/payment_plan/models/payment_plan_groups.dart';
import 'package:user_ui/repositories/payment_plan/models/payment_plan_rules.dart';
import 'package:user_ui/repositories/registration/models/registration.dart';
import 'package:user_ui/repositories/scheme_app_manager/models/scheme_app_manager.dart';
import 'package:user_ui/repositories/scheme_document_type/models/scheme_document_type.dart';
import 'package:user_ui/repositories/scheme_organization/models/scheme_organization.dart';

import 'connection/connection.dart' as impl;

part 'encrypted_database.g.dart';

@DriftDatabase(tables: [
  FinancialClaimsInformationRequestTable,
  FinancialClaimsInformationConfigurationTable,
  FinancialClaimsInformationInboxTable,
  FinancialClaimsInformationReceivedTable,
  RegistrationTable,
  CertificateTable,
  OrganizationSelectionTable,
  AppManagerSelectionTable,
  SchemeAppManagerTable,
  SchemeDocumentTypeTable,
  SchemeOrganizationTable,
  PaymentPlanGroupsTable,
  PaymentPlanRulesStorageTable,
  AppKeyPairTable,
  AppSessionTable,
  FinancialClaimsInformationStorageTable,
  LogRecordTable
])
class EncryptedDatabase extends _$EncryptedDatabase {
  EncryptedDatabase() : super(impl.connect("app.db.enc", "passphrase"));

  // Named constructor for creating in-memory database
  EncryptedDatabase.forTesting(super.e);

  @override
  int get schemaVersion => 10;

  @override
  MigrationStrategy get migration {
    return MigrationStrategy(
      onCreate: (Migrator m) async {
        await m.createAll();
      },
      onUpgrade: (Migrator m, int from, int to) async {
        // NOTE: removed migration to version 4, as the renamed table has been dropped.

        if (from < schemaVersion) {
          // TODO: for now drop and create all tables after new schema version. In the future we need to be backwards compatible.
          for (final table in allTables) {
            await m.drop(table);
          }

          await m.createAll();
        }
      },
    );
  }
}

final encryptedDatabaseProvider = Provider<EncryptedDatabase>(
  (ref) => EncryptedDatabase(),
);

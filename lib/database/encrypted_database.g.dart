// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'encrypted_database.dart';

// ignore_for_file: type=lint
class $FinancialClaimsInformationRequestTableTable
    extends FinancialClaimsInformationRequestTable
    with
        TableInfo<$FinancialClaimsInformationRequestTableTable,
            FinancialClaimsInformationRequestDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $FinancialClaimsInformationRequestTableTable(this.attachedDatabase,
      [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _dateTimeRequestedMeta =
      const VerificationMeta('dateTimeRequested');
  @override
  late final GeneratedColumn<DateTime> dateTimeRequested =
      GeneratedColumn<DateTime>('date_time_requested', aliasedName, false,
          type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _statusMeta = const VerificationMeta('status');
  @override
  late final GeneratedColumn<String> status = GeneratedColumn<String>(
      'status', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _lockMeta = const VerificationMeta('lock');
  @override
  late final GeneratedColumn<bool> lock = GeneratedColumn<bool>(
      'lock', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("lock" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns =>
      [id, oin, dateTimeRequested, status, lock];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'financial_claims_information_request_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<FinancialClaimsInformationRequestDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    if (data.containsKey('date_time_requested')) {
      context.handle(
          _dateTimeRequestedMeta,
          dateTimeRequested.isAcceptableOrUnknown(
              data['date_time_requested']!, _dateTimeRequestedMeta));
    } else if (isInserting) {
      context.missing(_dateTimeRequestedMeta);
    }
    if (data.containsKey('status')) {
      context.handle(_statusMeta,
          status.isAcceptableOrUnknown(data['status']!, _statusMeta));
    } else if (isInserting) {
      context.missing(_statusMeta);
    }
    if (data.containsKey('lock')) {
      context.handle(
          _lockMeta, lock.isAcceptableOrUnknown(data['lock']!, _lockMeta));
    } else if (isInserting) {
      context.missing(_lockMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  FinancialClaimsInformationRequestDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return FinancialClaimsInformationRequestDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
      dateTimeRequested: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime,
          data['${effectivePrefix}date_time_requested'])!,
      status: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}status'])!,
      lock: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}lock'])!,
    );
  }

  @override
  $FinancialClaimsInformationRequestTableTable createAlias(String alias) {
    return $FinancialClaimsInformationRequestTableTable(
        attachedDatabase, alias);
  }
}

class FinancialClaimsInformationRequestDriftModel extends DataClass
    implements Insertable<FinancialClaimsInformationRequestDriftModel> {
  final int id;
  final String oin;
  final DateTime dateTimeRequested;
  final String status;
  final bool lock;
  const FinancialClaimsInformationRequestDriftModel(
      {required this.id,
      required this.oin,
      required this.dateTimeRequested,
      required this.status,
      required this.lock});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    map['date_time_requested'] = Variable<DateTime>(dateTimeRequested);
    map['status'] = Variable<String>(status);
    map['lock'] = Variable<bool>(lock);
    return map;
  }

  FinancialClaimsInformationRequestTableCompanion toCompanion(
      bool nullToAbsent) {
    return FinancialClaimsInformationRequestTableCompanion(
      id: Value(id),
      oin: Value(oin),
      dateTimeRequested: Value(dateTimeRequested),
      status: Value(status),
      lock: Value(lock),
    );
  }

  factory FinancialClaimsInformationRequestDriftModel.fromJson(
      Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return FinancialClaimsInformationRequestDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
      dateTimeRequested:
          serializer.fromJson<DateTime>(json['dateTimeRequested']),
      status: serializer.fromJson<String>(json['status']),
      lock: serializer.fromJson<bool>(json['lock']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
      'dateTimeRequested': serializer.toJson<DateTime>(dateTimeRequested),
      'status': serializer.toJson<String>(status),
      'lock': serializer.toJson<bool>(lock),
    };
  }

  FinancialClaimsInformationRequestDriftModel copyWith(
          {int? id,
          String? oin,
          DateTime? dateTimeRequested,
          String? status,
          bool? lock}) =>
      FinancialClaimsInformationRequestDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
        dateTimeRequested: dateTimeRequested ?? this.dateTimeRequested,
        status: status ?? this.status,
        lock: lock ?? this.lock,
      );
  FinancialClaimsInformationRequestDriftModel copyWithCompanion(
      FinancialClaimsInformationRequestTableCompanion data) {
    return FinancialClaimsInformationRequestDriftModel(
      id: data.id.present ? data.id.value : this.id,
      oin: data.oin.present ? data.oin.value : this.oin,
      dateTimeRequested: data.dateTimeRequested.present
          ? data.dateTimeRequested.value
          : this.dateTimeRequested,
      status: data.status.present ? data.status.value : this.status,
      lock: data.lock.present ? data.lock.value : this.lock,
    );
  }

  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationRequestDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('dateTimeRequested: $dateTimeRequested, ')
          ..write('status: $status, ')
          ..write('lock: $lock')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, oin, dateTimeRequested, status, lock);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FinancialClaimsInformationRequestDriftModel &&
          other.id == this.id &&
          other.oin == this.oin &&
          other.dateTimeRequested == this.dateTimeRequested &&
          other.status == this.status &&
          other.lock == this.lock);
}

class FinancialClaimsInformationRequestTableCompanion
    extends UpdateCompanion<FinancialClaimsInformationRequestDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  final Value<DateTime> dateTimeRequested;
  final Value<String> status;
  final Value<bool> lock;
  const FinancialClaimsInformationRequestTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
    this.dateTimeRequested = const Value.absent(),
    this.status = const Value.absent(),
    this.lock = const Value.absent(),
  });
  FinancialClaimsInformationRequestTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
    required DateTime dateTimeRequested,
    required String status,
    required bool lock,
  })  : oin = Value(oin),
        dateTimeRequested = Value(dateTimeRequested),
        status = Value(status),
        lock = Value(lock);
  static Insertable<FinancialClaimsInformationRequestDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
    Expression<DateTime>? dateTimeRequested,
    Expression<String>? status,
    Expression<bool>? lock,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
      if (dateTimeRequested != null) 'date_time_requested': dateTimeRequested,
      if (status != null) 'status': status,
      if (lock != null) 'lock': lock,
    });
  }

  FinancialClaimsInformationRequestTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? oin,
      Value<DateTime>? dateTimeRequested,
      Value<String>? status,
      Value<bool>? lock}) {
    return FinancialClaimsInformationRequestTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
      dateTimeRequested: dateTimeRequested ?? this.dateTimeRequested,
      status: status ?? this.status,
      lock: lock ?? this.lock,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    if (dateTimeRequested.present) {
      map['date_time_requested'] = Variable<DateTime>(dateTimeRequested.value);
    }
    if (status.present) {
      map['status'] = Variable<String>(status.value);
    }
    if (lock.present) {
      map['lock'] = Variable<bool>(lock.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationRequestTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('dateTimeRequested: $dateTimeRequested, ')
          ..write('status: $status, ')
          ..write('lock: $lock')
          ..write(')'))
        .toString();
  }
}

class $FinancialClaimsInformationConfigurationTableTable
    extends FinancialClaimsInformationConfigurationTable
    with
        TableInfo<$FinancialClaimsInformationConfigurationTableTable,
            FinancialClaimsInformationConfigurationDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $FinancialClaimsInformationConfigurationTableTable(this.attachedDatabase,
      [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _documentMeta =
      const VerificationMeta('document');
  @override
  late final GeneratedColumn<String> document = GeneratedColumn<String>(
      'document', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _documentSignatureMeta =
      const VerificationMeta('documentSignature');
  @override
  late final GeneratedColumn<String> documentSignature =
      GeneratedColumn<String>('document_signature', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _envelopeMeta =
      const VerificationMeta('envelope');
  @override
  late final GeneratedColumn<String> envelope = GeneratedColumn<String>(
      'envelope', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _encryptedEnvelopeMeta =
      const VerificationMeta('encryptedEnvelope');
  @override
  late final GeneratedColumn<String> encryptedEnvelope =
      GeneratedColumn<String>('encrypted_envelope', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _configurationMeta =
      const VerificationMeta('configuration');
  @override
  late final GeneratedColumn<String> configuration = GeneratedColumn<String>(
      'configuration', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _expiredMeta =
      const VerificationMeta('expired');
  @override
  late final GeneratedColumn<bool> expired = GeneratedColumn<bool>(
      'expired', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("expired" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        oin,
        document,
        documentSignature,
        envelope,
        encryptedEnvelope,
        configuration,
        expired
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name =
      'financial_claims_information_configuration_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<FinancialClaimsInformationConfigurationDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    if (data.containsKey('document')) {
      context.handle(_documentMeta,
          document.isAcceptableOrUnknown(data['document']!, _documentMeta));
    }
    if (data.containsKey('document_signature')) {
      context.handle(
          _documentSignatureMeta,
          documentSignature.isAcceptableOrUnknown(
              data['document_signature']!, _documentSignatureMeta));
    }
    if (data.containsKey('envelope')) {
      context.handle(_envelopeMeta,
          envelope.isAcceptableOrUnknown(data['envelope']!, _envelopeMeta));
    }
    if (data.containsKey('encrypted_envelope')) {
      context.handle(
          _encryptedEnvelopeMeta,
          encryptedEnvelope.isAcceptableOrUnknown(
              data['encrypted_envelope']!, _encryptedEnvelopeMeta));
    }
    if (data.containsKey('configuration')) {
      context.handle(
          _configurationMeta,
          configuration.isAcceptableOrUnknown(
              data['configuration']!, _configurationMeta));
    }
    if (data.containsKey('expired')) {
      context.handle(_expiredMeta,
          expired.isAcceptableOrUnknown(data['expired']!, _expiredMeta));
    } else if (isInserting) {
      context.missing(_expiredMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  FinancialClaimsInformationConfigurationDriftModel map(
      Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return FinancialClaimsInformationConfigurationDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
      document: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}document']),
      documentSignature: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}document_signature']),
      envelope: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}envelope']),
      encryptedEnvelope: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}encrypted_envelope']),
      configuration: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}configuration']),
      expired: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}expired'])!,
    );
  }

  @override
  $FinancialClaimsInformationConfigurationTableTable createAlias(String alias) {
    return $FinancialClaimsInformationConfigurationTableTable(
        attachedDatabase, alias);
  }
}

class FinancialClaimsInformationConfigurationDriftModel extends DataClass
    implements Insertable<FinancialClaimsInformationConfigurationDriftModel> {
  final int id;
  final String oin;
  final String? document;
  final String? documentSignature;
  final String? envelope;
  final String? encryptedEnvelope;
  final String? configuration;
  final bool expired;
  const FinancialClaimsInformationConfigurationDriftModel(
      {required this.id,
      required this.oin,
      this.document,
      this.documentSignature,
      this.envelope,
      this.encryptedEnvelope,
      this.configuration,
      required this.expired});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    if (!nullToAbsent || document != null) {
      map['document'] = Variable<String>(document);
    }
    if (!nullToAbsent || documentSignature != null) {
      map['document_signature'] = Variable<String>(documentSignature);
    }
    if (!nullToAbsent || envelope != null) {
      map['envelope'] = Variable<String>(envelope);
    }
    if (!nullToAbsent || encryptedEnvelope != null) {
      map['encrypted_envelope'] = Variable<String>(encryptedEnvelope);
    }
    if (!nullToAbsent || configuration != null) {
      map['configuration'] = Variable<String>(configuration);
    }
    map['expired'] = Variable<bool>(expired);
    return map;
  }

  FinancialClaimsInformationConfigurationTableCompanion toCompanion(
      bool nullToAbsent) {
    return FinancialClaimsInformationConfigurationTableCompanion(
      id: Value(id),
      oin: Value(oin),
      document: document == null && nullToAbsent
          ? const Value.absent()
          : Value(document),
      documentSignature: documentSignature == null && nullToAbsent
          ? const Value.absent()
          : Value(documentSignature),
      envelope: envelope == null && nullToAbsent
          ? const Value.absent()
          : Value(envelope),
      encryptedEnvelope: encryptedEnvelope == null && nullToAbsent
          ? const Value.absent()
          : Value(encryptedEnvelope),
      configuration: configuration == null && nullToAbsent
          ? const Value.absent()
          : Value(configuration),
      expired: Value(expired),
    );
  }

  factory FinancialClaimsInformationConfigurationDriftModel.fromJson(
      Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return FinancialClaimsInformationConfigurationDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
      document: serializer.fromJson<String?>(json['document']),
      documentSignature:
          serializer.fromJson<String?>(json['documentSignature']),
      envelope: serializer.fromJson<String?>(json['envelope']),
      encryptedEnvelope:
          serializer.fromJson<String?>(json['encryptedEnvelope']),
      configuration: serializer.fromJson<String?>(json['configuration']),
      expired: serializer.fromJson<bool>(json['expired']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
      'document': serializer.toJson<String?>(document),
      'documentSignature': serializer.toJson<String?>(documentSignature),
      'envelope': serializer.toJson<String?>(envelope),
      'encryptedEnvelope': serializer.toJson<String?>(encryptedEnvelope),
      'configuration': serializer.toJson<String?>(configuration),
      'expired': serializer.toJson<bool>(expired),
    };
  }

  FinancialClaimsInformationConfigurationDriftModel copyWith(
          {int? id,
          String? oin,
          Value<String?> document = const Value.absent(),
          Value<String?> documentSignature = const Value.absent(),
          Value<String?> envelope = const Value.absent(),
          Value<String?> encryptedEnvelope = const Value.absent(),
          Value<String?> configuration = const Value.absent(),
          bool? expired}) =>
      FinancialClaimsInformationConfigurationDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
        document: document.present ? document.value : this.document,
        documentSignature: documentSignature.present
            ? documentSignature.value
            : this.documentSignature,
        envelope: envelope.present ? envelope.value : this.envelope,
        encryptedEnvelope: encryptedEnvelope.present
            ? encryptedEnvelope.value
            : this.encryptedEnvelope,
        configuration:
            configuration.present ? configuration.value : this.configuration,
        expired: expired ?? this.expired,
      );
  FinancialClaimsInformationConfigurationDriftModel copyWithCompanion(
      FinancialClaimsInformationConfigurationTableCompanion data) {
    return FinancialClaimsInformationConfigurationDriftModel(
      id: data.id.present ? data.id.value : this.id,
      oin: data.oin.present ? data.oin.value : this.oin,
      document: data.document.present ? data.document.value : this.document,
      documentSignature: data.documentSignature.present
          ? data.documentSignature.value
          : this.documentSignature,
      envelope: data.envelope.present ? data.envelope.value : this.envelope,
      encryptedEnvelope: data.encryptedEnvelope.present
          ? data.encryptedEnvelope.value
          : this.encryptedEnvelope,
      configuration: data.configuration.present
          ? data.configuration.value
          : this.configuration,
      expired: data.expired.present ? data.expired.value : this.expired,
    );
  }

  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationConfigurationDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('document: $document, ')
          ..write('documentSignature: $documentSignature, ')
          ..write('envelope: $envelope, ')
          ..write('encryptedEnvelope: $encryptedEnvelope, ')
          ..write('configuration: $configuration, ')
          ..write('expired: $expired')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, oin, document, documentSignature,
      envelope, encryptedEnvelope, configuration, expired);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FinancialClaimsInformationConfigurationDriftModel &&
          other.id == this.id &&
          other.oin == this.oin &&
          other.document == this.document &&
          other.documentSignature == this.documentSignature &&
          other.envelope == this.envelope &&
          other.encryptedEnvelope == this.encryptedEnvelope &&
          other.configuration == this.configuration &&
          other.expired == this.expired);
}

class FinancialClaimsInformationConfigurationTableCompanion
    extends UpdateCompanion<FinancialClaimsInformationConfigurationDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  final Value<String?> document;
  final Value<String?> documentSignature;
  final Value<String?> envelope;
  final Value<String?> encryptedEnvelope;
  final Value<String?> configuration;
  final Value<bool> expired;
  const FinancialClaimsInformationConfigurationTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
    this.document = const Value.absent(),
    this.documentSignature = const Value.absent(),
    this.envelope = const Value.absent(),
    this.encryptedEnvelope = const Value.absent(),
    this.configuration = const Value.absent(),
    this.expired = const Value.absent(),
  });
  FinancialClaimsInformationConfigurationTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
    this.document = const Value.absent(),
    this.documentSignature = const Value.absent(),
    this.envelope = const Value.absent(),
    this.encryptedEnvelope = const Value.absent(),
    this.configuration = const Value.absent(),
    required bool expired,
  })  : oin = Value(oin),
        expired = Value(expired);
  static Insertable<FinancialClaimsInformationConfigurationDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
    Expression<String>? document,
    Expression<String>? documentSignature,
    Expression<String>? envelope,
    Expression<String>? encryptedEnvelope,
    Expression<String>? configuration,
    Expression<bool>? expired,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
      if (document != null) 'document': document,
      if (documentSignature != null) 'document_signature': documentSignature,
      if (envelope != null) 'envelope': envelope,
      if (encryptedEnvelope != null) 'encrypted_envelope': encryptedEnvelope,
      if (configuration != null) 'configuration': configuration,
      if (expired != null) 'expired': expired,
    });
  }

  FinancialClaimsInformationConfigurationTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? oin,
      Value<String?>? document,
      Value<String?>? documentSignature,
      Value<String?>? envelope,
      Value<String?>? encryptedEnvelope,
      Value<String?>? configuration,
      Value<bool>? expired}) {
    return FinancialClaimsInformationConfigurationTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
      document: document ?? this.document,
      documentSignature: documentSignature ?? this.documentSignature,
      envelope: envelope ?? this.envelope,
      encryptedEnvelope: encryptedEnvelope ?? this.encryptedEnvelope,
      configuration: configuration ?? this.configuration,
      expired: expired ?? this.expired,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    if (document.present) {
      map['document'] = Variable<String>(document.value);
    }
    if (documentSignature.present) {
      map['document_signature'] = Variable<String>(documentSignature.value);
    }
    if (envelope.present) {
      map['envelope'] = Variable<String>(envelope.value);
    }
    if (encryptedEnvelope.present) {
      map['encrypted_envelope'] = Variable<String>(encryptedEnvelope.value);
    }
    if (configuration.present) {
      map['configuration'] = Variable<String>(configuration.value);
    }
    if (expired.present) {
      map['expired'] = Variable<bool>(expired.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer(
            'FinancialClaimsInformationConfigurationTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('document: $document, ')
          ..write('documentSignature: $documentSignature, ')
          ..write('envelope: $envelope, ')
          ..write('encryptedEnvelope: $encryptedEnvelope, ')
          ..write('configuration: $configuration, ')
          ..write('expired: $expired')
          ..write(')'))
        .toString();
  }
}

class $FinancialClaimsInformationInboxTableTable
    extends FinancialClaimsInformationInboxTable
    with
        TableInfo<$FinancialClaimsInformationInboxTableTable,
            FinancialClaimsInformationInboxDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $FinancialClaimsInformationInboxTableTable(this.attachedDatabase,
      [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _dateTimeRequestedMeta =
      const VerificationMeta('dateTimeRequested');
  @override
  late final GeneratedColumn<DateTime> dateTimeRequested =
      GeneratedColumn<DateTime>('date_time_requested', aliasedName, false,
          type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _dateTimeReceivedMeta =
      const VerificationMeta('dateTimeReceived');
  @override
  late final GeneratedColumn<DateTime> dateTimeReceived =
      GeneratedColumn<DateTime>('date_time_received', aliasedName, false,
          type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _financialClaimsInformationDocumentMeta =
      const VerificationMeta('financialClaimsInformationDocument');
  @override
  late final GeneratedColumn<String> financialClaimsInformationDocument =
      GeneratedColumn<String>(
          'financial_claims_information_document', aliasedName, false,
          type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _copiedToStorageMeta =
      const VerificationMeta('copiedToStorage');
  @override
  late final GeneratedColumn<bool> copiedToStorage = GeneratedColumn<bool>(
      'copied_to_storage', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'CHECK ("copied_to_storage" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        oin,
        dateTimeRequested,
        dateTimeReceived,
        financialClaimsInformationDocument,
        copiedToStorage
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'financial_claims_information_inbox_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<FinancialClaimsInformationInboxDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    if (data.containsKey('date_time_requested')) {
      context.handle(
          _dateTimeRequestedMeta,
          dateTimeRequested.isAcceptableOrUnknown(
              data['date_time_requested']!, _dateTimeRequestedMeta));
    } else if (isInserting) {
      context.missing(_dateTimeRequestedMeta);
    }
    if (data.containsKey('date_time_received')) {
      context.handle(
          _dateTimeReceivedMeta,
          dateTimeReceived.isAcceptableOrUnknown(
              data['date_time_received']!, _dateTimeReceivedMeta));
    } else if (isInserting) {
      context.missing(_dateTimeReceivedMeta);
    }
    if (data.containsKey('financial_claims_information_document')) {
      context.handle(
          _financialClaimsInformationDocumentMeta,
          financialClaimsInformationDocument.isAcceptableOrUnknown(
              data['financial_claims_information_document']!,
              _financialClaimsInformationDocumentMeta));
    } else if (isInserting) {
      context.missing(_financialClaimsInformationDocumentMeta);
    }
    if (data.containsKey('copied_to_storage')) {
      context.handle(
          _copiedToStorageMeta,
          copiedToStorage.isAcceptableOrUnknown(
              data['copied_to_storage']!, _copiedToStorageMeta));
    } else if (isInserting) {
      context.missing(_copiedToStorageMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  FinancialClaimsInformationInboxDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return FinancialClaimsInformationInboxDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
      dateTimeRequested: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime,
          data['${effectivePrefix}date_time_requested'])!,
      dateTimeReceived: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime, data['${effectivePrefix}date_time_received'])!,
      financialClaimsInformationDocument: attachedDatabase.typeMapping.read(
          DriftSqlType.string,
          data['${effectivePrefix}financial_claims_information_document'])!,
      copiedToStorage: attachedDatabase.typeMapping.read(
          DriftSqlType.bool, data['${effectivePrefix}copied_to_storage'])!,
    );
  }

  @override
  $FinancialClaimsInformationInboxTableTable createAlias(String alias) {
    return $FinancialClaimsInformationInboxTableTable(attachedDatabase, alias);
  }
}

class FinancialClaimsInformationInboxDriftModel extends DataClass
    implements Insertable<FinancialClaimsInformationInboxDriftModel> {
  final int id;
  final String oin;
  final DateTime dateTimeRequested;
  final DateTime dateTimeReceived;
  final String financialClaimsInformationDocument;
  final bool copiedToStorage;
  const FinancialClaimsInformationInboxDriftModel(
      {required this.id,
      required this.oin,
      required this.dateTimeRequested,
      required this.dateTimeReceived,
      required this.financialClaimsInformationDocument,
      required this.copiedToStorage});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    map['date_time_requested'] = Variable<DateTime>(dateTimeRequested);
    map['date_time_received'] = Variable<DateTime>(dateTimeReceived);
    map['financial_claims_information_document'] =
        Variable<String>(financialClaimsInformationDocument);
    map['copied_to_storage'] = Variable<bool>(copiedToStorage);
    return map;
  }

  FinancialClaimsInformationInboxTableCompanion toCompanion(bool nullToAbsent) {
    return FinancialClaimsInformationInboxTableCompanion(
      id: Value(id),
      oin: Value(oin),
      dateTimeRequested: Value(dateTimeRequested),
      dateTimeReceived: Value(dateTimeReceived),
      financialClaimsInformationDocument:
          Value(financialClaimsInformationDocument),
      copiedToStorage: Value(copiedToStorage),
    );
  }

  factory FinancialClaimsInformationInboxDriftModel.fromJson(
      Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return FinancialClaimsInformationInboxDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
      dateTimeRequested:
          serializer.fromJson<DateTime>(json['dateTimeRequested']),
      dateTimeReceived: serializer.fromJson<DateTime>(json['dateTimeReceived']),
      financialClaimsInformationDocument: serializer
          .fromJson<String>(json['financialClaimsInformationDocument']),
      copiedToStorage: serializer.fromJson<bool>(json['copiedToStorage']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
      'dateTimeRequested': serializer.toJson<DateTime>(dateTimeRequested),
      'dateTimeReceived': serializer.toJson<DateTime>(dateTimeReceived),
      'financialClaimsInformationDocument':
          serializer.toJson<String>(financialClaimsInformationDocument),
      'copiedToStorage': serializer.toJson<bool>(copiedToStorage),
    };
  }

  FinancialClaimsInformationInboxDriftModel copyWith(
          {int? id,
          String? oin,
          DateTime? dateTimeRequested,
          DateTime? dateTimeReceived,
          String? financialClaimsInformationDocument,
          bool? copiedToStorage}) =>
      FinancialClaimsInformationInboxDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
        dateTimeRequested: dateTimeRequested ?? this.dateTimeRequested,
        dateTimeReceived: dateTimeReceived ?? this.dateTimeReceived,
        financialClaimsInformationDocument:
            financialClaimsInformationDocument ??
                this.financialClaimsInformationDocument,
        copiedToStorage: copiedToStorage ?? this.copiedToStorage,
      );
  FinancialClaimsInformationInboxDriftModel copyWithCompanion(
      FinancialClaimsInformationInboxTableCompanion data) {
    return FinancialClaimsInformationInboxDriftModel(
      id: data.id.present ? data.id.value : this.id,
      oin: data.oin.present ? data.oin.value : this.oin,
      dateTimeRequested: data.dateTimeRequested.present
          ? data.dateTimeRequested.value
          : this.dateTimeRequested,
      dateTimeReceived: data.dateTimeReceived.present
          ? data.dateTimeReceived.value
          : this.dateTimeReceived,
      financialClaimsInformationDocument:
          data.financialClaimsInformationDocument.present
              ? data.financialClaimsInformationDocument.value
              : this.financialClaimsInformationDocument,
      copiedToStorage: data.copiedToStorage.present
          ? data.copiedToStorage.value
          : this.copiedToStorage,
    );
  }

  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationInboxDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('dateTimeRequested: $dateTimeRequested, ')
          ..write('dateTimeReceived: $dateTimeReceived, ')
          ..write(
              'financialClaimsInformationDocument: $financialClaimsInformationDocument, ')
          ..write('copiedToStorage: $copiedToStorage')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, oin, dateTimeRequested, dateTimeReceived,
      financialClaimsInformationDocument, copiedToStorage);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FinancialClaimsInformationInboxDriftModel &&
          other.id == this.id &&
          other.oin == this.oin &&
          other.dateTimeRequested == this.dateTimeRequested &&
          other.dateTimeReceived == this.dateTimeReceived &&
          other.financialClaimsInformationDocument ==
              this.financialClaimsInformationDocument &&
          other.copiedToStorage == this.copiedToStorage);
}

class FinancialClaimsInformationInboxTableCompanion
    extends UpdateCompanion<FinancialClaimsInformationInboxDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  final Value<DateTime> dateTimeRequested;
  final Value<DateTime> dateTimeReceived;
  final Value<String> financialClaimsInformationDocument;
  final Value<bool> copiedToStorage;
  const FinancialClaimsInformationInboxTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
    this.dateTimeRequested = const Value.absent(),
    this.dateTimeReceived = const Value.absent(),
    this.financialClaimsInformationDocument = const Value.absent(),
    this.copiedToStorage = const Value.absent(),
  });
  FinancialClaimsInformationInboxTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
    required DateTime dateTimeRequested,
    required DateTime dateTimeReceived,
    required String financialClaimsInformationDocument,
    required bool copiedToStorage,
  })  : oin = Value(oin),
        dateTimeRequested = Value(dateTimeRequested),
        dateTimeReceived = Value(dateTimeReceived),
        financialClaimsInformationDocument =
            Value(financialClaimsInformationDocument),
        copiedToStorage = Value(copiedToStorage);
  static Insertable<FinancialClaimsInformationInboxDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
    Expression<DateTime>? dateTimeRequested,
    Expression<DateTime>? dateTimeReceived,
    Expression<String>? financialClaimsInformationDocument,
    Expression<bool>? copiedToStorage,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
      if (dateTimeRequested != null) 'date_time_requested': dateTimeRequested,
      if (dateTimeReceived != null) 'date_time_received': dateTimeReceived,
      if (financialClaimsInformationDocument != null)
        'financial_claims_information_document':
            financialClaimsInformationDocument,
      if (copiedToStorage != null) 'copied_to_storage': copiedToStorage,
    });
  }

  FinancialClaimsInformationInboxTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? oin,
      Value<DateTime>? dateTimeRequested,
      Value<DateTime>? dateTimeReceived,
      Value<String>? financialClaimsInformationDocument,
      Value<bool>? copiedToStorage}) {
    return FinancialClaimsInformationInboxTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
      dateTimeRequested: dateTimeRequested ?? this.dateTimeRequested,
      dateTimeReceived: dateTimeReceived ?? this.dateTimeReceived,
      financialClaimsInformationDocument: financialClaimsInformationDocument ??
          this.financialClaimsInformationDocument,
      copiedToStorage: copiedToStorage ?? this.copiedToStorage,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    if (dateTimeRequested.present) {
      map['date_time_requested'] = Variable<DateTime>(dateTimeRequested.value);
    }
    if (dateTimeReceived.present) {
      map['date_time_received'] = Variable<DateTime>(dateTimeReceived.value);
    }
    if (financialClaimsInformationDocument.present) {
      map['financial_claims_information_document'] =
          Variable<String>(financialClaimsInformationDocument.value);
    }
    if (copiedToStorage.present) {
      map['copied_to_storage'] = Variable<bool>(copiedToStorage.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationInboxTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('dateTimeRequested: $dateTimeRequested, ')
          ..write('dateTimeReceived: $dateTimeReceived, ')
          ..write(
              'financialClaimsInformationDocument: $financialClaimsInformationDocument, ')
          ..write('copiedToStorage: $copiedToStorage')
          ..write(')'))
        .toString();
  }
}

class $FinancialClaimsInformationReceivedTableTable
    extends FinancialClaimsInformationReceivedTable
    with
        TableInfo<$FinancialClaimsInformationReceivedTableTable,
            FinancialClaimsInformationReceivedDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $FinancialClaimsInformationReceivedTableTable(this.attachedDatabase,
      [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _dateTimeRequestedMeta =
      const VerificationMeta('dateTimeRequested');
  @override
  late final GeneratedColumn<DateTime> dateTimeRequested =
      GeneratedColumn<DateTime>('date_time_requested', aliasedName, false,
          type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _dateTimeReceivedMeta =
      const VerificationMeta('dateTimeReceived');
  @override
  late final GeneratedColumn<DateTime> dateTimeReceived =
      GeneratedColumn<DateTime>('date_time_received', aliasedName, false,
          type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta
      _encryptedFinancialClaimsInformationDocumentMeta =
      const VerificationMeta('encryptedFinancialClaimsInformationDocument');
  @override
  late final GeneratedColumn<String>
      encryptedFinancialClaimsInformationDocument = GeneratedColumn<String>(
          'encrypted_financial_claims_information_document', aliasedName, false,
          type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _financialClaimsInformationDocumentMeta =
      const VerificationMeta('financialClaimsInformationDocument');
  @override
  late final GeneratedColumn<String> financialClaimsInformationDocument =
      GeneratedColumn<String>(
          'financial_claims_information_document', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _changedMeta =
      const VerificationMeta('changed');
  @override
  late final GeneratedColumn<bool> changed = GeneratedColumn<bool>(
      'changed', aliasedName, true,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("changed" IN (0, 1))'));
  static const VerificationMeta _copiedToInboxMeta =
      const VerificationMeta('copiedToInbox');
  @override
  late final GeneratedColumn<bool> copiedToInbox = GeneratedColumn<bool>(
      'copied_to_inbox', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'CHECK ("copied_to_inbox" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        oin,
        dateTimeRequested,
        dateTimeReceived,
        encryptedFinancialClaimsInformationDocument,
        financialClaimsInformationDocument,
        changed,
        copiedToInbox
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'financial_claims_information_received_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<FinancialClaimsInformationReceivedDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    if (data.containsKey('date_time_requested')) {
      context.handle(
          _dateTimeRequestedMeta,
          dateTimeRequested.isAcceptableOrUnknown(
              data['date_time_requested']!, _dateTimeRequestedMeta));
    } else if (isInserting) {
      context.missing(_dateTimeRequestedMeta);
    }
    if (data.containsKey('date_time_received')) {
      context.handle(
          _dateTimeReceivedMeta,
          dateTimeReceived.isAcceptableOrUnknown(
              data['date_time_received']!, _dateTimeReceivedMeta));
    } else if (isInserting) {
      context.missing(_dateTimeReceivedMeta);
    }
    if (data.containsKey('encrypted_financial_claims_information_document')) {
      context.handle(
          _encryptedFinancialClaimsInformationDocumentMeta,
          encryptedFinancialClaimsInformationDocument.isAcceptableOrUnknown(
              data['encrypted_financial_claims_information_document']!,
              _encryptedFinancialClaimsInformationDocumentMeta));
    } else if (isInserting) {
      context.missing(_encryptedFinancialClaimsInformationDocumentMeta);
    }
    if (data.containsKey('financial_claims_information_document')) {
      context.handle(
          _financialClaimsInformationDocumentMeta,
          financialClaimsInformationDocument.isAcceptableOrUnknown(
              data['financial_claims_information_document']!,
              _financialClaimsInformationDocumentMeta));
    }
    if (data.containsKey('changed')) {
      context.handle(_changedMeta,
          changed.isAcceptableOrUnknown(data['changed']!, _changedMeta));
    }
    if (data.containsKey('copied_to_inbox')) {
      context.handle(
          _copiedToInboxMeta,
          copiedToInbox.isAcceptableOrUnknown(
              data['copied_to_inbox']!, _copiedToInboxMeta));
    } else if (isInserting) {
      context.missing(_copiedToInboxMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  FinancialClaimsInformationReceivedDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return FinancialClaimsInformationReceivedDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
      dateTimeRequested: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime,
          data['${effectivePrefix}date_time_requested'])!,
      dateTimeReceived: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime, data['${effectivePrefix}date_time_received'])!,
      encryptedFinancialClaimsInformationDocument: attachedDatabase.typeMapping
          .read(
              DriftSqlType.string,
              data[
                  '${effectivePrefix}encrypted_financial_claims_information_document'])!,
      financialClaimsInformationDocument: attachedDatabase.typeMapping.read(
          DriftSqlType.string,
          data['${effectivePrefix}financial_claims_information_document']),
      changed: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}changed']),
      copiedToInbox: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}copied_to_inbox'])!,
    );
  }

  @override
  $FinancialClaimsInformationReceivedTableTable createAlias(String alias) {
    return $FinancialClaimsInformationReceivedTableTable(
        attachedDatabase, alias);
  }
}

class FinancialClaimsInformationReceivedDriftModel extends DataClass
    implements Insertable<FinancialClaimsInformationReceivedDriftModel> {
  final int id;
  final String oin;
  final DateTime dateTimeRequested;
  final DateTime dateTimeReceived;
  final String encryptedFinancialClaimsInformationDocument;
  final String? financialClaimsInformationDocument;
  final bool? changed;
  final bool copiedToInbox;
  const FinancialClaimsInformationReceivedDriftModel(
      {required this.id,
      required this.oin,
      required this.dateTimeRequested,
      required this.dateTimeReceived,
      required this.encryptedFinancialClaimsInformationDocument,
      this.financialClaimsInformationDocument,
      this.changed,
      required this.copiedToInbox});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    map['date_time_requested'] = Variable<DateTime>(dateTimeRequested);
    map['date_time_received'] = Variable<DateTime>(dateTimeReceived);
    map['encrypted_financial_claims_information_document'] =
        Variable<String>(encryptedFinancialClaimsInformationDocument);
    if (!nullToAbsent || financialClaimsInformationDocument != null) {
      map['financial_claims_information_document'] =
          Variable<String>(financialClaimsInformationDocument);
    }
    if (!nullToAbsent || changed != null) {
      map['changed'] = Variable<bool>(changed);
    }
    map['copied_to_inbox'] = Variable<bool>(copiedToInbox);
    return map;
  }

  FinancialClaimsInformationReceivedTableCompanion toCompanion(
      bool nullToAbsent) {
    return FinancialClaimsInformationReceivedTableCompanion(
      id: Value(id),
      oin: Value(oin),
      dateTimeRequested: Value(dateTimeRequested),
      dateTimeReceived: Value(dateTimeReceived),
      encryptedFinancialClaimsInformationDocument:
          Value(encryptedFinancialClaimsInformationDocument),
      financialClaimsInformationDocument:
          financialClaimsInformationDocument == null && nullToAbsent
              ? const Value.absent()
              : Value(financialClaimsInformationDocument),
      changed: changed == null && nullToAbsent
          ? const Value.absent()
          : Value(changed),
      copiedToInbox: Value(copiedToInbox),
    );
  }

  factory FinancialClaimsInformationReceivedDriftModel.fromJson(
      Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return FinancialClaimsInformationReceivedDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
      dateTimeRequested:
          serializer.fromJson<DateTime>(json['dateTimeRequested']),
      dateTimeReceived: serializer.fromJson<DateTime>(json['dateTimeReceived']),
      encryptedFinancialClaimsInformationDocument: serializer.fromJson<String>(
          json['encryptedFinancialClaimsInformationDocument']),
      financialClaimsInformationDocument: serializer
          .fromJson<String?>(json['financialClaimsInformationDocument']),
      changed: serializer.fromJson<bool?>(json['changed']),
      copiedToInbox: serializer.fromJson<bool>(json['copiedToInbox']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
      'dateTimeRequested': serializer.toJson<DateTime>(dateTimeRequested),
      'dateTimeReceived': serializer.toJson<DateTime>(dateTimeReceived),
      'encryptedFinancialClaimsInformationDocument': serializer
          .toJson<String>(encryptedFinancialClaimsInformationDocument),
      'financialClaimsInformationDocument':
          serializer.toJson<String?>(financialClaimsInformationDocument),
      'changed': serializer.toJson<bool?>(changed),
      'copiedToInbox': serializer.toJson<bool>(copiedToInbox),
    };
  }

  FinancialClaimsInformationReceivedDriftModel copyWith(
          {int? id,
          String? oin,
          DateTime? dateTimeRequested,
          DateTime? dateTimeReceived,
          String? encryptedFinancialClaimsInformationDocument,
          Value<String?> financialClaimsInformationDocument =
              const Value.absent(),
          Value<bool?> changed = const Value.absent(),
          bool? copiedToInbox}) =>
      FinancialClaimsInformationReceivedDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
        dateTimeRequested: dateTimeRequested ?? this.dateTimeRequested,
        dateTimeReceived: dateTimeReceived ?? this.dateTimeReceived,
        encryptedFinancialClaimsInformationDocument:
            encryptedFinancialClaimsInformationDocument ??
                this.encryptedFinancialClaimsInformationDocument,
        financialClaimsInformationDocument:
            financialClaimsInformationDocument.present
                ? financialClaimsInformationDocument.value
                : this.financialClaimsInformationDocument,
        changed: changed.present ? changed.value : this.changed,
        copiedToInbox: copiedToInbox ?? this.copiedToInbox,
      );
  FinancialClaimsInformationReceivedDriftModel copyWithCompanion(
      FinancialClaimsInformationReceivedTableCompanion data) {
    return FinancialClaimsInformationReceivedDriftModel(
      id: data.id.present ? data.id.value : this.id,
      oin: data.oin.present ? data.oin.value : this.oin,
      dateTimeRequested: data.dateTimeRequested.present
          ? data.dateTimeRequested.value
          : this.dateTimeRequested,
      dateTimeReceived: data.dateTimeReceived.present
          ? data.dateTimeReceived.value
          : this.dateTimeReceived,
      encryptedFinancialClaimsInformationDocument:
          data.encryptedFinancialClaimsInformationDocument.present
              ? data.encryptedFinancialClaimsInformationDocument.value
              : this.encryptedFinancialClaimsInformationDocument,
      financialClaimsInformationDocument:
          data.financialClaimsInformationDocument.present
              ? data.financialClaimsInformationDocument.value
              : this.financialClaimsInformationDocument,
      changed: data.changed.present ? data.changed.value : this.changed,
      copiedToInbox: data.copiedToInbox.present
          ? data.copiedToInbox.value
          : this.copiedToInbox,
    );
  }

  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationReceivedDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('dateTimeRequested: $dateTimeRequested, ')
          ..write('dateTimeReceived: $dateTimeReceived, ')
          ..write(
              'encryptedFinancialClaimsInformationDocument: $encryptedFinancialClaimsInformationDocument, ')
          ..write(
              'financialClaimsInformationDocument: $financialClaimsInformationDocument, ')
          ..write('changed: $changed, ')
          ..write('copiedToInbox: $copiedToInbox')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id,
      oin,
      dateTimeRequested,
      dateTimeReceived,
      encryptedFinancialClaimsInformationDocument,
      financialClaimsInformationDocument,
      changed,
      copiedToInbox);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FinancialClaimsInformationReceivedDriftModel &&
          other.id == this.id &&
          other.oin == this.oin &&
          other.dateTimeRequested == this.dateTimeRequested &&
          other.dateTimeReceived == this.dateTimeReceived &&
          other.encryptedFinancialClaimsInformationDocument ==
              this.encryptedFinancialClaimsInformationDocument &&
          other.financialClaimsInformationDocument ==
              this.financialClaimsInformationDocument &&
          other.changed == this.changed &&
          other.copiedToInbox == this.copiedToInbox);
}

class FinancialClaimsInformationReceivedTableCompanion
    extends UpdateCompanion<FinancialClaimsInformationReceivedDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  final Value<DateTime> dateTimeRequested;
  final Value<DateTime> dateTimeReceived;
  final Value<String> encryptedFinancialClaimsInformationDocument;
  final Value<String?> financialClaimsInformationDocument;
  final Value<bool?> changed;
  final Value<bool> copiedToInbox;
  const FinancialClaimsInformationReceivedTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
    this.dateTimeRequested = const Value.absent(),
    this.dateTimeReceived = const Value.absent(),
    this.encryptedFinancialClaimsInformationDocument = const Value.absent(),
    this.financialClaimsInformationDocument = const Value.absent(),
    this.changed = const Value.absent(),
    this.copiedToInbox = const Value.absent(),
  });
  FinancialClaimsInformationReceivedTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
    required DateTime dateTimeRequested,
    required DateTime dateTimeReceived,
    required String encryptedFinancialClaimsInformationDocument,
    this.financialClaimsInformationDocument = const Value.absent(),
    this.changed = const Value.absent(),
    required bool copiedToInbox,
  })  : oin = Value(oin),
        dateTimeRequested = Value(dateTimeRequested),
        dateTimeReceived = Value(dateTimeReceived),
        encryptedFinancialClaimsInformationDocument =
            Value(encryptedFinancialClaimsInformationDocument),
        copiedToInbox = Value(copiedToInbox);
  static Insertable<FinancialClaimsInformationReceivedDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
    Expression<DateTime>? dateTimeRequested,
    Expression<DateTime>? dateTimeReceived,
    Expression<String>? encryptedFinancialClaimsInformationDocument,
    Expression<String>? financialClaimsInformationDocument,
    Expression<bool>? changed,
    Expression<bool>? copiedToInbox,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
      if (dateTimeRequested != null) 'date_time_requested': dateTimeRequested,
      if (dateTimeReceived != null) 'date_time_received': dateTimeReceived,
      if (encryptedFinancialClaimsInformationDocument != null)
        'encrypted_financial_claims_information_document':
            encryptedFinancialClaimsInformationDocument,
      if (financialClaimsInformationDocument != null)
        'financial_claims_information_document':
            financialClaimsInformationDocument,
      if (changed != null) 'changed': changed,
      if (copiedToInbox != null) 'copied_to_inbox': copiedToInbox,
    });
  }

  FinancialClaimsInformationReceivedTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? oin,
      Value<DateTime>? dateTimeRequested,
      Value<DateTime>? dateTimeReceived,
      Value<String>? encryptedFinancialClaimsInformationDocument,
      Value<String?>? financialClaimsInformationDocument,
      Value<bool?>? changed,
      Value<bool>? copiedToInbox}) {
    return FinancialClaimsInformationReceivedTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
      dateTimeRequested: dateTimeRequested ?? this.dateTimeRequested,
      dateTimeReceived: dateTimeReceived ?? this.dateTimeReceived,
      encryptedFinancialClaimsInformationDocument:
          encryptedFinancialClaimsInformationDocument ??
              this.encryptedFinancialClaimsInformationDocument,
      financialClaimsInformationDocument: financialClaimsInformationDocument ??
          this.financialClaimsInformationDocument,
      changed: changed ?? this.changed,
      copiedToInbox: copiedToInbox ?? this.copiedToInbox,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    if (dateTimeRequested.present) {
      map['date_time_requested'] = Variable<DateTime>(dateTimeRequested.value);
    }
    if (dateTimeReceived.present) {
      map['date_time_received'] = Variable<DateTime>(dateTimeReceived.value);
    }
    if (encryptedFinancialClaimsInformationDocument.present) {
      map['encrypted_financial_claims_information_document'] =
          Variable<String>(encryptedFinancialClaimsInformationDocument.value);
    }
    if (financialClaimsInformationDocument.present) {
      map['financial_claims_information_document'] =
          Variable<String>(financialClaimsInformationDocument.value);
    }
    if (changed.present) {
      map['changed'] = Variable<bool>(changed.value);
    }
    if (copiedToInbox.present) {
      map['copied_to_inbox'] = Variable<bool>(copiedToInbox.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationReceivedTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('dateTimeRequested: $dateTimeRequested, ')
          ..write('dateTimeReceived: $dateTimeReceived, ')
          ..write(
              'encryptedFinancialClaimsInformationDocument: $encryptedFinancialClaimsInformationDocument, ')
          ..write(
              'financialClaimsInformationDocument: $financialClaimsInformationDocument, ')
          ..write('changed: $changed, ')
          ..write('copiedToInbox: $copiedToInbox')
          ..write(')'))
        .toString();
  }
}

class $RegistrationTableTable extends RegistrationTable
    with TableInfo<$RegistrationTableTable, RegistrationDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $RegistrationTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _dateTimeStartedMeta =
      const VerificationMeta('dateTimeStarted');
  @override
  late final GeneratedColumn<DateTime> dateTimeStarted =
      GeneratedColumn<DateTime>('date_time_started', aliasedName, false,
          type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _appPublicKeyMeta =
      const VerificationMeta('appPublicKey');
  @override
  late final GeneratedColumn<String> appPublicKey = GeneratedColumn<String>(
      'app_public_key', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _appManagerOinMeta =
      const VerificationMeta('appManagerOin');
  @override
  late final GeneratedColumn<String> appManagerOin = GeneratedColumn<String>(
      'app_manager_oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _appManagerPublicKeyMeta =
      const VerificationMeta('appManagerPublicKey');
  @override
  late final GeneratedColumn<String> appManagerPublicKey =
      GeneratedColumn<String>('app_manager_public_key', aliasedName, false,
          type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _registrationTokenMeta =
      const VerificationMeta('registrationToken');
  @override
  late final GeneratedColumn<String> registrationToken =
      GeneratedColumn<String>('registration_token', aliasedName, false,
          type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _dateTimeCompletedMeta =
      const VerificationMeta('dateTimeCompleted');
  @override
  late final GeneratedColumn<DateTime> dateTimeCompleted =
      GeneratedColumn<DateTime>('date_time_completed', aliasedName, true,
          type: DriftSqlType.dateTime, requiredDuringInsert: false);
  static const VerificationMeta _givenNameMeta =
      const VerificationMeta('givenName');
  @override
  late final GeneratedColumn<String> givenName = GeneratedColumn<String>(
      'given_name', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _expiredMeta =
      const VerificationMeta('expired');
  @override
  late final GeneratedColumn<bool> expired = GeneratedColumn<bool>(
      'expired', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("expired" IN (0, 1))'));
  static const VerificationMeta _revokedMeta =
      const VerificationMeta('revoked');
  @override
  late final GeneratedColumn<bool> revoked = GeneratedColumn<bool>(
      'revoked', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("revoked" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        dateTimeStarted,
        appPublicKey,
        appManagerOin,
        appManagerPublicKey,
        registrationToken,
        dateTimeCompleted,
        givenName,
        expired,
        revoked
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'registration_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<RegistrationDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('date_time_started')) {
      context.handle(
          _dateTimeStartedMeta,
          dateTimeStarted.isAcceptableOrUnknown(
              data['date_time_started']!, _dateTimeStartedMeta));
    } else if (isInserting) {
      context.missing(_dateTimeStartedMeta);
    }
    if (data.containsKey('app_public_key')) {
      context.handle(
          _appPublicKeyMeta,
          appPublicKey.isAcceptableOrUnknown(
              data['app_public_key']!, _appPublicKeyMeta));
    } else if (isInserting) {
      context.missing(_appPublicKeyMeta);
    }
    if (data.containsKey('app_manager_oin')) {
      context.handle(
          _appManagerOinMeta,
          appManagerOin.isAcceptableOrUnknown(
              data['app_manager_oin']!, _appManagerOinMeta));
    } else if (isInserting) {
      context.missing(_appManagerOinMeta);
    }
    if (data.containsKey('app_manager_public_key')) {
      context.handle(
          _appManagerPublicKeyMeta,
          appManagerPublicKey.isAcceptableOrUnknown(
              data['app_manager_public_key']!, _appManagerPublicKeyMeta));
    } else if (isInserting) {
      context.missing(_appManagerPublicKeyMeta);
    }
    if (data.containsKey('registration_token')) {
      context.handle(
          _registrationTokenMeta,
          registrationToken.isAcceptableOrUnknown(
              data['registration_token']!, _registrationTokenMeta));
    } else if (isInserting) {
      context.missing(_registrationTokenMeta);
    }
    if (data.containsKey('date_time_completed')) {
      context.handle(
          _dateTimeCompletedMeta,
          dateTimeCompleted.isAcceptableOrUnknown(
              data['date_time_completed']!, _dateTimeCompletedMeta));
    }
    if (data.containsKey('given_name')) {
      context.handle(_givenNameMeta,
          givenName.isAcceptableOrUnknown(data['given_name']!, _givenNameMeta));
    }
    if (data.containsKey('expired')) {
      context.handle(_expiredMeta,
          expired.isAcceptableOrUnknown(data['expired']!, _expiredMeta));
    } else if (isInserting) {
      context.missing(_expiredMeta);
    }
    if (data.containsKey('revoked')) {
      context.handle(_revokedMeta,
          revoked.isAcceptableOrUnknown(data['revoked']!, _revokedMeta));
    } else if (isInserting) {
      context.missing(_revokedMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  RegistrationDriftModel map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return RegistrationDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      dateTimeStarted: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime, data['${effectivePrefix}date_time_started'])!,
      appPublicKey: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}app_public_key'])!,
      appManagerOin: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}app_manager_oin'])!,
      appManagerPublicKey: attachedDatabase.typeMapping.read(
          DriftSqlType.string,
          data['${effectivePrefix}app_manager_public_key'])!,
      registrationToken: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}registration_token'])!,
      dateTimeCompleted: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime, data['${effectivePrefix}date_time_completed']),
      givenName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}given_name']),
      expired: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}expired'])!,
      revoked: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}revoked'])!,
    );
  }

  @override
  $RegistrationTableTable createAlias(String alias) {
    return $RegistrationTableTable(attachedDatabase, alias);
  }
}

class RegistrationDriftModel extends DataClass
    implements Insertable<RegistrationDriftModel> {
  final int id;
  final DateTime dateTimeStarted;
  final String appPublicKey;
  final String appManagerOin;
  final String appManagerPublicKey;
  final String registrationToken;
  final DateTime? dateTimeCompleted;
  final String? givenName;
  final bool expired;
  final bool revoked;
  const RegistrationDriftModel(
      {required this.id,
      required this.dateTimeStarted,
      required this.appPublicKey,
      required this.appManagerOin,
      required this.appManagerPublicKey,
      required this.registrationToken,
      this.dateTimeCompleted,
      this.givenName,
      required this.expired,
      required this.revoked});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['date_time_started'] = Variable<DateTime>(dateTimeStarted);
    map['app_public_key'] = Variable<String>(appPublicKey);
    map['app_manager_oin'] = Variable<String>(appManagerOin);
    map['app_manager_public_key'] = Variable<String>(appManagerPublicKey);
    map['registration_token'] = Variable<String>(registrationToken);
    if (!nullToAbsent || dateTimeCompleted != null) {
      map['date_time_completed'] = Variable<DateTime>(dateTimeCompleted);
    }
    if (!nullToAbsent || givenName != null) {
      map['given_name'] = Variable<String>(givenName);
    }
    map['expired'] = Variable<bool>(expired);
    map['revoked'] = Variable<bool>(revoked);
    return map;
  }

  RegistrationTableCompanion toCompanion(bool nullToAbsent) {
    return RegistrationTableCompanion(
      id: Value(id),
      dateTimeStarted: Value(dateTimeStarted),
      appPublicKey: Value(appPublicKey),
      appManagerOin: Value(appManagerOin),
      appManagerPublicKey: Value(appManagerPublicKey),
      registrationToken: Value(registrationToken),
      dateTimeCompleted: dateTimeCompleted == null && nullToAbsent
          ? const Value.absent()
          : Value(dateTimeCompleted),
      givenName: givenName == null && nullToAbsent
          ? const Value.absent()
          : Value(givenName),
      expired: Value(expired),
      revoked: Value(revoked),
    );
  }

  factory RegistrationDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return RegistrationDriftModel(
      id: serializer.fromJson<int>(json['id']),
      dateTimeStarted: serializer.fromJson<DateTime>(json['dateTimeStarted']),
      appPublicKey: serializer.fromJson<String>(json['appPublicKey']),
      appManagerOin: serializer.fromJson<String>(json['appManagerOin']),
      appManagerPublicKey:
          serializer.fromJson<String>(json['appManagerPublicKey']),
      registrationToken: serializer.fromJson<String>(json['registrationToken']),
      dateTimeCompleted:
          serializer.fromJson<DateTime?>(json['dateTimeCompleted']),
      givenName: serializer.fromJson<String?>(json['givenName']),
      expired: serializer.fromJson<bool>(json['expired']),
      revoked: serializer.fromJson<bool>(json['revoked']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'dateTimeStarted': serializer.toJson<DateTime>(dateTimeStarted),
      'appPublicKey': serializer.toJson<String>(appPublicKey),
      'appManagerOin': serializer.toJson<String>(appManagerOin),
      'appManagerPublicKey': serializer.toJson<String>(appManagerPublicKey),
      'registrationToken': serializer.toJson<String>(registrationToken),
      'dateTimeCompleted': serializer.toJson<DateTime?>(dateTimeCompleted),
      'givenName': serializer.toJson<String?>(givenName),
      'expired': serializer.toJson<bool>(expired),
      'revoked': serializer.toJson<bool>(revoked),
    };
  }

  RegistrationDriftModel copyWith(
          {int? id,
          DateTime? dateTimeStarted,
          String? appPublicKey,
          String? appManagerOin,
          String? appManagerPublicKey,
          String? registrationToken,
          Value<DateTime?> dateTimeCompleted = const Value.absent(),
          Value<String?> givenName = const Value.absent(),
          bool? expired,
          bool? revoked}) =>
      RegistrationDriftModel(
        id: id ?? this.id,
        dateTimeStarted: dateTimeStarted ?? this.dateTimeStarted,
        appPublicKey: appPublicKey ?? this.appPublicKey,
        appManagerOin: appManagerOin ?? this.appManagerOin,
        appManagerPublicKey: appManagerPublicKey ?? this.appManagerPublicKey,
        registrationToken: registrationToken ?? this.registrationToken,
        dateTimeCompleted: dateTimeCompleted.present
            ? dateTimeCompleted.value
            : this.dateTimeCompleted,
        givenName: givenName.present ? givenName.value : this.givenName,
        expired: expired ?? this.expired,
        revoked: revoked ?? this.revoked,
      );
  RegistrationDriftModel copyWithCompanion(RegistrationTableCompanion data) {
    return RegistrationDriftModel(
      id: data.id.present ? data.id.value : this.id,
      dateTimeStarted: data.dateTimeStarted.present
          ? data.dateTimeStarted.value
          : this.dateTimeStarted,
      appPublicKey: data.appPublicKey.present
          ? data.appPublicKey.value
          : this.appPublicKey,
      appManagerOin: data.appManagerOin.present
          ? data.appManagerOin.value
          : this.appManagerOin,
      appManagerPublicKey: data.appManagerPublicKey.present
          ? data.appManagerPublicKey.value
          : this.appManagerPublicKey,
      registrationToken: data.registrationToken.present
          ? data.registrationToken.value
          : this.registrationToken,
      dateTimeCompleted: data.dateTimeCompleted.present
          ? data.dateTimeCompleted.value
          : this.dateTimeCompleted,
      givenName: data.givenName.present ? data.givenName.value : this.givenName,
      expired: data.expired.present ? data.expired.value : this.expired,
      revoked: data.revoked.present ? data.revoked.value : this.revoked,
    );
  }

  @override
  String toString() {
    return (StringBuffer('RegistrationDriftModel(')
          ..write('id: $id, ')
          ..write('dateTimeStarted: $dateTimeStarted, ')
          ..write('appPublicKey: $appPublicKey, ')
          ..write('appManagerOin: $appManagerOin, ')
          ..write('appManagerPublicKey: $appManagerPublicKey, ')
          ..write('registrationToken: $registrationToken, ')
          ..write('dateTimeCompleted: $dateTimeCompleted, ')
          ..write('givenName: $givenName, ')
          ..write('expired: $expired, ')
          ..write('revoked: $revoked')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id,
      dateTimeStarted,
      appPublicKey,
      appManagerOin,
      appManagerPublicKey,
      registrationToken,
      dateTimeCompleted,
      givenName,
      expired,
      revoked);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is RegistrationDriftModel &&
          other.id == this.id &&
          other.dateTimeStarted == this.dateTimeStarted &&
          other.appPublicKey == this.appPublicKey &&
          other.appManagerOin == this.appManagerOin &&
          other.appManagerPublicKey == this.appManagerPublicKey &&
          other.registrationToken == this.registrationToken &&
          other.dateTimeCompleted == this.dateTimeCompleted &&
          other.givenName == this.givenName &&
          other.expired == this.expired &&
          other.revoked == this.revoked);
}

class RegistrationTableCompanion
    extends UpdateCompanion<RegistrationDriftModel> {
  final Value<int> id;
  final Value<DateTime> dateTimeStarted;
  final Value<String> appPublicKey;
  final Value<String> appManagerOin;
  final Value<String> appManagerPublicKey;
  final Value<String> registrationToken;
  final Value<DateTime?> dateTimeCompleted;
  final Value<String?> givenName;
  final Value<bool> expired;
  final Value<bool> revoked;
  const RegistrationTableCompanion({
    this.id = const Value.absent(),
    this.dateTimeStarted = const Value.absent(),
    this.appPublicKey = const Value.absent(),
    this.appManagerOin = const Value.absent(),
    this.appManagerPublicKey = const Value.absent(),
    this.registrationToken = const Value.absent(),
    this.dateTimeCompleted = const Value.absent(),
    this.givenName = const Value.absent(),
    this.expired = const Value.absent(),
    this.revoked = const Value.absent(),
  });
  RegistrationTableCompanion.insert({
    this.id = const Value.absent(),
    required DateTime dateTimeStarted,
    required String appPublicKey,
    required String appManagerOin,
    required String appManagerPublicKey,
    required String registrationToken,
    this.dateTimeCompleted = const Value.absent(),
    this.givenName = const Value.absent(),
    required bool expired,
    required bool revoked,
  })  : dateTimeStarted = Value(dateTimeStarted),
        appPublicKey = Value(appPublicKey),
        appManagerOin = Value(appManagerOin),
        appManagerPublicKey = Value(appManagerPublicKey),
        registrationToken = Value(registrationToken),
        expired = Value(expired),
        revoked = Value(revoked);
  static Insertable<RegistrationDriftModel> custom({
    Expression<int>? id,
    Expression<DateTime>? dateTimeStarted,
    Expression<String>? appPublicKey,
    Expression<String>? appManagerOin,
    Expression<String>? appManagerPublicKey,
    Expression<String>? registrationToken,
    Expression<DateTime>? dateTimeCompleted,
    Expression<String>? givenName,
    Expression<bool>? expired,
    Expression<bool>? revoked,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (dateTimeStarted != null) 'date_time_started': dateTimeStarted,
      if (appPublicKey != null) 'app_public_key': appPublicKey,
      if (appManagerOin != null) 'app_manager_oin': appManagerOin,
      if (appManagerPublicKey != null)
        'app_manager_public_key': appManagerPublicKey,
      if (registrationToken != null) 'registration_token': registrationToken,
      if (dateTimeCompleted != null) 'date_time_completed': dateTimeCompleted,
      if (givenName != null) 'given_name': givenName,
      if (expired != null) 'expired': expired,
      if (revoked != null) 'revoked': revoked,
    });
  }

  RegistrationTableCompanion copyWith(
      {Value<int>? id,
      Value<DateTime>? dateTimeStarted,
      Value<String>? appPublicKey,
      Value<String>? appManagerOin,
      Value<String>? appManagerPublicKey,
      Value<String>? registrationToken,
      Value<DateTime?>? dateTimeCompleted,
      Value<String?>? givenName,
      Value<bool>? expired,
      Value<bool>? revoked}) {
    return RegistrationTableCompanion(
      id: id ?? this.id,
      dateTimeStarted: dateTimeStarted ?? this.dateTimeStarted,
      appPublicKey: appPublicKey ?? this.appPublicKey,
      appManagerOin: appManagerOin ?? this.appManagerOin,
      appManagerPublicKey: appManagerPublicKey ?? this.appManagerPublicKey,
      registrationToken: registrationToken ?? this.registrationToken,
      dateTimeCompleted: dateTimeCompleted ?? this.dateTimeCompleted,
      givenName: givenName ?? this.givenName,
      expired: expired ?? this.expired,
      revoked: revoked ?? this.revoked,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (dateTimeStarted.present) {
      map['date_time_started'] = Variable<DateTime>(dateTimeStarted.value);
    }
    if (appPublicKey.present) {
      map['app_public_key'] = Variable<String>(appPublicKey.value);
    }
    if (appManagerOin.present) {
      map['app_manager_oin'] = Variable<String>(appManagerOin.value);
    }
    if (appManagerPublicKey.present) {
      map['app_manager_public_key'] =
          Variable<String>(appManagerPublicKey.value);
    }
    if (registrationToken.present) {
      map['registration_token'] = Variable<String>(registrationToken.value);
    }
    if (dateTimeCompleted.present) {
      map['date_time_completed'] = Variable<DateTime>(dateTimeCompleted.value);
    }
    if (givenName.present) {
      map['given_name'] = Variable<String>(givenName.value);
    }
    if (expired.present) {
      map['expired'] = Variable<bool>(expired.value);
    }
    if (revoked.present) {
      map['revoked'] = Variable<bool>(revoked.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('RegistrationTableCompanion(')
          ..write('id: $id, ')
          ..write('dateTimeStarted: $dateTimeStarted, ')
          ..write('appPublicKey: $appPublicKey, ')
          ..write('appManagerOin: $appManagerOin, ')
          ..write('appManagerPublicKey: $appManagerPublicKey, ')
          ..write('registrationToken: $registrationToken, ')
          ..write('dateTimeCompleted: $dateTimeCompleted, ')
          ..write('givenName: $givenName, ')
          ..write('expired: $expired, ')
          ..write('revoked: $revoked')
          ..write(')'))
        .toString();
  }
}

class $CertificateTableTable extends CertificateTable
    with TableInfo<$CertificateTableTable, CertificateDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $CertificateTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _typeMeta = const VerificationMeta('type');
  @override
  late final GeneratedColumn<String> type = GeneratedColumn<String>(
      'type', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _valueMeta = const VerificationMeta('value');
  @override
  late final GeneratedColumn<Uint8List> value = GeneratedColumn<Uint8List>(
      'value', aliasedName, false,
      type: DriftSqlType.blob, requiredDuringInsert: true);
  static const VerificationMeta _bsnMeta = const VerificationMeta('bsn');
  @override
  late final GeneratedColumn<String> bsn = GeneratedColumn<String>(
      'bsn', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _givenNameMeta =
      const VerificationMeta('givenName');
  @override
  late final GeneratedColumn<String> givenName = GeneratedColumn<String>(
      'given_name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _expiresAtMeta =
      const VerificationMeta('expiresAt');
  @override
  late final GeneratedColumn<DateTime> expiresAt = GeneratedColumn<DateTime>(
      'expires_at', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _deemedExpiredBySourceOrganizationMeta =
      const VerificationMeta('deemedExpiredBySourceOrganization');
  @override
  late final GeneratedColumn<bool> deemedExpiredBySourceOrganization =
      GeneratedColumn<bool>(
          'deemed_expired_by_source_organization', aliasedName, false,
          type: DriftSqlType.bool,
          requiredDuringInsert: true,
          defaultConstraints: GeneratedColumn.constraintIsAlways(
              'CHECK ("deemed_expired_by_source_organization" IN (0, 1))'));
  static const VerificationMeta _scopeMeta = const VerificationMeta('scope');
  @override
  late final GeneratedColumn<String> scope = GeneratedColumn<String>(
      'scope', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [
        id,
        type,
        value,
        bsn,
        givenName,
        expiresAt,
        deemedExpiredBySourceOrganization,
        scope
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'certificate_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<CertificateDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('type')) {
      context.handle(
          _typeMeta, type.isAcceptableOrUnknown(data['type']!, _typeMeta));
    } else if (isInserting) {
      context.missing(_typeMeta);
    }
    if (data.containsKey('value')) {
      context.handle(
          _valueMeta, value.isAcceptableOrUnknown(data['value']!, _valueMeta));
    } else if (isInserting) {
      context.missing(_valueMeta);
    }
    if (data.containsKey('bsn')) {
      context.handle(
          _bsnMeta, bsn.isAcceptableOrUnknown(data['bsn']!, _bsnMeta));
    } else if (isInserting) {
      context.missing(_bsnMeta);
    }
    if (data.containsKey('given_name')) {
      context.handle(_givenNameMeta,
          givenName.isAcceptableOrUnknown(data['given_name']!, _givenNameMeta));
    } else if (isInserting) {
      context.missing(_givenNameMeta);
    }
    if (data.containsKey('expires_at')) {
      context.handle(_expiresAtMeta,
          expiresAt.isAcceptableOrUnknown(data['expires_at']!, _expiresAtMeta));
    } else if (isInserting) {
      context.missing(_expiresAtMeta);
    }
    if (data.containsKey('deemed_expired_by_source_organization')) {
      context.handle(
          _deemedExpiredBySourceOrganizationMeta,
          deemedExpiredBySourceOrganization.isAcceptableOrUnknown(
              data['deemed_expired_by_source_organization']!,
              _deemedExpiredBySourceOrganizationMeta));
    } else if (isInserting) {
      context.missing(_deemedExpiredBySourceOrganizationMeta);
    }
    if (data.containsKey('scope')) {
      context.handle(
          _scopeMeta, scope.isAcceptableOrUnknown(data['scope']!, _scopeMeta));
    } else if (isInserting) {
      context.missing(_scopeMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  CertificateDriftModel map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return CertificateDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      type: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}type'])!,
      value: attachedDatabase.typeMapping
          .read(DriftSqlType.blob, data['${effectivePrefix}value'])!,
      bsn: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}bsn'])!,
      givenName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}given_name'])!,
      expiresAt: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}expires_at'])!,
      deemedExpiredBySourceOrganization: attachedDatabase.typeMapping.read(
          DriftSqlType.bool,
          data['${effectivePrefix}deemed_expired_by_source_organization'])!,
      scope: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}scope'])!,
    );
  }

  @override
  $CertificateTableTable createAlias(String alias) {
    return $CertificateTableTable(attachedDatabase, alias);
  }
}

class CertificateDriftModel extends DataClass
    implements Insertable<CertificateDriftModel> {
  final int id;
  final String type;
  final Uint8List value;
  final String bsn;
  final String givenName;
  final DateTime expiresAt;
  final bool deemedExpiredBySourceOrganization;
  final String scope;
  const CertificateDriftModel(
      {required this.id,
      required this.type,
      required this.value,
      required this.bsn,
      required this.givenName,
      required this.expiresAt,
      required this.deemedExpiredBySourceOrganization,
      required this.scope});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['type'] = Variable<String>(type);
    map['value'] = Variable<Uint8List>(value);
    map['bsn'] = Variable<String>(bsn);
    map['given_name'] = Variable<String>(givenName);
    map['expires_at'] = Variable<DateTime>(expiresAt);
    map['deemed_expired_by_source_organization'] =
        Variable<bool>(deemedExpiredBySourceOrganization);
    map['scope'] = Variable<String>(scope);
    return map;
  }

  CertificateTableCompanion toCompanion(bool nullToAbsent) {
    return CertificateTableCompanion(
      id: Value(id),
      type: Value(type),
      value: Value(value),
      bsn: Value(bsn),
      givenName: Value(givenName),
      expiresAt: Value(expiresAt),
      deemedExpiredBySourceOrganization:
          Value(deemedExpiredBySourceOrganization),
      scope: Value(scope),
    );
  }

  factory CertificateDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return CertificateDriftModel(
      id: serializer.fromJson<int>(json['id']),
      type: serializer.fromJson<String>(json['type']),
      value: serializer.fromJson<Uint8List>(json['value']),
      bsn: serializer.fromJson<String>(json['bsn']),
      givenName: serializer.fromJson<String>(json['givenName']),
      expiresAt: serializer.fromJson<DateTime>(json['expiresAt']),
      deemedExpiredBySourceOrganization:
          serializer.fromJson<bool>(json['deemedExpiredBySourceOrganization']),
      scope: serializer.fromJson<String>(json['scope']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'type': serializer.toJson<String>(type),
      'value': serializer.toJson<Uint8List>(value),
      'bsn': serializer.toJson<String>(bsn),
      'givenName': serializer.toJson<String>(givenName),
      'expiresAt': serializer.toJson<DateTime>(expiresAt),
      'deemedExpiredBySourceOrganization':
          serializer.toJson<bool>(deemedExpiredBySourceOrganization),
      'scope': serializer.toJson<String>(scope),
    };
  }

  CertificateDriftModel copyWith(
          {int? id,
          String? type,
          Uint8List? value,
          String? bsn,
          String? givenName,
          DateTime? expiresAt,
          bool? deemedExpiredBySourceOrganization,
          String? scope}) =>
      CertificateDriftModel(
        id: id ?? this.id,
        type: type ?? this.type,
        value: value ?? this.value,
        bsn: bsn ?? this.bsn,
        givenName: givenName ?? this.givenName,
        expiresAt: expiresAt ?? this.expiresAt,
        deemedExpiredBySourceOrganization: deemedExpiredBySourceOrganization ??
            this.deemedExpiredBySourceOrganization,
        scope: scope ?? this.scope,
      );
  CertificateDriftModel copyWithCompanion(CertificateTableCompanion data) {
    return CertificateDriftModel(
      id: data.id.present ? data.id.value : this.id,
      type: data.type.present ? data.type.value : this.type,
      value: data.value.present ? data.value.value : this.value,
      bsn: data.bsn.present ? data.bsn.value : this.bsn,
      givenName: data.givenName.present ? data.givenName.value : this.givenName,
      expiresAt: data.expiresAt.present ? data.expiresAt.value : this.expiresAt,
      deemedExpiredBySourceOrganization:
          data.deemedExpiredBySourceOrganization.present
              ? data.deemedExpiredBySourceOrganization.value
              : this.deemedExpiredBySourceOrganization,
      scope: data.scope.present ? data.scope.value : this.scope,
    );
  }

  @override
  String toString() {
    return (StringBuffer('CertificateDriftModel(')
          ..write('id: $id, ')
          ..write('type: $type, ')
          ..write('value: $value, ')
          ..write('bsn: $bsn, ')
          ..write('givenName: $givenName, ')
          ..write('expiresAt: $expiresAt, ')
          ..write(
              'deemedExpiredBySourceOrganization: $deemedExpiredBySourceOrganization, ')
          ..write('scope: $scope')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, type, $driftBlobEquality.hash(value), bsn,
      givenName, expiresAt, deemedExpiredBySourceOrganization, scope);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CertificateDriftModel &&
          other.id == this.id &&
          other.type == this.type &&
          $driftBlobEquality.equals(other.value, this.value) &&
          other.bsn == this.bsn &&
          other.givenName == this.givenName &&
          other.expiresAt == this.expiresAt &&
          other.deemedExpiredBySourceOrganization ==
              this.deemedExpiredBySourceOrganization &&
          other.scope == this.scope);
}

class CertificateTableCompanion extends UpdateCompanion<CertificateDriftModel> {
  final Value<int> id;
  final Value<String> type;
  final Value<Uint8List> value;
  final Value<String> bsn;
  final Value<String> givenName;
  final Value<DateTime> expiresAt;
  final Value<bool> deemedExpiredBySourceOrganization;
  final Value<String> scope;
  const CertificateTableCompanion({
    this.id = const Value.absent(),
    this.type = const Value.absent(),
    this.value = const Value.absent(),
    this.bsn = const Value.absent(),
    this.givenName = const Value.absent(),
    this.expiresAt = const Value.absent(),
    this.deemedExpiredBySourceOrganization = const Value.absent(),
    this.scope = const Value.absent(),
  });
  CertificateTableCompanion.insert({
    this.id = const Value.absent(),
    required String type,
    required Uint8List value,
    required String bsn,
    required String givenName,
    required DateTime expiresAt,
    required bool deemedExpiredBySourceOrganization,
    required String scope,
  })  : type = Value(type),
        value = Value(value),
        bsn = Value(bsn),
        givenName = Value(givenName),
        expiresAt = Value(expiresAt),
        deemedExpiredBySourceOrganization =
            Value(deemedExpiredBySourceOrganization),
        scope = Value(scope);
  static Insertable<CertificateDriftModel> custom({
    Expression<int>? id,
    Expression<String>? type,
    Expression<Uint8List>? value,
    Expression<String>? bsn,
    Expression<String>? givenName,
    Expression<DateTime>? expiresAt,
    Expression<bool>? deemedExpiredBySourceOrganization,
    Expression<String>? scope,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (type != null) 'type': type,
      if (value != null) 'value': value,
      if (bsn != null) 'bsn': bsn,
      if (givenName != null) 'given_name': givenName,
      if (expiresAt != null) 'expires_at': expiresAt,
      if (deemedExpiredBySourceOrganization != null)
        'deemed_expired_by_source_organization':
            deemedExpiredBySourceOrganization,
      if (scope != null) 'scope': scope,
    });
  }

  CertificateTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? type,
      Value<Uint8List>? value,
      Value<String>? bsn,
      Value<String>? givenName,
      Value<DateTime>? expiresAt,
      Value<bool>? deemedExpiredBySourceOrganization,
      Value<String>? scope}) {
    return CertificateTableCompanion(
      id: id ?? this.id,
      type: type ?? this.type,
      value: value ?? this.value,
      bsn: bsn ?? this.bsn,
      givenName: givenName ?? this.givenName,
      expiresAt: expiresAt ?? this.expiresAt,
      deemedExpiredBySourceOrganization: deemedExpiredBySourceOrganization ??
          this.deemedExpiredBySourceOrganization,
      scope: scope ?? this.scope,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (type.present) {
      map['type'] = Variable<String>(type.value);
    }
    if (value.present) {
      map['value'] = Variable<Uint8List>(value.value);
    }
    if (bsn.present) {
      map['bsn'] = Variable<String>(bsn.value);
    }
    if (givenName.present) {
      map['given_name'] = Variable<String>(givenName.value);
    }
    if (expiresAt.present) {
      map['expires_at'] = Variable<DateTime>(expiresAt.value);
    }
    if (deemedExpiredBySourceOrganization.present) {
      map['deemed_expired_by_source_organization'] =
          Variable<bool>(deemedExpiredBySourceOrganization.value);
    }
    if (scope.present) {
      map['scope'] = Variable<String>(scope.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('CertificateTableCompanion(')
          ..write('id: $id, ')
          ..write('type: $type, ')
          ..write('value: $value, ')
          ..write('bsn: $bsn, ')
          ..write('givenName: $givenName, ')
          ..write('expiresAt: $expiresAt, ')
          ..write(
              'deemedExpiredBySourceOrganization: $deemedExpiredBySourceOrganization, ')
          ..write('scope: $scope')
          ..write(')'))
        .toString();
  }
}

class $OrganizationSelectionTableTable extends OrganizationSelectionTable
    with
        TableInfo<$OrganizationSelectionTableTable,
            OrganizationSelectionDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $OrganizationSelectionTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _selectedMeta =
      const VerificationMeta('selected');
  @override
  late final GeneratedColumn<bool> selected = GeneratedColumn<bool>(
      'selected', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("selected" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [id, oin, selected];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'organization_selection_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<OrganizationSelectionDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    if (data.containsKey('selected')) {
      context.handle(_selectedMeta,
          selected.isAcceptableOrUnknown(data['selected']!, _selectedMeta));
    } else if (isInserting) {
      context.missing(_selectedMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  OrganizationSelectionDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return OrganizationSelectionDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
      selected: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}selected'])!,
    );
  }

  @override
  $OrganizationSelectionTableTable createAlias(String alias) {
    return $OrganizationSelectionTableTable(attachedDatabase, alias);
  }
}

class OrganizationSelectionDriftModel extends DataClass
    implements Insertable<OrganizationSelectionDriftModel> {
  final int id;
  final String oin;
  final bool selected;
  const OrganizationSelectionDriftModel(
      {required this.id, required this.oin, required this.selected});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    map['selected'] = Variable<bool>(selected);
    return map;
  }

  OrganizationSelectionTableCompanion toCompanion(bool nullToAbsent) {
    return OrganizationSelectionTableCompanion(
      id: Value(id),
      oin: Value(oin),
      selected: Value(selected),
    );
  }

  factory OrganizationSelectionDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return OrganizationSelectionDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
      selected: serializer.fromJson<bool>(json['selected']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
      'selected': serializer.toJson<bool>(selected),
    };
  }

  OrganizationSelectionDriftModel copyWith(
          {int? id, String? oin, bool? selected}) =>
      OrganizationSelectionDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
        selected: selected ?? this.selected,
      );
  OrganizationSelectionDriftModel copyWithCompanion(
      OrganizationSelectionTableCompanion data) {
    return OrganizationSelectionDriftModel(
      id: data.id.present ? data.id.value : this.id,
      oin: data.oin.present ? data.oin.value : this.oin,
      selected: data.selected.present ? data.selected.value : this.selected,
    );
  }

  @override
  String toString() {
    return (StringBuffer('OrganizationSelectionDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('selected: $selected')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, oin, selected);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is OrganizationSelectionDriftModel &&
          other.id == this.id &&
          other.oin == this.oin &&
          other.selected == this.selected);
}

class OrganizationSelectionTableCompanion
    extends UpdateCompanion<OrganizationSelectionDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  final Value<bool> selected;
  const OrganizationSelectionTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
    this.selected = const Value.absent(),
  });
  OrganizationSelectionTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
    required bool selected,
  })  : oin = Value(oin),
        selected = Value(selected);
  static Insertable<OrganizationSelectionDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
    Expression<bool>? selected,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
      if (selected != null) 'selected': selected,
    });
  }

  OrganizationSelectionTableCompanion copyWith(
      {Value<int>? id, Value<String>? oin, Value<bool>? selected}) {
    return OrganizationSelectionTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
      selected: selected ?? this.selected,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    if (selected.present) {
      map['selected'] = Variable<bool>(selected.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('OrganizationSelectionTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('selected: $selected')
          ..write(')'))
        .toString();
  }
}

class $AppManagerSelectionTableTable extends AppManagerSelectionTable
    with
        TableInfo<$AppManagerSelectionTableTable,
            AppManagerSelectionDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $AppManagerSelectionTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [id, oin];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'app_manager_selection_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<AppManagerSelectionDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  AppManagerSelectionDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return AppManagerSelectionDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
    );
  }

  @override
  $AppManagerSelectionTableTable createAlias(String alias) {
    return $AppManagerSelectionTableTable(attachedDatabase, alias);
  }
}

class AppManagerSelectionDriftModel extends DataClass
    implements Insertable<AppManagerSelectionDriftModel> {
  final int id;
  final String oin;
  const AppManagerSelectionDriftModel({required this.id, required this.oin});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    return map;
  }

  AppManagerSelectionTableCompanion toCompanion(bool nullToAbsent) {
    return AppManagerSelectionTableCompanion(
      id: Value(id),
      oin: Value(oin),
    );
  }

  factory AppManagerSelectionDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return AppManagerSelectionDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
    };
  }

  AppManagerSelectionDriftModel copyWith({int? id, String? oin}) =>
      AppManagerSelectionDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
      );
  AppManagerSelectionDriftModel copyWithCompanion(
      AppManagerSelectionTableCompanion data) {
    return AppManagerSelectionDriftModel(
      id: data.id.present ? data.id.value : this.id,
      oin: data.oin.present ? data.oin.value : this.oin,
    );
  }

  @override
  String toString() {
    return (StringBuffer('AppManagerSelectionDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, oin);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is AppManagerSelectionDriftModel &&
          other.id == this.id &&
          other.oin == this.oin);
}

class AppManagerSelectionTableCompanion
    extends UpdateCompanion<AppManagerSelectionDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  const AppManagerSelectionTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
  });
  AppManagerSelectionTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
  }) : oin = Value(oin);
  static Insertable<AppManagerSelectionDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
    });
  }

  AppManagerSelectionTableCompanion copyWith(
      {Value<int>? id, Value<String>? oin}) {
    return AppManagerSelectionTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('AppManagerSelectionTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin')
          ..write(')'))
        .toString();
  }
}

class $SchemeAppManagerTableTable extends SchemeAppManagerTable
    with TableInfo<$SchemeAppManagerTableTable, SchemeAppManagerDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $SchemeAppManagerTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _publicKeyMeta =
      const VerificationMeta('publicKey');
  @override
  late final GeneratedColumn<String> publicKey = GeneratedColumn<String>(
      'public_key', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _discoveryUrlMeta =
      const VerificationMeta('discoveryUrl');
  @override
  late final GeneratedColumn<String> discoveryUrl = GeneratedColumn<String>(
      'discovery_url', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _availableMeta =
      const VerificationMeta('available');
  @override
  late final GeneratedColumn<bool> available = GeneratedColumn<bool>(
      'available', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("available" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns =>
      [id, oin, name, publicKey, discoveryUrl, available];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'scheme_app_manager_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<SchemeAppManagerDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('public_key')) {
      context.handle(_publicKeyMeta,
          publicKey.isAcceptableOrUnknown(data['public_key']!, _publicKeyMeta));
    } else if (isInserting) {
      context.missing(_publicKeyMeta);
    }
    if (data.containsKey('discovery_url')) {
      context.handle(
          _discoveryUrlMeta,
          discoveryUrl.isAcceptableOrUnknown(
              data['discovery_url']!, _discoveryUrlMeta));
    } else if (isInserting) {
      context.missing(_discoveryUrlMeta);
    }
    if (data.containsKey('available')) {
      context.handle(_availableMeta,
          available.isAcceptableOrUnknown(data['available']!, _availableMeta));
    } else if (isInserting) {
      context.missing(_availableMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  SchemeAppManagerDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return SchemeAppManagerDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      publicKey: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}public_key'])!,
      discoveryUrl: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}discovery_url'])!,
      available: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}available'])!,
    );
  }

  @override
  $SchemeAppManagerTableTable createAlias(String alias) {
    return $SchemeAppManagerTableTable(attachedDatabase, alias);
  }
}

class SchemeAppManagerDriftModel extends DataClass
    implements Insertable<SchemeAppManagerDriftModel> {
  final int id;
  final String oin;
  final String name;
  final String publicKey;
  final String discoveryUrl;
  final bool available;
  const SchemeAppManagerDriftModel(
      {required this.id,
      required this.oin,
      required this.name,
      required this.publicKey,
      required this.discoveryUrl,
      required this.available});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    map['name'] = Variable<String>(name);
    map['public_key'] = Variable<String>(publicKey);
    map['discovery_url'] = Variable<String>(discoveryUrl);
    map['available'] = Variable<bool>(available);
    return map;
  }

  SchemeAppManagerTableCompanion toCompanion(bool nullToAbsent) {
    return SchemeAppManagerTableCompanion(
      id: Value(id),
      oin: Value(oin),
      name: Value(name),
      publicKey: Value(publicKey),
      discoveryUrl: Value(discoveryUrl),
      available: Value(available),
    );
  }

  factory SchemeAppManagerDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return SchemeAppManagerDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
      name: serializer.fromJson<String>(json['name']),
      publicKey: serializer.fromJson<String>(json['publicKey']),
      discoveryUrl: serializer.fromJson<String>(json['discoveryUrl']),
      available: serializer.fromJson<bool>(json['available']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
      'name': serializer.toJson<String>(name),
      'publicKey': serializer.toJson<String>(publicKey),
      'discoveryUrl': serializer.toJson<String>(discoveryUrl),
      'available': serializer.toJson<bool>(available),
    };
  }

  SchemeAppManagerDriftModel copyWith(
          {int? id,
          String? oin,
          String? name,
          String? publicKey,
          String? discoveryUrl,
          bool? available}) =>
      SchemeAppManagerDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
        name: name ?? this.name,
        publicKey: publicKey ?? this.publicKey,
        discoveryUrl: discoveryUrl ?? this.discoveryUrl,
        available: available ?? this.available,
      );
  SchemeAppManagerDriftModel copyWithCompanion(
      SchemeAppManagerTableCompanion data) {
    return SchemeAppManagerDriftModel(
      id: data.id.present ? data.id.value : this.id,
      oin: data.oin.present ? data.oin.value : this.oin,
      name: data.name.present ? data.name.value : this.name,
      publicKey: data.publicKey.present ? data.publicKey.value : this.publicKey,
      discoveryUrl: data.discoveryUrl.present
          ? data.discoveryUrl.value
          : this.discoveryUrl,
      available: data.available.present ? data.available.value : this.available,
    );
  }

  @override
  String toString() {
    return (StringBuffer('SchemeAppManagerDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('name: $name, ')
          ..write('publicKey: $publicKey, ')
          ..write('discoveryUrl: $discoveryUrl, ')
          ..write('available: $available')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(id, oin, name, publicKey, discoveryUrl, available);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is SchemeAppManagerDriftModel &&
          other.id == this.id &&
          other.oin == this.oin &&
          other.name == this.name &&
          other.publicKey == this.publicKey &&
          other.discoveryUrl == this.discoveryUrl &&
          other.available == this.available);
}

class SchemeAppManagerTableCompanion
    extends UpdateCompanion<SchemeAppManagerDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  final Value<String> name;
  final Value<String> publicKey;
  final Value<String> discoveryUrl;
  final Value<bool> available;
  const SchemeAppManagerTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
    this.name = const Value.absent(),
    this.publicKey = const Value.absent(),
    this.discoveryUrl = const Value.absent(),
    this.available = const Value.absent(),
  });
  SchemeAppManagerTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
    required String name,
    required String publicKey,
    required String discoveryUrl,
    required bool available,
  })  : oin = Value(oin),
        name = Value(name),
        publicKey = Value(publicKey),
        discoveryUrl = Value(discoveryUrl),
        available = Value(available);
  static Insertable<SchemeAppManagerDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
    Expression<String>? name,
    Expression<String>? publicKey,
    Expression<String>? discoveryUrl,
    Expression<bool>? available,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
      if (name != null) 'name': name,
      if (publicKey != null) 'public_key': publicKey,
      if (discoveryUrl != null) 'discovery_url': discoveryUrl,
      if (available != null) 'available': available,
    });
  }

  SchemeAppManagerTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? oin,
      Value<String>? name,
      Value<String>? publicKey,
      Value<String>? discoveryUrl,
      Value<bool>? available}) {
    return SchemeAppManagerTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
      name: name ?? this.name,
      publicKey: publicKey ?? this.publicKey,
      discoveryUrl: discoveryUrl ?? this.discoveryUrl,
      available: available ?? this.available,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (publicKey.present) {
      map['public_key'] = Variable<String>(publicKey.value);
    }
    if (discoveryUrl.present) {
      map['discovery_url'] = Variable<String>(discoveryUrl.value);
    }
    if (available.present) {
      map['available'] = Variable<bool>(available.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('SchemeAppManagerTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('name: $name, ')
          ..write('publicKey: $publicKey, ')
          ..write('discoveryUrl: $discoveryUrl, ')
          ..write('available: $available')
          ..write(')'))
        .toString();
  }
}

class $SchemeDocumentTypeTableTable extends SchemeDocumentTypeTable
    with
        TableInfo<$SchemeDocumentTypeTableTable, SchemeDocumentTypeDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $SchemeDocumentTypeTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _availableMeta =
      const VerificationMeta('available');
  @override
  late final GeneratedColumn<bool> available = GeneratedColumn<bool>(
      'available', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("available" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [id, name, available];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'scheme_document_type_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<SchemeDocumentTypeDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('available')) {
      context.handle(_availableMeta,
          available.isAcceptableOrUnknown(data['available']!, _availableMeta));
    } else if (isInserting) {
      context.missing(_availableMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  SchemeDocumentTypeDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return SchemeDocumentTypeDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      available: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}available'])!,
    );
  }

  @override
  $SchemeDocumentTypeTableTable createAlias(String alias) {
    return $SchemeDocumentTypeTableTable(attachedDatabase, alias);
  }
}

class SchemeDocumentTypeDriftModel extends DataClass
    implements Insertable<SchemeDocumentTypeDriftModel> {
  final int id;
  final String name;
  final bool available;
  const SchemeDocumentTypeDriftModel(
      {required this.id, required this.name, required this.available});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['name'] = Variable<String>(name);
    map['available'] = Variable<bool>(available);
    return map;
  }

  SchemeDocumentTypeTableCompanion toCompanion(bool nullToAbsent) {
    return SchemeDocumentTypeTableCompanion(
      id: Value(id),
      name: Value(name),
      available: Value(available),
    );
  }

  factory SchemeDocumentTypeDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return SchemeDocumentTypeDriftModel(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      available: serializer.fromJson<bool>(json['available']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'available': serializer.toJson<bool>(available),
    };
  }

  SchemeDocumentTypeDriftModel copyWith(
          {int? id, String? name, bool? available}) =>
      SchemeDocumentTypeDriftModel(
        id: id ?? this.id,
        name: name ?? this.name,
        available: available ?? this.available,
      );
  SchemeDocumentTypeDriftModel copyWithCompanion(
      SchemeDocumentTypeTableCompanion data) {
    return SchemeDocumentTypeDriftModel(
      id: data.id.present ? data.id.value : this.id,
      name: data.name.present ? data.name.value : this.name,
      available: data.available.present ? data.available.value : this.available,
    );
  }

  @override
  String toString() {
    return (StringBuffer('SchemeDocumentTypeDriftModel(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('available: $available')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, name, available);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is SchemeDocumentTypeDriftModel &&
          other.id == this.id &&
          other.name == this.name &&
          other.available == this.available);
}

class SchemeDocumentTypeTableCompanion
    extends UpdateCompanion<SchemeDocumentTypeDriftModel> {
  final Value<int> id;
  final Value<String> name;
  final Value<bool> available;
  const SchemeDocumentTypeTableCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.available = const Value.absent(),
  });
  SchemeDocumentTypeTableCompanion.insert({
    this.id = const Value.absent(),
    required String name,
    required bool available,
  })  : name = Value(name),
        available = Value(available);
  static Insertable<SchemeDocumentTypeDriftModel> custom({
    Expression<int>? id,
    Expression<String>? name,
    Expression<bool>? available,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (available != null) 'available': available,
    });
  }

  SchemeDocumentTypeTableCompanion copyWith(
      {Value<int>? id, Value<String>? name, Value<bool>? available}) {
    return SchemeDocumentTypeTableCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      available: available ?? this.available,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (available.present) {
      map['available'] = Variable<bool>(available.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('SchemeDocumentTypeTableCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('available: $available')
          ..write(')'))
        .toString();
  }
}

class $SchemeOrganizationTableTable extends SchemeOrganizationTable
    with
        TableInfo<$SchemeOrganizationTableTable, SchemeOrganizationDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $SchemeOrganizationTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _publicKeyMeta =
      const VerificationMeta('publicKey');
  @override
  late final GeneratedColumn<String> publicKey = GeneratedColumn<String>(
      'public_key', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _discoveryUrlMeta =
      const VerificationMeta('discoveryUrl');
  @override
  late final GeneratedColumn<String> discoveryUrl = GeneratedColumn<String>(
      'discovery_url', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _availableMeta =
      const VerificationMeta('available');
  @override
  late final GeneratedColumn<bool> available = GeneratedColumn<bool>(
      'available', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("available" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns =>
      [id, oin, name, publicKey, discoveryUrl, available];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'scheme_organization_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<SchemeOrganizationDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('public_key')) {
      context.handle(_publicKeyMeta,
          publicKey.isAcceptableOrUnknown(data['public_key']!, _publicKeyMeta));
    } else if (isInserting) {
      context.missing(_publicKeyMeta);
    }
    if (data.containsKey('discovery_url')) {
      context.handle(
          _discoveryUrlMeta,
          discoveryUrl.isAcceptableOrUnknown(
              data['discovery_url']!, _discoveryUrlMeta));
    } else if (isInserting) {
      context.missing(_discoveryUrlMeta);
    }
    if (data.containsKey('available')) {
      context.handle(_availableMeta,
          available.isAcceptableOrUnknown(data['available']!, _availableMeta));
    } else if (isInserting) {
      context.missing(_availableMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  SchemeOrganizationDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return SchemeOrganizationDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      publicKey: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}public_key'])!,
      discoveryUrl: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}discovery_url'])!,
      available: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}available'])!,
    );
  }

  @override
  $SchemeOrganizationTableTable createAlias(String alias) {
    return $SchemeOrganizationTableTable(attachedDatabase, alias);
  }
}

class SchemeOrganizationDriftModel extends DataClass
    implements Insertable<SchemeOrganizationDriftModel> {
  final int id;
  final String oin;
  final String name;
  final String publicKey;
  final String discoveryUrl;
  final bool available;
  const SchemeOrganizationDriftModel(
      {required this.id,
      required this.oin,
      required this.name,
      required this.publicKey,
      required this.discoveryUrl,
      required this.available});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    map['name'] = Variable<String>(name);
    map['public_key'] = Variable<String>(publicKey);
    map['discovery_url'] = Variable<String>(discoveryUrl);
    map['available'] = Variable<bool>(available);
    return map;
  }

  SchemeOrganizationTableCompanion toCompanion(bool nullToAbsent) {
    return SchemeOrganizationTableCompanion(
      id: Value(id),
      oin: Value(oin),
      name: Value(name),
      publicKey: Value(publicKey),
      discoveryUrl: Value(discoveryUrl),
      available: Value(available),
    );
  }

  factory SchemeOrganizationDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return SchemeOrganizationDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
      name: serializer.fromJson<String>(json['name']),
      publicKey: serializer.fromJson<String>(json['publicKey']),
      discoveryUrl: serializer.fromJson<String>(json['discoveryUrl']),
      available: serializer.fromJson<bool>(json['available']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
      'name': serializer.toJson<String>(name),
      'publicKey': serializer.toJson<String>(publicKey),
      'discoveryUrl': serializer.toJson<String>(discoveryUrl),
      'available': serializer.toJson<bool>(available),
    };
  }

  SchemeOrganizationDriftModel copyWith(
          {int? id,
          String? oin,
          String? name,
          String? publicKey,
          String? discoveryUrl,
          bool? available}) =>
      SchemeOrganizationDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
        name: name ?? this.name,
        publicKey: publicKey ?? this.publicKey,
        discoveryUrl: discoveryUrl ?? this.discoveryUrl,
        available: available ?? this.available,
      );
  SchemeOrganizationDriftModel copyWithCompanion(
      SchemeOrganizationTableCompanion data) {
    return SchemeOrganizationDriftModel(
      id: data.id.present ? data.id.value : this.id,
      oin: data.oin.present ? data.oin.value : this.oin,
      name: data.name.present ? data.name.value : this.name,
      publicKey: data.publicKey.present ? data.publicKey.value : this.publicKey,
      discoveryUrl: data.discoveryUrl.present
          ? data.discoveryUrl.value
          : this.discoveryUrl,
      available: data.available.present ? data.available.value : this.available,
    );
  }

  @override
  String toString() {
    return (StringBuffer('SchemeOrganizationDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('name: $name, ')
          ..write('publicKey: $publicKey, ')
          ..write('discoveryUrl: $discoveryUrl, ')
          ..write('available: $available')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(id, oin, name, publicKey, discoveryUrl, available);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is SchemeOrganizationDriftModel &&
          other.id == this.id &&
          other.oin == this.oin &&
          other.name == this.name &&
          other.publicKey == this.publicKey &&
          other.discoveryUrl == this.discoveryUrl &&
          other.available == this.available);
}

class SchemeOrganizationTableCompanion
    extends UpdateCompanion<SchemeOrganizationDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  final Value<String> name;
  final Value<String> publicKey;
  final Value<String> discoveryUrl;
  final Value<bool> available;
  const SchemeOrganizationTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
    this.name = const Value.absent(),
    this.publicKey = const Value.absent(),
    this.discoveryUrl = const Value.absent(),
    this.available = const Value.absent(),
  });
  SchemeOrganizationTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
    required String name,
    required String publicKey,
    required String discoveryUrl,
    required bool available,
  })  : oin = Value(oin),
        name = Value(name),
        publicKey = Value(publicKey),
        discoveryUrl = Value(discoveryUrl),
        available = Value(available);
  static Insertable<SchemeOrganizationDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
    Expression<String>? name,
    Expression<String>? publicKey,
    Expression<String>? discoveryUrl,
    Expression<bool>? available,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
      if (name != null) 'name': name,
      if (publicKey != null) 'public_key': publicKey,
      if (discoveryUrl != null) 'discovery_url': discoveryUrl,
      if (available != null) 'available': available,
    });
  }

  SchemeOrganizationTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? oin,
      Value<String>? name,
      Value<String>? publicKey,
      Value<String>? discoveryUrl,
      Value<bool>? available}) {
    return SchemeOrganizationTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
      name: name ?? this.name,
      publicKey: publicKey ?? this.publicKey,
      discoveryUrl: discoveryUrl ?? this.discoveryUrl,
      available: available ?? this.available,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (publicKey.present) {
      map['public_key'] = Variable<String>(publicKey.value);
    }
    if (discoveryUrl.present) {
      map['discovery_url'] = Variable<String>(discoveryUrl.value);
    }
    if (available.present) {
      map['available'] = Variable<bool>(available.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('SchemeOrganizationTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('name: $name, ')
          ..write('publicKey: $publicKey, ')
          ..write('discoveryUrl: $discoveryUrl, ')
          ..write('available: $available')
          ..write(')'))
        .toString();
  }
}

class $PaymentPlanGroupsTableTable extends PaymentPlanGroupsTable
    with TableInfo<$PaymentPlanGroupsTableTable, PaymentPlanGroupsModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $PaymentPlanGroupsTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _zaakKenmerkenMeta =
      const VerificationMeta('zaakKenmerken');
  @override
  late final GeneratedColumn<String> zaakKenmerken = GeneratedColumn<String>(
      'zaak_kenmerken', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _saldoMeta = const VerificationMeta('saldo');
  @override
  late final GeneratedColumn<int> saldo = GeneratedColumn<int>(
      'saldo', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _termijnenMeta =
      const VerificationMeta('termijnen');
  @override
  late final GeneratedColumn<int> termijnen = GeneratedColumn<int>(
      'termijnen', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _paymentRulesMeta =
      const VerificationMeta('paymentRules');
  @override
  late final GeneratedColumn<int> paymentRules = GeneratedColumn<int>(
      'payment_rules', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns =>
      [id, zaakKenmerken, saldo, termijnen, paymentRules];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'payment_plan_groups_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<PaymentPlanGroupsModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('zaak_kenmerken')) {
      context.handle(
          _zaakKenmerkenMeta,
          zaakKenmerken.isAcceptableOrUnknown(
              data['zaak_kenmerken']!, _zaakKenmerkenMeta));
    } else if (isInserting) {
      context.missing(_zaakKenmerkenMeta);
    }
    if (data.containsKey('saldo')) {
      context.handle(
          _saldoMeta, saldo.isAcceptableOrUnknown(data['saldo']!, _saldoMeta));
    } else if (isInserting) {
      context.missing(_saldoMeta);
    }
    if (data.containsKey('termijnen')) {
      context.handle(_termijnenMeta,
          termijnen.isAcceptableOrUnknown(data['termijnen']!, _termijnenMeta));
    } else if (isInserting) {
      context.missing(_termijnenMeta);
    }
    if (data.containsKey('payment_rules')) {
      context.handle(
          _paymentRulesMeta,
          paymentRules.isAcceptableOrUnknown(
              data['payment_rules']!, _paymentRulesMeta));
    } else if (isInserting) {
      context.missing(_paymentRulesMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  PaymentPlanGroupsModel map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return PaymentPlanGroupsModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      zaakKenmerken: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}zaak_kenmerken'])!,
      saldo: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}saldo'])!,
      termijnen: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}termijnen'])!,
      paymentRules: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}payment_rules'])!,
    );
  }

  @override
  $PaymentPlanGroupsTableTable createAlias(String alias) {
    return $PaymentPlanGroupsTableTable(attachedDatabase, alias);
  }
}

class PaymentPlanGroupsModel extends DataClass
    implements Insertable<PaymentPlanGroupsModel> {
  final int id;
  final String zaakKenmerken;
  final int saldo;
  final int termijnen;
  final int paymentRules;
  const PaymentPlanGroupsModel(
      {required this.id,
      required this.zaakKenmerken,
      required this.saldo,
      required this.termijnen,
      required this.paymentRules});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['zaak_kenmerken'] = Variable<String>(zaakKenmerken);
    map['saldo'] = Variable<int>(saldo);
    map['termijnen'] = Variable<int>(termijnen);
    map['payment_rules'] = Variable<int>(paymentRules);
    return map;
  }

  PaymentPlanGroupsTableCompanion toCompanion(bool nullToAbsent) {
    return PaymentPlanGroupsTableCompanion(
      id: Value(id),
      zaakKenmerken: Value(zaakKenmerken),
      saldo: Value(saldo),
      termijnen: Value(termijnen),
      paymentRules: Value(paymentRules),
    );
  }

  factory PaymentPlanGroupsModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return PaymentPlanGroupsModel(
      id: serializer.fromJson<int>(json['id']),
      zaakKenmerken: serializer.fromJson<String>(json['zaakKenmerken']),
      saldo: serializer.fromJson<int>(json['saldo']),
      termijnen: serializer.fromJson<int>(json['termijnen']),
      paymentRules: serializer.fromJson<int>(json['paymentRules']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'zaakKenmerken': serializer.toJson<String>(zaakKenmerken),
      'saldo': serializer.toJson<int>(saldo),
      'termijnen': serializer.toJson<int>(termijnen),
      'paymentRules': serializer.toJson<int>(paymentRules),
    };
  }

  PaymentPlanGroupsModel copyWith(
          {int? id,
          String? zaakKenmerken,
          int? saldo,
          int? termijnen,
          int? paymentRules}) =>
      PaymentPlanGroupsModel(
        id: id ?? this.id,
        zaakKenmerken: zaakKenmerken ?? this.zaakKenmerken,
        saldo: saldo ?? this.saldo,
        termijnen: termijnen ?? this.termijnen,
        paymentRules: paymentRules ?? this.paymentRules,
      );
  PaymentPlanGroupsModel copyWithCompanion(
      PaymentPlanGroupsTableCompanion data) {
    return PaymentPlanGroupsModel(
      id: data.id.present ? data.id.value : this.id,
      zaakKenmerken: data.zaakKenmerken.present
          ? data.zaakKenmerken.value
          : this.zaakKenmerken,
      saldo: data.saldo.present ? data.saldo.value : this.saldo,
      termijnen: data.termijnen.present ? data.termijnen.value : this.termijnen,
      paymentRules: data.paymentRules.present
          ? data.paymentRules.value
          : this.paymentRules,
    );
  }

  @override
  String toString() {
    return (StringBuffer('PaymentPlanGroupsModel(')
          ..write('id: $id, ')
          ..write('zaakKenmerken: $zaakKenmerken, ')
          ..write('saldo: $saldo, ')
          ..write('termijnen: $termijnen, ')
          ..write('paymentRules: $paymentRules')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(id, zaakKenmerken, saldo, termijnen, paymentRules);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is PaymentPlanGroupsModel &&
          other.id == this.id &&
          other.zaakKenmerken == this.zaakKenmerken &&
          other.saldo == this.saldo &&
          other.termijnen == this.termijnen &&
          other.paymentRules == this.paymentRules);
}

class PaymentPlanGroupsTableCompanion
    extends UpdateCompanion<PaymentPlanGroupsModel> {
  final Value<int> id;
  final Value<String> zaakKenmerken;
  final Value<int> saldo;
  final Value<int> termijnen;
  final Value<int> paymentRules;
  const PaymentPlanGroupsTableCompanion({
    this.id = const Value.absent(),
    this.zaakKenmerken = const Value.absent(),
    this.saldo = const Value.absent(),
    this.termijnen = const Value.absent(),
    this.paymentRules = const Value.absent(),
  });
  PaymentPlanGroupsTableCompanion.insert({
    this.id = const Value.absent(),
    required String zaakKenmerken,
    required int saldo,
    required int termijnen,
    required int paymentRules,
  })  : zaakKenmerken = Value(zaakKenmerken),
        saldo = Value(saldo),
        termijnen = Value(termijnen),
        paymentRules = Value(paymentRules);
  static Insertable<PaymentPlanGroupsModel> custom({
    Expression<int>? id,
    Expression<String>? zaakKenmerken,
    Expression<int>? saldo,
    Expression<int>? termijnen,
    Expression<int>? paymentRules,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (zaakKenmerken != null) 'zaak_kenmerken': zaakKenmerken,
      if (saldo != null) 'saldo': saldo,
      if (termijnen != null) 'termijnen': termijnen,
      if (paymentRules != null) 'payment_rules': paymentRules,
    });
  }

  PaymentPlanGroupsTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? zaakKenmerken,
      Value<int>? saldo,
      Value<int>? termijnen,
      Value<int>? paymentRules}) {
    return PaymentPlanGroupsTableCompanion(
      id: id ?? this.id,
      zaakKenmerken: zaakKenmerken ?? this.zaakKenmerken,
      saldo: saldo ?? this.saldo,
      termijnen: termijnen ?? this.termijnen,
      paymentRules: paymentRules ?? this.paymentRules,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (zaakKenmerken.present) {
      map['zaak_kenmerken'] = Variable<String>(zaakKenmerken.value);
    }
    if (saldo.present) {
      map['saldo'] = Variable<int>(saldo.value);
    }
    if (termijnen.present) {
      map['termijnen'] = Variable<int>(termijnen.value);
    }
    if (paymentRules.present) {
      map['payment_rules'] = Variable<int>(paymentRules.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('PaymentPlanGroupsTableCompanion(')
          ..write('id: $id, ')
          ..write('zaakKenmerken: $zaakKenmerken, ')
          ..write('saldo: $saldo, ')
          ..write('termijnen: $termijnen, ')
          ..write('paymentRules: $paymentRules')
          ..write(')'))
        .toString();
  }
}

class $PaymentPlanRulesStorageTableTable extends PaymentPlanRulesStorageTable
    with
        TableInfo<$PaymentPlanRulesStorageTableTable,
            PaymentPlanRulesStorageDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $PaymentPlanRulesStorageTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways('UNIQUE'));
  static const VerificationMeta _organisatiesMeta =
      const VerificationMeta('organisaties');
  @override
  late final GeneratedColumn<String> organisaties = GeneratedColumn<String>(
      'organisaties', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _minimumBedragMeta =
      const VerificationMeta('minimumBedrag');
  @override
  late final GeneratedColumn<int> minimumBedrag = GeneratedColumn<int>(
      'minimum_bedrag', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _maximumBedragMeta =
      const VerificationMeta('maximumBedrag');
  @override
  late final GeneratedColumn<int> maximumBedrag = GeneratedColumn<int>(
      'maximum_bedrag', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _typeMeta = const VerificationMeta('type');
  @override
  late final GeneratedColumn<String> type = GeneratedColumn<String>(
      'type', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns =>
      [id, organisaties, minimumBedrag, maximumBedrag, type];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'payment_plan_rules_storage_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<PaymentPlanRulesStorageDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('organisaties')) {
      context.handle(
          _organisatiesMeta,
          organisaties.isAcceptableOrUnknown(
              data['organisaties']!, _organisatiesMeta));
    } else if (isInserting) {
      context.missing(_organisatiesMeta);
    }
    if (data.containsKey('minimum_bedrag')) {
      context.handle(
          _minimumBedragMeta,
          minimumBedrag.isAcceptableOrUnknown(
              data['minimum_bedrag']!, _minimumBedragMeta));
    } else if (isInserting) {
      context.missing(_minimumBedragMeta);
    }
    if (data.containsKey('maximum_bedrag')) {
      context.handle(
          _maximumBedragMeta,
          maximumBedrag.isAcceptableOrUnknown(
              data['maximum_bedrag']!, _maximumBedragMeta));
    } else if (isInserting) {
      context.missing(_maximumBedragMeta);
    }
    if (data.containsKey('type')) {
      context.handle(
          _typeMeta, type.isAcceptableOrUnknown(data['type']!, _typeMeta));
    } else if (isInserting) {
      context.missing(_typeMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => const {};
  @override
  PaymentPlanRulesStorageDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return PaymentPlanRulesStorageDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      organisaties: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}organisaties'])!,
      minimumBedrag: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}minimum_bedrag'])!,
      maximumBedrag: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}maximum_bedrag'])!,
      type: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}type'])!,
    );
  }

  @override
  $PaymentPlanRulesStorageTableTable createAlias(String alias) {
    return $PaymentPlanRulesStorageTableTable(attachedDatabase, alias);
  }
}

class PaymentPlanRulesStorageDriftModel extends DataClass
    implements Insertable<PaymentPlanRulesStorageDriftModel> {
  final int id;
  final String organisaties;
  final int minimumBedrag;
  final int maximumBedrag;
  final String type;
  const PaymentPlanRulesStorageDriftModel(
      {required this.id,
      required this.organisaties,
      required this.minimumBedrag,
      required this.maximumBedrag,
      required this.type});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['organisaties'] = Variable<String>(organisaties);
    map['minimum_bedrag'] = Variable<int>(minimumBedrag);
    map['maximum_bedrag'] = Variable<int>(maximumBedrag);
    map['type'] = Variable<String>(type);
    return map;
  }

  PaymentPlanRulesStorageTableCompanion toCompanion(bool nullToAbsent) {
    return PaymentPlanRulesStorageTableCompanion(
      id: Value(id),
      organisaties: Value(organisaties),
      minimumBedrag: Value(minimumBedrag),
      maximumBedrag: Value(maximumBedrag),
      type: Value(type),
    );
  }

  factory PaymentPlanRulesStorageDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return PaymentPlanRulesStorageDriftModel(
      id: serializer.fromJson<int>(json['id']),
      organisaties: serializer.fromJson<String>(json['organisaties']),
      minimumBedrag: serializer.fromJson<int>(json['minimumBedrag']),
      maximumBedrag: serializer.fromJson<int>(json['maximumBedrag']),
      type: serializer.fromJson<String>(json['type']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'organisaties': serializer.toJson<String>(organisaties),
      'minimumBedrag': serializer.toJson<int>(minimumBedrag),
      'maximumBedrag': serializer.toJson<int>(maximumBedrag),
      'type': serializer.toJson<String>(type),
    };
  }

  PaymentPlanRulesStorageDriftModel copyWith(
          {int? id,
          String? organisaties,
          int? minimumBedrag,
          int? maximumBedrag,
          String? type}) =>
      PaymentPlanRulesStorageDriftModel(
        id: id ?? this.id,
        organisaties: organisaties ?? this.organisaties,
        minimumBedrag: minimumBedrag ?? this.minimumBedrag,
        maximumBedrag: maximumBedrag ?? this.maximumBedrag,
        type: type ?? this.type,
      );
  PaymentPlanRulesStorageDriftModel copyWithCompanion(
      PaymentPlanRulesStorageTableCompanion data) {
    return PaymentPlanRulesStorageDriftModel(
      id: data.id.present ? data.id.value : this.id,
      organisaties: data.organisaties.present
          ? data.organisaties.value
          : this.organisaties,
      minimumBedrag: data.minimumBedrag.present
          ? data.minimumBedrag.value
          : this.minimumBedrag,
      maximumBedrag: data.maximumBedrag.present
          ? data.maximumBedrag.value
          : this.maximumBedrag,
      type: data.type.present ? data.type.value : this.type,
    );
  }

  @override
  String toString() {
    return (StringBuffer('PaymentPlanRulesStorageDriftModel(')
          ..write('id: $id, ')
          ..write('organisaties: $organisaties, ')
          ..write('minimumBedrag: $minimumBedrag, ')
          ..write('maximumBedrag: $maximumBedrag, ')
          ..write('type: $type')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(id, organisaties, minimumBedrag, maximumBedrag, type);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is PaymentPlanRulesStorageDriftModel &&
          other.id == this.id &&
          other.organisaties == this.organisaties &&
          other.minimumBedrag == this.minimumBedrag &&
          other.maximumBedrag == this.maximumBedrag &&
          other.type == this.type);
}

class PaymentPlanRulesStorageTableCompanion
    extends UpdateCompanion<PaymentPlanRulesStorageDriftModel> {
  final Value<int> id;
  final Value<String> organisaties;
  final Value<int> minimumBedrag;
  final Value<int> maximumBedrag;
  final Value<String> type;
  final Value<int> rowid;
  const PaymentPlanRulesStorageTableCompanion({
    this.id = const Value.absent(),
    this.organisaties = const Value.absent(),
    this.minimumBedrag = const Value.absent(),
    this.maximumBedrag = const Value.absent(),
    this.type = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  PaymentPlanRulesStorageTableCompanion.insert({
    required int id,
    required String organisaties,
    required int minimumBedrag,
    required int maximumBedrag,
    required String type,
    this.rowid = const Value.absent(),
  })  : id = Value(id),
        organisaties = Value(organisaties),
        minimumBedrag = Value(minimumBedrag),
        maximumBedrag = Value(maximumBedrag),
        type = Value(type);
  static Insertable<PaymentPlanRulesStorageDriftModel> custom({
    Expression<int>? id,
    Expression<String>? organisaties,
    Expression<int>? minimumBedrag,
    Expression<int>? maximumBedrag,
    Expression<String>? type,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (organisaties != null) 'organisaties': organisaties,
      if (minimumBedrag != null) 'minimum_bedrag': minimumBedrag,
      if (maximumBedrag != null) 'maximum_bedrag': maximumBedrag,
      if (type != null) 'type': type,
      if (rowid != null) 'rowid': rowid,
    });
  }

  PaymentPlanRulesStorageTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? organisaties,
      Value<int>? minimumBedrag,
      Value<int>? maximumBedrag,
      Value<String>? type,
      Value<int>? rowid}) {
    return PaymentPlanRulesStorageTableCompanion(
      id: id ?? this.id,
      organisaties: organisaties ?? this.organisaties,
      minimumBedrag: minimumBedrag ?? this.minimumBedrag,
      maximumBedrag: maximumBedrag ?? this.maximumBedrag,
      type: type ?? this.type,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (organisaties.present) {
      map['organisaties'] = Variable<String>(organisaties.value);
    }
    if (minimumBedrag.present) {
      map['minimum_bedrag'] = Variable<int>(minimumBedrag.value);
    }
    if (maximumBedrag.present) {
      map['maximum_bedrag'] = Variable<int>(maximumBedrag.value);
    }
    if (type.present) {
      map['type'] = Variable<String>(type.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('PaymentPlanRulesStorageTableCompanion(')
          ..write('id: $id, ')
          ..write('organisaties: $organisaties, ')
          ..write('minimumBedrag: $minimumBedrag, ')
          ..write('maximumBedrag: $maximumBedrag, ')
          ..write('type: $type, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $AppKeyPairTableTable extends AppKeyPairTable
    with TableInfo<$AppKeyPairTableTable, AppKeyPairDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $AppKeyPairTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _privateKeyMeta =
      const VerificationMeta('privateKey');
  @override
  late final GeneratedColumn<String> privateKey = GeneratedColumn<String>(
      'private_key', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _publicKeyMeta =
      const VerificationMeta('publicKey');
  @override
  late final GeneratedColumn<String> publicKey = GeneratedColumn<String>(
      'public_key', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [id, privateKey, publicKey];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'app_key_pair_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<AppKeyPairDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('private_key')) {
      context.handle(
          _privateKeyMeta,
          privateKey.isAcceptableOrUnknown(
              data['private_key']!, _privateKeyMeta));
    } else if (isInserting) {
      context.missing(_privateKeyMeta);
    }
    if (data.containsKey('public_key')) {
      context.handle(_publicKeyMeta,
          publicKey.isAcceptableOrUnknown(data['public_key']!, _publicKeyMeta));
    } else if (isInserting) {
      context.missing(_publicKeyMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  AppKeyPairDriftModel map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return AppKeyPairDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      privateKey: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}private_key'])!,
      publicKey: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}public_key'])!,
    );
  }

  @override
  $AppKeyPairTableTable createAlias(String alias) {
    return $AppKeyPairTableTable(attachedDatabase, alias);
  }
}

class AppKeyPairDriftModel extends DataClass
    implements Insertable<AppKeyPairDriftModel> {
  final int id;
  final String privateKey;
  final String publicKey;
  const AppKeyPairDriftModel(
      {required this.id, required this.privateKey, required this.publicKey});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['private_key'] = Variable<String>(privateKey);
    map['public_key'] = Variable<String>(publicKey);
    return map;
  }

  AppKeyPairTableCompanion toCompanion(bool nullToAbsent) {
    return AppKeyPairTableCompanion(
      id: Value(id),
      privateKey: Value(privateKey),
      publicKey: Value(publicKey),
    );
  }

  factory AppKeyPairDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return AppKeyPairDriftModel(
      id: serializer.fromJson<int>(json['id']),
      privateKey: serializer.fromJson<String>(json['privateKey']),
      publicKey: serializer.fromJson<String>(json['publicKey']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'privateKey': serializer.toJson<String>(privateKey),
      'publicKey': serializer.toJson<String>(publicKey),
    };
  }

  AppKeyPairDriftModel copyWith(
          {int? id, String? privateKey, String? publicKey}) =>
      AppKeyPairDriftModel(
        id: id ?? this.id,
        privateKey: privateKey ?? this.privateKey,
        publicKey: publicKey ?? this.publicKey,
      );
  AppKeyPairDriftModel copyWithCompanion(AppKeyPairTableCompanion data) {
    return AppKeyPairDriftModel(
      id: data.id.present ? data.id.value : this.id,
      privateKey:
          data.privateKey.present ? data.privateKey.value : this.privateKey,
      publicKey: data.publicKey.present ? data.publicKey.value : this.publicKey,
    );
  }

  @override
  String toString() {
    return (StringBuffer('AppKeyPairDriftModel(')
          ..write('id: $id, ')
          ..write('privateKey: $privateKey, ')
          ..write('publicKey: $publicKey')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, privateKey, publicKey);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is AppKeyPairDriftModel &&
          other.id == this.id &&
          other.privateKey == this.privateKey &&
          other.publicKey == this.publicKey);
}

class AppKeyPairTableCompanion extends UpdateCompanion<AppKeyPairDriftModel> {
  final Value<int> id;
  final Value<String> privateKey;
  final Value<String> publicKey;
  const AppKeyPairTableCompanion({
    this.id = const Value.absent(),
    this.privateKey = const Value.absent(),
    this.publicKey = const Value.absent(),
  });
  AppKeyPairTableCompanion.insert({
    this.id = const Value.absent(),
    required String privateKey,
    required String publicKey,
  })  : privateKey = Value(privateKey),
        publicKey = Value(publicKey);
  static Insertable<AppKeyPairDriftModel> custom({
    Expression<int>? id,
    Expression<String>? privateKey,
    Expression<String>? publicKey,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (privateKey != null) 'private_key': privateKey,
      if (publicKey != null) 'public_key': publicKey,
    });
  }

  AppKeyPairTableCompanion copyWith(
      {Value<int>? id, Value<String>? privateKey, Value<String>? publicKey}) {
    return AppKeyPairTableCompanion(
      id: id ?? this.id,
      privateKey: privateKey ?? this.privateKey,
      publicKey: publicKey ?? this.publicKey,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (privateKey.present) {
      map['private_key'] = Variable<String>(privateKey.value);
    }
    if (publicKey.present) {
      map['public_key'] = Variable<String>(publicKey.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('AppKeyPairTableCompanion(')
          ..write('id: $id, ')
          ..write('privateKey: $privateKey, ')
          ..write('publicKey: $publicKey')
          ..write(')'))
        .toString();
  }
}

class $AppSessionTableTable extends AppSessionTable
    with TableInfo<$AppSessionTableTable, AppSessionDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $AppSessionTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _keyMeta = const VerificationMeta('key');
  @override
  late final GeneratedColumn<String> key = GeneratedColumn<String>(
      'key', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _tokenMeta = const VerificationMeta('token');
  @override
  late final GeneratedColumn<String> token = GeneratedColumn<String>(
      'token', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [id, key, token, oin];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'app_session_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<AppSessionDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('key')) {
      context.handle(
          _keyMeta, key.isAcceptableOrUnknown(data['key']!, _keyMeta));
    } else if (isInserting) {
      context.missing(_keyMeta);
    }
    if (data.containsKey('token')) {
      context.handle(
          _tokenMeta, token.isAcceptableOrUnknown(data['token']!, _tokenMeta));
    } else if (isInserting) {
      context.missing(_tokenMeta);
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  AppSessionDriftModel map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return AppSessionDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      key: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}key'])!,
      token: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}token'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
    );
  }

  @override
  $AppSessionTableTable createAlias(String alias) {
    return $AppSessionTableTable(attachedDatabase, alias);
  }
}

class AppSessionDriftModel extends DataClass
    implements Insertable<AppSessionDriftModel> {
  final int id;
  final String key;
  final String token;
  final String oin;
  const AppSessionDriftModel(
      {required this.id,
      required this.key,
      required this.token,
      required this.oin});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['key'] = Variable<String>(key);
    map['token'] = Variable<String>(token);
    map['oin'] = Variable<String>(oin);
    return map;
  }

  AppSessionTableCompanion toCompanion(bool nullToAbsent) {
    return AppSessionTableCompanion(
      id: Value(id),
      key: Value(key),
      token: Value(token),
      oin: Value(oin),
    );
  }

  factory AppSessionDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return AppSessionDriftModel(
      id: serializer.fromJson<int>(json['id']),
      key: serializer.fromJson<String>(json['key']),
      token: serializer.fromJson<String>(json['token']),
      oin: serializer.fromJson<String>(json['oin']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'key': serializer.toJson<String>(key),
      'token': serializer.toJson<String>(token),
      'oin': serializer.toJson<String>(oin),
    };
  }

  AppSessionDriftModel copyWith(
          {int? id, String? key, String? token, String? oin}) =>
      AppSessionDriftModel(
        id: id ?? this.id,
        key: key ?? this.key,
        token: token ?? this.token,
        oin: oin ?? this.oin,
      );
  AppSessionDriftModel copyWithCompanion(AppSessionTableCompanion data) {
    return AppSessionDriftModel(
      id: data.id.present ? data.id.value : this.id,
      key: data.key.present ? data.key.value : this.key,
      token: data.token.present ? data.token.value : this.token,
      oin: data.oin.present ? data.oin.value : this.oin,
    );
  }

  @override
  String toString() {
    return (StringBuffer('AppSessionDriftModel(')
          ..write('id: $id, ')
          ..write('key: $key, ')
          ..write('token: $token, ')
          ..write('oin: $oin')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, key, token, oin);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is AppSessionDriftModel &&
          other.id == this.id &&
          other.key == this.key &&
          other.token == this.token &&
          other.oin == this.oin);
}

class AppSessionTableCompanion extends UpdateCompanion<AppSessionDriftModel> {
  final Value<int> id;
  final Value<String> key;
  final Value<String> token;
  final Value<String> oin;
  const AppSessionTableCompanion({
    this.id = const Value.absent(),
    this.key = const Value.absent(),
    this.token = const Value.absent(),
    this.oin = const Value.absent(),
  });
  AppSessionTableCompanion.insert({
    this.id = const Value.absent(),
    required String key,
    required String token,
    required String oin,
  })  : key = Value(key),
        token = Value(token),
        oin = Value(oin);
  static Insertable<AppSessionDriftModel> custom({
    Expression<int>? id,
    Expression<String>? key,
    Expression<String>? token,
    Expression<String>? oin,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (key != null) 'key': key,
      if (token != null) 'token': token,
      if (oin != null) 'oin': oin,
    });
  }

  AppSessionTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? key,
      Value<String>? token,
      Value<String>? oin}) {
    return AppSessionTableCompanion(
      id: id ?? this.id,
      key: key ?? this.key,
      token: token ?? this.token,
      oin: oin ?? this.oin,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (key.present) {
      map['key'] = Variable<String>(key.value);
    }
    if (token.present) {
      map['token'] = Variable<String>(token.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('AppSessionTableCompanion(')
          ..write('id: $id, ')
          ..write('key: $key, ')
          ..write('token: $token, ')
          ..write('oin: $oin')
          ..write(')'))
        .toString();
  }
}

class $FinancialClaimsInformationStorageTableTable
    extends FinancialClaimsInformationStorageTable
    with
        TableInfo<$FinancialClaimsInformationStorageTableTable,
            FinancialClaimsInformationStorageDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $FinancialClaimsInformationStorageTableTable(this.attachedDatabase,
      [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _dateTimeRequestedMeta =
      const VerificationMeta('dateTimeRequested');
  @override
  late final GeneratedColumn<DateTime> dateTimeRequested =
      GeneratedColumn<DateTime>('date_time_requested', aliasedName, false,
          type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _dateTimeReceivedMeta =
      const VerificationMeta('dateTimeReceived');
  @override
  late final GeneratedColumn<DateTime> dateTimeReceived =
      GeneratedColumn<DateTime>('date_time_received', aliasedName, false,
          type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _financialClaimsInformationDocumentMeta =
      const VerificationMeta('financialClaimsInformationDocument');
  @override
  late final GeneratedColumn<String> financialClaimsInformationDocument =
      GeneratedColumn<String>(
          'financial_claims_information_document', aliasedName, false,
          type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [
        id,
        oin,
        dateTimeRequested,
        dateTimeReceived,
        financialClaimsInformationDocument
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'financial_claims_information_storage_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<FinancialClaimsInformationStorageDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    if (data.containsKey('date_time_requested')) {
      context.handle(
          _dateTimeRequestedMeta,
          dateTimeRequested.isAcceptableOrUnknown(
              data['date_time_requested']!, _dateTimeRequestedMeta));
    } else if (isInserting) {
      context.missing(_dateTimeRequestedMeta);
    }
    if (data.containsKey('date_time_received')) {
      context.handle(
          _dateTimeReceivedMeta,
          dateTimeReceived.isAcceptableOrUnknown(
              data['date_time_received']!, _dateTimeReceivedMeta));
    } else if (isInserting) {
      context.missing(_dateTimeReceivedMeta);
    }
    if (data.containsKey('financial_claims_information_document')) {
      context.handle(
          _financialClaimsInformationDocumentMeta,
          financialClaimsInformationDocument.isAcceptableOrUnknown(
              data['financial_claims_information_document']!,
              _financialClaimsInformationDocumentMeta));
    } else if (isInserting) {
      context.missing(_financialClaimsInformationDocumentMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  FinancialClaimsInformationStorageDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return FinancialClaimsInformationStorageDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
      dateTimeRequested: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime,
          data['${effectivePrefix}date_time_requested'])!,
      dateTimeReceived: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime, data['${effectivePrefix}date_time_received'])!,
      financialClaimsInformationDocument: attachedDatabase.typeMapping.read(
          DriftSqlType.string,
          data['${effectivePrefix}financial_claims_information_document'])!,
    );
  }

  @override
  $FinancialClaimsInformationStorageTableTable createAlias(String alias) {
    return $FinancialClaimsInformationStorageTableTable(
        attachedDatabase, alias);
  }
}

class FinancialClaimsInformationStorageDriftModel extends DataClass
    implements Insertable<FinancialClaimsInformationStorageDriftModel> {
  final int id;
  final String oin;
  final DateTime dateTimeRequested;
  final DateTime dateTimeReceived;
  final String financialClaimsInformationDocument;
  const FinancialClaimsInformationStorageDriftModel(
      {required this.id,
      required this.oin,
      required this.dateTimeRequested,
      required this.dateTimeReceived,
      required this.financialClaimsInformationDocument});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    map['date_time_requested'] = Variable<DateTime>(dateTimeRequested);
    map['date_time_received'] = Variable<DateTime>(dateTimeReceived);
    map['financial_claims_information_document'] =
        Variable<String>(financialClaimsInformationDocument);
    return map;
  }

  FinancialClaimsInformationStorageTableCompanion toCompanion(
      bool nullToAbsent) {
    return FinancialClaimsInformationStorageTableCompanion(
      id: Value(id),
      oin: Value(oin),
      dateTimeRequested: Value(dateTimeRequested),
      dateTimeReceived: Value(dateTimeReceived),
      financialClaimsInformationDocument:
          Value(financialClaimsInformationDocument),
    );
  }

  factory FinancialClaimsInformationStorageDriftModel.fromJson(
      Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return FinancialClaimsInformationStorageDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
      dateTimeRequested:
          serializer.fromJson<DateTime>(json['dateTimeRequested']),
      dateTimeReceived: serializer.fromJson<DateTime>(json['dateTimeReceived']),
      financialClaimsInformationDocument: serializer
          .fromJson<String>(json['financialClaimsInformationDocument']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
      'dateTimeRequested': serializer.toJson<DateTime>(dateTimeRequested),
      'dateTimeReceived': serializer.toJson<DateTime>(dateTimeReceived),
      'financialClaimsInformationDocument':
          serializer.toJson<String>(financialClaimsInformationDocument),
    };
  }

  FinancialClaimsInformationStorageDriftModel copyWith(
          {int? id,
          String? oin,
          DateTime? dateTimeRequested,
          DateTime? dateTimeReceived,
          String? financialClaimsInformationDocument}) =>
      FinancialClaimsInformationStorageDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
        dateTimeRequested: dateTimeRequested ?? this.dateTimeRequested,
        dateTimeReceived: dateTimeReceived ?? this.dateTimeReceived,
        financialClaimsInformationDocument:
            financialClaimsInformationDocument ??
                this.financialClaimsInformationDocument,
      );
  FinancialClaimsInformationStorageDriftModel copyWithCompanion(
      FinancialClaimsInformationStorageTableCompanion data) {
    return FinancialClaimsInformationStorageDriftModel(
      id: data.id.present ? data.id.value : this.id,
      oin: data.oin.present ? data.oin.value : this.oin,
      dateTimeRequested: data.dateTimeRequested.present
          ? data.dateTimeRequested.value
          : this.dateTimeRequested,
      dateTimeReceived: data.dateTimeReceived.present
          ? data.dateTimeReceived.value
          : this.dateTimeReceived,
      financialClaimsInformationDocument:
          data.financialClaimsInformationDocument.present
              ? data.financialClaimsInformationDocument.value
              : this.financialClaimsInformationDocument,
    );
  }

  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationStorageDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('dateTimeRequested: $dateTimeRequested, ')
          ..write('dateTimeReceived: $dateTimeReceived, ')
          ..write(
              'financialClaimsInformationDocument: $financialClaimsInformationDocument')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, oin, dateTimeRequested, dateTimeReceived,
      financialClaimsInformationDocument);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FinancialClaimsInformationStorageDriftModel &&
          other.id == this.id &&
          other.oin == this.oin &&
          other.dateTimeRequested == this.dateTimeRequested &&
          other.dateTimeReceived == this.dateTimeReceived &&
          other.financialClaimsInformationDocument ==
              this.financialClaimsInformationDocument);
}

class FinancialClaimsInformationStorageTableCompanion
    extends UpdateCompanion<FinancialClaimsInformationStorageDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  final Value<DateTime> dateTimeRequested;
  final Value<DateTime> dateTimeReceived;
  final Value<String> financialClaimsInformationDocument;
  const FinancialClaimsInformationStorageTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
    this.dateTimeRequested = const Value.absent(),
    this.dateTimeReceived = const Value.absent(),
    this.financialClaimsInformationDocument = const Value.absent(),
  });
  FinancialClaimsInformationStorageTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
    required DateTime dateTimeRequested,
    required DateTime dateTimeReceived,
    required String financialClaimsInformationDocument,
  })  : oin = Value(oin),
        dateTimeRequested = Value(dateTimeRequested),
        dateTimeReceived = Value(dateTimeReceived),
        financialClaimsInformationDocument =
            Value(financialClaimsInformationDocument);
  static Insertable<FinancialClaimsInformationStorageDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
    Expression<DateTime>? dateTimeRequested,
    Expression<DateTime>? dateTimeReceived,
    Expression<String>? financialClaimsInformationDocument,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
      if (dateTimeRequested != null) 'date_time_requested': dateTimeRequested,
      if (dateTimeReceived != null) 'date_time_received': dateTimeReceived,
      if (financialClaimsInformationDocument != null)
        'financial_claims_information_document':
            financialClaimsInformationDocument,
    });
  }

  FinancialClaimsInformationStorageTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? oin,
      Value<DateTime>? dateTimeRequested,
      Value<DateTime>? dateTimeReceived,
      Value<String>? financialClaimsInformationDocument}) {
    return FinancialClaimsInformationStorageTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
      dateTimeRequested: dateTimeRequested ?? this.dateTimeRequested,
      dateTimeReceived: dateTimeReceived ?? this.dateTimeReceived,
      financialClaimsInformationDocument: financialClaimsInformationDocument ??
          this.financialClaimsInformationDocument,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    if (dateTimeRequested.present) {
      map['date_time_requested'] = Variable<DateTime>(dateTimeRequested.value);
    }
    if (dateTimeReceived.present) {
      map['date_time_received'] = Variable<DateTime>(dateTimeReceived.value);
    }
    if (financialClaimsInformationDocument.present) {
      map['financial_claims_information_document'] =
          Variable<String>(financialClaimsInformationDocument.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationStorageTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('dateTimeRequested: $dateTimeRequested, ')
          ..write('dateTimeReceived: $dateTimeReceived, ')
          ..write(
              'financialClaimsInformationDocument: $financialClaimsInformationDocument')
          ..write(')'))
        .toString();
  }
}

class $LogRecordTableTable extends LogRecordTable
    with TableInfo<$LogRecordTableTable, LogRecordDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $LogRecordTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _timestampMeta =
      const VerificationMeta('timestamp');
  @override
  late final GeneratedColumn<DateTime> timestamp = GeneratedColumn<DateTime>(
      'timestamp', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _hostMeta = const VerificationMeta('host');
  @override
  late final GeneratedColumn<String> host = GeneratedColumn<String>(
      'host', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _cefMeta = const VerificationMeta('cef');
  @override
  late final GeneratedColumn<int> cef = GeneratedColumn<int>(
      'cef', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _deviceVendorMeta =
      const VerificationMeta('deviceVendor');
  @override
  late final GeneratedColumn<String> deviceVendor = GeneratedColumn<String>(
      'device_vendor', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _deviceProductMeta =
      const VerificationMeta('deviceProduct');
  @override
  late final GeneratedColumn<String> deviceProduct = GeneratedColumn<String>(
      'device_product', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _deviceVersionMeta =
      const VerificationMeta('deviceVersion');
  @override
  late final GeneratedColumn<String> deviceVersion = GeneratedColumn<String>(
      'device_version', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _deviceEventClassIdMeta =
      const VerificationMeta('deviceEventClassId');
  @override
  late final GeneratedColumn<String> deviceEventClassId =
      GeneratedColumn<String>('device_event_class_id', aliasedName, false,
          type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _severityMeta =
      const VerificationMeta('severity');
  @override
  late final GeneratedColumn<int> severity = GeneratedColumn<int>(
      'severity', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _flexString1LabelMeta =
      const VerificationMeta('flexString1Label');
  @override
  late final GeneratedColumn<String> flexString1Label = GeneratedColumn<String>(
      'flex_string1_label', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _flexString1Meta =
      const VerificationMeta('flexString1');
  @override
  late final GeneratedColumn<String> flexString1 = GeneratedColumn<String>(
      'flex_string1', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _flexString2LabelMeta =
      const VerificationMeta('flexString2Label');
  @override
  late final GeneratedColumn<String> flexString2Label = GeneratedColumn<String>(
      'flex_string2_label', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _flexString2Meta =
      const VerificationMeta('flexString2');
  @override
  late final GeneratedColumn<String> flexString2 = GeneratedColumn<String>(
      'flex_string2', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _actMeta = const VerificationMeta('act');
  @override
  late final GeneratedColumn<String> act = GeneratedColumn<String>(
      'act', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _appMeta = const VerificationMeta('app');
  @override
  late final GeneratedColumn<String> app = GeneratedColumn<String>(
      'app', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _requestMeta =
      const VerificationMeta('request');
  @override
  late final GeneratedColumn<String> request = GeneratedColumn<String>(
      'request', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _requestMethodMeta =
      const VerificationMeta('requestMethod');
  @override
  late final GeneratedColumn<String> requestMethod = GeneratedColumn<String>(
      'request_method', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [
        id,
        timestamp,
        host,
        cef,
        deviceVendor,
        deviceProduct,
        deviceVersion,
        deviceEventClassId,
        name,
        severity,
        flexString1Label,
        flexString1,
        flexString2Label,
        flexString2,
        act,
        app,
        request,
        requestMethod
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'log_record_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<LogRecordDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('timestamp')) {
      context.handle(_timestampMeta,
          timestamp.isAcceptableOrUnknown(data['timestamp']!, _timestampMeta));
    } else if (isInserting) {
      context.missing(_timestampMeta);
    }
    if (data.containsKey('host')) {
      context.handle(
          _hostMeta, host.isAcceptableOrUnknown(data['host']!, _hostMeta));
    } else if (isInserting) {
      context.missing(_hostMeta);
    }
    if (data.containsKey('cef')) {
      context.handle(
          _cefMeta, cef.isAcceptableOrUnknown(data['cef']!, _cefMeta));
    } else if (isInserting) {
      context.missing(_cefMeta);
    }
    if (data.containsKey('device_vendor')) {
      context.handle(
          _deviceVendorMeta,
          deviceVendor.isAcceptableOrUnknown(
              data['device_vendor']!, _deviceVendorMeta));
    } else if (isInserting) {
      context.missing(_deviceVendorMeta);
    }
    if (data.containsKey('device_product')) {
      context.handle(
          _deviceProductMeta,
          deviceProduct.isAcceptableOrUnknown(
              data['device_product']!, _deviceProductMeta));
    } else if (isInserting) {
      context.missing(_deviceProductMeta);
    }
    if (data.containsKey('device_version')) {
      context.handle(
          _deviceVersionMeta,
          deviceVersion.isAcceptableOrUnknown(
              data['device_version']!, _deviceVersionMeta));
    } else if (isInserting) {
      context.missing(_deviceVersionMeta);
    }
    if (data.containsKey('device_event_class_id')) {
      context.handle(
          _deviceEventClassIdMeta,
          deviceEventClassId.isAcceptableOrUnknown(
              data['device_event_class_id']!, _deviceEventClassIdMeta));
    } else if (isInserting) {
      context.missing(_deviceEventClassIdMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('severity')) {
      context.handle(_severityMeta,
          severity.isAcceptableOrUnknown(data['severity']!, _severityMeta));
    } else if (isInserting) {
      context.missing(_severityMeta);
    }
    if (data.containsKey('flex_string1_label')) {
      context.handle(
          _flexString1LabelMeta,
          flexString1Label.isAcceptableOrUnknown(
              data['flex_string1_label']!, _flexString1LabelMeta));
    } else if (isInserting) {
      context.missing(_flexString1LabelMeta);
    }
    if (data.containsKey('flex_string1')) {
      context.handle(
          _flexString1Meta,
          flexString1.isAcceptableOrUnknown(
              data['flex_string1']!, _flexString1Meta));
    } else if (isInserting) {
      context.missing(_flexString1Meta);
    }
    if (data.containsKey('flex_string2_label')) {
      context.handle(
          _flexString2LabelMeta,
          flexString2Label.isAcceptableOrUnknown(
              data['flex_string2_label']!, _flexString2LabelMeta));
    } else if (isInserting) {
      context.missing(_flexString2LabelMeta);
    }
    if (data.containsKey('flex_string2')) {
      context.handle(
          _flexString2Meta,
          flexString2.isAcceptableOrUnknown(
              data['flex_string2']!, _flexString2Meta));
    } else if (isInserting) {
      context.missing(_flexString2Meta);
    }
    if (data.containsKey('act')) {
      context.handle(
          _actMeta, act.isAcceptableOrUnknown(data['act']!, _actMeta));
    } else if (isInserting) {
      context.missing(_actMeta);
    }
    if (data.containsKey('app')) {
      context.handle(
          _appMeta, app.isAcceptableOrUnknown(data['app']!, _appMeta));
    } else if (isInserting) {
      context.missing(_appMeta);
    }
    if (data.containsKey('request')) {
      context.handle(_requestMeta,
          request.isAcceptableOrUnknown(data['request']!, _requestMeta));
    } else if (isInserting) {
      context.missing(_requestMeta);
    }
    if (data.containsKey('request_method')) {
      context.handle(
          _requestMethodMeta,
          requestMethod.isAcceptableOrUnknown(
              data['request_method']!, _requestMethodMeta));
    } else if (isInserting) {
      context.missing(_requestMethodMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  LogRecordDriftModel map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return LogRecordDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      timestamp: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}timestamp'])!,
      host: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}host'])!,
      cef: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}cef'])!,
      deviceVendor: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}device_vendor'])!,
      deviceProduct: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}device_product'])!,
      deviceVersion: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}device_version'])!,
      deviceEventClassId: attachedDatabase.typeMapping.read(DriftSqlType.string,
          data['${effectivePrefix}device_event_class_id'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      severity: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}severity'])!,
      flexString1Label: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}flex_string1_label'])!,
      flexString1: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}flex_string1'])!,
      flexString2Label: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}flex_string2_label'])!,
      flexString2: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}flex_string2'])!,
      act: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}act'])!,
      app: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}app'])!,
      request: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}request'])!,
      requestMethod: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}request_method'])!,
    );
  }

  @override
  $LogRecordTableTable createAlias(String alias) {
    return $LogRecordTableTable(attachedDatabase, alias);
  }
}

class LogRecordDriftModel extends DataClass
    implements Insertable<LogRecordDriftModel> {
  final int id;
  final DateTime timestamp;
  final String host;
  final int cef;
  final String deviceVendor;
  final String deviceProduct;
  final String deviceVersion;
  final String deviceEventClassId;
  final String name;
  final int severity;
  final String flexString1Label;
  final String flexString1;
  final String flexString2Label;
  final String flexString2;
  final String act;
  final String app;
  final String request;
  final String requestMethod;
  const LogRecordDriftModel(
      {required this.id,
      required this.timestamp,
      required this.host,
      required this.cef,
      required this.deviceVendor,
      required this.deviceProduct,
      required this.deviceVersion,
      required this.deviceEventClassId,
      required this.name,
      required this.severity,
      required this.flexString1Label,
      required this.flexString1,
      required this.flexString2Label,
      required this.flexString2,
      required this.act,
      required this.app,
      required this.request,
      required this.requestMethod});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['timestamp'] = Variable<DateTime>(timestamp);
    map['host'] = Variable<String>(host);
    map['cef'] = Variable<int>(cef);
    map['device_vendor'] = Variable<String>(deviceVendor);
    map['device_product'] = Variable<String>(deviceProduct);
    map['device_version'] = Variable<String>(deviceVersion);
    map['device_event_class_id'] = Variable<String>(deviceEventClassId);
    map['name'] = Variable<String>(name);
    map['severity'] = Variable<int>(severity);
    map['flex_string1_label'] = Variable<String>(flexString1Label);
    map['flex_string1'] = Variable<String>(flexString1);
    map['flex_string2_label'] = Variable<String>(flexString2Label);
    map['flex_string2'] = Variable<String>(flexString2);
    map['act'] = Variable<String>(act);
    map['app'] = Variable<String>(app);
    map['request'] = Variable<String>(request);
    map['request_method'] = Variable<String>(requestMethod);
    return map;
  }

  LogRecordTableCompanion toCompanion(bool nullToAbsent) {
    return LogRecordTableCompanion(
      id: Value(id),
      timestamp: Value(timestamp),
      host: Value(host),
      cef: Value(cef),
      deviceVendor: Value(deviceVendor),
      deviceProduct: Value(deviceProduct),
      deviceVersion: Value(deviceVersion),
      deviceEventClassId: Value(deviceEventClassId),
      name: Value(name),
      severity: Value(severity),
      flexString1Label: Value(flexString1Label),
      flexString1: Value(flexString1),
      flexString2Label: Value(flexString2Label),
      flexString2: Value(flexString2),
      act: Value(act),
      app: Value(app),
      request: Value(request),
      requestMethod: Value(requestMethod),
    );
  }

  factory LogRecordDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return LogRecordDriftModel(
      id: serializer.fromJson<int>(json['id']),
      timestamp: serializer.fromJson<DateTime>(json['timestamp']),
      host: serializer.fromJson<String>(json['host']),
      cef: serializer.fromJson<int>(json['cef']),
      deviceVendor: serializer.fromJson<String>(json['deviceVendor']),
      deviceProduct: serializer.fromJson<String>(json['deviceProduct']),
      deviceVersion: serializer.fromJson<String>(json['deviceVersion']),
      deviceEventClassId:
          serializer.fromJson<String>(json['deviceEventClassId']),
      name: serializer.fromJson<String>(json['name']),
      severity: serializer.fromJson<int>(json['severity']),
      flexString1Label: serializer.fromJson<String>(json['flexString1Label']),
      flexString1: serializer.fromJson<String>(json['flexString1']),
      flexString2Label: serializer.fromJson<String>(json['flexString2Label']),
      flexString2: serializer.fromJson<String>(json['flexString2']),
      act: serializer.fromJson<String>(json['act']),
      app: serializer.fromJson<String>(json['app']),
      request: serializer.fromJson<String>(json['request']),
      requestMethod: serializer.fromJson<String>(json['requestMethod']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'timestamp': serializer.toJson<DateTime>(timestamp),
      'host': serializer.toJson<String>(host),
      'cef': serializer.toJson<int>(cef),
      'deviceVendor': serializer.toJson<String>(deviceVendor),
      'deviceProduct': serializer.toJson<String>(deviceProduct),
      'deviceVersion': serializer.toJson<String>(deviceVersion),
      'deviceEventClassId': serializer.toJson<String>(deviceEventClassId),
      'name': serializer.toJson<String>(name),
      'severity': serializer.toJson<int>(severity),
      'flexString1Label': serializer.toJson<String>(flexString1Label),
      'flexString1': serializer.toJson<String>(flexString1),
      'flexString2Label': serializer.toJson<String>(flexString2Label),
      'flexString2': serializer.toJson<String>(flexString2),
      'act': serializer.toJson<String>(act),
      'app': serializer.toJson<String>(app),
      'request': serializer.toJson<String>(request),
      'requestMethod': serializer.toJson<String>(requestMethod),
    };
  }

  LogRecordDriftModel copyWith(
          {int? id,
          DateTime? timestamp,
          String? host,
          int? cef,
          String? deviceVendor,
          String? deviceProduct,
          String? deviceVersion,
          String? deviceEventClassId,
          String? name,
          int? severity,
          String? flexString1Label,
          String? flexString1,
          String? flexString2Label,
          String? flexString2,
          String? act,
          String? app,
          String? request,
          String? requestMethod}) =>
      LogRecordDriftModel(
        id: id ?? this.id,
        timestamp: timestamp ?? this.timestamp,
        host: host ?? this.host,
        cef: cef ?? this.cef,
        deviceVendor: deviceVendor ?? this.deviceVendor,
        deviceProduct: deviceProduct ?? this.deviceProduct,
        deviceVersion: deviceVersion ?? this.deviceVersion,
        deviceEventClassId: deviceEventClassId ?? this.deviceEventClassId,
        name: name ?? this.name,
        severity: severity ?? this.severity,
        flexString1Label: flexString1Label ?? this.flexString1Label,
        flexString1: flexString1 ?? this.flexString1,
        flexString2Label: flexString2Label ?? this.flexString2Label,
        flexString2: flexString2 ?? this.flexString2,
        act: act ?? this.act,
        app: app ?? this.app,
        request: request ?? this.request,
        requestMethod: requestMethod ?? this.requestMethod,
      );
  LogRecordDriftModel copyWithCompanion(LogRecordTableCompanion data) {
    return LogRecordDriftModel(
      id: data.id.present ? data.id.value : this.id,
      timestamp: data.timestamp.present ? data.timestamp.value : this.timestamp,
      host: data.host.present ? data.host.value : this.host,
      cef: data.cef.present ? data.cef.value : this.cef,
      deviceVendor: data.deviceVendor.present
          ? data.deviceVendor.value
          : this.deviceVendor,
      deviceProduct: data.deviceProduct.present
          ? data.deviceProduct.value
          : this.deviceProduct,
      deviceVersion: data.deviceVersion.present
          ? data.deviceVersion.value
          : this.deviceVersion,
      deviceEventClassId: data.deviceEventClassId.present
          ? data.deviceEventClassId.value
          : this.deviceEventClassId,
      name: data.name.present ? data.name.value : this.name,
      severity: data.severity.present ? data.severity.value : this.severity,
      flexString1Label: data.flexString1Label.present
          ? data.flexString1Label.value
          : this.flexString1Label,
      flexString1:
          data.flexString1.present ? data.flexString1.value : this.flexString1,
      flexString2Label: data.flexString2Label.present
          ? data.flexString2Label.value
          : this.flexString2Label,
      flexString2:
          data.flexString2.present ? data.flexString2.value : this.flexString2,
      act: data.act.present ? data.act.value : this.act,
      app: data.app.present ? data.app.value : this.app,
      request: data.request.present ? data.request.value : this.request,
      requestMethod: data.requestMethod.present
          ? data.requestMethod.value
          : this.requestMethod,
    );
  }

  @override
  String toString() {
    return (StringBuffer('LogRecordDriftModel(')
          ..write('id: $id, ')
          ..write('timestamp: $timestamp, ')
          ..write('host: $host, ')
          ..write('cef: $cef, ')
          ..write('deviceVendor: $deviceVendor, ')
          ..write('deviceProduct: $deviceProduct, ')
          ..write('deviceVersion: $deviceVersion, ')
          ..write('deviceEventClassId: $deviceEventClassId, ')
          ..write('name: $name, ')
          ..write('severity: $severity, ')
          ..write('flexString1Label: $flexString1Label, ')
          ..write('flexString1: $flexString1, ')
          ..write('flexString2Label: $flexString2Label, ')
          ..write('flexString2: $flexString2, ')
          ..write('act: $act, ')
          ..write('app: $app, ')
          ..write('request: $request, ')
          ..write('requestMethod: $requestMethod')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id,
      timestamp,
      host,
      cef,
      deviceVendor,
      deviceProduct,
      deviceVersion,
      deviceEventClassId,
      name,
      severity,
      flexString1Label,
      flexString1,
      flexString2Label,
      flexString2,
      act,
      app,
      request,
      requestMethod);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is LogRecordDriftModel &&
          other.id == this.id &&
          other.timestamp == this.timestamp &&
          other.host == this.host &&
          other.cef == this.cef &&
          other.deviceVendor == this.deviceVendor &&
          other.deviceProduct == this.deviceProduct &&
          other.deviceVersion == this.deviceVersion &&
          other.deviceEventClassId == this.deviceEventClassId &&
          other.name == this.name &&
          other.severity == this.severity &&
          other.flexString1Label == this.flexString1Label &&
          other.flexString1 == this.flexString1 &&
          other.flexString2Label == this.flexString2Label &&
          other.flexString2 == this.flexString2 &&
          other.act == this.act &&
          other.app == this.app &&
          other.request == this.request &&
          other.requestMethod == this.requestMethod);
}

class LogRecordTableCompanion extends UpdateCompanion<LogRecordDriftModel> {
  final Value<int> id;
  final Value<DateTime> timestamp;
  final Value<String> host;
  final Value<int> cef;
  final Value<String> deviceVendor;
  final Value<String> deviceProduct;
  final Value<String> deviceVersion;
  final Value<String> deviceEventClassId;
  final Value<String> name;
  final Value<int> severity;
  final Value<String> flexString1Label;
  final Value<String> flexString1;
  final Value<String> flexString2Label;
  final Value<String> flexString2;
  final Value<String> act;
  final Value<String> app;
  final Value<String> request;
  final Value<String> requestMethod;
  const LogRecordTableCompanion({
    this.id = const Value.absent(),
    this.timestamp = const Value.absent(),
    this.host = const Value.absent(),
    this.cef = const Value.absent(),
    this.deviceVendor = const Value.absent(),
    this.deviceProduct = const Value.absent(),
    this.deviceVersion = const Value.absent(),
    this.deviceEventClassId = const Value.absent(),
    this.name = const Value.absent(),
    this.severity = const Value.absent(),
    this.flexString1Label = const Value.absent(),
    this.flexString1 = const Value.absent(),
    this.flexString2Label = const Value.absent(),
    this.flexString2 = const Value.absent(),
    this.act = const Value.absent(),
    this.app = const Value.absent(),
    this.request = const Value.absent(),
    this.requestMethod = const Value.absent(),
  });
  LogRecordTableCompanion.insert({
    this.id = const Value.absent(),
    required DateTime timestamp,
    required String host,
    required int cef,
    required String deviceVendor,
    required String deviceProduct,
    required String deviceVersion,
    required String deviceEventClassId,
    required String name,
    required int severity,
    required String flexString1Label,
    required String flexString1,
    required String flexString2Label,
    required String flexString2,
    required String act,
    required String app,
    required String request,
    required String requestMethod,
  })  : timestamp = Value(timestamp),
        host = Value(host),
        cef = Value(cef),
        deviceVendor = Value(deviceVendor),
        deviceProduct = Value(deviceProduct),
        deviceVersion = Value(deviceVersion),
        deviceEventClassId = Value(deviceEventClassId),
        name = Value(name),
        severity = Value(severity),
        flexString1Label = Value(flexString1Label),
        flexString1 = Value(flexString1),
        flexString2Label = Value(flexString2Label),
        flexString2 = Value(flexString2),
        act = Value(act),
        app = Value(app),
        request = Value(request),
        requestMethod = Value(requestMethod);
  static Insertable<LogRecordDriftModel> custom({
    Expression<int>? id,
    Expression<DateTime>? timestamp,
    Expression<String>? host,
    Expression<int>? cef,
    Expression<String>? deviceVendor,
    Expression<String>? deviceProduct,
    Expression<String>? deviceVersion,
    Expression<String>? deviceEventClassId,
    Expression<String>? name,
    Expression<int>? severity,
    Expression<String>? flexString1Label,
    Expression<String>? flexString1,
    Expression<String>? flexString2Label,
    Expression<String>? flexString2,
    Expression<String>? act,
    Expression<String>? app,
    Expression<String>? request,
    Expression<String>? requestMethod,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (timestamp != null) 'timestamp': timestamp,
      if (host != null) 'host': host,
      if (cef != null) 'cef': cef,
      if (deviceVendor != null) 'device_vendor': deviceVendor,
      if (deviceProduct != null) 'device_product': deviceProduct,
      if (deviceVersion != null) 'device_version': deviceVersion,
      if (deviceEventClassId != null)
        'device_event_class_id': deviceEventClassId,
      if (name != null) 'name': name,
      if (severity != null) 'severity': severity,
      if (flexString1Label != null) 'flex_string1_label': flexString1Label,
      if (flexString1 != null) 'flex_string1': flexString1,
      if (flexString2Label != null) 'flex_string2_label': flexString2Label,
      if (flexString2 != null) 'flex_string2': flexString2,
      if (act != null) 'act': act,
      if (app != null) 'app': app,
      if (request != null) 'request': request,
      if (requestMethod != null) 'request_method': requestMethod,
    });
  }

  LogRecordTableCompanion copyWith(
      {Value<int>? id,
      Value<DateTime>? timestamp,
      Value<String>? host,
      Value<int>? cef,
      Value<String>? deviceVendor,
      Value<String>? deviceProduct,
      Value<String>? deviceVersion,
      Value<String>? deviceEventClassId,
      Value<String>? name,
      Value<int>? severity,
      Value<String>? flexString1Label,
      Value<String>? flexString1,
      Value<String>? flexString2Label,
      Value<String>? flexString2,
      Value<String>? act,
      Value<String>? app,
      Value<String>? request,
      Value<String>? requestMethod}) {
    return LogRecordTableCompanion(
      id: id ?? this.id,
      timestamp: timestamp ?? this.timestamp,
      host: host ?? this.host,
      cef: cef ?? this.cef,
      deviceVendor: deviceVendor ?? this.deviceVendor,
      deviceProduct: deviceProduct ?? this.deviceProduct,
      deviceVersion: deviceVersion ?? this.deviceVersion,
      deviceEventClassId: deviceEventClassId ?? this.deviceEventClassId,
      name: name ?? this.name,
      severity: severity ?? this.severity,
      flexString1Label: flexString1Label ?? this.flexString1Label,
      flexString1: flexString1 ?? this.flexString1,
      flexString2Label: flexString2Label ?? this.flexString2Label,
      flexString2: flexString2 ?? this.flexString2,
      act: act ?? this.act,
      app: app ?? this.app,
      request: request ?? this.request,
      requestMethod: requestMethod ?? this.requestMethod,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (timestamp.present) {
      map['timestamp'] = Variable<DateTime>(timestamp.value);
    }
    if (host.present) {
      map['host'] = Variable<String>(host.value);
    }
    if (cef.present) {
      map['cef'] = Variable<int>(cef.value);
    }
    if (deviceVendor.present) {
      map['device_vendor'] = Variable<String>(deviceVendor.value);
    }
    if (deviceProduct.present) {
      map['device_product'] = Variable<String>(deviceProduct.value);
    }
    if (deviceVersion.present) {
      map['device_version'] = Variable<String>(deviceVersion.value);
    }
    if (deviceEventClassId.present) {
      map['device_event_class_id'] = Variable<String>(deviceEventClassId.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (severity.present) {
      map['severity'] = Variable<int>(severity.value);
    }
    if (flexString1Label.present) {
      map['flex_string1_label'] = Variable<String>(flexString1Label.value);
    }
    if (flexString1.present) {
      map['flex_string1'] = Variable<String>(flexString1.value);
    }
    if (flexString2Label.present) {
      map['flex_string2_label'] = Variable<String>(flexString2Label.value);
    }
    if (flexString2.present) {
      map['flex_string2'] = Variable<String>(flexString2.value);
    }
    if (act.present) {
      map['act'] = Variable<String>(act.value);
    }
    if (app.present) {
      map['app'] = Variable<String>(app.value);
    }
    if (request.present) {
      map['request'] = Variable<String>(request.value);
    }
    if (requestMethod.present) {
      map['request_method'] = Variable<String>(requestMethod.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('LogRecordTableCompanion(')
          ..write('id: $id, ')
          ..write('timestamp: $timestamp, ')
          ..write('host: $host, ')
          ..write('cef: $cef, ')
          ..write('deviceVendor: $deviceVendor, ')
          ..write('deviceProduct: $deviceProduct, ')
          ..write('deviceVersion: $deviceVersion, ')
          ..write('deviceEventClassId: $deviceEventClassId, ')
          ..write('name: $name, ')
          ..write('severity: $severity, ')
          ..write('flexString1Label: $flexString1Label, ')
          ..write('flexString1: $flexString1, ')
          ..write('flexString2Label: $flexString2Label, ')
          ..write('flexString2: $flexString2, ')
          ..write('act: $act, ')
          ..write('app: $app, ')
          ..write('request: $request, ')
          ..write('requestMethod: $requestMethod')
          ..write(')'))
        .toString();
  }
}

abstract class _$EncryptedDatabase extends GeneratedDatabase {
  _$EncryptedDatabase(QueryExecutor e) : super(e);
  $EncryptedDatabaseManager get managers => $EncryptedDatabaseManager(this);
  late final $FinancialClaimsInformationRequestTableTable
      financialClaimsInformationRequestTable =
      $FinancialClaimsInformationRequestTableTable(this);
  late final $FinancialClaimsInformationConfigurationTableTable
      financialClaimsInformationConfigurationTable =
      $FinancialClaimsInformationConfigurationTableTable(this);
  late final $FinancialClaimsInformationInboxTableTable
      financialClaimsInformationInboxTable =
      $FinancialClaimsInformationInboxTableTable(this);
  late final $FinancialClaimsInformationReceivedTableTable
      financialClaimsInformationReceivedTable =
      $FinancialClaimsInformationReceivedTableTable(this);
  late final $RegistrationTableTable registrationTable =
      $RegistrationTableTable(this);
  late final $CertificateTableTable certificateTable =
      $CertificateTableTable(this);
  late final $OrganizationSelectionTableTable organizationSelectionTable =
      $OrganizationSelectionTableTable(this);
  late final $AppManagerSelectionTableTable appManagerSelectionTable =
      $AppManagerSelectionTableTable(this);
  late final $SchemeAppManagerTableTable schemeAppManagerTable =
      $SchemeAppManagerTableTable(this);
  late final $SchemeDocumentTypeTableTable schemeDocumentTypeTable =
      $SchemeDocumentTypeTableTable(this);
  late final $SchemeOrganizationTableTable schemeOrganizationTable =
      $SchemeOrganizationTableTable(this);
  late final $PaymentPlanGroupsTableTable paymentPlanGroupsTable =
      $PaymentPlanGroupsTableTable(this);
  late final $PaymentPlanRulesStorageTableTable paymentPlanRulesStorageTable =
      $PaymentPlanRulesStorageTableTable(this);
  late final $AppKeyPairTableTable appKeyPairTable =
      $AppKeyPairTableTable(this);
  late final $AppSessionTableTable appSessionTable =
      $AppSessionTableTable(this);
  late final $FinancialClaimsInformationStorageTableTable
      financialClaimsInformationStorageTable =
      $FinancialClaimsInformationStorageTableTable(this);
  late final $LogRecordTableTable logRecordTable = $LogRecordTableTable(this);
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        financialClaimsInformationRequestTable,
        financialClaimsInformationConfigurationTable,
        financialClaimsInformationInboxTable,
        financialClaimsInformationReceivedTable,
        registrationTable,
        certificateTable,
        organizationSelectionTable,
        appManagerSelectionTable,
        schemeAppManagerTable,
        schemeDocumentTypeTable,
        schemeOrganizationTable,
        paymentPlanGroupsTable,
        paymentPlanRulesStorageTable,
        appKeyPairTable,
        appSessionTable,
        financialClaimsInformationStorageTable,
        logRecordTable
      ];
}

typedef $$FinancialClaimsInformationRequestTableTableCreateCompanionBuilder
    = FinancialClaimsInformationRequestTableCompanion Function({
  Value<int> id,
  required String oin,
  required DateTime dateTimeRequested,
  required String status,
  required bool lock,
});
typedef $$FinancialClaimsInformationRequestTableTableUpdateCompanionBuilder
    = FinancialClaimsInformationRequestTableCompanion Function({
  Value<int> id,
  Value<String> oin,
  Value<DateTime> dateTimeRequested,
  Value<String> status,
  Value<bool> lock,
});

class $$FinancialClaimsInformationRequestTableTableFilterComposer
    extends Composer<_$EncryptedDatabase,
        $FinancialClaimsInformationRequestTableTable> {
  $$FinancialClaimsInformationRequestTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnFilters(column));

  ColumnFilters<DateTime> get dateTimeRequested => $composableBuilder(
      column: $table.dateTimeRequested,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get status => $composableBuilder(
      column: $table.status, builder: (column) => ColumnFilters(column));

  ColumnFilters<bool> get lock => $composableBuilder(
      column: $table.lock, builder: (column) => ColumnFilters(column));
}

class $$FinancialClaimsInformationRequestTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase,
        $FinancialClaimsInformationRequestTableTable> {
  $$FinancialClaimsInformationRequestTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<DateTime> get dateTimeRequested => $composableBuilder(
      column: $table.dateTimeRequested,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get status => $composableBuilder(
      column: $table.status, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<bool> get lock => $composableBuilder(
      column: $table.lock, builder: (column) => ColumnOrderings(column));
}

class $$FinancialClaimsInformationRequestTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase,
        $FinancialClaimsInformationRequestTableTable> {
  $$FinancialClaimsInformationRequestTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<String> get oin =>
      $composableBuilder(column: $table.oin, builder: (column) => column);

  GeneratedColumn<DateTime> get dateTimeRequested => $composableBuilder(
      column: $table.dateTimeRequested, builder: (column) => column);

  GeneratedColumn<String> get status =>
      $composableBuilder(column: $table.status, builder: (column) => column);

  GeneratedColumn<bool> get lock =>
      $composableBuilder(column: $table.lock, builder: (column) => column);
}

class $$FinancialClaimsInformationRequestTableTableTableManager
    extends RootTableManager<
        _$EncryptedDatabase,
        $FinancialClaimsInformationRequestTableTable,
        FinancialClaimsInformationRequestDriftModel,
        $$FinancialClaimsInformationRequestTableTableFilterComposer,
        $$FinancialClaimsInformationRequestTableTableOrderingComposer,
        $$FinancialClaimsInformationRequestTableTableAnnotationComposer,
        $$FinancialClaimsInformationRequestTableTableCreateCompanionBuilder,
        $$FinancialClaimsInformationRequestTableTableUpdateCompanionBuilder,
        (
          FinancialClaimsInformationRequestDriftModel,
          BaseReferences<
              _$EncryptedDatabase,
              $FinancialClaimsInformationRequestTableTable,
              FinancialClaimsInformationRequestDriftModel>
        ),
        FinancialClaimsInformationRequestDriftModel,
        PrefetchHooks Function()> {
  $$FinancialClaimsInformationRequestTableTableTableManager(
      _$EncryptedDatabase db,
      $FinancialClaimsInformationRequestTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$FinancialClaimsInformationRequestTableTableFilterComposer(
                  $db: db, $table: table),
          createOrderingComposer: () =>
              $$FinancialClaimsInformationRequestTableTableOrderingComposer(
                  $db: db, $table: table),
          createComputedFieldComposer: () =>
              $$FinancialClaimsInformationRequestTableTableAnnotationComposer(
                  $db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String> oin = const Value.absent(),
            Value<DateTime> dateTimeRequested = const Value.absent(),
            Value<String> status = const Value.absent(),
            Value<bool> lock = const Value.absent(),
          }) =>
              FinancialClaimsInformationRequestTableCompanion(
            id: id,
            oin: oin,
            dateTimeRequested: dateTimeRequested,
            status: status,
            lock: lock,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required String oin,
            required DateTime dateTimeRequested,
            required String status,
            required bool lock,
          }) =>
              FinancialClaimsInformationRequestTableCompanion.insert(
            id: id,
            oin: oin,
            dateTimeRequested: dateTimeRequested,
            status: status,
            lock: lock,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$FinancialClaimsInformationRequestTableTableProcessedTableManager
    = ProcessedTableManager<
        _$EncryptedDatabase,
        $FinancialClaimsInformationRequestTableTable,
        FinancialClaimsInformationRequestDriftModel,
        $$FinancialClaimsInformationRequestTableTableFilterComposer,
        $$FinancialClaimsInformationRequestTableTableOrderingComposer,
        $$FinancialClaimsInformationRequestTableTableAnnotationComposer,
        $$FinancialClaimsInformationRequestTableTableCreateCompanionBuilder,
        $$FinancialClaimsInformationRequestTableTableUpdateCompanionBuilder,
        (
          FinancialClaimsInformationRequestDriftModel,
          BaseReferences<
              _$EncryptedDatabase,
              $FinancialClaimsInformationRequestTableTable,
              FinancialClaimsInformationRequestDriftModel>
        ),
        FinancialClaimsInformationRequestDriftModel,
        PrefetchHooks Function()>;
typedef $$FinancialClaimsInformationConfigurationTableTableCreateCompanionBuilder
    = FinancialClaimsInformationConfigurationTableCompanion Function({
  Value<int> id,
  required String oin,
  Value<String?> document,
  Value<String?> documentSignature,
  Value<String?> envelope,
  Value<String?> encryptedEnvelope,
  Value<String?> configuration,
  required bool expired,
});
typedef $$FinancialClaimsInformationConfigurationTableTableUpdateCompanionBuilder
    = FinancialClaimsInformationConfigurationTableCompanion Function({
  Value<int> id,
  Value<String> oin,
  Value<String?> document,
  Value<String?> documentSignature,
  Value<String?> envelope,
  Value<String?> encryptedEnvelope,
  Value<String?> configuration,
  Value<bool> expired,
});

class $$FinancialClaimsInformationConfigurationTableTableFilterComposer
    extends Composer<_$EncryptedDatabase,
        $FinancialClaimsInformationConfigurationTableTable> {
  $$FinancialClaimsInformationConfigurationTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get document => $composableBuilder(
      column: $table.document, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get documentSignature => $composableBuilder(
      column: $table.documentSignature,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get envelope => $composableBuilder(
      column: $table.envelope, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get encryptedEnvelope => $composableBuilder(
      column: $table.encryptedEnvelope,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get configuration => $composableBuilder(
      column: $table.configuration, builder: (column) => ColumnFilters(column));

  ColumnFilters<bool> get expired => $composableBuilder(
      column: $table.expired, builder: (column) => ColumnFilters(column));
}

class $$FinancialClaimsInformationConfigurationTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase,
        $FinancialClaimsInformationConfigurationTableTable> {
  $$FinancialClaimsInformationConfigurationTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get document => $composableBuilder(
      column: $table.document, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get documentSignature => $composableBuilder(
      column: $table.documentSignature,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get envelope => $composableBuilder(
      column: $table.envelope, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get encryptedEnvelope => $composableBuilder(
      column: $table.encryptedEnvelope,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get configuration => $composableBuilder(
      column: $table.configuration,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<bool> get expired => $composableBuilder(
      column: $table.expired, builder: (column) => ColumnOrderings(column));
}

class $$FinancialClaimsInformationConfigurationTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase,
        $FinancialClaimsInformationConfigurationTableTable> {
  $$FinancialClaimsInformationConfigurationTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<String> get oin =>
      $composableBuilder(column: $table.oin, builder: (column) => column);

  GeneratedColumn<String> get document =>
      $composableBuilder(column: $table.document, builder: (column) => column);

  GeneratedColumn<String> get documentSignature => $composableBuilder(
      column: $table.documentSignature, builder: (column) => column);

  GeneratedColumn<String> get envelope =>
      $composableBuilder(column: $table.envelope, builder: (column) => column);

  GeneratedColumn<String> get encryptedEnvelope => $composableBuilder(
      column: $table.encryptedEnvelope, builder: (column) => column);

  GeneratedColumn<String> get configuration => $composableBuilder(
      column: $table.configuration, builder: (column) => column);

  GeneratedColumn<bool> get expired =>
      $composableBuilder(column: $table.expired, builder: (column) => column);
}

class $$FinancialClaimsInformationConfigurationTableTableTableManager
    extends RootTableManager<
        _$EncryptedDatabase,
        $FinancialClaimsInformationConfigurationTableTable,
        FinancialClaimsInformationConfigurationDriftModel,
        $$FinancialClaimsInformationConfigurationTableTableFilterComposer,
        $$FinancialClaimsInformationConfigurationTableTableOrderingComposer,
        $$FinancialClaimsInformationConfigurationTableTableAnnotationComposer,
        $$FinancialClaimsInformationConfigurationTableTableCreateCompanionBuilder,
        $$FinancialClaimsInformationConfigurationTableTableUpdateCompanionBuilder,
        (
          FinancialClaimsInformationConfigurationDriftModel,
          BaseReferences<
              _$EncryptedDatabase,
              $FinancialClaimsInformationConfigurationTableTable,
              FinancialClaimsInformationConfigurationDriftModel>
        ),
        FinancialClaimsInformationConfigurationDriftModel,
        PrefetchHooks Function()> {
  $$FinancialClaimsInformationConfigurationTableTableTableManager(
      _$EncryptedDatabase db,
      $FinancialClaimsInformationConfigurationTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$FinancialClaimsInformationConfigurationTableTableFilterComposer(
                  $db: db, $table: table),
          createOrderingComposer: () =>
              $$FinancialClaimsInformationConfigurationTableTableOrderingComposer(
                  $db: db, $table: table),
          createComputedFieldComposer: () =>
              $$FinancialClaimsInformationConfigurationTableTableAnnotationComposer(
                  $db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String> oin = const Value.absent(),
            Value<String?> document = const Value.absent(),
            Value<String?> documentSignature = const Value.absent(),
            Value<String?> envelope = const Value.absent(),
            Value<String?> encryptedEnvelope = const Value.absent(),
            Value<String?> configuration = const Value.absent(),
            Value<bool> expired = const Value.absent(),
          }) =>
              FinancialClaimsInformationConfigurationTableCompanion(
            id: id,
            oin: oin,
            document: document,
            documentSignature: documentSignature,
            envelope: envelope,
            encryptedEnvelope: encryptedEnvelope,
            configuration: configuration,
            expired: expired,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required String oin,
            Value<String?> document = const Value.absent(),
            Value<String?> documentSignature = const Value.absent(),
            Value<String?> envelope = const Value.absent(),
            Value<String?> encryptedEnvelope = const Value.absent(),
            Value<String?> configuration = const Value.absent(),
            required bool expired,
          }) =>
              FinancialClaimsInformationConfigurationTableCompanion.insert(
            id: id,
            oin: oin,
            document: document,
            documentSignature: documentSignature,
            envelope: envelope,
            encryptedEnvelope: encryptedEnvelope,
            configuration: configuration,
            expired: expired,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$FinancialClaimsInformationConfigurationTableTableProcessedTableManager
    = ProcessedTableManager<
        _$EncryptedDatabase,
        $FinancialClaimsInformationConfigurationTableTable,
        FinancialClaimsInformationConfigurationDriftModel,
        $$FinancialClaimsInformationConfigurationTableTableFilterComposer,
        $$FinancialClaimsInformationConfigurationTableTableOrderingComposer,
        $$FinancialClaimsInformationConfigurationTableTableAnnotationComposer,
        $$FinancialClaimsInformationConfigurationTableTableCreateCompanionBuilder,
        $$FinancialClaimsInformationConfigurationTableTableUpdateCompanionBuilder,
        (
          FinancialClaimsInformationConfigurationDriftModel,
          BaseReferences<
              _$EncryptedDatabase,
              $FinancialClaimsInformationConfigurationTableTable,
              FinancialClaimsInformationConfigurationDriftModel>
        ),
        FinancialClaimsInformationConfigurationDriftModel,
        PrefetchHooks Function()>;
typedef $$FinancialClaimsInformationInboxTableTableCreateCompanionBuilder
    = FinancialClaimsInformationInboxTableCompanion Function({
  Value<int> id,
  required String oin,
  required DateTime dateTimeRequested,
  required DateTime dateTimeReceived,
  required String financialClaimsInformationDocument,
  required bool copiedToStorage,
});
typedef $$FinancialClaimsInformationInboxTableTableUpdateCompanionBuilder
    = FinancialClaimsInformationInboxTableCompanion Function({
  Value<int> id,
  Value<String> oin,
  Value<DateTime> dateTimeRequested,
  Value<DateTime> dateTimeReceived,
  Value<String> financialClaimsInformationDocument,
  Value<bool> copiedToStorage,
});

class $$FinancialClaimsInformationInboxTableTableFilterComposer
    extends Composer<_$EncryptedDatabase,
        $FinancialClaimsInformationInboxTableTable> {
  $$FinancialClaimsInformationInboxTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnFilters(column));

  ColumnFilters<DateTime> get dateTimeRequested => $composableBuilder(
      column: $table.dateTimeRequested,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<DateTime> get dateTimeReceived => $composableBuilder(
      column: $table.dateTimeReceived,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get financialClaimsInformationDocument =>
      $composableBuilder(
          column: $table.financialClaimsInformationDocument,
          builder: (column) => ColumnFilters(column));

  ColumnFilters<bool> get copiedToStorage => $composableBuilder(
      column: $table.copiedToStorage,
      builder: (column) => ColumnFilters(column));
}

class $$FinancialClaimsInformationInboxTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase,
        $FinancialClaimsInformationInboxTableTable> {
  $$FinancialClaimsInformationInboxTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<DateTime> get dateTimeRequested => $composableBuilder(
      column: $table.dateTimeRequested,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<DateTime> get dateTimeReceived => $composableBuilder(
      column: $table.dateTimeReceived,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get financialClaimsInformationDocument =>
      $composableBuilder(
          column: $table.financialClaimsInformationDocument,
          builder: (column) => ColumnOrderings(column));

  ColumnOrderings<bool> get copiedToStorage => $composableBuilder(
      column: $table.copiedToStorage,
      builder: (column) => ColumnOrderings(column));
}

class $$FinancialClaimsInformationInboxTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase,
        $FinancialClaimsInformationInboxTableTable> {
  $$FinancialClaimsInformationInboxTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<String> get oin =>
      $composableBuilder(column: $table.oin, builder: (column) => column);

  GeneratedColumn<DateTime> get dateTimeRequested => $composableBuilder(
      column: $table.dateTimeRequested, builder: (column) => column);

  GeneratedColumn<DateTime> get dateTimeReceived => $composableBuilder(
      column: $table.dateTimeReceived, builder: (column) => column);

  GeneratedColumn<String> get financialClaimsInformationDocument =>
      $composableBuilder(
          column: $table.financialClaimsInformationDocument,
          builder: (column) => column);

  GeneratedColumn<bool> get copiedToStorage => $composableBuilder(
      column: $table.copiedToStorage, builder: (column) => column);
}

class $$FinancialClaimsInformationInboxTableTableTableManager
    extends RootTableManager<
        _$EncryptedDatabase,
        $FinancialClaimsInformationInboxTableTable,
        FinancialClaimsInformationInboxDriftModel,
        $$FinancialClaimsInformationInboxTableTableFilterComposer,
        $$FinancialClaimsInformationInboxTableTableOrderingComposer,
        $$FinancialClaimsInformationInboxTableTableAnnotationComposer,
        $$FinancialClaimsInformationInboxTableTableCreateCompanionBuilder,
        $$FinancialClaimsInformationInboxTableTableUpdateCompanionBuilder,
        (
          FinancialClaimsInformationInboxDriftModel,
          BaseReferences<
              _$EncryptedDatabase,
              $FinancialClaimsInformationInboxTableTable,
              FinancialClaimsInformationInboxDriftModel>
        ),
        FinancialClaimsInformationInboxDriftModel,
        PrefetchHooks Function()> {
  $$FinancialClaimsInformationInboxTableTableTableManager(
      _$EncryptedDatabase db, $FinancialClaimsInformationInboxTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$FinancialClaimsInformationInboxTableTableFilterComposer(
                  $db: db, $table: table),
          createOrderingComposer: () =>
              $$FinancialClaimsInformationInboxTableTableOrderingComposer(
                  $db: db, $table: table),
          createComputedFieldComposer: () =>
              $$FinancialClaimsInformationInboxTableTableAnnotationComposer(
                  $db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String> oin = const Value.absent(),
            Value<DateTime> dateTimeRequested = const Value.absent(),
            Value<DateTime> dateTimeReceived = const Value.absent(),
            Value<String> financialClaimsInformationDocument =
                const Value.absent(),
            Value<bool> copiedToStorage = const Value.absent(),
          }) =>
              FinancialClaimsInformationInboxTableCompanion(
            id: id,
            oin: oin,
            dateTimeRequested: dateTimeRequested,
            dateTimeReceived: dateTimeReceived,
            financialClaimsInformationDocument:
                financialClaimsInformationDocument,
            copiedToStorage: copiedToStorage,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required String oin,
            required DateTime dateTimeRequested,
            required DateTime dateTimeReceived,
            required String financialClaimsInformationDocument,
            required bool copiedToStorage,
          }) =>
              FinancialClaimsInformationInboxTableCompanion.insert(
            id: id,
            oin: oin,
            dateTimeRequested: dateTimeRequested,
            dateTimeReceived: dateTimeReceived,
            financialClaimsInformationDocument:
                financialClaimsInformationDocument,
            copiedToStorage: copiedToStorage,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$FinancialClaimsInformationInboxTableTableProcessedTableManager
    = ProcessedTableManager<
        _$EncryptedDatabase,
        $FinancialClaimsInformationInboxTableTable,
        FinancialClaimsInformationInboxDriftModel,
        $$FinancialClaimsInformationInboxTableTableFilterComposer,
        $$FinancialClaimsInformationInboxTableTableOrderingComposer,
        $$FinancialClaimsInformationInboxTableTableAnnotationComposer,
        $$FinancialClaimsInformationInboxTableTableCreateCompanionBuilder,
        $$FinancialClaimsInformationInboxTableTableUpdateCompanionBuilder,
        (
          FinancialClaimsInformationInboxDriftModel,
          BaseReferences<
              _$EncryptedDatabase,
              $FinancialClaimsInformationInboxTableTable,
              FinancialClaimsInformationInboxDriftModel>
        ),
        FinancialClaimsInformationInboxDriftModel,
        PrefetchHooks Function()>;
typedef $$FinancialClaimsInformationReceivedTableTableCreateCompanionBuilder
    = FinancialClaimsInformationReceivedTableCompanion Function({
  Value<int> id,
  required String oin,
  required DateTime dateTimeRequested,
  required DateTime dateTimeReceived,
  required String encryptedFinancialClaimsInformationDocument,
  Value<String?> financialClaimsInformationDocument,
  Value<bool?> changed,
  required bool copiedToInbox,
});
typedef $$FinancialClaimsInformationReceivedTableTableUpdateCompanionBuilder
    = FinancialClaimsInformationReceivedTableCompanion Function({
  Value<int> id,
  Value<String> oin,
  Value<DateTime> dateTimeRequested,
  Value<DateTime> dateTimeReceived,
  Value<String> encryptedFinancialClaimsInformationDocument,
  Value<String?> financialClaimsInformationDocument,
  Value<bool?> changed,
  Value<bool> copiedToInbox,
});

class $$FinancialClaimsInformationReceivedTableTableFilterComposer
    extends Composer<_$EncryptedDatabase,
        $FinancialClaimsInformationReceivedTableTable> {
  $$FinancialClaimsInformationReceivedTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnFilters(column));

  ColumnFilters<DateTime> get dateTimeRequested => $composableBuilder(
      column: $table.dateTimeRequested,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<DateTime> get dateTimeReceived => $composableBuilder(
      column: $table.dateTimeReceived,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get encryptedFinancialClaimsInformationDocument =>
      $composableBuilder(
          column: $table.encryptedFinancialClaimsInformationDocument,
          builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get financialClaimsInformationDocument =>
      $composableBuilder(
          column: $table.financialClaimsInformationDocument,
          builder: (column) => ColumnFilters(column));

  ColumnFilters<bool> get changed => $composableBuilder(
      column: $table.changed, builder: (column) => ColumnFilters(column));

  ColumnFilters<bool> get copiedToInbox => $composableBuilder(
      column: $table.copiedToInbox, builder: (column) => ColumnFilters(column));
}

class $$FinancialClaimsInformationReceivedTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase,
        $FinancialClaimsInformationReceivedTableTable> {
  $$FinancialClaimsInformationReceivedTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<DateTime> get dateTimeRequested => $composableBuilder(
      column: $table.dateTimeRequested,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<DateTime> get dateTimeReceived => $composableBuilder(
      column: $table.dateTimeReceived,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get encryptedFinancialClaimsInformationDocument =>
      $composableBuilder(
          column: $table.encryptedFinancialClaimsInformationDocument,
          builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get financialClaimsInformationDocument =>
      $composableBuilder(
          column: $table.financialClaimsInformationDocument,
          builder: (column) => ColumnOrderings(column));

  ColumnOrderings<bool> get changed => $composableBuilder(
      column: $table.changed, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<bool> get copiedToInbox => $composableBuilder(
      column: $table.copiedToInbox,
      builder: (column) => ColumnOrderings(column));
}

class $$FinancialClaimsInformationReceivedTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase,
        $FinancialClaimsInformationReceivedTableTable> {
  $$FinancialClaimsInformationReceivedTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<String> get oin =>
      $composableBuilder(column: $table.oin, builder: (column) => column);

  GeneratedColumn<DateTime> get dateTimeRequested => $composableBuilder(
      column: $table.dateTimeRequested, builder: (column) => column);

  GeneratedColumn<DateTime> get dateTimeReceived => $composableBuilder(
      column: $table.dateTimeReceived, builder: (column) => column);

  GeneratedColumn<String> get encryptedFinancialClaimsInformationDocument =>
      $composableBuilder(
          column: $table.encryptedFinancialClaimsInformationDocument,
          builder: (column) => column);

  GeneratedColumn<String> get financialClaimsInformationDocument =>
      $composableBuilder(
          column: $table.financialClaimsInformationDocument,
          builder: (column) => column);

  GeneratedColumn<bool> get changed =>
      $composableBuilder(column: $table.changed, builder: (column) => column);

  GeneratedColumn<bool> get copiedToInbox => $composableBuilder(
      column: $table.copiedToInbox, builder: (column) => column);
}

class $$FinancialClaimsInformationReceivedTableTableTableManager
    extends RootTableManager<
        _$EncryptedDatabase,
        $FinancialClaimsInformationReceivedTableTable,
        FinancialClaimsInformationReceivedDriftModel,
        $$FinancialClaimsInformationReceivedTableTableFilterComposer,
        $$FinancialClaimsInformationReceivedTableTableOrderingComposer,
        $$FinancialClaimsInformationReceivedTableTableAnnotationComposer,
        $$FinancialClaimsInformationReceivedTableTableCreateCompanionBuilder,
        $$FinancialClaimsInformationReceivedTableTableUpdateCompanionBuilder,
        (
          FinancialClaimsInformationReceivedDriftModel,
          BaseReferences<
              _$EncryptedDatabase,
              $FinancialClaimsInformationReceivedTableTable,
              FinancialClaimsInformationReceivedDriftModel>
        ),
        FinancialClaimsInformationReceivedDriftModel,
        PrefetchHooks Function()> {
  $$FinancialClaimsInformationReceivedTableTableTableManager(
      _$EncryptedDatabase db,
      $FinancialClaimsInformationReceivedTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$FinancialClaimsInformationReceivedTableTableFilterComposer(
                  $db: db, $table: table),
          createOrderingComposer: () =>
              $$FinancialClaimsInformationReceivedTableTableOrderingComposer(
                  $db: db, $table: table),
          createComputedFieldComposer: () =>
              $$FinancialClaimsInformationReceivedTableTableAnnotationComposer(
                  $db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String> oin = const Value.absent(),
            Value<DateTime> dateTimeRequested = const Value.absent(),
            Value<DateTime> dateTimeReceived = const Value.absent(),
            Value<String> encryptedFinancialClaimsInformationDocument =
                const Value.absent(),
            Value<String?> financialClaimsInformationDocument =
                const Value.absent(),
            Value<bool?> changed = const Value.absent(),
            Value<bool> copiedToInbox = const Value.absent(),
          }) =>
              FinancialClaimsInformationReceivedTableCompanion(
            id: id,
            oin: oin,
            dateTimeRequested: dateTimeRequested,
            dateTimeReceived: dateTimeReceived,
            encryptedFinancialClaimsInformationDocument:
                encryptedFinancialClaimsInformationDocument,
            financialClaimsInformationDocument:
                financialClaimsInformationDocument,
            changed: changed,
            copiedToInbox: copiedToInbox,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required String oin,
            required DateTime dateTimeRequested,
            required DateTime dateTimeReceived,
            required String encryptedFinancialClaimsInformationDocument,
            Value<String?> financialClaimsInformationDocument =
                const Value.absent(),
            Value<bool?> changed = const Value.absent(),
            required bool copiedToInbox,
          }) =>
              FinancialClaimsInformationReceivedTableCompanion.insert(
            id: id,
            oin: oin,
            dateTimeRequested: dateTimeRequested,
            dateTimeReceived: dateTimeReceived,
            encryptedFinancialClaimsInformationDocument:
                encryptedFinancialClaimsInformationDocument,
            financialClaimsInformationDocument:
                financialClaimsInformationDocument,
            changed: changed,
            copiedToInbox: copiedToInbox,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$FinancialClaimsInformationReceivedTableTableProcessedTableManager
    = ProcessedTableManager<
        _$EncryptedDatabase,
        $FinancialClaimsInformationReceivedTableTable,
        FinancialClaimsInformationReceivedDriftModel,
        $$FinancialClaimsInformationReceivedTableTableFilterComposer,
        $$FinancialClaimsInformationReceivedTableTableOrderingComposer,
        $$FinancialClaimsInformationReceivedTableTableAnnotationComposer,
        $$FinancialClaimsInformationReceivedTableTableCreateCompanionBuilder,
        $$FinancialClaimsInformationReceivedTableTableUpdateCompanionBuilder,
        (
          FinancialClaimsInformationReceivedDriftModel,
          BaseReferences<
              _$EncryptedDatabase,
              $FinancialClaimsInformationReceivedTableTable,
              FinancialClaimsInformationReceivedDriftModel>
        ),
        FinancialClaimsInformationReceivedDriftModel,
        PrefetchHooks Function()>;
typedef $$RegistrationTableTableCreateCompanionBuilder
    = RegistrationTableCompanion Function({
  Value<int> id,
  required DateTime dateTimeStarted,
  required String appPublicKey,
  required String appManagerOin,
  required String appManagerPublicKey,
  required String registrationToken,
  Value<DateTime?> dateTimeCompleted,
  Value<String?> givenName,
  required bool expired,
  required bool revoked,
});
typedef $$RegistrationTableTableUpdateCompanionBuilder
    = RegistrationTableCompanion Function({
  Value<int> id,
  Value<DateTime> dateTimeStarted,
  Value<String> appPublicKey,
  Value<String> appManagerOin,
  Value<String> appManagerPublicKey,
  Value<String> registrationToken,
  Value<DateTime?> dateTimeCompleted,
  Value<String?> givenName,
  Value<bool> expired,
  Value<bool> revoked,
});

class $$RegistrationTableTableFilterComposer
    extends Composer<_$EncryptedDatabase, $RegistrationTableTable> {
  $$RegistrationTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<DateTime> get dateTimeStarted => $composableBuilder(
      column: $table.dateTimeStarted,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get appPublicKey => $composableBuilder(
      column: $table.appPublicKey, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get appManagerOin => $composableBuilder(
      column: $table.appManagerOin, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get appManagerPublicKey => $composableBuilder(
      column: $table.appManagerPublicKey,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get registrationToken => $composableBuilder(
      column: $table.registrationToken,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<DateTime> get dateTimeCompleted => $composableBuilder(
      column: $table.dateTimeCompleted,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get givenName => $composableBuilder(
      column: $table.givenName, builder: (column) => ColumnFilters(column));

  ColumnFilters<bool> get expired => $composableBuilder(
      column: $table.expired, builder: (column) => ColumnFilters(column));

  ColumnFilters<bool> get revoked => $composableBuilder(
      column: $table.revoked, builder: (column) => ColumnFilters(column));
}

class $$RegistrationTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase, $RegistrationTableTable> {
  $$RegistrationTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<DateTime> get dateTimeStarted => $composableBuilder(
      column: $table.dateTimeStarted,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get appPublicKey => $composableBuilder(
      column: $table.appPublicKey,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get appManagerOin => $composableBuilder(
      column: $table.appManagerOin,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get appManagerPublicKey => $composableBuilder(
      column: $table.appManagerPublicKey,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get registrationToken => $composableBuilder(
      column: $table.registrationToken,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<DateTime> get dateTimeCompleted => $composableBuilder(
      column: $table.dateTimeCompleted,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get givenName => $composableBuilder(
      column: $table.givenName, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<bool> get expired => $composableBuilder(
      column: $table.expired, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<bool> get revoked => $composableBuilder(
      column: $table.revoked, builder: (column) => ColumnOrderings(column));
}

class $$RegistrationTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase, $RegistrationTableTable> {
  $$RegistrationTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<DateTime> get dateTimeStarted => $composableBuilder(
      column: $table.dateTimeStarted, builder: (column) => column);

  GeneratedColumn<String> get appPublicKey => $composableBuilder(
      column: $table.appPublicKey, builder: (column) => column);

  GeneratedColumn<String> get appManagerOin => $composableBuilder(
      column: $table.appManagerOin, builder: (column) => column);

  GeneratedColumn<String> get appManagerPublicKey => $composableBuilder(
      column: $table.appManagerPublicKey, builder: (column) => column);

  GeneratedColumn<String> get registrationToken => $composableBuilder(
      column: $table.registrationToken, builder: (column) => column);

  GeneratedColumn<DateTime> get dateTimeCompleted => $composableBuilder(
      column: $table.dateTimeCompleted, builder: (column) => column);

  GeneratedColumn<String> get givenName =>
      $composableBuilder(column: $table.givenName, builder: (column) => column);

  GeneratedColumn<bool> get expired =>
      $composableBuilder(column: $table.expired, builder: (column) => column);

  GeneratedColumn<bool> get revoked =>
      $composableBuilder(column: $table.revoked, builder: (column) => column);
}

class $$RegistrationTableTableTableManager extends RootTableManager<
    _$EncryptedDatabase,
    $RegistrationTableTable,
    RegistrationDriftModel,
    $$RegistrationTableTableFilterComposer,
    $$RegistrationTableTableOrderingComposer,
    $$RegistrationTableTableAnnotationComposer,
    $$RegistrationTableTableCreateCompanionBuilder,
    $$RegistrationTableTableUpdateCompanionBuilder,
    (
      RegistrationDriftModel,
      BaseReferences<_$EncryptedDatabase, $RegistrationTableTable,
          RegistrationDriftModel>
    ),
    RegistrationDriftModel,
    PrefetchHooks Function()> {
  $$RegistrationTableTableTableManager(
      _$EncryptedDatabase db, $RegistrationTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$RegistrationTableTableFilterComposer($db: db, $table: table),
          createOrderingComposer: () =>
              $$RegistrationTableTableOrderingComposer($db: db, $table: table),
          createComputedFieldComposer: () =>
              $$RegistrationTableTableAnnotationComposer(
                  $db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<DateTime> dateTimeStarted = const Value.absent(),
            Value<String> appPublicKey = const Value.absent(),
            Value<String> appManagerOin = const Value.absent(),
            Value<String> appManagerPublicKey = const Value.absent(),
            Value<String> registrationToken = const Value.absent(),
            Value<DateTime?> dateTimeCompleted = const Value.absent(),
            Value<String?> givenName = const Value.absent(),
            Value<bool> expired = const Value.absent(),
            Value<bool> revoked = const Value.absent(),
          }) =>
              RegistrationTableCompanion(
            id: id,
            dateTimeStarted: dateTimeStarted,
            appPublicKey: appPublicKey,
            appManagerOin: appManagerOin,
            appManagerPublicKey: appManagerPublicKey,
            registrationToken: registrationToken,
            dateTimeCompleted: dateTimeCompleted,
            givenName: givenName,
            expired: expired,
            revoked: revoked,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required DateTime dateTimeStarted,
            required String appPublicKey,
            required String appManagerOin,
            required String appManagerPublicKey,
            required String registrationToken,
            Value<DateTime?> dateTimeCompleted = const Value.absent(),
            Value<String?> givenName = const Value.absent(),
            required bool expired,
            required bool revoked,
          }) =>
              RegistrationTableCompanion.insert(
            id: id,
            dateTimeStarted: dateTimeStarted,
            appPublicKey: appPublicKey,
            appManagerOin: appManagerOin,
            appManagerPublicKey: appManagerPublicKey,
            registrationToken: registrationToken,
            dateTimeCompleted: dateTimeCompleted,
            givenName: givenName,
            expired: expired,
            revoked: revoked,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$RegistrationTableTableProcessedTableManager = ProcessedTableManager<
    _$EncryptedDatabase,
    $RegistrationTableTable,
    RegistrationDriftModel,
    $$RegistrationTableTableFilterComposer,
    $$RegistrationTableTableOrderingComposer,
    $$RegistrationTableTableAnnotationComposer,
    $$RegistrationTableTableCreateCompanionBuilder,
    $$RegistrationTableTableUpdateCompanionBuilder,
    (
      RegistrationDriftModel,
      BaseReferences<_$EncryptedDatabase, $RegistrationTableTable,
          RegistrationDriftModel>
    ),
    RegistrationDriftModel,
    PrefetchHooks Function()>;
typedef $$CertificateTableTableCreateCompanionBuilder
    = CertificateTableCompanion Function({
  Value<int> id,
  required String type,
  required Uint8List value,
  required String bsn,
  required String givenName,
  required DateTime expiresAt,
  required bool deemedExpiredBySourceOrganization,
  required String scope,
});
typedef $$CertificateTableTableUpdateCompanionBuilder
    = CertificateTableCompanion Function({
  Value<int> id,
  Value<String> type,
  Value<Uint8List> value,
  Value<String> bsn,
  Value<String> givenName,
  Value<DateTime> expiresAt,
  Value<bool> deemedExpiredBySourceOrganization,
  Value<String> scope,
});

class $$CertificateTableTableFilterComposer
    extends Composer<_$EncryptedDatabase, $CertificateTableTable> {
  $$CertificateTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get type => $composableBuilder(
      column: $table.type, builder: (column) => ColumnFilters(column));

  ColumnFilters<Uint8List> get value => $composableBuilder(
      column: $table.value, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get bsn => $composableBuilder(
      column: $table.bsn, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get givenName => $composableBuilder(
      column: $table.givenName, builder: (column) => ColumnFilters(column));

  ColumnFilters<DateTime> get expiresAt => $composableBuilder(
      column: $table.expiresAt, builder: (column) => ColumnFilters(column));

  ColumnFilters<bool> get deemedExpiredBySourceOrganization =>
      $composableBuilder(
          column: $table.deemedExpiredBySourceOrganization,
          builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get scope => $composableBuilder(
      column: $table.scope, builder: (column) => ColumnFilters(column));
}

class $$CertificateTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase, $CertificateTableTable> {
  $$CertificateTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get type => $composableBuilder(
      column: $table.type, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<Uint8List> get value => $composableBuilder(
      column: $table.value, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get bsn => $composableBuilder(
      column: $table.bsn, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get givenName => $composableBuilder(
      column: $table.givenName, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<DateTime> get expiresAt => $composableBuilder(
      column: $table.expiresAt, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<bool> get deemedExpiredBySourceOrganization =>
      $composableBuilder(
          column: $table.deemedExpiredBySourceOrganization,
          builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get scope => $composableBuilder(
      column: $table.scope, builder: (column) => ColumnOrderings(column));
}

class $$CertificateTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase, $CertificateTableTable> {
  $$CertificateTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<String> get type =>
      $composableBuilder(column: $table.type, builder: (column) => column);

  GeneratedColumn<Uint8List> get value =>
      $composableBuilder(column: $table.value, builder: (column) => column);

  GeneratedColumn<String> get bsn =>
      $composableBuilder(column: $table.bsn, builder: (column) => column);

  GeneratedColumn<String> get givenName =>
      $composableBuilder(column: $table.givenName, builder: (column) => column);

  GeneratedColumn<DateTime> get expiresAt =>
      $composableBuilder(column: $table.expiresAt, builder: (column) => column);

  GeneratedColumn<bool> get deemedExpiredBySourceOrganization =>
      $composableBuilder(
          column: $table.deemedExpiredBySourceOrganization,
          builder: (column) => column);

  GeneratedColumn<String> get scope =>
      $composableBuilder(column: $table.scope, builder: (column) => column);
}

class $$CertificateTableTableTableManager extends RootTableManager<
    _$EncryptedDatabase,
    $CertificateTableTable,
    CertificateDriftModel,
    $$CertificateTableTableFilterComposer,
    $$CertificateTableTableOrderingComposer,
    $$CertificateTableTableAnnotationComposer,
    $$CertificateTableTableCreateCompanionBuilder,
    $$CertificateTableTableUpdateCompanionBuilder,
    (
      CertificateDriftModel,
      BaseReferences<_$EncryptedDatabase, $CertificateTableTable,
          CertificateDriftModel>
    ),
    CertificateDriftModel,
    PrefetchHooks Function()> {
  $$CertificateTableTableTableManager(
      _$EncryptedDatabase db, $CertificateTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$CertificateTableTableFilterComposer($db: db, $table: table),
          createOrderingComposer: () =>
              $$CertificateTableTableOrderingComposer($db: db, $table: table),
          createComputedFieldComposer: () =>
              $$CertificateTableTableAnnotationComposer($db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String> type = const Value.absent(),
            Value<Uint8List> value = const Value.absent(),
            Value<String> bsn = const Value.absent(),
            Value<String> givenName = const Value.absent(),
            Value<DateTime> expiresAt = const Value.absent(),
            Value<bool> deemedExpiredBySourceOrganization =
                const Value.absent(),
            Value<String> scope = const Value.absent(),
          }) =>
              CertificateTableCompanion(
            id: id,
            type: type,
            value: value,
            bsn: bsn,
            givenName: givenName,
            expiresAt: expiresAt,
            deemedExpiredBySourceOrganization:
                deemedExpiredBySourceOrganization,
            scope: scope,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required String type,
            required Uint8List value,
            required String bsn,
            required String givenName,
            required DateTime expiresAt,
            required bool deemedExpiredBySourceOrganization,
            required String scope,
          }) =>
              CertificateTableCompanion.insert(
            id: id,
            type: type,
            value: value,
            bsn: bsn,
            givenName: givenName,
            expiresAt: expiresAt,
            deemedExpiredBySourceOrganization:
                deemedExpiredBySourceOrganization,
            scope: scope,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$CertificateTableTableProcessedTableManager = ProcessedTableManager<
    _$EncryptedDatabase,
    $CertificateTableTable,
    CertificateDriftModel,
    $$CertificateTableTableFilterComposer,
    $$CertificateTableTableOrderingComposer,
    $$CertificateTableTableAnnotationComposer,
    $$CertificateTableTableCreateCompanionBuilder,
    $$CertificateTableTableUpdateCompanionBuilder,
    (
      CertificateDriftModel,
      BaseReferences<_$EncryptedDatabase, $CertificateTableTable,
          CertificateDriftModel>
    ),
    CertificateDriftModel,
    PrefetchHooks Function()>;
typedef $$OrganizationSelectionTableTableCreateCompanionBuilder
    = OrganizationSelectionTableCompanion Function({
  Value<int> id,
  required String oin,
  required bool selected,
});
typedef $$OrganizationSelectionTableTableUpdateCompanionBuilder
    = OrganizationSelectionTableCompanion Function({
  Value<int> id,
  Value<String> oin,
  Value<bool> selected,
});

class $$OrganizationSelectionTableTableFilterComposer
    extends Composer<_$EncryptedDatabase, $OrganizationSelectionTableTable> {
  $$OrganizationSelectionTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnFilters(column));

  ColumnFilters<bool> get selected => $composableBuilder(
      column: $table.selected, builder: (column) => ColumnFilters(column));
}

class $$OrganizationSelectionTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase, $OrganizationSelectionTableTable> {
  $$OrganizationSelectionTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<bool> get selected => $composableBuilder(
      column: $table.selected, builder: (column) => ColumnOrderings(column));
}

class $$OrganizationSelectionTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase, $OrganizationSelectionTableTable> {
  $$OrganizationSelectionTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<String> get oin =>
      $composableBuilder(column: $table.oin, builder: (column) => column);

  GeneratedColumn<bool> get selected =>
      $composableBuilder(column: $table.selected, builder: (column) => column);
}

class $$OrganizationSelectionTableTableTableManager extends RootTableManager<
    _$EncryptedDatabase,
    $OrganizationSelectionTableTable,
    OrganizationSelectionDriftModel,
    $$OrganizationSelectionTableTableFilterComposer,
    $$OrganizationSelectionTableTableOrderingComposer,
    $$OrganizationSelectionTableTableAnnotationComposer,
    $$OrganizationSelectionTableTableCreateCompanionBuilder,
    $$OrganizationSelectionTableTableUpdateCompanionBuilder,
    (
      OrganizationSelectionDriftModel,
      BaseReferences<_$EncryptedDatabase, $OrganizationSelectionTableTable,
          OrganizationSelectionDriftModel>
    ),
    OrganizationSelectionDriftModel,
    PrefetchHooks Function()> {
  $$OrganizationSelectionTableTableTableManager(
      _$EncryptedDatabase db, $OrganizationSelectionTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$OrganizationSelectionTableTableFilterComposer(
                  $db: db, $table: table),
          createOrderingComposer: () =>
              $$OrganizationSelectionTableTableOrderingComposer(
                  $db: db, $table: table),
          createComputedFieldComposer: () =>
              $$OrganizationSelectionTableTableAnnotationComposer(
                  $db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String> oin = const Value.absent(),
            Value<bool> selected = const Value.absent(),
          }) =>
              OrganizationSelectionTableCompanion(
            id: id,
            oin: oin,
            selected: selected,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required String oin,
            required bool selected,
          }) =>
              OrganizationSelectionTableCompanion.insert(
            id: id,
            oin: oin,
            selected: selected,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$OrganizationSelectionTableTableProcessedTableManager
    = ProcessedTableManager<
        _$EncryptedDatabase,
        $OrganizationSelectionTableTable,
        OrganizationSelectionDriftModel,
        $$OrganizationSelectionTableTableFilterComposer,
        $$OrganizationSelectionTableTableOrderingComposer,
        $$OrganizationSelectionTableTableAnnotationComposer,
        $$OrganizationSelectionTableTableCreateCompanionBuilder,
        $$OrganizationSelectionTableTableUpdateCompanionBuilder,
        (
          OrganizationSelectionDriftModel,
          BaseReferences<_$EncryptedDatabase, $OrganizationSelectionTableTable,
              OrganizationSelectionDriftModel>
        ),
        OrganizationSelectionDriftModel,
        PrefetchHooks Function()>;
typedef $$AppManagerSelectionTableTableCreateCompanionBuilder
    = AppManagerSelectionTableCompanion Function({
  Value<int> id,
  required String oin,
});
typedef $$AppManagerSelectionTableTableUpdateCompanionBuilder
    = AppManagerSelectionTableCompanion Function({
  Value<int> id,
  Value<String> oin,
});

class $$AppManagerSelectionTableTableFilterComposer
    extends Composer<_$EncryptedDatabase, $AppManagerSelectionTableTable> {
  $$AppManagerSelectionTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnFilters(column));
}

class $$AppManagerSelectionTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase, $AppManagerSelectionTableTable> {
  $$AppManagerSelectionTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnOrderings(column));
}

class $$AppManagerSelectionTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase, $AppManagerSelectionTableTable> {
  $$AppManagerSelectionTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<String> get oin =>
      $composableBuilder(column: $table.oin, builder: (column) => column);
}

class $$AppManagerSelectionTableTableTableManager extends RootTableManager<
    _$EncryptedDatabase,
    $AppManagerSelectionTableTable,
    AppManagerSelectionDriftModel,
    $$AppManagerSelectionTableTableFilterComposer,
    $$AppManagerSelectionTableTableOrderingComposer,
    $$AppManagerSelectionTableTableAnnotationComposer,
    $$AppManagerSelectionTableTableCreateCompanionBuilder,
    $$AppManagerSelectionTableTableUpdateCompanionBuilder,
    (
      AppManagerSelectionDriftModel,
      BaseReferences<_$EncryptedDatabase, $AppManagerSelectionTableTable,
          AppManagerSelectionDriftModel>
    ),
    AppManagerSelectionDriftModel,
    PrefetchHooks Function()> {
  $$AppManagerSelectionTableTableTableManager(
      _$EncryptedDatabase db, $AppManagerSelectionTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$AppManagerSelectionTableTableFilterComposer(
                  $db: db, $table: table),
          createOrderingComposer: () =>
              $$AppManagerSelectionTableTableOrderingComposer(
                  $db: db, $table: table),
          createComputedFieldComposer: () =>
              $$AppManagerSelectionTableTableAnnotationComposer(
                  $db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String> oin = const Value.absent(),
          }) =>
              AppManagerSelectionTableCompanion(
            id: id,
            oin: oin,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required String oin,
          }) =>
              AppManagerSelectionTableCompanion.insert(
            id: id,
            oin: oin,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$AppManagerSelectionTableTableProcessedTableManager
    = ProcessedTableManager<
        _$EncryptedDatabase,
        $AppManagerSelectionTableTable,
        AppManagerSelectionDriftModel,
        $$AppManagerSelectionTableTableFilterComposer,
        $$AppManagerSelectionTableTableOrderingComposer,
        $$AppManagerSelectionTableTableAnnotationComposer,
        $$AppManagerSelectionTableTableCreateCompanionBuilder,
        $$AppManagerSelectionTableTableUpdateCompanionBuilder,
        (
          AppManagerSelectionDriftModel,
          BaseReferences<_$EncryptedDatabase, $AppManagerSelectionTableTable,
              AppManagerSelectionDriftModel>
        ),
        AppManagerSelectionDriftModel,
        PrefetchHooks Function()>;
typedef $$SchemeAppManagerTableTableCreateCompanionBuilder
    = SchemeAppManagerTableCompanion Function({
  Value<int> id,
  required String oin,
  required String name,
  required String publicKey,
  required String discoveryUrl,
  required bool available,
});
typedef $$SchemeAppManagerTableTableUpdateCompanionBuilder
    = SchemeAppManagerTableCompanion Function({
  Value<int> id,
  Value<String> oin,
  Value<String> name,
  Value<String> publicKey,
  Value<String> discoveryUrl,
  Value<bool> available,
});

class $$SchemeAppManagerTableTableFilterComposer
    extends Composer<_$EncryptedDatabase, $SchemeAppManagerTableTable> {
  $$SchemeAppManagerTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get name => $composableBuilder(
      column: $table.name, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get publicKey => $composableBuilder(
      column: $table.publicKey, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get discoveryUrl => $composableBuilder(
      column: $table.discoveryUrl, builder: (column) => ColumnFilters(column));

  ColumnFilters<bool> get available => $composableBuilder(
      column: $table.available, builder: (column) => ColumnFilters(column));
}

class $$SchemeAppManagerTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase, $SchemeAppManagerTableTable> {
  $$SchemeAppManagerTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get name => $composableBuilder(
      column: $table.name, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get publicKey => $composableBuilder(
      column: $table.publicKey, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get discoveryUrl => $composableBuilder(
      column: $table.discoveryUrl,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<bool> get available => $composableBuilder(
      column: $table.available, builder: (column) => ColumnOrderings(column));
}

class $$SchemeAppManagerTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase, $SchemeAppManagerTableTable> {
  $$SchemeAppManagerTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<String> get oin =>
      $composableBuilder(column: $table.oin, builder: (column) => column);

  GeneratedColumn<String> get name =>
      $composableBuilder(column: $table.name, builder: (column) => column);

  GeneratedColumn<String> get publicKey =>
      $composableBuilder(column: $table.publicKey, builder: (column) => column);

  GeneratedColumn<String> get discoveryUrl => $composableBuilder(
      column: $table.discoveryUrl, builder: (column) => column);

  GeneratedColumn<bool> get available =>
      $composableBuilder(column: $table.available, builder: (column) => column);
}

class $$SchemeAppManagerTableTableTableManager extends RootTableManager<
    _$EncryptedDatabase,
    $SchemeAppManagerTableTable,
    SchemeAppManagerDriftModel,
    $$SchemeAppManagerTableTableFilterComposer,
    $$SchemeAppManagerTableTableOrderingComposer,
    $$SchemeAppManagerTableTableAnnotationComposer,
    $$SchemeAppManagerTableTableCreateCompanionBuilder,
    $$SchemeAppManagerTableTableUpdateCompanionBuilder,
    (
      SchemeAppManagerDriftModel,
      BaseReferences<_$EncryptedDatabase, $SchemeAppManagerTableTable,
          SchemeAppManagerDriftModel>
    ),
    SchemeAppManagerDriftModel,
    PrefetchHooks Function()> {
  $$SchemeAppManagerTableTableTableManager(
      _$EncryptedDatabase db, $SchemeAppManagerTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$SchemeAppManagerTableTableFilterComposer(
                  $db: db, $table: table),
          createOrderingComposer: () =>
              $$SchemeAppManagerTableTableOrderingComposer(
                  $db: db, $table: table),
          createComputedFieldComposer: () =>
              $$SchemeAppManagerTableTableAnnotationComposer(
                  $db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String> oin = const Value.absent(),
            Value<String> name = const Value.absent(),
            Value<String> publicKey = const Value.absent(),
            Value<String> discoveryUrl = const Value.absent(),
            Value<bool> available = const Value.absent(),
          }) =>
              SchemeAppManagerTableCompanion(
            id: id,
            oin: oin,
            name: name,
            publicKey: publicKey,
            discoveryUrl: discoveryUrl,
            available: available,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required String oin,
            required String name,
            required String publicKey,
            required String discoveryUrl,
            required bool available,
          }) =>
              SchemeAppManagerTableCompanion.insert(
            id: id,
            oin: oin,
            name: name,
            publicKey: publicKey,
            discoveryUrl: discoveryUrl,
            available: available,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$SchemeAppManagerTableTableProcessedTableManager
    = ProcessedTableManager<
        _$EncryptedDatabase,
        $SchemeAppManagerTableTable,
        SchemeAppManagerDriftModel,
        $$SchemeAppManagerTableTableFilterComposer,
        $$SchemeAppManagerTableTableOrderingComposer,
        $$SchemeAppManagerTableTableAnnotationComposer,
        $$SchemeAppManagerTableTableCreateCompanionBuilder,
        $$SchemeAppManagerTableTableUpdateCompanionBuilder,
        (
          SchemeAppManagerDriftModel,
          BaseReferences<_$EncryptedDatabase, $SchemeAppManagerTableTable,
              SchemeAppManagerDriftModel>
        ),
        SchemeAppManagerDriftModel,
        PrefetchHooks Function()>;
typedef $$SchemeDocumentTypeTableTableCreateCompanionBuilder
    = SchemeDocumentTypeTableCompanion Function({
  Value<int> id,
  required String name,
  required bool available,
});
typedef $$SchemeDocumentTypeTableTableUpdateCompanionBuilder
    = SchemeDocumentTypeTableCompanion Function({
  Value<int> id,
  Value<String> name,
  Value<bool> available,
});

class $$SchemeDocumentTypeTableTableFilterComposer
    extends Composer<_$EncryptedDatabase, $SchemeDocumentTypeTableTable> {
  $$SchemeDocumentTypeTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get name => $composableBuilder(
      column: $table.name, builder: (column) => ColumnFilters(column));

  ColumnFilters<bool> get available => $composableBuilder(
      column: $table.available, builder: (column) => ColumnFilters(column));
}

class $$SchemeDocumentTypeTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase, $SchemeDocumentTypeTableTable> {
  $$SchemeDocumentTypeTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get name => $composableBuilder(
      column: $table.name, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<bool> get available => $composableBuilder(
      column: $table.available, builder: (column) => ColumnOrderings(column));
}

class $$SchemeDocumentTypeTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase, $SchemeDocumentTypeTableTable> {
  $$SchemeDocumentTypeTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<String> get name =>
      $composableBuilder(column: $table.name, builder: (column) => column);

  GeneratedColumn<bool> get available =>
      $composableBuilder(column: $table.available, builder: (column) => column);
}

class $$SchemeDocumentTypeTableTableTableManager extends RootTableManager<
    _$EncryptedDatabase,
    $SchemeDocumentTypeTableTable,
    SchemeDocumentTypeDriftModel,
    $$SchemeDocumentTypeTableTableFilterComposer,
    $$SchemeDocumentTypeTableTableOrderingComposer,
    $$SchemeDocumentTypeTableTableAnnotationComposer,
    $$SchemeDocumentTypeTableTableCreateCompanionBuilder,
    $$SchemeDocumentTypeTableTableUpdateCompanionBuilder,
    (
      SchemeDocumentTypeDriftModel,
      BaseReferences<_$EncryptedDatabase, $SchemeDocumentTypeTableTable,
          SchemeDocumentTypeDriftModel>
    ),
    SchemeDocumentTypeDriftModel,
    PrefetchHooks Function()> {
  $$SchemeDocumentTypeTableTableTableManager(
      _$EncryptedDatabase db, $SchemeDocumentTypeTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$SchemeDocumentTypeTableTableFilterComposer(
                  $db: db, $table: table),
          createOrderingComposer: () =>
              $$SchemeDocumentTypeTableTableOrderingComposer(
                  $db: db, $table: table),
          createComputedFieldComposer: () =>
              $$SchemeDocumentTypeTableTableAnnotationComposer(
                  $db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String> name = const Value.absent(),
            Value<bool> available = const Value.absent(),
          }) =>
              SchemeDocumentTypeTableCompanion(
            id: id,
            name: name,
            available: available,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required String name,
            required bool available,
          }) =>
              SchemeDocumentTypeTableCompanion.insert(
            id: id,
            name: name,
            available: available,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$SchemeDocumentTypeTableTableProcessedTableManager
    = ProcessedTableManager<
        _$EncryptedDatabase,
        $SchemeDocumentTypeTableTable,
        SchemeDocumentTypeDriftModel,
        $$SchemeDocumentTypeTableTableFilterComposer,
        $$SchemeDocumentTypeTableTableOrderingComposer,
        $$SchemeDocumentTypeTableTableAnnotationComposer,
        $$SchemeDocumentTypeTableTableCreateCompanionBuilder,
        $$SchemeDocumentTypeTableTableUpdateCompanionBuilder,
        (
          SchemeDocumentTypeDriftModel,
          BaseReferences<_$EncryptedDatabase, $SchemeDocumentTypeTableTable,
              SchemeDocumentTypeDriftModel>
        ),
        SchemeDocumentTypeDriftModel,
        PrefetchHooks Function()>;
typedef $$SchemeOrganizationTableTableCreateCompanionBuilder
    = SchemeOrganizationTableCompanion Function({
  Value<int> id,
  required String oin,
  required String name,
  required String publicKey,
  required String discoveryUrl,
  required bool available,
});
typedef $$SchemeOrganizationTableTableUpdateCompanionBuilder
    = SchemeOrganizationTableCompanion Function({
  Value<int> id,
  Value<String> oin,
  Value<String> name,
  Value<String> publicKey,
  Value<String> discoveryUrl,
  Value<bool> available,
});

class $$SchemeOrganizationTableTableFilterComposer
    extends Composer<_$EncryptedDatabase, $SchemeOrganizationTableTable> {
  $$SchemeOrganizationTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get name => $composableBuilder(
      column: $table.name, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get publicKey => $composableBuilder(
      column: $table.publicKey, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get discoveryUrl => $composableBuilder(
      column: $table.discoveryUrl, builder: (column) => ColumnFilters(column));

  ColumnFilters<bool> get available => $composableBuilder(
      column: $table.available, builder: (column) => ColumnFilters(column));
}

class $$SchemeOrganizationTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase, $SchemeOrganizationTableTable> {
  $$SchemeOrganizationTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get name => $composableBuilder(
      column: $table.name, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get publicKey => $composableBuilder(
      column: $table.publicKey, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get discoveryUrl => $composableBuilder(
      column: $table.discoveryUrl,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<bool> get available => $composableBuilder(
      column: $table.available, builder: (column) => ColumnOrderings(column));
}

class $$SchemeOrganizationTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase, $SchemeOrganizationTableTable> {
  $$SchemeOrganizationTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<String> get oin =>
      $composableBuilder(column: $table.oin, builder: (column) => column);

  GeneratedColumn<String> get name =>
      $composableBuilder(column: $table.name, builder: (column) => column);

  GeneratedColumn<String> get publicKey =>
      $composableBuilder(column: $table.publicKey, builder: (column) => column);

  GeneratedColumn<String> get discoveryUrl => $composableBuilder(
      column: $table.discoveryUrl, builder: (column) => column);

  GeneratedColumn<bool> get available =>
      $composableBuilder(column: $table.available, builder: (column) => column);
}

class $$SchemeOrganizationTableTableTableManager extends RootTableManager<
    _$EncryptedDatabase,
    $SchemeOrganizationTableTable,
    SchemeOrganizationDriftModel,
    $$SchemeOrganizationTableTableFilterComposer,
    $$SchemeOrganizationTableTableOrderingComposer,
    $$SchemeOrganizationTableTableAnnotationComposer,
    $$SchemeOrganizationTableTableCreateCompanionBuilder,
    $$SchemeOrganizationTableTableUpdateCompanionBuilder,
    (
      SchemeOrganizationDriftModel,
      BaseReferences<_$EncryptedDatabase, $SchemeOrganizationTableTable,
          SchemeOrganizationDriftModel>
    ),
    SchemeOrganizationDriftModel,
    PrefetchHooks Function()> {
  $$SchemeOrganizationTableTableTableManager(
      _$EncryptedDatabase db, $SchemeOrganizationTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$SchemeOrganizationTableTableFilterComposer(
                  $db: db, $table: table),
          createOrderingComposer: () =>
              $$SchemeOrganizationTableTableOrderingComposer(
                  $db: db, $table: table),
          createComputedFieldComposer: () =>
              $$SchemeOrganizationTableTableAnnotationComposer(
                  $db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String> oin = const Value.absent(),
            Value<String> name = const Value.absent(),
            Value<String> publicKey = const Value.absent(),
            Value<String> discoveryUrl = const Value.absent(),
            Value<bool> available = const Value.absent(),
          }) =>
              SchemeOrganizationTableCompanion(
            id: id,
            oin: oin,
            name: name,
            publicKey: publicKey,
            discoveryUrl: discoveryUrl,
            available: available,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required String oin,
            required String name,
            required String publicKey,
            required String discoveryUrl,
            required bool available,
          }) =>
              SchemeOrganizationTableCompanion.insert(
            id: id,
            oin: oin,
            name: name,
            publicKey: publicKey,
            discoveryUrl: discoveryUrl,
            available: available,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$SchemeOrganizationTableTableProcessedTableManager
    = ProcessedTableManager<
        _$EncryptedDatabase,
        $SchemeOrganizationTableTable,
        SchemeOrganizationDriftModel,
        $$SchemeOrganizationTableTableFilterComposer,
        $$SchemeOrganizationTableTableOrderingComposer,
        $$SchemeOrganizationTableTableAnnotationComposer,
        $$SchemeOrganizationTableTableCreateCompanionBuilder,
        $$SchemeOrganizationTableTableUpdateCompanionBuilder,
        (
          SchemeOrganizationDriftModel,
          BaseReferences<_$EncryptedDatabase, $SchemeOrganizationTableTable,
              SchemeOrganizationDriftModel>
        ),
        SchemeOrganizationDriftModel,
        PrefetchHooks Function()>;
typedef $$PaymentPlanGroupsTableTableCreateCompanionBuilder
    = PaymentPlanGroupsTableCompanion Function({
  Value<int> id,
  required String zaakKenmerken,
  required int saldo,
  required int termijnen,
  required int paymentRules,
});
typedef $$PaymentPlanGroupsTableTableUpdateCompanionBuilder
    = PaymentPlanGroupsTableCompanion Function({
  Value<int> id,
  Value<String> zaakKenmerken,
  Value<int> saldo,
  Value<int> termijnen,
  Value<int> paymentRules,
});

class $$PaymentPlanGroupsTableTableFilterComposer
    extends Composer<_$EncryptedDatabase, $PaymentPlanGroupsTableTable> {
  $$PaymentPlanGroupsTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get zaakKenmerken => $composableBuilder(
      column: $table.zaakKenmerken, builder: (column) => ColumnFilters(column));

  ColumnFilters<int> get saldo => $composableBuilder(
      column: $table.saldo, builder: (column) => ColumnFilters(column));

  ColumnFilters<int> get termijnen => $composableBuilder(
      column: $table.termijnen, builder: (column) => ColumnFilters(column));

  ColumnFilters<int> get paymentRules => $composableBuilder(
      column: $table.paymentRules, builder: (column) => ColumnFilters(column));
}

class $$PaymentPlanGroupsTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase, $PaymentPlanGroupsTableTable> {
  $$PaymentPlanGroupsTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get zaakKenmerken => $composableBuilder(
      column: $table.zaakKenmerken,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<int> get saldo => $composableBuilder(
      column: $table.saldo, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<int> get termijnen => $composableBuilder(
      column: $table.termijnen, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<int> get paymentRules => $composableBuilder(
      column: $table.paymentRules,
      builder: (column) => ColumnOrderings(column));
}

class $$PaymentPlanGroupsTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase, $PaymentPlanGroupsTableTable> {
  $$PaymentPlanGroupsTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<String> get zaakKenmerken => $composableBuilder(
      column: $table.zaakKenmerken, builder: (column) => column);

  GeneratedColumn<int> get saldo =>
      $composableBuilder(column: $table.saldo, builder: (column) => column);

  GeneratedColumn<int> get termijnen =>
      $composableBuilder(column: $table.termijnen, builder: (column) => column);

  GeneratedColumn<int> get paymentRules => $composableBuilder(
      column: $table.paymentRules, builder: (column) => column);
}

class $$PaymentPlanGroupsTableTableTableManager extends RootTableManager<
    _$EncryptedDatabase,
    $PaymentPlanGroupsTableTable,
    PaymentPlanGroupsModel,
    $$PaymentPlanGroupsTableTableFilterComposer,
    $$PaymentPlanGroupsTableTableOrderingComposer,
    $$PaymentPlanGroupsTableTableAnnotationComposer,
    $$PaymentPlanGroupsTableTableCreateCompanionBuilder,
    $$PaymentPlanGroupsTableTableUpdateCompanionBuilder,
    (
      PaymentPlanGroupsModel,
      BaseReferences<_$EncryptedDatabase, $PaymentPlanGroupsTableTable,
          PaymentPlanGroupsModel>
    ),
    PaymentPlanGroupsModel,
    PrefetchHooks Function()> {
  $$PaymentPlanGroupsTableTableTableManager(
      _$EncryptedDatabase db, $PaymentPlanGroupsTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$PaymentPlanGroupsTableTableFilterComposer(
                  $db: db, $table: table),
          createOrderingComposer: () =>
              $$PaymentPlanGroupsTableTableOrderingComposer(
                  $db: db, $table: table),
          createComputedFieldComposer: () =>
              $$PaymentPlanGroupsTableTableAnnotationComposer(
                  $db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String> zaakKenmerken = const Value.absent(),
            Value<int> saldo = const Value.absent(),
            Value<int> termijnen = const Value.absent(),
            Value<int> paymentRules = const Value.absent(),
          }) =>
              PaymentPlanGroupsTableCompanion(
            id: id,
            zaakKenmerken: zaakKenmerken,
            saldo: saldo,
            termijnen: termijnen,
            paymentRules: paymentRules,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required String zaakKenmerken,
            required int saldo,
            required int termijnen,
            required int paymentRules,
          }) =>
              PaymentPlanGroupsTableCompanion.insert(
            id: id,
            zaakKenmerken: zaakKenmerken,
            saldo: saldo,
            termijnen: termijnen,
            paymentRules: paymentRules,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$PaymentPlanGroupsTableTableProcessedTableManager
    = ProcessedTableManager<
        _$EncryptedDatabase,
        $PaymentPlanGroupsTableTable,
        PaymentPlanGroupsModel,
        $$PaymentPlanGroupsTableTableFilterComposer,
        $$PaymentPlanGroupsTableTableOrderingComposer,
        $$PaymentPlanGroupsTableTableAnnotationComposer,
        $$PaymentPlanGroupsTableTableCreateCompanionBuilder,
        $$PaymentPlanGroupsTableTableUpdateCompanionBuilder,
        (
          PaymentPlanGroupsModel,
          BaseReferences<_$EncryptedDatabase, $PaymentPlanGroupsTableTable,
              PaymentPlanGroupsModel>
        ),
        PaymentPlanGroupsModel,
        PrefetchHooks Function()>;
typedef $$PaymentPlanRulesStorageTableTableCreateCompanionBuilder
    = PaymentPlanRulesStorageTableCompanion Function({
  required int id,
  required String organisaties,
  required int minimumBedrag,
  required int maximumBedrag,
  required String type,
  Value<int> rowid,
});
typedef $$PaymentPlanRulesStorageTableTableUpdateCompanionBuilder
    = PaymentPlanRulesStorageTableCompanion Function({
  Value<int> id,
  Value<String> organisaties,
  Value<int> minimumBedrag,
  Value<int> maximumBedrag,
  Value<String> type,
  Value<int> rowid,
});

class $$PaymentPlanRulesStorageTableTableFilterComposer
    extends Composer<_$EncryptedDatabase, $PaymentPlanRulesStorageTableTable> {
  $$PaymentPlanRulesStorageTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get organisaties => $composableBuilder(
      column: $table.organisaties, builder: (column) => ColumnFilters(column));

  ColumnFilters<int> get minimumBedrag => $composableBuilder(
      column: $table.minimumBedrag, builder: (column) => ColumnFilters(column));

  ColumnFilters<int> get maximumBedrag => $composableBuilder(
      column: $table.maximumBedrag, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get type => $composableBuilder(
      column: $table.type, builder: (column) => ColumnFilters(column));
}

class $$PaymentPlanRulesStorageTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase, $PaymentPlanRulesStorageTableTable> {
  $$PaymentPlanRulesStorageTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get organisaties => $composableBuilder(
      column: $table.organisaties,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<int> get minimumBedrag => $composableBuilder(
      column: $table.minimumBedrag,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<int> get maximumBedrag => $composableBuilder(
      column: $table.maximumBedrag,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get type => $composableBuilder(
      column: $table.type, builder: (column) => ColumnOrderings(column));
}

class $$PaymentPlanRulesStorageTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase, $PaymentPlanRulesStorageTableTable> {
  $$PaymentPlanRulesStorageTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<String> get organisaties => $composableBuilder(
      column: $table.organisaties, builder: (column) => column);

  GeneratedColumn<int> get minimumBedrag => $composableBuilder(
      column: $table.minimumBedrag, builder: (column) => column);

  GeneratedColumn<int> get maximumBedrag => $composableBuilder(
      column: $table.maximumBedrag, builder: (column) => column);

  GeneratedColumn<String> get type =>
      $composableBuilder(column: $table.type, builder: (column) => column);
}

class $$PaymentPlanRulesStorageTableTableTableManager extends RootTableManager<
    _$EncryptedDatabase,
    $PaymentPlanRulesStorageTableTable,
    PaymentPlanRulesStorageDriftModel,
    $$PaymentPlanRulesStorageTableTableFilterComposer,
    $$PaymentPlanRulesStorageTableTableOrderingComposer,
    $$PaymentPlanRulesStorageTableTableAnnotationComposer,
    $$PaymentPlanRulesStorageTableTableCreateCompanionBuilder,
    $$PaymentPlanRulesStorageTableTableUpdateCompanionBuilder,
    (
      PaymentPlanRulesStorageDriftModel,
      BaseReferences<_$EncryptedDatabase, $PaymentPlanRulesStorageTableTable,
          PaymentPlanRulesStorageDriftModel>
    ),
    PaymentPlanRulesStorageDriftModel,
    PrefetchHooks Function()> {
  $$PaymentPlanRulesStorageTableTableTableManager(
      _$EncryptedDatabase db, $PaymentPlanRulesStorageTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$PaymentPlanRulesStorageTableTableFilterComposer(
                  $db: db, $table: table),
          createOrderingComposer: () =>
              $$PaymentPlanRulesStorageTableTableOrderingComposer(
                  $db: db, $table: table),
          createComputedFieldComposer: () =>
              $$PaymentPlanRulesStorageTableTableAnnotationComposer(
                  $db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String> organisaties = const Value.absent(),
            Value<int> minimumBedrag = const Value.absent(),
            Value<int> maximumBedrag = const Value.absent(),
            Value<String> type = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              PaymentPlanRulesStorageTableCompanion(
            id: id,
            organisaties: organisaties,
            minimumBedrag: minimumBedrag,
            maximumBedrag: maximumBedrag,
            type: type,
            rowid: rowid,
          ),
          createCompanionCallback: ({
            required int id,
            required String organisaties,
            required int minimumBedrag,
            required int maximumBedrag,
            required String type,
            Value<int> rowid = const Value.absent(),
          }) =>
              PaymentPlanRulesStorageTableCompanion.insert(
            id: id,
            organisaties: organisaties,
            minimumBedrag: minimumBedrag,
            maximumBedrag: maximumBedrag,
            type: type,
            rowid: rowid,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$PaymentPlanRulesStorageTableTableProcessedTableManager
    = ProcessedTableManager<
        _$EncryptedDatabase,
        $PaymentPlanRulesStorageTableTable,
        PaymentPlanRulesStorageDriftModel,
        $$PaymentPlanRulesStorageTableTableFilterComposer,
        $$PaymentPlanRulesStorageTableTableOrderingComposer,
        $$PaymentPlanRulesStorageTableTableAnnotationComposer,
        $$PaymentPlanRulesStorageTableTableCreateCompanionBuilder,
        $$PaymentPlanRulesStorageTableTableUpdateCompanionBuilder,
        (
          PaymentPlanRulesStorageDriftModel,
          BaseReferences<
              _$EncryptedDatabase,
              $PaymentPlanRulesStorageTableTable,
              PaymentPlanRulesStorageDriftModel>
        ),
        PaymentPlanRulesStorageDriftModel,
        PrefetchHooks Function()>;
typedef $$AppKeyPairTableTableCreateCompanionBuilder = AppKeyPairTableCompanion
    Function({
  Value<int> id,
  required String privateKey,
  required String publicKey,
});
typedef $$AppKeyPairTableTableUpdateCompanionBuilder = AppKeyPairTableCompanion
    Function({
  Value<int> id,
  Value<String> privateKey,
  Value<String> publicKey,
});

class $$AppKeyPairTableTableFilterComposer
    extends Composer<_$EncryptedDatabase, $AppKeyPairTableTable> {
  $$AppKeyPairTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get privateKey => $composableBuilder(
      column: $table.privateKey, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get publicKey => $composableBuilder(
      column: $table.publicKey, builder: (column) => ColumnFilters(column));
}

class $$AppKeyPairTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase, $AppKeyPairTableTable> {
  $$AppKeyPairTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get privateKey => $composableBuilder(
      column: $table.privateKey, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get publicKey => $composableBuilder(
      column: $table.publicKey, builder: (column) => ColumnOrderings(column));
}

class $$AppKeyPairTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase, $AppKeyPairTableTable> {
  $$AppKeyPairTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<String> get privateKey => $composableBuilder(
      column: $table.privateKey, builder: (column) => column);

  GeneratedColumn<String> get publicKey =>
      $composableBuilder(column: $table.publicKey, builder: (column) => column);
}

class $$AppKeyPairTableTableTableManager extends RootTableManager<
    _$EncryptedDatabase,
    $AppKeyPairTableTable,
    AppKeyPairDriftModel,
    $$AppKeyPairTableTableFilterComposer,
    $$AppKeyPairTableTableOrderingComposer,
    $$AppKeyPairTableTableAnnotationComposer,
    $$AppKeyPairTableTableCreateCompanionBuilder,
    $$AppKeyPairTableTableUpdateCompanionBuilder,
    (
      AppKeyPairDriftModel,
      BaseReferences<_$EncryptedDatabase, $AppKeyPairTableTable,
          AppKeyPairDriftModel>
    ),
    AppKeyPairDriftModel,
    PrefetchHooks Function()> {
  $$AppKeyPairTableTableTableManager(
      _$EncryptedDatabase db, $AppKeyPairTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$AppKeyPairTableTableFilterComposer($db: db, $table: table),
          createOrderingComposer: () =>
              $$AppKeyPairTableTableOrderingComposer($db: db, $table: table),
          createComputedFieldComposer: () =>
              $$AppKeyPairTableTableAnnotationComposer($db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String> privateKey = const Value.absent(),
            Value<String> publicKey = const Value.absent(),
          }) =>
              AppKeyPairTableCompanion(
            id: id,
            privateKey: privateKey,
            publicKey: publicKey,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required String privateKey,
            required String publicKey,
          }) =>
              AppKeyPairTableCompanion.insert(
            id: id,
            privateKey: privateKey,
            publicKey: publicKey,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$AppKeyPairTableTableProcessedTableManager = ProcessedTableManager<
    _$EncryptedDatabase,
    $AppKeyPairTableTable,
    AppKeyPairDriftModel,
    $$AppKeyPairTableTableFilterComposer,
    $$AppKeyPairTableTableOrderingComposer,
    $$AppKeyPairTableTableAnnotationComposer,
    $$AppKeyPairTableTableCreateCompanionBuilder,
    $$AppKeyPairTableTableUpdateCompanionBuilder,
    (
      AppKeyPairDriftModel,
      BaseReferences<_$EncryptedDatabase, $AppKeyPairTableTable,
          AppKeyPairDriftModel>
    ),
    AppKeyPairDriftModel,
    PrefetchHooks Function()>;
typedef $$AppSessionTableTableCreateCompanionBuilder = AppSessionTableCompanion
    Function({
  Value<int> id,
  required String key,
  required String token,
  required String oin,
});
typedef $$AppSessionTableTableUpdateCompanionBuilder = AppSessionTableCompanion
    Function({
  Value<int> id,
  Value<String> key,
  Value<String> token,
  Value<String> oin,
});

class $$AppSessionTableTableFilterComposer
    extends Composer<_$EncryptedDatabase, $AppSessionTableTable> {
  $$AppSessionTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get key => $composableBuilder(
      column: $table.key, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get token => $composableBuilder(
      column: $table.token, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnFilters(column));
}

class $$AppSessionTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase, $AppSessionTableTable> {
  $$AppSessionTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get key => $composableBuilder(
      column: $table.key, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get token => $composableBuilder(
      column: $table.token, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnOrderings(column));
}

class $$AppSessionTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase, $AppSessionTableTable> {
  $$AppSessionTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<String> get key =>
      $composableBuilder(column: $table.key, builder: (column) => column);

  GeneratedColumn<String> get token =>
      $composableBuilder(column: $table.token, builder: (column) => column);

  GeneratedColumn<String> get oin =>
      $composableBuilder(column: $table.oin, builder: (column) => column);
}

class $$AppSessionTableTableTableManager extends RootTableManager<
    _$EncryptedDatabase,
    $AppSessionTableTable,
    AppSessionDriftModel,
    $$AppSessionTableTableFilterComposer,
    $$AppSessionTableTableOrderingComposer,
    $$AppSessionTableTableAnnotationComposer,
    $$AppSessionTableTableCreateCompanionBuilder,
    $$AppSessionTableTableUpdateCompanionBuilder,
    (
      AppSessionDriftModel,
      BaseReferences<_$EncryptedDatabase, $AppSessionTableTable,
          AppSessionDriftModel>
    ),
    AppSessionDriftModel,
    PrefetchHooks Function()> {
  $$AppSessionTableTableTableManager(
      _$EncryptedDatabase db, $AppSessionTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$AppSessionTableTableFilterComposer($db: db, $table: table),
          createOrderingComposer: () =>
              $$AppSessionTableTableOrderingComposer($db: db, $table: table),
          createComputedFieldComposer: () =>
              $$AppSessionTableTableAnnotationComposer($db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String> key = const Value.absent(),
            Value<String> token = const Value.absent(),
            Value<String> oin = const Value.absent(),
          }) =>
              AppSessionTableCompanion(
            id: id,
            key: key,
            token: token,
            oin: oin,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required String key,
            required String token,
            required String oin,
          }) =>
              AppSessionTableCompanion.insert(
            id: id,
            key: key,
            token: token,
            oin: oin,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$AppSessionTableTableProcessedTableManager = ProcessedTableManager<
    _$EncryptedDatabase,
    $AppSessionTableTable,
    AppSessionDriftModel,
    $$AppSessionTableTableFilterComposer,
    $$AppSessionTableTableOrderingComposer,
    $$AppSessionTableTableAnnotationComposer,
    $$AppSessionTableTableCreateCompanionBuilder,
    $$AppSessionTableTableUpdateCompanionBuilder,
    (
      AppSessionDriftModel,
      BaseReferences<_$EncryptedDatabase, $AppSessionTableTable,
          AppSessionDriftModel>
    ),
    AppSessionDriftModel,
    PrefetchHooks Function()>;
typedef $$FinancialClaimsInformationStorageTableTableCreateCompanionBuilder
    = FinancialClaimsInformationStorageTableCompanion Function({
  Value<int> id,
  required String oin,
  required DateTime dateTimeRequested,
  required DateTime dateTimeReceived,
  required String financialClaimsInformationDocument,
});
typedef $$FinancialClaimsInformationStorageTableTableUpdateCompanionBuilder
    = FinancialClaimsInformationStorageTableCompanion Function({
  Value<int> id,
  Value<String> oin,
  Value<DateTime> dateTimeRequested,
  Value<DateTime> dateTimeReceived,
  Value<String> financialClaimsInformationDocument,
});

class $$FinancialClaimsInformationStorageTableTableFilterComposer
    extends Composer<_$EncryptedDatabase,
        $FinancialClaimsInformationStorageTableTable> {
  $$FinancialClaimsInformationStorageTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnFilters(column));

  ColumnFilters<DateTime> get dateTimeRequested => $composableBuilder(
      column: $table.dateTimeRequested,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<DateTime> get dateTimeReceived => $composableBuilder(
      column: $table.dateTimeReceived,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get financialClaimsInformationDocument =>
      $composableBuilder(
          column: $table.financialClaimsInformationDocument,
          builder: (column) => ColumnFilters(column));
}

class $$FinancialClaimsInformationStorageTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase,
        $FinancialClaimsInformationStorageTableTable> {
  $$FinancialClaimsInformationStorageTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get oin => $composableBuilder(
      column: $table.oin, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<DateTime> get dateTimeRequested => $composableBuilder(
      column: $table.dateTimeRequested,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<DateTime> get dateTimeReceived => $composableBuilder(
      column: $table.dateTimeReceived,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get financialClaimsInformationDocument =>
      $composableBuilder(
          column: $table.financialClaimsInformationDocument,
          builder: (column) => ColumnOrderings(column));
}

class $$FinancialClaimsInformationStorageTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase,
        $FinancialClaimsInformationStorageTableTable> {
  $$FinancialClaimsInformationStorageTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<String> get oin =>
      $composableBuilder(column: $table.oin, builder: (column) => column);

  GeneratedColumn<DateTime> get dateTimeRequested => $composableBuilder(
      column: $table.dateTimeRequested, builder: (column) => column);

  GeneratedColumn<DateTime> get dateTimeReceived => $composableBuilder(
      column: $table.dateTimeReceived, builder: (column) => column);

  GeneratedColumn<String> get financialClaimsInformationDocument =>
      $composableBuilder(
          column: $table.financialClaimsInformationDocument,
          builder: (column) => column);
}

class $$FinancialClaimsInformationStorageTableTableTableManager
    extends RootTableManager<
        _$EncryptedDatabase,
        $FinancialClaimsInformationStorageTableTable,
        FinancialClaimsInformationStorageDriftModel,
        $$FinancialClaimsInformationStorageTableTableFilterComposer,
        $$FinancialClaimsInformationStorageTableTableOrderingComposer,
        $$FinancialClaimsInformationStorageTableTableAnnotationComposer,
        $$FinancialClaimsInformationStorageTableTableCreateCompanionBuilder,
        $$FinancialClaimsInformationStorageTableTableUpdateCompanionBuilder,
        (
          FinancialClaimsInformationStorageDriftModel,
          BaseReferences<
              _$EncryptedDatabase,
              $FinancialClaimsInformationStorageTableTable,
              FinancialClaimsInformationStorageDriftModel>
        ),
        FinancialClaimsInformationStorageDriftModel,
        PrefetchHooks Function()> {
  $$FinancialClaimsInformationStorageTableTableTableManager(
      _$EncryptedDatabase db,
      $FinancialClaimsInformationStorageTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$FinancialClaimsInformationStorageTableTableFilterComposer(
                  $db: db, $table: table),
          createOrderingComposer: () =>
              $$FinancialClaimsInformationStorageTableTableOrderingComposer(
                  $db: db, $table: table),
          createComputedFieldComposer: () =>
              $$FinancialClaimsInformationStorageTableTableAnnotationComposer(
                  $db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String> oin = const Value.absent(),
            Value<DateTime> dateTimeRequested = const Value.absent(),
            Value<DateTime> dateTimeReceived = const Value.absent(),
            Value<String> financialClaimsInformationDocument =
                const Value.absent(),
          }) =>
              FinancialClaimsInformationStorageTableCompanion(
            id: id,
            oin: oin,
            dateTimeRequested: dateTimeRequested,
            dateTimeReceived: dateTimeReceived,
            financialClaimsInformationDocument:
                financialClaimsInformationDocument,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required String oin,
            required DateTime dateTimeRequested,
            required DateTime dateTimeReceived,
            required String financialClaimsInformationDocument,
          }) =>
              FinancialClaimsInformationStorageTableCompanion.insert(
            id: id,
            oin: oin,
            dateTimeRequested: dateTimeRequested,
            dateTimeReceived: dateTimeReceived,
            financialClaimsInformationDocument:
                financialClaimsInformationDocument,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$FinancialClaimsInformationStorageTableTableProcessedTableManager
    = ProcessedTableManager<
        _$EncryptedDatabase,
        $FinancialClaimsInformationStorageTableTable,
        FinancialClaimsInformationStorageDriftModel,
        $$FinancialClaimsInformationStorageTableTableFilterComposer,
        $$FinancialClaimsInformationStorageTableTableOrderingComposer,
        $$FinancialClaimsInformationStorageTableTableAnnotationComposer,
        $$FinancialClaimsInformationStorageTableTableCreateCompanionBuilder,
        $$FinancialClaimsInformationStorageTableTableUpdateCompanionBuilder,
        (
          FinancialClaimsInformationStorageDriftModel,
          BaseReferences<
              _$EncryptedDatabase,
              $FinancialClaimsInformationStorageTableTable,
              FinancialClaimsInformationStorageDriftModel>
        ),
        FinancialClaimsInformationStorageDriftModel,
        PrefetchHooks Function()>;
typedef $$LogRecordTableTableCreateCompanionBuilder = LogRecordTableCompanion
    Function({
  Value<int> id,
  required DateTime timestamp,
  required String host,
  required int cef,
  required String deviceVendor,
  required String deviceProduct,
  required String deviceVersion,
  required String deviceEventClassId,
  required String name,
  required int severity,
  required String flexString1Label,
  required String flexString1,
  required String flexString2Label,
  required String flexString2,
  required String act,
  required String app,
  required String request,
  required String requestMethod,
});
typedef $$LogRecordTableTableUpdateCompanionBuilder = LogRecordTableCompanion
    Function({
  Value<int> id,
  Value<DateTime> timestamp,
  Value<String> host,
  Value<int> cef,
  Value<String> deviceVendor,
  Value<String> deviceProduct,
  Value<String> deviceVersion,
  Value<String> deviceEventClassId,
  Value<String> name,
  Value<int> severity,
  Value<String> flexString1Label,
  Value<String> flexString1,
  Value<String> flexString2Label,
  Value<String> flexString2,
  Value<String> act,
  Value<String> app,
  Value<String> request,
  Value<String> requestMethod,
});

class $$LogRecordTableTableFilterComposer
    extends Composer<_$EncryptedDatabase, $LogRecordTableTable> {
  $$LogRecordTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<DateTime> get timestamp => $composableBuilder(
      column: $table.timestamp, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get host => $composableBuilder(
      column: $table.host, builder: (column) => ColumnFilters(column));

  ColumnFilters<int> get cef => $composableBuilder(
      column: $table.cef, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get deviceVendor => $composableBuilder(
      column: $table.deviceVendor, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get deviceProduct => $composableBuilder(
      column: $table.deviceProduct, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get deviceVersion => $composableBuilder(
      column: $table.deviceVersion, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get deviceEventClassId => $composableBuilder(
      column: $table.deviceEventClassId,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get name => $composableBuilder(
      column: $table.name, builder: (column) => ColumnFilters(column));

  ColumnFilters<int> get severity => $composableBuilder(
      column: $table.severity, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get flexString1Label => $composableBuilder(
      column: $table.flexString1Label,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get flexString1 => $composableBuilder(
      column: $table.flexString1, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get flexString2Label => $composableBuilder(
      column: $table.flexString2Label,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get flexString2 => $composableBuilder(
      column: $table.flexString2, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get act => $composableBuilder(
      column: $table.act, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get app => $composableBuilder(
      column: $table.app, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get request => $composableBuilder(
      column: $table.request, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get requestMethod => $composableBuilder(
      column: $table.requestMethod, builder: (column) => ColumnFilters(column));
}

class $$LogRecordTableTableOrderingComposer
    extends Composer<_$EncryptedDatabase, $LogRecordTableTable> {
  $$LogRecordTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<DateTime> get timestamp => $composableBuilder(
      column: $table.timestamp, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get host => $composableBuilder(
      column: $table.host, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<int> get cef => $composableBuilder(
      column: $table.cef, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get deviceVendor => $composableBuilder(
      column: $table.deviceVendor,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get deviceProduct => $composableBuilder(
      column: $table.deviceProduct,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get deviceVersion => $composableBuilder(
      column: $table.deviceVersion,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get deviceEventClassId => $composableBuilder(
      column: $table.deviceEventClassId,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get name => $composableBuilder(
      column: $table.name, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<int> get severity => $composableBuilder(
      column: $table.severity, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get flexString1Label => $composableBuilder(
      column: $table.flexString1Label,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get flexString1 => $composableBuilder(
      column: $table.flexString1, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get flexString2Label => $composableBuilder(
      column: $table.flexString2Label,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get flexString2 => $composableBuilder(
      column: $table.flexString2, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get act => $composableBuilder(
      column: $table.act, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get app => $composableBuilder(
      column: $table.app, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get request => $composableBuilder(
      column: $table.request, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get requestMethod => $composableBuilder(
      column: $table.requestMethod,
      builder: (column) => ColumnOrderings(column));
}

class $$LogRecordTableTableAnnotationComposer
    extends Composer<_$EncryptedDatabase, $LogRecordTableTable> {
  $$LogRecordTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<DateTime> get timestamp =>
      $composableBuilder(column: $table.timestamp, builder: (column) => column);

  GeneratedColumn<String> get host =>
      $composableBuilder(column: $table.host, builder: (column) => column);

  GeneratedColumn<int> get cef =>
      $composableBuilder(column: $table.cef, builder: (column) => column);

  GeneratedColumn<String> get deviceVendor => $composableBuilder(
      column: $table.deviceVendor, builder: (column) => column);

  GeneratedColumn<String> get deviceProduct => $composableBuilder(
      column: $table.deviceProduct, builder: (column) => column);

  GeneratedColumn<String> get deviceVersion => $composableBuilder(
      column: $table.deviceVersion, builder: (column) => column);

  GeneratedColumn<String> get deviceEventClassId => $composableBuilder(
      column: $table.deviceEventClassId, builder: (column) => column);

  GeneratedColumn<String> get name =>
      $composableBuilder(column: $table.name, builder: (column) => column);

  GeneratedColumn<int> get severity =>
      $composableBuilder(column: $table.severity, builder: (column) => column);

  GeneratedColumn<String> get flexString1Label => $composableBuilder(
      column: $table.flexString1Label, builder: (column) => column);

  GeneratedColumn<String> get flexString1 => $composableBuilder(
      column: $table.flexString1, builder: (column) => column);

  GeneratedColumn<String> get flexString2Label => $composableBuilder(
      column: $table.flexString2Label, builder: (column) => column);

  GeneratedColumn<String> get flexString2 => $composableBuilder(
      column: $table.flexString2, builder: (column) => column);

  GeneratedColumn<String> get act =>
      $composableBuilder(column: $table.act, builder: (column) => column);

  GeneratedColumn<String> get app =>
      $composableBuilder(column: $table.app, builder: (column) => column);

  GeneratedColumn<String> get request =>
      $composableBuilder(column: $table.request, builder: (column) => column);

  GeneratedColumn<String> get requestMethod => $composableBuilder(
      column: $table.requestMethod, builder: (column) => column);
}

class $$LogRecordTableTableTableManager extends RootTableManager<
    _$EncryptedDatabase,
    $LogRecordTableTable,
    LogRecordDriftModel,
    $$LogRecordTableTableFilterComposer,
    $$LogRecordTableTableOrderingComposer,
    $$LogRecordTableTableAnnotationComposer,
    $$LogRecordTableTableCreateCompanionBuilder,
    $$LogRecordTableTableUpdateCompanionBuilder,
    (
      LogRecordDriftModel,
      BaseReferences<_$EncryptedDatabase, $LogRecordTableTable,
          LogRecordDriftModel>
    ),
    LogRecordDriftModel,
    PrefetchHooks Function()> {
  $$LogRecordTableTableTableManager(
      _$EncryptedDatabase db, $LogRecordTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$LogRecordTableTableFilterComposer($db: db, $table: table),
          createOrderingComposer: () =>
              $$LogRecordTableTableOrderingComposer($db: db, $table: table),
          createComputedFieldComposer: () =>
              $$LogRecordTableTableAnnotationComposer($db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<DateTime> timestamp = const Value.absent(),
            Value<String> host = const Value.absent(),
            Value<int> cef = const Value.absent(),
            Value<String> deviceVendor = const Value.absent(),
            Value<String> deviceProduct = const Value.absent(),
            Value<String> deviceVersion = const Value.absent(),
            Value<String> deviceEventClassId = const Value.absent(),
            Value<String> name = const Value.absent(),
            Value<int> severity = const Value.absent(),
            Value<String> flexString1Label = const Value.absent(),
            Value<String> flexString1 = const Value.absent(),
            Value<String> flexString2Label = const Value.absent(),
            Value<String> flexString2 = const Value.absent(),
            Value<String> act = const Value.absent(),
            Value<String> app = const Value.absent(),
            Value<String> request = const Value.absent(),
            Value<String> requestMethod = const Value.absent(),
          }) =>
              LogRecordTableCompanion(
            id: id,
            timestamp: timestamp,
            host: host,
            cef: cef,
            deviceVendor: deviceVendor,
            deviceProduct: deviceProduct,
            deviceVersion: deviceVersion,
            deviceEventClassId: deviceEventClassId,
            name: name,
            severity: severity,
            flexString1Label: flexString1Label,
            flexString1: flexString1,
            flexString2Label: flexString2Label,
            flexString2: flexString2,
            act: act,
            app: app,
            request: request,
            requestMethod: requestMethod,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required DateTime timestamp,
            required String host,
            required int cef,
            required String deviceVendor,
            required String deviceProduct,
            required String deviceVersion,
            required String deviceEventClassId,
            required String name,
            required int severity,
            required String flexString1Label,
            required String flexString1,
            required String flexString2Label,
            required String flexString2,
            required String act,
            required String app,
            required String request,
            required String requestMethod,
          }) =>
              LogRecordTableCompanion.insert(
            id: id,
            timestamp: timestamp,
            host: host,
            cef: cef,
            deviceVendor: deviceVendor,
            deviceProduct: deviceProduct,
            deviceVersion: deviceVersion,
            deviceEventClassId: deviceEventClassId,
            name: name,
            severity: severity,
            flexString1Label: flexString1Label,
            flexString1: flexString1,
            flexString2Label: flexString2Label,
            flexString2: flexString2,
            act: act,
            app: app,
            request: request,
            requestMethod: requestMethod,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$LogRecordTableTableProcessedTableManager = ProcessedTableManager<
    _$EncryptedDatabase,
    $LogRecordTableTable,
    LogRecordDriftModel,
    $$LogRecordTableTableFilterComposer,
    $$LogRecordTableTableOrderingComposer,
    $$LogRecordTableTableAnnotationComposer,
    $$LogRecordTableTableCreateCompanionBuilder,
    $$LogRecordTableTableUpdateCompanionBuilder,
    (
      LogRecordDriftModel,
      BaseReferences<_$EncryptedDatabase, $LogRecordTableTable,
          LogRecordDriftModel>
    ),
    LogRecordDriftModel,
    PrefetchHooks Function()>;

class $EncryptedDatabaseManager {
  final _$EncryptedDatabase _db;
  $EncryptedDatabaseManager(this._db);
  $$FinancialClaimsInformationRequestTableTableTableManager
      get financialClaimsInformationRequestTable =>
          $$FinancialClaimsInformationRequestTableTableTableManager(
              _db, _db.financialClaimsInformationRequestTable);
  $$FinancialClaimsInformationConfigurationTableTableTableManager
      get financialClaimsInformationConfigurationTable =>
          $$FinancialClaimsInformationConfigurationTableTableTableManager(
              _db, _db.financialClaimsInformationConfigurationTable);
  $$FinancialClaimsInformationInboxTableTableTableManager
      get financialClaimsInformationInboxTable =>
          $$FinancialClaimsInformationInboxTableTableTableManager(
              _db, _db.financialClaimsInformationInboxTable);
  $$FinancialClaimsInformationReceivedTableTableTableManager
      get financialClaimsInformationReceivedTable =>
          $$FinancialClaimsInformationReceivedTableTableTableManager(
              _db, _db.financialClaimsInformationReceivedTable);
  $$RegistrationTableTableTableManager get registrationTable =>
      $$RegistrationTableTableTableManager(_db, _db.registrationTable);
  $$CertificateTableTableTableManager get certificateTable =>
      $$CertificateTableTableTableManager(_db, _db.certificateTable);
  $$OrganizationSelectionTableTableTableManager
      get organizationSelectionTable =>
          $$OrganizationSelectionTableTableTableManager(
              _db, _db.organizationSelectionTable);
  $$AppManagerSelectionTableTableTableManager get appManagerSelectionTable =>
      $$AppManagerSelectionTableTableTableManager(
          _db, _db.appManagerSelectionTable);
  $$SchemeAppManagerTableTableTableManager get schemeAppManagerTable =>
      $$SchemeAppManagerTableTableTableManager(_db, _db.schemeAppManagerTable);
  $$SchemeDocumentTypeTableTableTableManager get schemeDocumentTypeTable =>
      $$SchemeDocumentTypeTableTableTableManager(
          _db, _db.schemeDocumentTypeTable);
  $$SchemeOrganizationTableTableTableManager get schemeOrganizationTable =>
      $$SchemeOrganizationTableTableTableManager(
          _db, _db.schemeOrganizationTable);
  $$PaymentPlanGroupsTableTableTableManager get paymentPlanGroupsTable =>
      $$PaymentPlanGroupsTableTableTableManager(
          _db, _db.paymentPlanGroupsTable);
  $$PaymentPlanRulesStorageTableTableTableManager
      get paymentPlanRulesStorageTable =>
          $$PaymentPlanRulesStorageTableTableTableManager(
              _db, _db.paymentPlanRulesStorageTable);
  $$AppKeyPairTableTableTableManager get appKeyPairTable =>
      $$AppKeyPairTableTableTableManager(_db, _db.appKeyPairTable);
  $$AppSessionTableTableTableManager get appSessionTable =>
      $$AppSessionTableTableTableManager(_db, _db.appSessionTable);
  $$FinancialClaimsInformationStorageTableTableTableManager
      get financialClaimsInformationStorageTable =>
          $$FinancialClaimsInformationStorageTableTableTableManager(
              _db, _db.financialClaimsInformationStorageTable);
  $$LogRecordTableTableTableManager get logRecordTable =>
      $$LogRecordTableTableTableManager(_db, _db.logRecordTable);
}

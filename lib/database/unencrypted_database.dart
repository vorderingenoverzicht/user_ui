import 'package:drift/drift.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/logging/models/log_record.dart';

import 'connection/connection.dart' as impl;

part 'unencrypted_database.g.dart';

@DriftDatabase(tables: [LogRecordTable])
class UnencryptedDatabase extends _$UnencryptedDatabase {
  UnencryptedDatabase() : super(impl.connect("app.db", ""));

  // Named constructor for creating in-memory database
  UnencryptedDatabase.forTesting(super.e);

  @override
  int get schemaVersion => 10;

  @override
  MigrationStrategy get migration {
    return MigrationStrategy(
      onCreate: (Migrator m) async {
        await m.createAll();
      },
      onUpgrade: (Migrator m, int from, int to) async {
        // NOTE: removed migration to version 4, as the renamed table has been dropped.

        if (from < schemaVersion) {
          // TODO: for now drop and create all tables after new schema version. In the future we need to be backwards compatible.
          for (final table in allTables) {
            await m.drop(table);
          }

          await m.createAll();
        }
      },
    );
  }
}

final unencryptedDatabaseProvider = Provider<UnencryptedDatabase>(
  (ref) => UnencryptedDatabase(),
);

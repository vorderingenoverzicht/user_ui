// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/certificate.dart';
import 'package:user_ui/repositories/certificate/certificate_repository.dart';

part 'latest_certificate_with_scope_notifier.g.dart';

@riverpod
class LatestCertificateWithScope extends _$LatestCertificateWithScope {
  @override
  Future<Certificate?> build(String scope) async {
    final certificates = await ref.watch(certificateRepositoryProvider.future);

    final scopedCertificates =
        certificates.where((certificate) => certificate.scope == scope);

    if (scopedCertificates.isEmpty) {
      return null;
    }

    return scopedCertificates.last;
  }
}

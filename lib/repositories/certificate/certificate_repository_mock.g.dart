// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'certificate_repository_mock.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$certificateRepositoryMockHash() =>
    r'9b4bf71e9a8f38a738b1c38670abcc755bf1c5da';

/// See also [CertificateRepositoryMock].
@ProviderFor(CertificateRepositoryMock)
final certificateRepositoryMockProvider = AutoDisposeAsyncNotifierProvider<
    CertificateRepositoryMock, List<Certificate>>.internal(
  CertificateRepositoryMock.new,
  name: r'certificateRepositoryMockProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$certificateRepositoryMockHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$CertificateRepositoryMock
    = AutoDisposeAsyncNotifier<List<Certificate>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

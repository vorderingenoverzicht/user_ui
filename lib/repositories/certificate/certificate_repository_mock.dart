// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:drift/drift.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/certificate.dart';
import 'package:user_ui/entities/certificate_type.dart';
import 'package:user_ui/repositories/certificate/certificate_repository.dart';

part 'certificate_repository_mock.g.dart';

@riverpod
class CertificateRepositoryMock extends _$CertificateRepositoryMock
    implements CertificateRepository {
  final List<Certificate> _certificateList = [];

  Future<List<Certificate>> get _certificates async {
    return _certificateList;
  }

  @override
  Future<List<Certificate>> build() {
    return _certificates;
  }

  @override
  Future<void> addCertificate({
    required CertificateType type,
    required Uint8List value,
    required String bsn,
    required String givenName,
    required DateTime expiresAt,
    required String scope,
  }) async {
    _certificateList.add(Certificate(
      id: 1,
      type: type,
      value: value,
      bsn: bsn,
      givenName: givenName,
      expiresAt: expiresAt,
      deemedExpiredBySourceOrganization: false,
      scope: scope,
    ));

    state = AsyncData(await _certificates);
  }

  @override
  Future<void> updateCertificate(Certificate certificate) async {
    _certificateList.removeWhere((element) => element.id == certificate.id);
    _certificateList.add(certificate);
    state = AsyncData(await _certificates);
  }

  @override
  Future<void> clear() async {
    _certificateList.clear();
    state = AsyncData(await _certificates);
  }
}

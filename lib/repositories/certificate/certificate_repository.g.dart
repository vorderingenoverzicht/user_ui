// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'certificate_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$certificateRepositoryHash() =>
    r'00db78867f286a9439dca8fae07b1cd390682b08';

/// See also [CertificateRepository].
@ProviderFor(CertificateRepository)
final certificateRepositoryProvider = AutoDisposeAsyncNotifierProvider<
    CertificateRepository, List<Certificate>>.internal(
  CertificateRepository.new,
  name: r'certificateRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$certificateRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$CertificateRepository = AutoDisposeAsyncNotifier<List<Certificate>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

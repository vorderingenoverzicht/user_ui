// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:drift/drift.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/database/encrypted_database.dart';
import 'package:user_ui/entities/certificate.dart';
import 'package:user_ui/entities/certificate_type.dart';

part 'certificate_repository.g.dart';

@riverpod
class CertificateRepository extends _$CertificateRepository {
  Future<List<Certificate>> get _certificates async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    var certificateModelList =
        await database.select(database.certificateTable).get();

    return certificateModelList
        .map(
          (model) => Certificate(
            id: model.id,
            type: CertificateType.values
                .firstWhere((type) => type.value == model.type),
            value: model.value,
            bsn: model.bsn,
            givenName: model.givenName,
            expiresAt: model.expiresAt,
            deemedExpiredBySourceOrganization:
                model.deemedExpiredBySourceOrganization,
            scope: model.scope,
          ),
        )
        .toList();
  }

  @override
  Future<List<Certificate>> build() {
    return _certificates;
  }

  Future<void> addCertificate({
    required CertificateType type,
    required Uint8List value,
    required String bsn,
    required String givenName,
    required DateTime expiresAt,
    required String scope,
  }) async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    await database.into(database.certificateTable).insert(
          CertificateTableCompanion.insert(
            type: type.value,
            value: value,
            bsn: bsn,
            givenName: givenName,
            expiresAt: expiresAt,
            deemedExpiredBySourceOrganization: false,
            scope: scope,
          ),
        );

    state = AsyncData(await _certificates);
  }

  Future<void> updateCertificate(Certificate certificate) async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    var certificateModel = await (database.select(database.certificateTable)
          ..where((table) => table.id.equals(certificate.id)))
        .getSingle();

    var certificateUpdateModel = certificateModel.copyWith(
      type: certificate.type.value,
      value: certificate.value,
      bsn: certificate.bsn,
      givenName: certificate.givenName,
      expiresAt: certificate.expiresAt,
      deemedExpiredBySourceOrganization:
          certificate.deemedExpiredBySourceOrganization,
      scope: certificate.scope,
    );

    await database
        .update(database.certificateTable)
        .replace(certificateUpdateModel);

    state = AsyncData(await _certificates);
  }

  Future<void> clear() async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    await database.delete(database.certificateTable).go();

    state = AsyncData(await _certificates);
  }
}

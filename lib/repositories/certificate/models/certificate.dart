import 'package:drift/drift.dart';

@DataClassName('CertificateDriftModel')
class CertificateTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get type => text()();
  BlobColumn get value => blob()();
  TextColumn get bsn => text()();
  TextColumn get givenName => text()();
  DateTimeColumn get expiresAt => dateTime()();
  BoolColumn get deemedExpiredBySourceOrganization => boolean()();
  TextColumn get scope => text()();
}

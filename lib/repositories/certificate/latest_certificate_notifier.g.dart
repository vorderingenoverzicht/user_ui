// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'latest_certificate_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$latestCertificateHash() => r'd02c409d418cb266aae6afea140cb1d8154581f8';

/// See also [LatestCertificate].
@ProviderFor(LatestCertificate)
final latestCertificateProvider =
    AutoDisposeAsyncNotifierProvider<LatestCertificate, Certificate?>.internal(
  LatestCertificate.new,
  name: r'latestCertificateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$latestCertificateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$LatestCertificate = AutoDisposeAsyncNotifier<Certificate?>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/financial_claims_information_storage/financial_claims_information_storage_repository.dart';

class FinancialClaimsInformationRepositoryMock
    extends StateNotifier<List<FinancialClaimsInformationStorage>>
    implements FinancialClaimsInformationStorageRepository {
  List<FinancialClaimsInformationStorage> listOfStorage = [];

  FinancialClaimsInformationRepositoryMock() : super([]) {
    financialClaimsInformationStorage.then(
        (financialClaimsInformationStorage) =>
            state = financialClaimsInformationStorage);
  }

  @override
  Future<void> add(
    String oin,
    DateTime dateTimeRequested,
    DateTime dateTimeReceived,
    String financialClaimsInformationDocument,
  ) async {
    final mockFcidV4Json =
        await rootBundle.loadString('assets/mock_fcid_v4.json');

    FinancialClaimsInformationStorage mockFcidV4 =
        FinancialClaimsInformationStorage(
      id: 1,
      oin: '00000002003214394002',
      dateTimeRequested: DateTime.now(),
      dateTimeReceived: DateTime.now(),
      financialClaimsInformationDocument: mockFcidV4Json,
    );

    listOfStorage.add(mockFcidV4);
    state = listOfStorage;
  }

  @override
  Future<List<FinancialClaimsInformationStorage>>
      get financialClaimsInformationStorage async {
    return listOfStorage;
  }

  @override
  Future<FinancialClaimsInformationStorage> getById(int id) async {
    return listOfStorage.firstWhere((f) => f.id == id);
  }

  @override
  Future<void> clear() async {
    listOfStorage = [];
    state = listOfStorage;
  }
}

final financialClaimsInformationStorageRepositoryMockProvider =
    StateNotifierProvider<FinancialClaimsInformationRepositoryMock,
            List<FinancialClaimsInformationStorage>>(
        (ref) => FinancialClaimsInformationRepositoryMock());

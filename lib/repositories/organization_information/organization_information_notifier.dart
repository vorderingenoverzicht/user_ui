import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/organization_information.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

part 'organization_information_notifier.g.dart';

@riverpod
Future<OrganizationInformation> getOrganizationInformation(Ref ref,
    {required String oin}) async {
  final loggingHelper = ref.read(loggingProvider);

  // Load JSON from the assets folder
  String jsonString =
      await rootBundle.loadString('assets/organization_claims_overview.json');

  // Parse JSON into a List of Organization objects
  final jsonList = jsonDecode(jsonString);

  // Convert to List of organizations
  List<OrganizationInformation> organizationsInformationList = jsonList
      .map<OrganizationInformation>(
          (json) => OrganizationInformation.fromJson(json))
      .toList();

  // Return selected organization or null
  final information = organizationsInformationList.firstWhere(
    (org) => org.oin == oin,
    orElse: () {
      loggingHelper.addLog(DeviceEvent.uu_71,
          "Failed to get organization information - no organization matches oin: $oin");
      throw Exception(
          "Failed to get organization information - no organization matches oin: $oin");
    },
  );

  return information;
}

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:drift/drift.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/encrypted_database.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

abstract class FinancialClaimsInformationRequestRepository
    implements StateNotifier<List<FinancialClaimsInformationRequest>> {
  Future<void> add({
    required String oin,
    required DateTime dateTimeRequested,
    required FinancialClaimsInformationRequestStatus status,
  });
  Future<void> update(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  );
  Future<List<FinancialClaimsInformationRequest>>
      get financialClaimsInformationRequests;
  Stream<void> watchProcessable();
  Future<FinancialClaimsInformationRequest?> getAndLockNextProcessable();
  Future<void> releaseLockForUncompletedRequests();
  Future<void> cancelAllUncompletedRequests();
  Future<void> cancelFailedRequests(int id);
  Future<void> clear();
  Future<void> releaseLock(int id);
}

class DriftFinancialClaimsInformationRequestRepository
    extends StateNotifier<List<FinancialClaimsInformationRequest>>
    implements FinancialClaimsInformationRequestRepository {
  final EncryptedDatabase database;
  final LoggingHelper loggingHelper;

  DriftFinancialClaimsInformationRequestRepository(
      this.database, this.loggingHelper)
      : super([]) {
    financialClaimsInformationRequests.then(
        (financialClaimInformationRequests) =>
            state = financialClaimInformationRequests);
  }

  @override
  Future<void> add(
      {required String oin,
      required DateTime dateTimeRequested,
      required FinancialClaimsInformationRequestStatus status}) async {
    await database.into(database.financialClaimsInformationRequestTable).insert(
        FinancialClaimsInformationRequestTableCompanion.insert(
            oin: oin,
            dateTimeRequested: dateTimeRequested,
            status: status.value,
            lock: false));

    state = await financialClaimsInformationRequests;
  }

  @override
  Future<void> clear() async {
    await database.delete(database.financialClaimsInformationRequestTable).go();

    state = await financialClaimsInformationRequests;
  }

  @override
  Future<List<FinancialClaimsInformationRequest>>
      get financialClaimsInformationRequests async {
    var financialClaimsInformationRequestModelList = await database
        .select(database.financialClaimsInformationRequestTable)
        .get();

    return financialClaimsInformationRequestModelList
        .map(
          (model) => FinancialClaimsInformationRequest(
            id: model.id,
            oin: model.oin,
            dateTimeRequested: model.dateTimeRequested,
            lock: model.lock,
            status: FinancialClaimsInformationRequestStatus.values
                .firstWhere((e) => e.value == model.status),
          ),
        )
        .toList();
  }

  @override
  Future<FinancialClaimsInformationRequest?> getAndLockNextProcessable() async {
    return database.transaction(() async {
      var financialClaimsInformationRequestModel = await (database
              .select(database.financialClaimsInformationRequestTable)
            ..where(
              (table) =>
                  table.lock.not() &
                  table.status.isNotIn(
                    financialClaimsInformationRequestEndStatusList
                        .map((e) => e.value),
                  ),
            )
            ..limit(1))
          .getSingleOrNull();

      if (financialClaimsInformationRequestModel == null) {
        return null;
      }

      var financialClaimsInformationRequestUpdateModel =
          financialClaimsInformationRequestModel.copyWith(lock: true);

      await database
          .update(database.financialClaimsInformationRequestTable)
          .replace(financialClaimsInformationRequestUpdateModel);

      state = await financialClaimsInformationRequests;

      return FinancialClaimsInformationRequest(
        id: financialClaimsInformationRequestUpdateModel.id,
        oin: financialClaimsInformationRequestUpdateModel.oin,
        dateTimeRequested:
            financialClaimsInformationRequestUpdateModel.dateTimeRequested,
        lock: financialClaimsInformationRequestUpdateModel.lock,
        status: FinancialClaimsInformationRequestStatus.values.firstWhere((e) =>
            e.value == financialClaimsInformationRequestUpdateModel.status),
      );
    });
  }

  @override
  Future<void> releaseLock(int id) async {
    await (database.update(database.financialClaimsInformationRequestTable)
          ..where(
            (table) => table.id.isValue(id),
          ))
        .write(
      const FinancialClaimsInformationRequestTableCompanion(
        lock: Value(false),
      ),
    );

    state = await financialClaimsInformationRequests;
  }

  @override
  Future<void> releaseLockForUncompletedRequests() async {
    await (database.update(database.financialClaimsInformationRequestTable)
          ..where(
            (table) =>
                table.lock.isValue(false) &
                table.status.isNotIn(
                  financialClaimsInformationRequestEndStatusList
                      .map((e) => e.value),
                ),
          ))
        .write(
      const FinancialClaimsInformationRequestTableCompanion(
        lock: Value(false),
      ),
    );

    state = await financialClaimsInformationRequests;
  }

  @override
  Future<void> cancelAllUncompletedRequests() async {
    await (database.update(database.financialClaimsInformationRequestTable)
          ..where(
            (table) => table.status.isNotIn(
              financialClaimsInformationRequestEndStatusList
                  .map((e) => e.value),
            ),
          ))
        .write(
      FinancialClaimsInformationRequestTableCompanion(
        lock: const Value(false),
        status: Value(FinancialClaimsInformationRequestStatus.cancelled.value),
      ),
    );

    state = await financialClaimsInformationRequests;
  }

  @override
  Future<void> update(
      FinancialClaimsInformationRequest
          financialClaimsInformationRequest) async {
    var financialClaimsInformationRequestModel =
        await (database.select(database.financialClaimsInformationRequestTable)
              ..where(
                (table) =>
                    table.id.equals(financialClaimsInformationRequest.id),
              ))
            .getSingle();

    if (financialClaimsInformationRequestModel.status ==
        FinancialClaimsInformationRequestStatus.cancelled.value) {
      loggingHelper.addLog(
          DeviceEvent.uu_81, "can not update request, as it is cancelled");
      throw Exception("can not update request, as it is cancelled");
    }

    var financialClaimsInformationRequestUpdateModel =
        financialClaimsInformationRequestModel.copyWith(
      dateTimeRequested: financialClaimsInformationRequest.dateTimeRequested,
      lock: financialClaimsInformationRequest.lock,
      oin: financialClaimsInformationRequest.oin,
      status: financialClaimsInformationRequest.status.value,
    );

    await database
        .update(database.financialClaimsInformationRequestTable)
        .replace(financialClaimsInformationRequestUpdateModel);

    state = await financialClaimsInformationRequests;
  }

  @override
  Stream<void> watchProcessable() {
    var variablePlaceholders = "";

    /// This loop iterates over the financialClaimsInformationRequestEndStatusList to create a comma-separated list of ? placeholders.
    /// These placeholders will be used in the SQL query to safely insert the status values.
    for (var i = 0;
        i < financialClaimsInformationRequestEndStatusList.length;
        i++) {
      if (variablePlaceholders.isNotEmpty) {
        variablePlaceholders += ",";
      }

      variablePlaceholders += "?";
    }

    return database.customSelect(
        'SELECT EXISTS ('
        ' SELECT 1 FROM financial_claims_information_request_table'
        ' WHERE lock = 0 AND status NOT IN ($variablePlaceholders)'
        ')',
        variables: financialClaimsInformationRequestEndStatusList
            .map((e) => Variable.withString(e.value))
            .toList(),
        readsFrom: {database.financialClaimsInformationRequestTable}).watch();
  }

  @override
  Future<void> cancelFailedRequests(int id) async {
    await (database.update(database.financialClaimsInformationRequestTable)
          ..where(
            (table) =>
                table.status.isNotIn(
                  financialClaimsInformationRequestEndStatusList
                      .map((e) => e.value),
                ) &
                table.id.isValue(id),
          ))
        .write(
      FinancialClaimsInformationRequestTableCompanion(
        lock: const Value(false),
        status: Value(FinancialClaimsInformationRequestStatus.cancelled.value),
      ),
    );

    state = await financialClaimsInformationRequests;
  }
}

final financialClaimsInformationRequestRepositoryProvider =
    StateNotifierProvider<FinancialClaimsInformationRequestRepository,
        List<FinancialClaimsInformationRequest>>((ref) {
  final database = ref.read(encryptedDatabaseProvider);

  final loggingHelper = ref.read(loggingProvider);

  return DriftFinancialClaimsInformationRequestRepository(
      database, loggingHelper);
});

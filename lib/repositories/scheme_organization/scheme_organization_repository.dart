// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:fcid_library/fcid_library.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/encrypted_database.dart';

abstract class SchemeOrganizationRepository
    implements StateNotifier<List<SchemeOrganization>> {
  Future<List<SchemeOrganization>> get organizations;
  Future<void> add(
      {required String oin,
      required String name,
      required String publicKey,
      required String discoveryUrl,
      required bool available});
  Future<void> update(SchemeOrganization organization);
  Future<void> clear();
}

class DriftSchemeOrganizationRepository
    extends StateNotifier<List<SchemeOrganization>>
    implements SchemeOrganizationRepository {
  final EncryptedDatabase database;

  DriftSchemeOrganizationRepository(this.database) : super([]) {
    organizations.then((organizations) => state = organizations);
  }

  @override
  Future<void> add(
      {required String oin,
      required String name,
      required String publicKey,
      required String discoveryUrl,
      required bool available}) async {
    await database.into(database.schemeOrganizationTable).insert(
        SchemeOrganizationTableCompanion.insert(
            oin: oin,
            name: name,
            publicKey: publicKey,
            discoveryUrl: discoveryUrl,
            available: available));

    state = await organizations;
  }

  @override
  Future<List<SchemeOrganization>> get organizations async {
    var schemeOrganizationModelList =
        await database.select(database.schemeOrganizationTable).get();

    return schemeOrganizationModelList
        .map(
          (model) => SchemeOrganization(
            id: model.id,
            oin: model.oin,
            available: model.available,
            discoveryUrl: model.discoveryUrl,
            name: model.name,
            publicKey: model.publicKey,
          ),
        )
        .toList();
  }

  @override
  Future<void> update(
    SchemeOrganization organization,
  ) async {
    var schemeOrganizationModel =
        await (database.select(database.schemeOrganizationTable)
              ..where((table) => table.id.equals(organization.id)))
            .getSingle();

    var schemeOrganizationUpdateModel = schemeOrganizationModel.copyWith(
      oin: organization.oin,
      available: organization.available,
      discoveryUrl: organization.discoveryUrl,
      name: organization.name,
      publicKey: organization.publicKey,
    );

    await database
        .update(database.schemeOrganizationTable)
        .replace(schemeOrganizationUpdateModel);

    state = await organizations;
  }

  @override
  Future<void> clear() async {
    await database.delete(database.schemeOrganizationTable).go();

    state = await organizations;
  }
}

final schemeOrganizationRepositoryProvider = StateNotifierProvider<
    SchemeOrganizationRepository, List<SchemeOrganization>>((ref) {
  final database = ref.watch(encryptedDatabaseProvider);

  return DriftSchemeOrganizationRepository(database);
});

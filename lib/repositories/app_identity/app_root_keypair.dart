// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:hardware_keystore/hardware_keystore.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';

part 'app_root_keypair.g.dart';

@riverpod
class AppRootKeypair extends _$AppRootKeypair {
  static const String keyId = "vo_rijk_key_id";

  @override
  Future<bool> build() async {
    return await _verifyKeypair();
  }

  Future<bool> _verifyKeypair() async {
    String? publicKey;
    try {
      publicKey = await HardwareKeystore.getPublicKey(keyId);
    } catch (_) {
      ref
          .read(loggingProvider)
          .addLog(DeviceEvent.uu_118, "Failed to verify app root public key");
      return false;
    }

    if (publicKey == null) {
      ref
          .read(loggingProvider)
          .addLog(DeviceEvent.uu_119, "App root public key does not exist");
      return false;
    }
    return true;
  }

  Future<bool> generateAndStoreAppKeyPair() async {
    var userSettings = ref.read(userSettingsRepositoryProvider);
    if (userSettings.delay) {
      await Future.delayed(const Duration(seconds: 2));
    }

    await HardwareKeystore.generateKeyPairs(keyId);
    state = AsyncValue.data(await _verifyKeypair());
    return _verifyKeypair();
  }

  Future<String?> getPublicKey() async {
    String? rootAppPublicKey;

    try {
      rootAppPublicKey = await HardwareKeystore.getPublicKey(keyId);
    } catch (ex) {
      ref.read(loggingProvider).addLog(DeviceEvent.uu_126, ex.toString());
      throw Exception('Failed to get app root public key');
    }

    return rootAppPublicKey;
  }

  Future<String?> sign(String message) async {
    String? signature;

    try {
      signature = await HardwareKeystore.sign(
        keyId,
        message,
      );
    } catch (ex) {
      ref.read(loggingProvider).addLog(DeviceEvent.uu_125, ex.toString());
      throw Exception('Failed to sign message');
    }

    return signature;
  }

  Future<bool?> deleteKeyPairs() async {
    bool? isDeleted;

    try {
      isDeleted = await HardwareKeystore.deleteKeyPairs(keyId);
    } catch (ex) {
      ref.read(loggingProvider).addLog(DeviceEvent.uu_123, ex.toString());
      throw Exception('Failed to delete keypair');
    }

    return isDeleted;
  }
}

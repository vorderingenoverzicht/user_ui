import 'package:drift/drift.dart';

@DataClassName('AppKeyPairDriftModel')
class AppKeyPairTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get privateKey => text()();
  TextColumn get publicKey => text()();
}

import 'package:drift/drift.dart';

@DataClassName('AppSessionDriftModel')
class AppSessionTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get key => text()();
  TextColumn get token => text()();
  TextColumn get oin => text()();
}

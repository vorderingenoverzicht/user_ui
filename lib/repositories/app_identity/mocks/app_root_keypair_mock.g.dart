// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_root_keypair_mock.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$appRootKeypairMockHash() =>
    r'8dd621f8b708949da16b45c7db592a66aa2eab7a';

/// See also [AppRootKeypairMock].
@ProviderFor(AppRootKeypairMock)
final appRootKeypairMockProvider =
    AutoDisposeAsyncNotifierProvider<AppRootKeypairMock, bool>.internal(
  AppRootKeypairMock.new,
  name: r'appRootKeypairMockProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$appRootKeypairMockHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AppRootKeypairMock = AutoDisposeAsyncNotifier<bool>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

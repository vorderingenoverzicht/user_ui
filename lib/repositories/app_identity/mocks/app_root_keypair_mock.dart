// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/app_identity/app_root_keypair.dart';

part 'app_root_keypair_mock.g.dart';

@riverpod
class AppRootKeypairMock extends _$AppRootKeypairMock
    implements AppRootKeypair {
  @override
  Future<bool> build() async {
    return true;
  }

  @override
  Future<bool?> deleteKeyPairs() async {
    return true;
  }

  @override
  Future<bool> generateAndStoreAppKeyPair() async {
    return true;
  }

  @override
  Future<String?> getPublicKey() async {
    final keys =
        jsonDecode(await rootBundle.loadString('assets/mock_app_keys.json'));
    return keys["publicKey"];
  }

  @override
  Future<String?> sign(String message) async {
    return "signature";
  }
}

import 'package:drift/drift.dart';

@DataClassName('PaymentPlanRulesStorageDriftModel')
class PaymentPlanRulesStorageTable extends Table {
  IntColumn get id => integer().unique()();
  TextColumn get organisaties => text()();
  IntColumn get minimumBedrag => integer()();
  IntColumn get maximumBedrag => integer()();
  TextColumn get type => text()();
}

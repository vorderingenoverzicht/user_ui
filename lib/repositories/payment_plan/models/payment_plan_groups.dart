import 'package:drift/drift.dart';

@DataClassName('PaymentPlanGroupsModel')
class PaymentPlanGroupsTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get zaakKenmerken => text()();
  IntColumn get saldo => integer()();
  IntColumn get termijnen => integer()();
  IntColumn get paymentRules => integer()();
}

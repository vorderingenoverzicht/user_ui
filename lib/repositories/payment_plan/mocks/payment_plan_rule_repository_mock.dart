import 'package:fcid_library/fcid_library.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/encrypted_database.dart';
import 'package:user_ui/repositories/payment_plan/payment_plan_rule_repository.dart';

abstract class PaymentPlanRuleRepositoryMock
    implements StateNotifier<List<PaymentPlanRule>> {
  Future<void> addRule(PaymentPlanRule paymentPlanRule);
  Future<List<PaymentPlanRule>> get rules;
  Future<void> clear();
  Future<void> addRules(List<PaymentPlanRule> paymentPlanRules);
}

class DriftPaymentPlanRuleRepositoryMock
    extends StateNotifier<List<PaymentPlanRule>>
    implements PaymentPlanRuleRepository {
  List<PaymentPlanRule> listOfRules = [];

  DriftPaymentPlanRuleRepositoryMock() : super([]) {
    rules.then((rules) => state = rules);
  }

  @override
  Future<void> addRule(PaymentPlanRule paymentPlanRule) async {
    listOfRules.add(paymentPlanRule);
    state = listOfRules;
  }

  @override
  Future<void> clear() async {
    listOfRules = [];
    state = await rules;
  }

  @override
  Future<List<PaymentPlanRule>> get rules async {
    return listOfRules;
  }

  @override
  Future<void> addRules(List<PaymentPlanRule> paymentPlanRules) async {
    listOfRules.addAll(paymentPlanRules);
    state = await rules;
  }
}

final paymentPlanRuleRepositoryMockProvider =
    StateNotifierProvider<PaymentPlanRuleRepository, List<PaymentPlanRule>>(
        (ref) {
  final database = ref.watch(encryptedDatabaseProvider);
  return DriftPaymentPlanRuleRepository(database);
});

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/encrypted_database.dart';
import 'package:user_ui/entities/scheme_app_manager.dart';

abstract class SchemeAppManagerRepository
    implements StateNotifier<List<SchemeAppManager>> {
  Future<List<SchemeAppManager>> get appManagers;
  Future<void> add(
      {required String oin,
      required String name,
      required String publicKey,
      required String discoveryUrl,
      required bool available});
  Future<void> update(SchemeAppManager appManager);
  Future<void> clear();
}

class DriftSchemeAppManagerRepository
    extends StateNotifier<List<SchemeAppManager>>
    implements SchemeAppManagerRepository {
  final EncryptedDatabase database;

  DriftSchemeAppManagerRepository(this.database) : super([]) {
    appManagers.then((appManagers) => state = appManagers);
  }

  @override
  Future<void> add(
      {required String oin,
      required String name,
      required String publicKey,
      required String discoveryUrl,
      required bool available}) async {
    await database
        .into(database.schemeAppManagerTable)
        .insert(SchemeAppManagerTableCompanion.insert(
          oin: oin,
          name: name,
          publicKey: publicKey,
          discoveryUrl: discoveryUrl,
          available: available,
        ));

    state = await appManagers;
  }

  @override
  Future<List<SchemeAppManager>> get appManagers async {
    var schemeAppManagerModelList =
        await database.select(database.schemeAppManagerTable).get();
    return schemeAppManagerModelList
        .map(
          (model) => SchemeAppManager(
            id: model.id,
            oin: model.oin,
            available: model.available,
            discoveryUrl: model.discoveryUrl,
            name: model.name,
            publicKey: model.publicKey,
          ),
        )
        .toList();
  }

  @override
  Future<void> update(
    SchemeAppManager appManager,
  ) async {
    var schemeAppManagerModel =
        await (database.select(database.schemeAppManagerTable)
              ..where((table) => table.id.equals(appManager.id)))
            .getSingle();

    var schemeAppManagerUpdateModel = schemeAppManagerModel.copyWith(
      oin: appManager.oin,
      available: appManager.available,
      discoveryUrl: appManager.discoveryUrl,
      name: appManager.name,
      publicKey: appManager.publicKey,
    );

    await database
        .update(database.schemeAppManagerTable)
        .replace(schemeAppManagerUpdateModel);

    state = await appManagers;
  }

  @override
  Future<void> clear() async {
    await database.delete(database.schemeAppManagerTable).go();

    state = await appManagers;
  }
}

final schemeAppManagerRepositoryProvider =
    StateNotifierProvider<SchemeAppManagerRepository, List<SchemeAppManager>>(
        (ref) {
  final database = ref.watch(encryptedDatabaseProvider);

  return DriftSchemeAppManagerRepository(database);
});

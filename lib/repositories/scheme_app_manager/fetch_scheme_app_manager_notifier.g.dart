// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fetch_scheme_app_manager_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$fetchSchemeAppManagerHash() =>
    r'c18d3035954c692c6949ac385cee91c520513e9d';

/// See also [FetchSchemeAppManager].
@ProviderFor(FetchSchemeAppManager)
final fetchSchemeAppManagerProvider = AutoDisposeAsyncNotifierProvider<
    FetchSchemeAppManager, SchemeAppManager>.internal(
  FetchSchemeAppManager.new,
  name: r'fetchSchemeAppManagerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$fetchSchemeAppManagerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$FetchSchemeAppManager = AutoDisposeAsyncNotifier<SchemeAppManager>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

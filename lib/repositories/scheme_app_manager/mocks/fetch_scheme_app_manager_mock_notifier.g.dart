// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fetch_scheme_app_manager_mock_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$fetchSchemeAppManagerMockHash() =>
    r'123e53e6b0cb6652fc8779d848a149b6bfbb3138';

/// See also [FetchSchemeAppManagerMock].
@ProviderFor(FetchSchemeAppManagerMock)
final fetchSchemeAppManagerMockProvider = AutoDisposeAsyncNotifierProvider<
    FetchSchemeAppManagerMock, SchemeAppManager>.internal(
  FetchSchemeAppManagerMock.new,
  name: r'fetchSchemeAppManagerMockProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$fetchSchemeAppManagerMockHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$FetchSchemeAppManagerMock
    = AutoDisposeAsyncNotifier<SchemeAppManager>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

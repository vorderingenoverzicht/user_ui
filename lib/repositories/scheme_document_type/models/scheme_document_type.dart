import 'package:drift/drift.dart';

@DataClassName('SchemeDocumentTypeDriftModel')
class SchemeDocumentTypeTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text()();
  BoolColumn get available => boolean()();
}

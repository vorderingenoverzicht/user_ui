// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/registration.dart';
import 'package:user_ui/repositories/registration/latest_usable_registration_notifier.dart';

part 'latest_usable_registration_mock_notifier.g.dart';

@riverpod
class LatestUsableRegistrationMock extends _$LatestUsableRegistrationMock
    implements LatestUsableRegistration {
  @override
  Future<Registration> build() async {
    return Registration(
      id: 1,
      dateTimeStarted: DateTime.now(),
      appPublicKey: 'appPublicKey',
      appManagerOin: 'appManagerOin',
      appManagerPublicKey: 'appManagerPublicKey',
      registrationToken: 'registrationToken',
      dateTimeCompleted: DateTime.now(),
      givenName: 'givenName',
      expired: false,
      revoked: false,
    );
  }
}

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/registration.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';

part 'latest_usable_registration_notifier.g.dart';

@riverpod
class LatestUsableRegistration extends _$LatestUsableRegistration {
  @override
  Future<Registration> build() async {
    final registrations =
        await ref.watch(registrationRepositoryProvider.future);

    var usableRegistrations = registrations.where((registration) =>
        !registration.revoked &&
        !registration.expired &&
        registration.dateTimeCompleted != null);

    return usableRegistrations.last;
  }
}

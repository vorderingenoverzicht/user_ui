// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'latest_usable_registration_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$latestUsableRegistrationHash() =>
    r'5bed5c55187393687cf51d9a795524670c690d5c';

/// See also [LatestUsableRegistration].
@ProviderFor(LatestUsableRegistration)
final latestUsableRegistrationProvider = AutoDisposeAsyncNotifierProvider<
    LatestUsableRegistration, Registration>.internal(
  LatestUsableRegistration.new,
  name: r'latestUsableRegistrationProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$latestUsableRegistrationHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$LatestUsableRegistration = AutoDisposeAsyncNotifier<Registration>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

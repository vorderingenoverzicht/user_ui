// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:drift/drift.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/database/encrypted_database.dart';
import 'package:user_ui/entities/registration.dart';

part 'registration_repository.g.dart';

@riverpod
class RegistrationRepository extends _$RegistrationRepository {
  Future<List<Registration>> get _registrations async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    var registrationModelList =
        await database.select(database.registrationTable).get();

    return registrationModelList
        .map(
          (model) => Registration(
            id: model.id,
            dateTimeStarted: model.dateTimeStarted,
            appPublicKey: model.appPublicKey,
            appManagerOin: model.appManagerOin,
            appManagerPublicKey: model.appManagerPublicKey,
            registrationToken: model.registrationToken,
            dateTimeCompleted: model.dateTimeCompleted,
            givenName: model.givenName,
            expired: model.expired,
            revoked: model.revoked,
          ),
        )
        .toList();
  }

  @override
  Future<List<Registration>> build() {
    return _registrations;
  }

  Future<void> addRegistration({
    required DateTime dateTimeStarted,
    required String appPublicKey,
    required String appManagerOin,
    required String appManagerPublicKey,
    required String registrationToken,
  }) async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    await database.into(database.registrationTable).insert(
          RegistrationTableCompanion.insert(
            dateTimeStarted: dateTimeStarted,
            appPublicKey: appPublicKey,
            appManagerOin: appManagerOin,
            appManagerPublicKey: appManagerPublicKey,
            registrationToken: registrationToken,
            expired: false,
            revoked: false,
          ),
        );

    state = AsyncData(await _registrations);
  }

  Future<void> updateRegistration(Registration registration) async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    var registrationModel = await (database.select(database.registrationTable)
          ..where((table) => table.id.equals(registration.id)))
        .getSingle();

    var registrationUpdateModel = registrationModel.copyWith(
      registrationToken: registration.registrationToken,
      appManagerOin: registration.appManagerOin,
      appManagerPublicKey: registration.appManagerPublicKey,
      appPublicKey: registration.appPublicKey,
      dateTimeCompleted: Value(registration.dateTimeCompleted),
      dateTimeStarted: registration.dateTimeStarted,
      givenName: Value(registration.givenName),
      expired: registration.expired,
      revoked: registration.revoked,
    );

    await database
        .update(database.registrationTable)
        .replace(registrationUpdateModel);

    state = AsyncData(await _registrations);
  }

  Future<void> deleteRegistration(int id) async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    await (database.delete(database.registrationTable)
          ..where((table) => table.id.equals(id)))
        .go();

    state = AsyncData(await _registrations);
  }

  Future<void> clear() async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    await database.delete(database.registrationTable).go();

    state = AsyncData(await _registrations);
  }
}

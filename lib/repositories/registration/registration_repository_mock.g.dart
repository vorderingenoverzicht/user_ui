// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'registration_repository_mock.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$registrationRepositoryMockHash() =>
    r'b72a937055af29a53cabe085d7da499a624455a4';

/// See also [RegistrationRepositoryMock].
@ProviderFor(RegistrationRepositoryMock)
final registrationRepositoryMockProvider = AutoDisposeAsyncNotifierProvider<
    RegistrationRepositoryMock, List<Registration>>.internal(
  RegistrationRepositoryMock.new,
  name: r'registrationRepositoryMockProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$registrationRepositoryMockHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$RegistrationRepositoryMock
    = AutoDisposeAsyncNotifier<List<Registration>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

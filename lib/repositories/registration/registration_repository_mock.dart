// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/registration.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';

part 'registration_repository_mock.g.dart';

@riverpod
class RegistrationRepositoryMock extends _$RegistrationRepositoryMock
    implements RegistrationRepository {
  final List<Registration> _registrationList = [];

  Future<List<Registration>> get _registrations async {
    return _registrationList;
  }

  @override
  Future<List<Registration>> build() {
    return _registrations;
  }

  @override
  Future<void> addRegistration({
    required DateTime dateTimeStarted,
    required String appPublicKey,
    required String appManagerOin,
    required String appManagerPublicKey,
    required String registrationToken,
  }) async {
    _registrationList.add(
      Registration(
        dateTimeStarted: dateTimeStarted,
        appPublicKey: appPublicKey,
        appManagerOin: appManagerOin,
        appManagerPublicKey: appManagerPublicKey,
        registrationToken: registrationToken,
        expired: false,
        revoked: false,
        id: 1,
        dateTimeCompleted: DateTime.now(),
        givenName: '',
      ),
    );

    state = AsyncData(await _registrations);
  }

  @override
  Future<void> updateRegistration(Registration registration) async {
    _registrationList.removeWhere((element) => element.id == registration.id);
    _registrationList.add(registration);
    state = AsyncData([registration]);
  }

  @override
  Future<void> deleteRegistration(int id) async {
    _registrationList.removeWhere((element) => element.id == id);
    state = AsyncData(await _registrations);
  }

  @override
  Future<void> clear() async {
    _registrationList.clear();
    state = AsyncData(await _registrations);
  }
}

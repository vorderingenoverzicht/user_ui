import 'package:drift/drift.dart';

@DataClassName('RegistrationDriftModel')
class RegistrationTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  DateTimeColumn get dateTimeStarted => dateTime()();
  TextColumn get appPublicKey => text()();
  TextColumn get appManagerOin => text()();
  TextColumn get appManagerPublicKey => text()();
  TextColumn get registrationToken => text()();
  DateTimeColumn get dateTimeCompleted => dateTime().nullable()();
  TextColumn get givenName => text().nullable()();
  BoolColumn get expired => boolean()();
  BoolColumn get revoked => boolean()();
}

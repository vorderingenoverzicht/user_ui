// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/registration.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';

part 'latest_completed_registration_notifier.g.dart';

@riverpod
class LatestCompletedRegistration extends _$LatestCompletedRegistration {
  @override
  Future<Registration> build() async {
    final registrations =
        await ref.watch(registrationRepositoryProvider.future);

    var completedRegistrations = registrations
        .where((registration) => registration.dateTimeCompleted != null);

    if (completedRegistrations.isEmpty) {
      final loggingHelper = ref.read(loggingProvider);
      loggingHelper.addLog(
          DeviceEvent.uu_89, 'No (latest) registrations available');
      throw Exception("no (latest) registrations available");
    }

    return completedRegistrations.last;
  }
}

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/unencrypted_database.dart';
import 'package:user_ui/entities/log_record.dart';

import 'logging_repository.dart';

class UnencryptedLoggingRepository extends StateNotifier<List<LogRecord>>
    implements LoggingRepository {
  final UnencryptedDatabase database;

  UnencryptedLoggingRepository(this.database) : super([]) {
    logs.then((logs) => state = logs);
  }

  @override
  Future<void> add(LogRecord log) async {
    await database.into(database.logRecordTable).insert(
          LogRecordTableCompanion.insert(
            timestamp: log.timestamp,
            host: log.host,
            cef: log.cef,
            deviceVendor: log.deviceVendor,
            deviceProduct: log.deviceProduct,
            deviceVersion: log.deviceVersion,
            deviceEventClassId: log.deviceEventClassId,
            name: log.name,
            severity: log.severity,
            flexString1Label: log.flexString1Label,
            flexString1: log.flexString1,
            flexString2Label: log.flexString2Label,
            flexString2: log.flexString2,
            act: log.act,
            app: log.app,
            request: log.request,
            requestMethod: log.requestMethod,
          ),
        );

    state = await logs;
  }

  void delete(LogRecord log) {
    database.delete(database.logRecordTable).where((t) => t.id.equals(log.id!));
  }

  @override
  Future<List<LogRecord>> get logs async {
    List<LogRecord> entries = [];
    await database
        .select(database.logRecordTable)
        .get()
        .then((logRecordsList) => {
              for (var logRecord in logRecordsList)
                {
                  entries.add(LogRecord(
                      id: logRecord.id,
                      timestamp: logRecord.timestamp,
                      host: logRecord.host,
                      cef: logRecord.cef,
                      deviceVendor: logRecord.deviceVendor,
                      deviceProduct: logRecord.deviceProduct,
                      deviceVersion: logRecord.deviceVersion,
                      deviceEventClassId: logRecord.deviceEventClassId,
                      severity: logRecord.severity,
                      flexString1Label: logRecord.flexString1Label,
                      flexString1: logRecord.flexString1,
                      flexString2Label: logRecord.flexString2Label,
                      flexString2: logRecord.flexString2,
                      act: logRecord.act,
                      app: logRecord.app,
                      request: logRecord.request,
                      requestMethod: logRecord.requestMethod,
                      name: logRecord.name))
                }
            });
    entries.sort((a, b) => b.timestamp.compareTo(a.timestamp));
    return entries;
  }

  @override
  Future<void> clear() async {
    await database.delete(database.logRecordTable).go();

    state = await logs;
  }
}

final unencryptedLoggingRepositoryProvider =
    StateNotifierProvider<UnencryptedLoggingRepository, List<LogRecord>>((ref) {
  final unencryptedDatabase = ref.watch(unencryptedDatabaseProvider);
  return UnencryptedLoggingRepository(unencryptedDatabase);
});

enum DeviceType {
  blank(""),
  web("Web"),
  android("Android"),
  ios("IOS");

  const DeviceType(this.value);

  final String value;
}

enum EventSeverityLevel {
  unknown("Unkown"),
  low("Low"),
  medium("Medium"),
  high("High"),
  veryHigh("Very High");

  const EventSeverityLevel(this.value);

  final String value;

  int toInt() {
    if (value == unknown.value) {
      return 0;
    }
    if (value == low.value) {
      return 2;
    }
    if (value == medium.value) {
      return 5;
    }
    if (value == high.value) {
      return 7;
    }
    if (value == veryHigh.value) {
      return 10;
    }
    return -1;
  }
}

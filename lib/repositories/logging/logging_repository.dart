import 'package:user_ui/entities/log_record.dart';

abstract class LoggingRepository {
  Future<void> add(LogRecord log);
  Future<void> clear();
  Future<List<LogRecord>> get logs;
}

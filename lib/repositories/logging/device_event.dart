// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:user_ui/repositories/logging/event_severity_level.dart';

enum DeviceEvent {
  uu_1("start work", EventSeverityLevel.low),
  uu_2("Fetch certificate, session error.", EventSeverityLevel.low),
  uu_3("Fetch certificate, registration error.", EventSeverityLevel.high),
  uu_4("Fetch certificate, error.", EventSeverityLevel.high),
  uu_5("Register app, session error. ", EventSeverityLevel.high),
  uu_6("Register app, error.", EventSeverityLevel.high),
  uu_7("Unregister app, session error.", EventSeverityLevel.high),
  uu_8("Unregister app, error.", EventSeverityLevel.high),
  uu_9("Get unexpired configuration for OIN. to many found",
      EventSeverityLevel.high),
  uu_10("Fetch Scheme app manager, no app manager.", EventSeverityLevel.high),
  @Deprecated("This device event is removed")
  uu_11("Start registration, key pair error", EventSeverityLevel.high),
  uu_12("Start registration, app manager error", EventSeverityLevel.low),
  @Deprecated("This device event is removed")
  uu_13("Get financial claims overview, unknown claim type",
      EventSeverityLevel.medium),
  uu_14("Get financial claims overview, json error ", EventSeverityLevel.low),
  @Deprecated("This device event is removed")
  uu_15("Create document signature, key pair error", EventSeverityLevel.low),
  uu_16("Configure Claims request.", EventSeverityLevel.high),
  uu_17("Configure claims request, session error.", EventSeverityLevel.low),
  uu_18(
      "Configure claims request, certificate error", EventSeverityLevel.medium),
  uu_19("request financial claims information", EventSeverityLevel.high),
  uu_20("Fetch scheme", EventSeverityLevel.high),
  uu_21("Fetch bk configuration", EventSeverityLevel.high),
  uu_22("Create challenge", EventSeverityLevel.high),
  uu_23("Complete challenge", EventSeverityLevel.high),
  uu_24("Fetch and store certificate, registration error",
      EventSeverityLevel.high),
  uu_25("Fetch and store certificate, app manager error",
      EventSeverityLevel.medium),
  uu_26("Start session, signature error", EventSeverityLevel.medium),
  uu_27("Add Certificate, config not found", EventSeverityLevel.low),
  @Deprecated("This device event is removed")
  uu_28("Fetch activation, keypair error", EventSeverityLevel.medium),
  uu_29("Fetch activation, public key error", EventSeverityLevel.high),
  uu_30("Fetch and store certificate, JWT expired", EventSeverityLevel.low),
  uu_31("Fetch activation, Signature error", EventSeverityLevel.medium),
  uu_32("Fetch activation, public key error", EventSeverityLevel.high),
  uu_33("Fetch activation, certificate error", EventSeverityLevel.high),
  uu_34("Discovered services, financial claims request api error",
      EventSeverityLevel.medium),
  uu_35("Discovered services, session api error", EventSeverityLevel.low),
  uu_36("Discovered services, registration api error", EventSeverityLevel.low),
  @Deprecated("This device event is removed")
  uu_37("Store Financial claims information inbox", EventSeverityLevel.high),
  uu_38("Get Financial claims information inbox from storage, unknown event",
      EventSeverityLevel.high),
  @Deprecated("This device event is removed")
  uu_39(
      "Create Financial claims information request, organization selection error",
      EventSeverityLevel.low),
  uu_40("Create configuration, already in progress", EventSeverityLevel.low),
  uu_41("Create document, config not found", EventSeverityLevel.medium),
  uu_42("Create document signature, config not found", EventSeverityLevel.low),
  uu_43("Create document signature, document not found",
      EventSeverityLevel.medium),
  uu_44("Verify certificate, no id", EventSeverityLevel.low),
  uu_45("Verify certificate, config not found", EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_46("Fetch new certificate, config not found", EventSeverityLevel.low),
  uu_47("Discovered services, app manager error", EventSeverityLevel.low),
  uu_48("Get app manager ui, app manager ui url not provided",
      EventSeverityLevel.low),
  @Deprecated("This device event is removed")
  uu_49("", EventSeverityLevel.low),
  uu_50("Create envelope, config not found", EventSeverityLevel.medium),
  uu_51("Create envelope, document not found", EventSeverityLevel.medium),
  uu_52("Create envelope, no signature", EventSeverityLevel.medium),
  uu_53("Create envelope, certificate id error", EventSeverityLevel.low),
  uu_54("", EventSeverityLevel.low),
  uu_55("Create configuration request, config not found",
      EventSeverityLevel.medium),
  uu_56("Create configuration request, envelope not found",
      EventSeverityLevel.medium),
  uu_57(
      "Configure claims request, config not found", EventSeverityLevel.medium),
  uu_58("Configure claims request, encrypted envelope error",
      EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_59("Configure claims request, public key error", EventSeverityLevel.high),
  uu_60("Request financial claims information, config not found",
      EventSeverityLevel.medium),
  uu_61("Request financial claims information, config empty",
      EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_62("Request financial claims information, public key error",
      EventSeverityLevel.high),
  @Deprecated("This device event is removed")
  uu_63("Process financial claims information request, unknown status error",
      EventSeverityLevel.high),
  @Deprecated("This device event is removed")
  uu_64("", EventSeverityLevel.low),
  @Deprecated("This device event is removed")
  uu_65("", EventSeverityLevel.low),
  uu_66("Update local scheme, database inconsistent",
      EventSeverityLevel.veryHigh),
  uu_67("Update local scheme, duplicate document type names",
      EventSeverityLevel.high),
  uu_68("Update local scheme, duplicate app manager oins",
      EventSeverityLevel.high),
  uu_69("Revoke registration, app manager error", EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_70("Revoke registration, key pair error", EventSeverityLevel.high),
  uu_71("Get organization information, information not found",
      EventSeverityLevel.low),
  uu_72("Revoke registrations, revoking registration error",
      EventSeverityLevel.low),
  @Deprecated("This device event is removed")
  uu_73("Get latest app key pair, key pair not found", EventSeverityLevel.low),
  uu_74(
      "Release lock for uncompleted requests, release lock for uncompleted requests error",
      EventSeverityLevel.veryHigh),
  uu_75("Do work, do work error", EventSeverityLevel.veryHigh),
  uu_76("Process financial claims information request, unknown status error",
      EventSeverityLevel.high),
  uu_77("Revoke registrations, revoking registration error",
      EventSeverityLevel.low),
  uu_78("Revoke registrations, revoking registration error",
      EventSeverityLevel.low),
  uu_79(
      "Create Financial claims information request, organization selection error",
      EventSeverityLevel.low),
  uu_80("Fetch and store certificate, registration expired error",
      EventSeverityLevel.low),
  uu_81("Update financial claims information request, request status invalid",
      EventSeverityLevel.low),
  uu_82("Unhandled Flutter error", EventSeverityLevel.medium),
  uu_83("Unhandled Platform error", EventSeverityLevel.medium),
  uu_84("Select app manager, failed to select an app manager",
      EventSeverityLevel.high),
  uu_85("Start activation, failed to launch url", EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_86("Select organization information, failed to launch email url",
      EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_87("Select organization information, failed to launch telephone url",
      EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_88("Select organization information, failed to launch website url",
      EventSeverityLevel.medium),
  uu_89("Get latest completed registration, no registrations found",
      EventSeverityLevel.high),
  @Deprecated("This device event is removed")
  uu_90("Get financial claims overview, organization unknown",
      EventSeverityLevel.high),
  uu_91("Deep link, initial uri helper platform exception",
      EventSeverityLevel.high),
  uu_92("Deep link, initial uri helper format exception",
      EventSeverityLevel.high),
  uu_93("Deep link, incoming link helper exception", EventSeverityLevel.high),
  uu_94("Start session, root organization EC key error",
      EventSeverityLevel.medium),
  uu_95("Get latest app session, session not found", EventSeverityLevel.low),
  uu_96("Start session, Ecdsa verify exception", EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_97("Start session, Ecdsa sign exception", EventSeverityLevel.medium),
  uu_98("Start session, certificate expiration exception",
      EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_99("Start session, Ecies encrypt exception", EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_100("Start session, Ecies decrypt exception", EventSeverityLevel.medium),
  uu_101("Get latest app session, organization session not found",
      EventSeverityLevel.medium),
  uu_102("Request financial claims information, session error.",
      EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_103("Select organization information, failed to launch",
      EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_104("FCID version invalid, failed to parse", EventSeverityLevel.high),
  uu_105("Select no digiD, failed to launch url", EventSeverityLevel.medium),
  uu_106("Configure claims request", EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_107(
      "Revoke registration, failed to sign token", EventSeverityLevel.medium),
  uu_108("Revoke registration, signature is null", EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_109("Start registration, Failed to get app root public key",
      EventSeverityLevel.medium),
  uu_110("Start registration, App root public key is null",
      EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_111("Start session, Failed to get app root public key",
      EventSeverityLevel.medium),
  uu_112(
      "Start session, App root public key is null", EventSeverityLevel.medium),
  uu_113("Start session, Ecies encrypt exception", EventSeverityLevel.medium),
  uu_114("Start session, Aes decrypt exception", EventSeverityLevel.medium),
  uu_115("Start session, signature is null", EventSeverityLevel.medium),
  uu_116("Create document signature, signature is null",
      EventSeverityLevel.medium),
  @Deprecated("This device event is removed")
  uu_117("Create document signature, failed to sign token",
      EventSeverityLevel.medium),
  uu_118("Verify app root public key, failed to verify key",
      EventSeverityLevel.medium),
  uu_119("Verify app root public key, key does not exist",
      EventSeverityLevel.medium),
  uu_120(
      "Start registration and get redirect uri, failed to determine client id",
      EventSeverityLevel.medium),
  uu_121("Fetch rules", EventSeverityLevel.high),
  @Deprecated("Unused?")
  uu_122("Fcid widgets, unknown error", EventSeverityLevel.high),
  uu_123("Delete keypair, failed to delete", EventSeverityLevel.medium),
  uu_124("Failed to load client ID: Invalid environment type",
      EventSeverityLevel.medium),
  uu_125("Sign message, EC sign exception", EventSeverityLevel.medium),
  uu_126("Get public key, Failed to get app root public key",
      EventSeverityLevel.medium),
  uu_127("Worker process error", EventSeverityLevel.veryHigh),
  ;

  const DeviceEvent(this.eventName, this.level);

  final String eventName;
  final EventSeverityLevel level;
}

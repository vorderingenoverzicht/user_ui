import 'dart:developer';

import 'package:flutter/rendering.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/entities/log_record.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/device_type.dart';
import 'package:user_ui/repositories/logging/unencrypted_logging_repository.dart';
import 'package:user_ui/screens/features/pincode/pincode_screen_state.dart';

import 'encrypted_logging_repository.dart';
import 'logging_repository.dart';

abstract class LoggingHelper {
  void addLog(DeviceEvent eventClassId, String message);
  void addFcidWidgetsLog(
    String eventClassId,
    String eventName,
    int severityLevel,
    String message,
  );

  static String _createCEFString(
      String eventClassId,
      String eventName,
      String severityLevel,
      String errorMessage,
      String device,
      String action,
      String applicationProtocol,
      String requestUri,
      String method) {
    var timestamp = DateTime.now().toIso8601String();
    var host = "FIX THIS!";
    var cef = "CEF:1";
    var vendor = "VORIJK";
    var product = "user_ui";
    var version = "fix";
    var deviceEventClassId = eventClassId;
    var name = eventName;
    var severity = severityLevel;
    var flexString1Label = "error message";
    var flexString1 = errorMessage;
    var flexString2Label = "device";
    var flexString2 = device;
    var act = action;
    var app = applicationProtocol;
    var request = requestUri;
    var requestMethod = method;
    return "$timestamp $host $cef|$vendor|$product|$version|$deviceEventClassId|$name|"
        "$severity|$flexString1Label|$flexString1|$flexString2Label|$flexString2|"
        "$act|$app|$request|$requestMethod";
  }
}

class ConsoleLoggingHelper extends LoggingHelper {
  @override
  void addLog(DeviceEvent eventClassId, String message) async {
    log(
      LoggingHelper._createCEFString(
        eventClassId.name,
        eventClassId.eventName,
        eventClassId.level.value,
        message,
        "-",
        "-",
        "-",
        "-",
        "-",
      ),
    );
  }

  @override
  void addFcidWidgetsLog(
    String eventClassId,
    String eventName,
    int severityLevel,
    String message,
  ) {
    log(
      LoggingHelper._createCEFString(
        eventClassId,
        eventName,
        severityLevel.toString(),
        message,
        "-",
        "-",
        "-",
        "-",
        "-",
      ),
    );
  }
}

class DatabaseLoggingHelper extends LoggingHelper {
  final EncryptedLoggingRepository encryptedLoggingRepository;
  final UnencryptedLoggingRepository unencryptedLoggingRepository;
  final Ref ref;
  var databaseUnlocked = false;
  DatabaseLoggingHelper(this.encryptedLoggingRepository,
      this.unencryptedLoggingRepository, this.ref) {
    ref.listen(
      pincodeScreenStateNotifierProvider.future,
      (previous, next) {
        next.then(
          (pincodeState) {
            debugPrint('pincodeState: $pincodeState');
            if (pincodeState == PincodeScreenState.unlocked) {
              databaseUnlocked = true;
              migrateLogToEncryptedDatabase();
            } else {
              databaseUnlocked = false;
            }
          },
        );
      },
    );
  }

  @override
  void addLog(DeviceEvent eventClassId, String message) {
    var timestamp = DateTime.now();
    var host = "TODO";
    var cef = 1;
    var deviceVendor = "VORIJK";
    var deviceProduct = "user_ui";
    var deviceVersion = "TODO";
    var flexString1Label = "error message";
    var flexString2Label = "device";
    getLoggingRepo().add(
      LogRecord(
        timestamp: timestamp,
        host: host,
        cef: cef,
        deviceVendor: deviceVendor,
        deviceProduct: deviceProduct,
        deviceVersion: deviceVersion,
        deviceEventClassId: eventClassId.name,
        severity: eventClassId.level.toInt(),
        flexString1Label: flexString1Label,
        flexString1: message,
        flexString2Label: flexString2Label,
        flexString2: DeviceType.blank.value,
        act: "",
        app: "",
        request: "",
        requestMethod: "",
        name: eventClassId.eventName,
      ),
    );
  }

  Future<void> migrateLogToEncryptedDatabase() async {
    List<LogRecord> unEncryptedLogRecords =
        await unencryptedLoggingRepository.logs;

    await encryptedLoggingRepository.addBulk(unEncryptedLogRecords);
    await unencryptedLoggingRepository.clear();
  }

  LoggingRepository getLoggingRepo() {
    if (databaseUnlocked) {
      return encryptedLoggingRepository;
    } else {
      return unencryptedLoggingRepository;
    }
  }

  @override
  void addFcidWidgetsLog(
    String eventClassId,
    String eventName,
    int severityLevel,
    String message,
  ) {
    var timestamp = DateTime.now();
    var host = "TODO";
    var cef = 1;
    var deviceVendor = "VORIJK";
    var deviceProduct = "fcid_library";
    var deviceVersion = "TODO";
    var flexString1Label = "error message";
    var flexString2Label = "device";
    getLoggingRepo().add(
      LogRecord(
        timestamp: timestamp,
        host: host,
        cef: cef,
        deviceVendor: deviceVendor,
        deviceProduct: deviceProduct,
        deviceVersion: deviceVersion,
        deviceEventClassId: eventClassId,
        severity: severityLevel,
        flexString1Label: flexString1Label,
        flexString1: message,
        flexString2Label: flexString2Label,
        flexString2: DeviceType.blank.value,
        act: "",
        app: "",
        request: "",
        requestMethod: "",
        name: eventName,
      ),
    );
  }
}

final loggingProvider = Provider<LoggingHelper>((ref) {
  final encryptedLoggingRepository =
      ref.watch(encryptedLoggingRepositoryProvider.notifier);
  final unencryptedLoggingRepository =
      ref.watch(unencryptedLoggingRepositoryProvider.notifier);
  return DatabaseLoggingHelper(
      encryptedLoggingRepository, unencryptedLoggingRepository, ref);
});

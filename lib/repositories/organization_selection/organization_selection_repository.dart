// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/encrypted_database.dart';
import 'package:user_ui/entities/organization_selection.dart';

abstract class OrganizationSelectionRepository {
  Future<void> add({required String oin});
  Future<void> update(OrganizationSelection organizationSelection);
  Future<List<OrganizationSelection>> get organizationSelections;
  Future<void> clear();
}

class DriftOrganizationSelectionRepository
    extends StateNotifier<List<OrganizationSelection>>
    implements OrganizationSelectionRepository {
  final EncryptedDatabase database;

  DriftOrganizationSelectionRepository(this.database) : super([]) {
    organizationSelections
        .then((organizationSelections) => state = organizationSelections);
  }

  @override
  Future<void> add({required String oin}) async {
    await database
        .into(database.organizationSelectionTable)
        .insert(OrganizationSelectionTableCompanion.insert(
          oin: oin,
          selected: false,
        ));

    state = await organizationSelections;
  }

  @override
  Future<void> update(OrganizationSelection organizationSelection) async {
    var organizationSelectionModel =
        await (database.select(database.organizationSelectionTable)
              ..where((table) => table.id.equals(organizationSelection.id)))
            .getSingle();

    var organizationSelectionUpdateModel = organizationSelectionModel.copyWith(
      oin: organizationSelection.oin,
      selected: organizationSelection.selected,
    );

    await database
        .update(database.organizationSelectionTable)
        .replace(organizationSelectionUpdateModel);

    state = await organizationSelections;
  }

  @override
  Future<List<OrganizationSelection>> get organizationSelections async {
    var organizationSelectionModelList =
        await database.select(database.organizationSelectionTable).get();

    return organizationSelectionModelList
        .map(
          (model) => OrganizationSelection(
            id: model.id,
            oin: model.oin,
            selected: model.selected,
          ),
        )
        .toList();
  }

  @override
  Future<void> clear() async {
    await database.delete(database.organizationSelectionTable).go();

    state = await organizationSelections;
  }
}

final organizationSelectionRepositoryProvider = StateNotifierProvider<
    DriftOrganizationSelectionRepository, List<OrganizationSelection>>((ref) {
  final database = ref.watch(encryptedDatabaseProvider);

  return DriftOrganizationSelectionRepository(database);
});

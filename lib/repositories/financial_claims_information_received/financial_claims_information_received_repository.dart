// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:drift/drift.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/database/encrypted_database.dart';
import 'package:user_ui/entities/financial_claims_information_received.dart';

part 'financial_claims_information_received_repository.g.dart';

@riverpod
class FinancialClaimsInformationReceivedRepository
    extends _$FinancialClaimsInformationReceivedRepository {
  @override
  Future<List<FinancialClaimsInformationReceived>> build() {
    return _financialClaimsInformationReceived;
  }

  Future<List<FinancialClaimsInformationReceived>>
      get _financialClaimsInformationReceived async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    var financialClaimsInformationReceivedModelList = await database
        .select(database.financialClaimsInformationReceivedTable)
        .get();

    return financialClaimsInformationReceivedModelList
        .map(
          (model) => FinancialClaimsInformationReceived(
            id: model.id,
            oin: model.oin,
            copiedToInbox: model.copiedToInbox,
            dateTimeReceived: model.dateTimeReceived,
            dateTimeRequested: model.dateTimeRequested,
            financialClaimsInformationDocument:
                model.financialClaimsInformationDocument,
            encryptedFinancialClaimsInformationDocument:
                model.encryptedFinancialClaimsInformationDocument,
            changed: model.changed,
          ),
        )
        .toList();
  }

  Future<void> add({
    required String oin,
    required DateTime dateTimeRequested,
    required DateTime dateTimeReceived,
    required String encryptedFinancialClaimsInformationDocument,
  }) async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    await database
        .into(database.financialClaimsInformationReceivedTable)
        .insert(FinancialClaimsInformationReceivedTableCompanion.insert(
            oin: oin,
            dateTimeRequested: dateTimeRequested,
            dateTimeReceived: dateTimeReceived,
            encryptedFinancialClaimsInformationDocument:
                encryptedFinancialClaimsInformationDocument,
            copiedToInbox: false));

    state = AsyncData(await _financialClaimsInformationReceived);
  }

  Future<void> updateFinancialClaimsInformationReceived(
      FinancialClaimsInformationReceived
          financialClaimsInformationReceived) async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    var financialClaimsInformationReceivedModel =
        await (database.select(database.financialClaimsInformationReceivedTable)
              ..where((table) =>
                  table.id.equals(financialClaimsInformationReceived.id)))
            .getSingle();

    var financialClaimsInformationReceivedUpdateModel =
        financialClaimsInformationReceivedModel.copyWith(
            copiedToInbox: financialClaimsInformationReceived.copiedToInbox,
            dateTimeReceived:
                financialClaimsInformationReceived.dateTimeReceived,
            dateTimeRequested:
                financialClaimsInformationReceived.dateTimeRequested,
            encryptedFinancialClaimsInformationDocument:
                financialClaimsInformationReceived
                    .encryptedFinancialClaimsInformationDocument,
            financialClaimsInformationDocument: Value(
                financialClaimsInformationReceived
                    .financialClaimsInformationDocument),
            oin: financialClaimsInformationReceived.oin,
            changed: Value(financialClaimsInformationReceived.changed));

    await database
        .update(database.financialClaimsInformationReceivedTable)
        .replace(financialClaimsInformationReceivedUpdateModel);

    state = AsyncData(await _financialClaimsInformationReceived);
  }

  Future<void> clear() async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    await database
        .delete(database.financialClaimsInformationReceivedTable)
        .go();

    state = AsyncData(await _financialClaimsInformationReceived);
  }
}

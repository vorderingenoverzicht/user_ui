// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'financial_claims_information_received_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$financialClaimsInformationReceivedRepositoryHash() =>
    r'608e240511f55d9eeee1b0925c052a9a9ed2bb7e';

/// See also [FinancialClaimsInformationReceivedRepository].
@ProviderFor(FinancialClaimsInformationReceivedRepository)
final financialClaimsInformationReceivedRepositoryProvider =
    AutoDisposeAsyncNotifierProvider<
        FinancialClaimsInformationReceivedRepository,
        List<FinancialClaimsInformationReceived>>.internal(
  FinancialClaimsInformationReceivedRepository.new,
  name: r'financialClaimsInformationReceivedRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$financialClaimsInformationReceivedRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$FinancialClaimsInformationReceivedRepository
    = AutoDisposeAsyncNotifier<List<FinancialClaimsInformationReceived>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

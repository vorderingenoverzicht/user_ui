// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'financial_claims_information_received_repository_mock.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$financialClaimsInformationReceivedRepositoryMockHash() =>
    r'7eb2844e532f7bd7ae0c150216d047438019bbe6';

/// See also [FinancialClaimsInformationReceivedRepositoryMock].
@ProviderFor(FinancialClaimsInformationReceivedRepositoryMock)
final financialClaimsInformationReceivedRepositoryMockProvider =
    AutoDisposeAsyncNotifierProvider<
        FinancialClaimsInformationReceivedRepositoryMock,
        List<FinancialClaimsInformationReceived>>.internal(
  FinancialClaimsInformationReceivedRepositoryMock.new,
  name: r'financialClaimsInformationReceivedRepositoryMockProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$financialClaimsInformationReceivedRepositoryMockHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$FinancialClaimsInformationReceivedRepositoryMock
    = AutoDisposeAsyncNotifier<List<FinancialClaimsInformationReceived>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

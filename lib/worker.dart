// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:stream_transform/stream_transform.dart';
import 'package:user_ui/repositories/app_manager_selection/app_manager_selection_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/usecases/request_financial_claims_information/process_financial_claims_information_requests.dart';

part 'worker.g.dart';

@Riverpod(keepAlive: true)
Future<void> worker(Ref ref) async {
  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  final loggingHelper = ref.read(loggingProvider);

  ref.read(appManagerSelectionRepositoryProvider.notifier);

  var watch = financialClaimsInformationRequestRepository.watchProcessable();

  try {
    await financialClaimsInformationRequestRepository
        .releaseLockForUncompletedRequests();
  } catch (error) {
    loggingHelper.addLog(
        DeviceEvent.uu_74, "release lock for uncompleted requests error");

    debugPrint("WORKER RELEASE LOCKS ERROR: $error");
  }

  watch.debounce(const Duration(milliseconds: 100)).listen(
    (_) async {
      try {
        var financialClaimsInformationRequest =
            await financialClaimsInformationRequestRepository
                .getAndLockNextProcessable();

        if (financialClaimsInformationRequest != null) {
          try {
            await ref.read(processFinancialClaimsInformationRequestsProvider(
                    financialClaimsInformationRequest)
                .future);
          } catch (e) {
            loggingHelper.addLog(
              DeviceEvent.uu_127,
              "Failed to process request for oin: ${financialClaimsInformationRequest.oin}, Error: ${e.toString()}",
            );

            debugPrint(
              "WORKER PROCESS ERROR: Request oin: ${financialClaimsInformationRequest.oin}, Error: ${e.toString()}",
            );

            await financialClaimsInformationRequestRepository
                .cancelFailedRequests(financialClaimsInformationRequest.id);

            await ref
                .read(financialClaimsInformationConfigurationRepositoryProvider
                    .notifier)
                .expireForOin(financialClaimsInformationRequest.oin);
          } finally {
            await financialClaimsInformationRequestRepository
                .releaseLock(financialClaimsInformationRequest.id);
          }
        }
      } catch (e) {
        loggingHelper.addLog(DeviceEvent.uu_75, e.toString());

        debugPrint("WORKER GET NEXT PROCESS ERROR: ${e.toString()}");
      }
    },
  );
}

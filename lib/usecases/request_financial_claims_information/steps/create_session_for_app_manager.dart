import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/entities/organization_type.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/scheme_app_manager/fetch_scheme_app_manager_notifier.dart';
import 'package:user_ui/usecases/helpers/start_session.dart';

part 'create_session_for_app_manager.g.dart';

@riverpod
Future<void> createSessionForAppManager(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  final serviceDiscoveryClient = ref.read(serviceDiscoveryClientProvider);

  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  final loggingHelper = ref.watch(loggingProvider);
  debugPrint(
      "     ${financialClaimsInformationRequest.id} Step: 7 createSession"); // TODO nummers

  final selectedSchemeAppManager =
      await ref.read(fetchSchemeAppManagerProvider.future);

  final discoveredServices = await serviceDiscoveryClient
      .fetchDiscoveredServices(selectedSchemeAppManager.discoveryUrl);

  await ref.read(
    startSessionProvider(
      sessionApiUrl: discoveredServices.getSessionApiV1(loggingHelper),
      rootOrganizationEcPublicKey: selectedSchemeAppManager.publicKey,
      organizationOin: selectedSchemeAppManager.oin,
      contactType: OrganizationType.appManager,
    ).future,
  );

  final financialClaimsInformationRequestUpdate =
      financialClaimsInformationRequest.copyWith(
          status: FinancialClaimsInformationRequestStatus.certificateInvalid);

  await financialClaimsInformationRequestRepository.update(
    financialClaimsInformationRequestUpdate,
  );
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reuse_or_create_configuration.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$reuseOrCreateConfigurationHash() =>
    r'b240d938f10b510509407ca5602355558036ed5b';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [reuseOrCreateConfiguration].
@ProviderFor(reuseOrCreateConfiguration)
const reuseOrCreateConfigurationProvider = ReuseOrCreateConfigurationFamily();

/// See also [reuseOrCreateConfiguration].
class ReuseOrCreateConfigurationFamily extends Family<AsyncValue<void>> {
  /// See also [reuseOrCreateConfiguration].
  const ReuseOrCreateConfigurationFamily();

  /// See also [reuseOrCreateConfiguration].
  ReuseOrCreateConfigurationProvider call(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) {
    return ReuseOrCreateConfigurationProvider(
      financialClaimsInformationRequest,
    );
  }

  @override
  ReuseOrCreateConfigurationProvider getProviderOverride(
    covariant ReuseOrCreateConfigurationProvider provider,
  ) {
    return call(
      provider.financialClaimsInformationRequest,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'reuseOrCreateConfigurationProvider';
}

/// See also [reuseOrCreateConfiguration].
class ReuseOrCreateConfigurationProvider
    extends AutoDisposeFutureProvider<void> {
  /// See also [reuseOrCreateConfiguration].
  ReuseOrCreateConfigurationProvider(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) : this._internal(
          (ref) => reuseOrCreateConfiguration(
            ref as ReuseOrCreateConfigurationRef,
            financialClaimsInformationRequest,
          ),
          from: reuseOrCreateConfigurationProvider,
          name: r'reuseOrCreateConfigurationProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$reuseOrCreateConfigurationHash,
          dependencies: ReuseOrCreateConfigurationFamily._dependencies,
          allTransitiveDependencies:
              ReuseOrCreateConfigurationFamily._allTransitiveDependencies,
          financialClaimsInformationRequest: financialClaimsInformationRequest,
        );

  ReuseOrCreateConfigurationProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.financialClaimsInformationRequest,
  }) : super.internal();

  final FinancialClaimsInformationRequest financialClaimsInformationRequest;

  @override
  Override overrideWith(
    FutureOr<void> Function(ReuseOrCreateConfigurationRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ReuseOrCreateConfigurationProvider._internal(
        (ref) => create(ref as ReuseOrCreateConfigurationRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        financialClaimsInformationRequest: financialClaimsInformationRequest,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _ReuseOrCreateConfigurationProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ReuseOrCreateConfigurationProvider &&
        other.financialClaimsInformationRequest ==
            financialClaimsInformationRequest;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash =
        _SystemHash.combine(hash, financialClaimsInformationRequest.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ReuseOrCreateConfigurationRef on AutoDisposeFutureProviderRef<void> {
  /// The parameter `financialClaimsInformationRequest` of this provider.
  FinancialClaimsInformationRequest get financialClaimsInformationRequest;
}

class _ReuseOrCreateConfigurationProviderElement
    extends AutoDisposeFutureProviderElement<void>
    with ReuseOrCreateConfigurationRef {
  _ReuseOrCreateConfigurationProviderElement(super.provider);

  @override
  FinancialClaimsInformationRequest get financialClaimsInformationRequest =>
      (origin as ReuseOrCreateConfigurationProvider)
          .financialClaimsInformationRequest;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

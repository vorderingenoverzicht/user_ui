// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_document.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$createDocumentHash() => r'f4be856831843ddcbb3cf10f01e1491f46f2cd85';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [createDocument].
@ProviderFor(createDocument)
const createDocumentProvider = CreateDocumentFamily();

/// See also [createDocument].
class CreateDocumentFamily extends Family<AsyncValue<void>> {
  /// See also [createDocument].
  const CreateDocumentFamily();

  /// See also [createDocument].
  CreateDocumentProvider call(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) {
    return CreateDocumentProvider(
      financialClaimsInformationRequest,
    );
  }

  @override
  CreateDocumentProvider getProviderOverride(
    covariant CreateDocumentProvider provider,
  ) {
    return call(
      provider.financialClaimsInformationRequest,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'createDocumentProvider';
}

/// See also [createDocument].
class CreateDocumentProvider extends AutoDisposeFutureProvider<void> {
  /// See also [createDocument].
  CreateDocumentProvider(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) : this._internal(
          (ref) => createDocument(
            ref as CreateDocumentRef,
            financialClaimsInformationRequest,
          ),
          from: createDocumentProvider,
          name: r'createDocumentProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$createDocumentHash,
          dependencies: CreateDocumentFamily._dependencies,
          allTransitiveDependencies:
              CreateDocumentFamily._allTransitiveDependencies,
          financialClaimsInformationRequest: financialClaimsInformationRequest,
        );

  CreateDocumentProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.financialClaimsInformationRequest,
  }) : super.internal();

  final FinancialClaimsInformationRequest financialClaimsInformationRequest;

  @override
  Override overrideWith(
    FutureOr<void> Function(CreateDocumentRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: CreateDocumentProvider._internal(
        (ref) => create(ref as CreateDocumentRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        financialClaimsInformationRequest: financialClaimsInformationRequest,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _CreateDocumentProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CreateDocumentProvider &&
        other.financialClaimsInformationRequest ==
            financialClaimsInformationRequest;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash =
        _SystemHash.combine(hash, financialClaimsInformationRequest.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin CreateDocumentRef on AutoDisposeFutureProviderRef<void> {
  /// The parameter `financialClaimsInformationRequest` of this provider.
  FinancialClaimsInformationRequest get financialClaimsInformationRequest;
}

class _CreateDocumentProviderElement
    extends AutoDisposeFutureProviderElement<void> with CreateDocumentRef {
  _CreateDocumentProviderElement(super.provider);

  @override
  FinancialClaimsInformationRequest get financialClaimsInformationRequest =>
      (origin as CreateDocumentProvider).financialClaimsInformationRequest;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/financial_claims_information_inbox/financial_claims_information_inbox_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_received/financial_claims_information_received_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';

part 'copy_financial_claims_information_to_inbox.g.dart';

@riverpod
Future<void> copyFinancialClaimsInformationToInbox(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);
  final financialClaimsInformationInboxRepository =
      ref.read(financialClaimsInformationInboxRepositoryProvider.notifier);
  debugPrint(
      "     ${financialClaimsInformationRequest.id} Step: 13 copyFinancialClaimsInformationToInbox");
  final financialClaimsInformationReceivedList = await ref
      .read(financialClaimsInformationReceivedRepositoryProvider.future);

  final financialClaimsInformationReceived =
      financialClaimsInformationReceivedList.lastWhere(
    (financialClaimsInformationReceived) =>
        financialClaimsInformationReceived.oin ==
            financialClaimsInformationRequest.oin &&
        financialClaimsInformationReceived.changed! &&
        !financialClaimsInformationReceived.copiedToInbox &&
        financialClaimsInformationReceived.financialClaimsInformationDocument !=
            null,
  );

  financialClaimsInformationInboxRepository.add(
    oin: financialClaimsInformationReceived.oin,
    dateTimeRequested: financialClaimsInformationReceived.dateTimeRequested,
    dateTimeReceived: financialClaimsInformationReceived.dateTimeReceived,
    financialClaimsInformationDocument:
        financialClaimsInformationReceived.financialClaimsInformationDocument!,
  );

  final financialClaimsInformationReceivedUpdate =
      financialClaimsInformationReceived.copyWith(copiedToInbox: true);

  await ref
      .read(financialClaimsInformationReceivedRepositoryProvider.notifier)
      .updateFinancialClaimsInformationReceived(
          financialClaimsInformationReceivedUpdate);

  final financialClaimsInformationRequestUpdate =
      financialClaimsInformationRequest.copyWith(
          status: FinancialClaimsInformationRequestStatus
              .financialClaimsInformationCopiedToInbox);

  await financialClaimsInformationRequestRepository.update(
    financialClaimsInformationRequestUpdate,
  );
}

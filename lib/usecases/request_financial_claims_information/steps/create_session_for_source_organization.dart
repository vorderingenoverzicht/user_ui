import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/entities/organization_type.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_organization_repository.dart';
import 'package:user_ui/usecases/helpers/start_session.dart';
import 'package:user_ui/usecases/helpers/start_session_delegate.dart';

part 'create_session_for_source_organization.g.dart';

@riverpod
Future<void> createSessionForSourceOrganization(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  final serviceDiscoveryClient = ref.read(serviceDiscoveryClientProvider);

  final schemeOrganizationRepository =
      ref.read(schemeOrganizationRepositoryProvider.notifier);

  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  final financialClaimsInformationConfigurationRepository = ref
      .read(financialClaimsInformationConfigurationRepositoryProvider.notifier);

  final loggingHelper = ref.watch(loggingProvider);
  debugPrint(
      "     ${financialClaimsInformationRequest.id} Step: 5 createSession");

  final financialClaimsInformationConfiguration =
      await financialClaimsInformationConfigurationRepository
          .getUnexpiredConfigurationForOin(
              financialClaimsInformationRequest.oin);

  if (financialClaimsInformationConfiguration == null) {
    loggingHelper.addLog(
        DeviceEvent.uu_55, "financial claims configuration not found");
    throw Exception("financial claims configuration not found");
  }

  final organizations = await schemeOrganizationRepository.organizations;

  // TODO: what if the organization is no longer in the scheme
  final organization = organizations.firstWhere((SchemeOrganization l) =>
      l.oin == financialClaimsInformationConfiguration.oin);

  final discoveredServices = await serviceDiscoveryClient
      .fetchDiscoveredServices(organization.discoveryUrl);

  try {
    await ref.read(
      startSessionProvider(
        sessionApiUrl: discoveredServices.getSessionApiV1(loggingHelper),
        rootOrganizationEcPublicKey: organization.publicKey,
        organizationOin: organization.oin,
        contactType: OrganizationType.organization,
      ).future,
    );
  } on AppCertificateExpiredException {
    final financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status: FinancialClaimsInformationRequestStatus.certificateInvalid);

    await financialClaimsInformationRequestRepository.update(
      financialClaimsInformationRequestUpdate,
    );

    return;
  }

  final financialClaimsInformationRequestUpdate =
      financialClaimsInformationRequest.copyWith(
          status: FinancialClaimsInformationRequestStatus.sessionCreated);

  await financialClaimsInformationRequestRepository.update(
    financialClaimsInformationRequestUpdate,
  );
}

import 'dart:convert';

import 'package:fcid_library/fcid_v3.dart' as v3;
import 'package:fcid_library/fcid_v4.dart' as v4;
import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/financial_claims_information_received/financial_claims_information_received_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_storage/financial_claims_information_storage_repository.dart';

part 'compare_financial_claims_received_with_latest_storage.g.dart';

@riverpod
Future<void> compareFinancialClaimsReceivedWithLatestStorage(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  debugPrint(
      "     ${financialClaimsInformationRequest.id} Step: 12 compareFinancialClaimsReceivedWithLatestStorage");
  final financialClaimsInformationReceivedList = await ref
      .read(financialClaimsInformationReceivedRepositoryProvider.future);

  final financialClaimsInformationReceived =
      financialClaimsInformationReceivedList.lastWhere(
    (financialClaimsInformationReceived) =>
        financialClaimsInformationReceived.oin ==
            financialClaimsInformationRequest.oin &&
        financialClaimsInformationReceived.changed == null &&
        financialClaimsInformationReceived.financialClaimsInformationDocument !=
            null,
  );

  final receivedFinancialClaimsInformationDocumentJson = json.decode(
      financialClaimsInformationReceived.financialClaimsInformationDocument!);

  final receivedFinancialClaimsInformationDocument =
      FinancialClaimsInformationDocumentBase.fromJson(
          receivedFinancialClaimsInformationDocumentJson);

  final financialClaimsInformationStorageList = ref
      .read(financialClaimsInformationStorageRepositoryProvider)
      .where((FinancialClaimsInformationStorage
              financialClaimsInformationStorage) =>
          financialClaimsInformationStorage.oin ==
          financialClaimsInformationRequest.oin);

  var changed = true;

  if (financialClaimsInformationStorageList.isNotEmpty) {
    final storageFinancialClaimsInformationDocumentJson = json.decode(
        financialClaimsInformationStorageList
            .last.financialClaimsInformationDocument);

    final storageFinancialClaimsInformationDocument =
        FinancialClaimsInformationDocumentBase.fromJson(
            storageFinancialClaimsInformationDocumentJson);

    if (receivedFinancialClaimsInformationDocument.version ==
        storageFinancialClaimsInformationDocument.version) {
      switch (receivedFinancialClaimsInformationDocument.version) {
        case '4':
          if (v4.compareFinancieleZaken(
              (receivedFinancialClaimsInformationDocument
                      as v4.FinancialClaimsInformationDocument)
                  .body
                  .financieleZaken,
              (storageFinancialClaimsInformationDocument
                      as v4.FinancialClaimsInformationDocument)
                  .body
                  .financieleZaken)) {
            changed = false;
          }
        case '3':
          if (v3.compareFinancieleZaken(
              (receivedFinancialClaimsInformationDocument
                      as v3.FinancialClaimsInformationDocument)
                  .body
                  .financieleZaken,
              (storageFinancialClaimsInformationDocument
                      as v3.FinancialClaimsInformationDocument)
                  .body
                  .financieleZaken)) {
            changed = false;
          }
      }
    }
  } else {
    switch (receivedFinancialClaimsInformationDocument.version) {
      case '4':
        if ((receivedFinancialClaimsInformationDocument
                as v4.FinancialClaimsInformationDocument)
            .body
            .financieleZaken
            .isEmpty) {
          changed = false;
        }
      case '3':
        if ((receivedFinancialClaimsInformationDocument
                as v3.FinancialClaimsInformationDocument)
            .body
            .financieleZaken
            .isEmpty) {
          changed = false;
        }
    }
  }

  FinancialClaimsInformationRequest financialClaimsInformationRequestUpdate;

  if (changed) {
    financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status: FinancialClaimsInformationRequestStatus
                .financialClaimsInformationChanged);
  } else {
    financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status: FinancialClaimsInformationRequestStatus
                .financialClaimsInformationUnchanged);
  }

  final financialClaimsInformationReceivedUpdate =
      financialClaimsInformationReceived.copyWith(changed: changed);

  await ref
      .read(financialClaimsInformationReceivedRepositoryProvider.notifier)
      .updateFinancialClaimsInformationReceived(
          financialClaimsInformationReceivedUpdate);

  await financialClaimsInformationRequestRepository.update(
    financialClaimsInformationRequestUpdate,
  );
}

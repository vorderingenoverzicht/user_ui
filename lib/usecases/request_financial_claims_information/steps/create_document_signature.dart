import 'dart:convert';

import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/app_identity/app_root_keypair.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

part 'create_document_signature.g.dart';

@riverpod
Future<void> createDocumentSignature(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  final financialClaimsInformationConfigurationRepository = ref
      .read(financialClaimsInformationConfigurationRepositoryProvider.notifier);

  final loggingHelper = ref.watch(loggingProvider);

  debugPrint(
      "     ${financialClaimsInformationRequest.id} Step: 3 CreateDocumentSignature");

  final financialClaimsInformationConfiguration =
      await financialClaimsInformationConfigurationRepository
          .getUnexpiredConfigurationForOin(
              financialClaimsInformationRequest.oin);

  if (financialClaimsInformationConfiguration == null) {
    loggingHelper.addLog(
        DeviceEvent.uu_42, "financial claims configuration not found.");
    throw Exception("financial claims configuration not found");
  }

  if (financialClaimsInformationConfiguration.document == null) {
    loggingHelper.addLog(DeviceEvent.uu_43, "document not found");
    throw Exception("document not found");
  }

  final documentJson =
      json.decode(financialClaimsInformationConfiguration.document!);
  final document = Document.fromJson(documentJson);

  final documentSignature =
      await ref.read(appRootKeypairProvider.notifier).sign(
            json.encode(document),
          );

  if (documentSignature == null) {
    loggingHelper.addLog(DeviceEvent.uu_116, "Document signature is null");
    throw Exception('Document signature cannot be null');
  }

  final financialClaimsInformationConfigurationUpdate =
      financialClaimsInformationConfiguration.copyWith(
          documentSignature: documentSignature);

  await financialClaimsInformationConfigurationRepository
      .update(financialClaimsInformationConfigurationUpdate);

  final financialClaimsInformationRequestUpdate =
      financialClaimsInformationRequest.copyWith(
          status: FinancialClaimsInformationRequestStatus.documentSigned);

  await financialClaimsInformationRequestRepository.update(
    financialClaimsInformationRequestUpdate,
  );
}

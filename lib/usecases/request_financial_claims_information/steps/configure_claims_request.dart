import 'dart:convert';

import 'package:dart_connect/dart_connect.dart';
import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/citizen_financial_claim/citizen_financial_claim_client_live.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/clients/session_expired_exception.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/app_identity/latest_app_session.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_organization_repository.dart';

part 'configure_claims_request.g.dart';

@riverpod
Future<void> configureClaimsRequest(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  final serviceDiscoveryClient = ref.read(serviceDiscoveryClientProvider);
  final citizenFinancialClaimClient =
      ref.read(citizenFinancialClaimClientProvider);

  final schemeOrganizationRepository =
      ref.read(schemeOrganizationRepositoryProvider.notifier);

  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  final financialClaimsInformationConfigurationRepository = ref
      .read(financialClaimsInformationConfigurationRepositoryProvider.notifier);

  final loggingHelper = ref.watch(loggingProvider);
  debugPrint(
      "     ${financialClaimsInformationRequest.id} Step: 9 ConfigureClaimsRequest");

  final financialClaimsInformationConfiguration =
      await financialClaimsInformationConfigurationRepository
          .getUnexpiredConfigurationForOin(
              financialClaimsInformationRequest.oin);

  if (financialClaimsInformationConfiguration == null) {
    loggingHelper.addLog(
        DeviceEvent.uu_57, "financial claims configuration not found");
    throw Exception("financial claims configuration not found");
  }

  if (financialClaimsInformationConfiguration.encryptedEnvelope == null) {
    loggingHelper.addLog(DeviceEvent.uu_58, "encryptedEnvelope not found");
    throw Exception("encryptedEnvelope not found");
  }

  final organizations = await schemeOrganizationRepository.organizations;

  // TODO: what if the organization is no longer in the scheme
  final organization = organizations.firstWhere((SchemeOrganization l) =>
      l.oin == financialClaimsInformationConfiguration.oin);

  final discoveredServices = await serviceDiscoveryClient
      .fetchDiscoveredServices(organization.discoveryUrl);

  final appSession =
      await ref.read(latestAppSessionProvider(oin: organization.oin).future);

  String? encodedEncryptedConfiguration;
  try {
    encodedEncryptedConfiguration =
        await citizenFinancialClaimClient.configureClaimsRequest(
      discoveredServices.getFinancialClaimRequestApiV3(loggingHelper),
      appSession.token,
      financialClaimsInformationConfiguration.encryptedEnvelope!,
    );
  } on SessionExpiredException catch (_) {
    final financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status: FinancialClaimsInformationRequestStatus
                .appManagerSessionInvalid);

    await financialClaimsInformationRequestRepository.update(
      financialClaimsInformationRequestUpdate,
    );

    return;
  } catch (ex) {
    debugPrint(ex.toString());
    loggingHelper.addLog(DeviceEvent.uu_106,
        "Failed to configure claims request for organization: ${organization.name}");
    rethrow;
  }

  final encryptedConfiguration = Base64.toBytes(encodedEncryptedConfiguration);

  final aesKeyBytes = Base64.toBytes(appSession.key);

  String? configuration;

  try {
    configuration = utf8.decode(
      Aes.decrypt(
        aesKeyBytes,
        encryptedConfiguration,
      ),
    );
  } catch (ex) {
    debugPrint("decrypt encrypted configuration failed");
    throw Exception("decrypt encrypted configuration failed");
  }

  final financialClaimsInformationConfigurationUpdate =
      financialClaimsInformationConfiguration.copyWith(
          configuration: configuration);

  await financialClaimsInformationConfigurationRepository
      .update(financialClaimsInformationConfigurationUpdate);

  final financialClaimsInformationRequestUpdate =
      financialClaimsInformationRequest.copyWith(
          status:
              FinancialClaimsInformationRequestStatus.encryptedEnvelopeMailed);

  await financialClaimsInformationRequestRepository.update(
    financialClaimsInformationRequestUpdate,
  );
}

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/registration/has_usable_registration_notifier.dart';
import 'package:user_ui/router/router.dart';
import 'package:user_ui/screens/features/activation/reactivation_screen.dart';
import 'package:user_ui/usecases/helpers/create_financial_claims_information_requests_for_selected_organizations_helper.dart';

part 'create_financial_claims_information_requests_for_selected_organizations.g.dart';

@riverpod
Future<void> createFinancialClaimsInformationRequestsForSelectedOrganizations(
    Ref ref) async {
  final hasUsableRegistration =
      await ref.read(hasUsableRegistrationProvider.future);

  if (!hasUsableRegistration) {
    ref.read(goRouterProvider).pushNamed(ReactivationScreen.routeName);
    return;
  }

  await ref.read(
      createFinancialClaimsInformationRequestsForSelectedOrganizationsHelperProvider
          .future);
}

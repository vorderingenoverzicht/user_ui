// Copyright © Centraal Justitieel Incassobureau (CJIB) 2024
// Licensed under the EUPL

import 'package:user_ui/usecases/request_financial_claims_information/models/base/enums/betalingsverplichting_betaalwijze.dart';

String createDueDateDescription(
    BetalingsverplichtingBetaalwijze betaalwijze, int dueDateFromNowInDays) {
  var dueDateDescription = "";

  if (betaalwijze == BetalingsverplichtingBetaalwijze.handmatig) {
    if (dueDateFromNowInDays > 1) {
      dueDateDescription = "Nog $dueDateFromNowInDays dagen om te betalen";
    } else if (dueDateFromNowInDays == 1) {
      dueDateDescription = "Nog 1 dag om te betalen";
    } else {
      dueDateDescription = "Betaaltermijn verstreken";
    }
  } else if (betaalwijze ==
      BetalingsverplichtingBetaalwijze.automatischIncasso) {
    if (dueDateFromNowInDays > 1) {
      dueDateDescription =
          "Wordt over $dueDateFromNowInDays dagen afgeschreven";
    } else if (dueDateFromNowInDays == 1) {
      dueDateDescription = "Wordt over 1 dag afgeschreven";
    } else {
      dueDateDescription = "Is reeds afgeschreven";
    }
  }

  return dueDateDescription;
}

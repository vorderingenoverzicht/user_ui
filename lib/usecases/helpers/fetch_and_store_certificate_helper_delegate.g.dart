// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fetch_and_store_certificate_helper_delegate.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$fetchAndStoreCertificateHelperDelegateHash() =>
    r'b4f9933a4964a3bd9389a589cb4d23cbae5e7a41';

/// This delegate is needed because family providers are not mockable in Riverpod 2.0.
/// see: https://github.com/rrousselGit/riverpod/discussions/2510
/// and also: https://github.com/rrousselGit/riverpod/issues/3009
///
/// Copied from [fetchAndStoreCertificateHelperDelegate].
@ProviderFor(fetchAndStoreCertificateHelperDelegate)
final fetchAndStoreCertificateHelperDelegateProvider = AutoDisposeProvider<
    Future<void> Function(Ref ref, Registration registration)>.internal(
  fetchAndStoreCertificateHelperDelegate,
  name: r'fetchAndStoreCertificateHelperDelegateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$fetchAndStoreCertificateHelperDelegateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef FetchAndStoreCertificateHelperDelegateRef = AutoDisposeProviderRef<
    Future<void> Function(Ref ref, Registration registration)>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

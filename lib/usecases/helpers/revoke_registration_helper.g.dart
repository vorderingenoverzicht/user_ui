// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'revoke_registration_helper.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$revokeRegistrationHelperHash() =>
    r'bc0fe6a2d46224d9a8eb1c3e2c8b5294c1368733';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [revokeRegistrationHelper].
@ProviderFor(revokeRegistrationHelper)
const revokeRegistrationHelperProvider = RevokeRegistrationHelperFamily();

/// See also [revokeRegistrationHelper].
class RevokeRegistrationHelperFamily extends Family<AsyncValue<void>> {
  /// See also [revokeRegistrationHelper].
  const RevokeRegistrationHelperFamily();

  /// See also [revokeRegistrationHelper].
  RevokeRegistrationHelperProvider call(
    Registration registration,
  ) {
    return RevokeRegistrationHelperProvider(
      registration,
    );
  }

  @override
  RevokeRegistrationHelperProvider getProviderOverride(
    covariant RevokeRegistrationHelperProvider provider,
  ) {
    return call(
      provider.registration,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'revokeRegistrationHelperProvider';
}

/// See also [revokeRegistrationHelper].
class RevokeRegistrationHelperProvider extends AutoDisposeFutureProvider<void> {
  /// See also [revokeRegistrationHelper].
  RevokeRegistrationHelperProvider(
    Registration registration,
  ) : this._internal(
          (ref) => revokeRegistrationHelper(
            ref as RevokeRegistrationHelperRef,
            registration,
          ),
          from: revokeRegistrationHelperProvider,
          name: r'revokeRegistrationHelperProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$revokeRegistrationHelperHash,
          dependencies: RevokeRegistrationHelperFamily._dependencies,
          allTransitiveDependencies:
              RevokeRegistrationHelperFamily._allTransitiveDependencies,
          registration: registration,
        );

  RevokeRegistrationHelperProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.registration,
  }) : super.internal();

  final Registration registration;

  @override
  Override overrideWith(
    FutureOr<void> Function(RevokeRegistrationHelperRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: RevokeRegistrationHelperProvider._internal(
        (ref) => create(ref as RevokeRegistrationHelperRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        registration: registration,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _RevokeRegistrationHelperProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is RevokeRegistrationHelperProvider &&
        other.registration == registration;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, registration.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin RevokeRegistrationHelperRef on AutoDisposeFutureProviderRef<void> {
  /// The parameter `registration` of this provider.
  Registration get registration;
}

class _RevokeRegistrationHelperProviderElement
    extends AutoDisposeFutureProviderElement<void>
    with RevokeRegistrationHelperRef {
  _RevokeRegistrationHelperProviderElement(super.provider);

  @override
  Registration get registration =>
      (origin as RevokeRegistrationHelperProvider).registration;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

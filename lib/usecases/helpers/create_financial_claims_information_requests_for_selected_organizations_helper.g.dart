// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_financial_claims_information_requests_for_selected_organizations_helper.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String
    _$createFinancialClaimsInformationRequestsForSelectedOrganizationsHelperHash() =>
        r'e820b177e1fe71e8c0d3c80d163006f3826bfb32';

/// See also [createFinancialClaimsInformationRequestsForSelectedOrganizationsHelper].
@ProviderFor(
    createFinancialClaimsInformationRequestsForSelectedOrganizationsHelper)
final createFinancialClaimsInformationRequestsForSelectedOrganizationsHelperProvider =
    AutoDisposeFutureProvider<void>.internal(
  createFinancialClaimsInformationRequestsForSelectedOrganizationsHelper,
  name:
      r'createFinancialClaimsInformationRequestsForSelectedOrganizationsHelperProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$createFinancialClaimsInformationRequestsForSelectedOrganizationsHelperHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef CreateFinancialClaimsInformationRequestsForSelectedOrganizationsHelperRef
    = AutoDisposeFutureProviderRef<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

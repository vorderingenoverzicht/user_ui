// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/app_manager/models/unregister_app_request.dart';
import 'package:user_ui/clients/app_manager/unregister_app.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/entities/registration.dart';
import 'package:user_ui/repositories/app_identity/app_root_keypair.dart';
import 'package:user_ui/repositories/app_identity/latest_app_session.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';
import 'package:user_ui/repositories/scheme_app_manager/fetch_scheme_app_manager_notifier.dart';

part 'revoke_registration_helper.g.dart';

@riverpod
Future<void> revokeRegistrationHelper(
    Ref ref, Registration registration) async {
  final loggingHelper = ref.read(loggingProvider);

  final selectedSchemeAppManager =
      await ref.read(fetchSchemeAppManagerProvider.future);

  final discoveredServices = await ref
      .read(serviceDiscoveryClientProvider)
      .fetchDiscoveredServices(selectedSchemeAppManager.discoveryUrl);

  final signature = await ref
      .read(appRootKeypairProvider.notifier)
      .sign(registration.registrationToken);

  if (signature == null) {
    loggingHelper.addLog(DeviceEvent.uu_108, "Signature is null");
    throw Exception('Signature cannot be null');
  }

  final unregisterAppRequest = UnregisterAppRequest(
      registrationToken: registration.registrationToken, signature: signature);

  final appSession = await ref
      .read(latestAppSessionProvider(oin: selectedSchemeAppManager.oin).future);

  await ref.read(unregisterAppProvider(
    serviceUrl: discoveredServices.getRegistrationApiV1(loggingHelper),
    sessionToken: appSession.token,
    unregisterAppRequest: unregisterAppRequest,
  ).future);

  final revokedRegistration = registration.copyWith(revoked: true);

  await ref
      .read(registrationRepositoryProvider.notifier)
      .updateRegistration(revokedRegistration);
}

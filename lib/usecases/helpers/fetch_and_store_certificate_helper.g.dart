// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fetch_and_store_certificate_helper.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$fetchAndStoreCertificateHelperHash() =>
    r'112090db2d46aeb26b93972ab9ebbb93cbd5de3c';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [fetchAndStoreCertificateHelper].
@ProviderFor(fetchAndStoreCertificateHelper)
const fetchAndStoreCertificateHelperProvider =
    FetchAndStoreCertificateHelperFamily();

/// See also [fetchAndStoreCertificateHelper].
class FetchAndStoreCertificateHelperFamily extends Family<AsyncValue<void>> {
  /// See also [fetchAndStoreCertificateHelper].
  const FetchAndStoreCertificateHelperFamily();

  /// See also [fetchAndStoreCertificateHelper].
  FetchAndStoreCertificateHelperProvider call({
    required Registration registration,
  }) {
    return FetchAndStoreCertificateHelperProvider(
      registration: registration,
    );
  }

  @override
  FetchAndStoreCertificateHelperProvider getProviderOverride(
    covariant FetchAndStoreCertificateHelperProvider provider,
  ) {
    return call(
      registration: provider.registration,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'fetchAndStoreCertificateHelperProvider';
}

/// See also [fetchAndStoreCertificateHelper].
class FetchAndStoreCertificateHelperProvider
    extends AutoDisposeFutureProvider<void> {
  /// See also [fetchAndStoreCertificateHelper].
  FetchAndStoreCertificateHelperProvider({
    required Registration registration,
  }) : this._internal(
          (ref) => fetchAndStoreCertificateHelper(
            ref as FetchAndStoreCertificateHelperRef,
            registration: registration,
          ),
          from: fetchAndStoreCertificateHelperProvider,
          name: r'fetchAndStoreCertificateHelperProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$fetchAndStoreCertificateHelperHash,
          dependencies: FetchAndStoreCertificateHelperFamily._dependencies,
          allTransitiveDependencies:
              FetchAndStoreCertificateHelperFamily._allTransitiveDependencies,
          registration: registration,
        );

  FetchAndStoreCertificateHelperProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.registration,
  }) : super.internal();

  final Registration registration;

  @override
  Override overrideWith(
    FutureOr<void> Function(FetchAndStoreCertificateHelperRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FetchAndStoreCertificateHelperProvider._internal(
        (ref) => create(ref as FetchAndStoreCertificateHelperRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        registration: registration,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _FetchAndStoreCertificateHelperProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FetchAndStoreCertificateHelperProvider &&
        other.registration == registration;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, registration.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FetchAndStoreCertificateHelperRef on AutoDisposeFutureProviderRef<void> {
  /// The parameter `registration` of this provider.
  Registration get registration;
}

class _FetchAndStoreCertificateHelperProviderElement
    extends AutoDisposeFutureProviderElement<void>
    with FetchAndStoreCertificateHelperRef {
  _FetchAndStoreCertificateHelperProviderElement(super.provider);

  @override
  Registration get registration =>
      (origin as FetchAndStoreCertificateHelperProvider).registration;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

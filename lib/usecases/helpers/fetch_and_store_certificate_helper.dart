// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/registration.dart';
import 'package:user_ui/usecases/helpers/fetch_and_store_certificate_helper_delegate.dart';

part 'fetch_and_store_certificate_helper.g.dart';

@riverpod
Future<void> fetchAndStoreCertificateHelper(Ref ref,
        {required Registration registration}) async =>
    ref.watch(fetchAndStoreCertificateHelperDelegateProvider)(
        ref, registration);

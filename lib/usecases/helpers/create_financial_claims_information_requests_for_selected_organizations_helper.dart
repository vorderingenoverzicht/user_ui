// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/organization_selection/organization_selection_repository.dart';

part 'create_financial_claims_information_requests_for_selected_organizations_helper.g.dart';

@riverpod
Future<void>
    createFinancialClaimsInformationRequestsForSelectedOrganizationsHelper(
        Ref ref) async {
  final organizationSelections =
      ref.read(organizationSelectionRepositoryProvider);

  final selectedOrganizations = organizationSelections
      .where((organizationSelection) => organizationSelection.selected);

  if (selectedOrganizations.isEmpty) {
    ref
        .read(loggingProvider)
        .addLog(DeviceEvent.uu_79, "Empty organization selection.");
    throw Exception("Empty organization selection");
  }

  final dateTimeRequested = DateTime.now();

  final financialClaimsInformationRequests =
      ref.read(financialClaimsInformationRequestRepositoryProvider);

  for (final organization in selectedOrganizations) {
    var uncompletedFinancialClaimsInformationRequests =
        financialClaimsInformationRequests.where((request) =>
            request.oin == organization.oin &&
            !financialClaimsInformationRequestEndStatusList
                .contains(request.status));

    if (uncompletedFinancialClaimsInformationRequests.isNotEmpty) {
      //Note: Do not add financial claims request for this organization as there is a uncompleted one
      continue;
    }

    await ref
        .read(financialClaimsInformationRequestRepositoryProvider.notifier)
        .add(
          oin: organization.oin,
          dateTimeRequested: dateTimeRequested,
          status: FinancialClaimsInformationRequestStatus.queue,
        );
  }
}

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/app_identity/app_root_keypair.dart';
import 'package:user_ui/repositories/app_manager_selection/app_manager_selection_repository.dart';
import 'package:user_ui/repositories/certificate/certificate_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_inbox/financial_claims_information_inbox_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_storage/financial_claims_information_storage_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/organization_selection/organization_selection_repository.dart';
import 'package:user_ui/repositories/payment_plan/payment_plan_group_repository.dart';
import 'package:user_ui/repositories/payment_plan/payment_plan_rule_repository.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';
import 'package:user_ui/repositories/scheme_app_manager/scheme_app_manager_repository.dart';
import 'package:user_ui/repositories/scheme_document_type/scheme_document_type_repository.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_organization_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/features/pincode/pincode_screen_state.dart';
import 'package:user_ui/usecases/helpers/revoke_registration_helper.dart';

part 'remove_all_data_and_user_settings.g.dart';

@riverpod
Future<void> removeAllDataAndUserSettings(Ref ref) async {
  try {
    final registrations = await ref.read(registrationRepositoryProvider.future);

    for (var registration in registrations.where((registration) =>
        registration.dateTimeCompleted != null && !registration.revoked)) {
      await ref.read(revokeRegistrationHelperProvider(registration).future);
    }
  } catch (_) {
    final loggingHelper = ref.read(loggingProvider);

    loggingHelper.addLog(DeviceEvent.uu_72, "revoking registrations error");
  }

  await ref
      .read(financialClaimsInformationStorageRepositoryProvider.notifier)
      .clear();

  await ref
      .read(financialClaimsInformationInboxRepositoryProvider.notifier)
      .clear();

  await ref
      .read(financialClaimsInformationRequestRepositoryProvider.notifier)
      .clear();

  await ref.read(organizationSelectionRepositoryProvider.notifier).clear();

  await ref
      .read(financialClaimsInformationConfigurationRepositoryProvider.notifier)
      .clear();

  await ref.read(registrationRepositoryProvider.notifier).clear();

  await ref.read(certificateRepositoryProvider.notifier).clear();

  await ref.read(appManagerSelectionRepositoryProvider.notifier).clear();

  await ref.read(schemeOrganizationRepositoryProvider.notifier).clear();

  await ref.read(schemeAppManagerRepositoryProvider.notifier).clear();

  await ref.read(schemeDocumentTypeRepositoryProvider.notifier).clear();

  await ref.read(userSettingsRepositoryProvider.notifier).clear();

  await ref.read(pincodeScreenStateNotifierProvider.notifier).clear();

  await ref.read(paymentPlanRuleRepositoryProvider.notifier).clear();

  await ref.read(paymentPlanGroupRepositoryProvider.notifier).clear();

  await ref.read(appRootKeypairProvider.notifier).deleteKeyPairs();
  //Note: logging not removed
}

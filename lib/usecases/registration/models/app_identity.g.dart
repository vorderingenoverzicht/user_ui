// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_identity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$AppIdentityImpl _$$AppIdentityImplFromJson(Map<String, dynamic> json) =>
    _$AppIdentityImpl(
      appPublicKey: json['appPublicKey'] as String,
      clientId: json['clientId'] as String,
    );

Map<String, dynamic> _$$AppIdentityImplToJson(_$AppIdentityImpl instance) =>
    <String, dynamic>{
      'appPublicKey': instance.appPublicKey,
      'clientId': instance.clientId,
    };

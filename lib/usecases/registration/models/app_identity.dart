import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'app_identity.freezed.dart';
part 'app_identity.g.dart';

@freezed
class AppIdentity with _$AppIdentity {
  const factory AppIdentity({
    required String appPublicKey,
    required String clientId,
  }) = _AppIdentity;

  factory AppIdentity.fromJson(Map<String, Object?> json) =>
      _$AppIdentityFromJson(json);
}

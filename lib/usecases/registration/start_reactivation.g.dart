// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'start_reactivation.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$startReactivationHash() => r'2d0de17fd7704b6435389e449e660472b064f51f';

/// See also [startReactivation].
@ProviderFor(startReactivation)
final startReactivationProvider = AutoDisposeFutureProvider<void>.internal(
  startReactivation,
  name: r'startReactivationProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$startReactivationHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef StartReactivationRef = AutoDisposeFutureProviderRef<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

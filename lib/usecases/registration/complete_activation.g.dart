// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'complete_activation.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$completeActivationHash() =>
    r'82510fe30405e48e0ed377a45735bd857606459d';

/// See also [completeActivation].
@ProviderFor(completeActivation)
final completeActivationProvider = AutoDisposeFutureProvider<void>.internal(
  completeActivation,
  name: r'completeActivationProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$completeActivationHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef CompleteActivationRef = AutoDisposeFutureProviderRef<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

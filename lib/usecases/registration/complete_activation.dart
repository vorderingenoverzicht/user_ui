// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/organization_selection/organization_selection_repository.dart';
import 'package:user_ui/repositories/registration/latest_started_registration_notifier.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/usecases/helpers/create_financial_claims_information_requests_for_selected_organizations_helper.dart';
import 'package:user_ui/usecases/helpers/fetch_and_store_certificate_helper.dart';

part 'complete_activation.g.dart';

@riverpod
Future<void> completeActivation(Ref ref) async {
  final latestStartedRegistration =
      await ref.read(latestStartedRegistrationProvider.future);

  await ref.read(fetchAndStoreCertificateHelperProvider(
          registration: latestStartedRegistration)
      .future);

  await ref
      .read(userSettingsRepositoryProvider.notifier)
      .setHasCompletedRegistration(true);

  final organizationSelections =
      ref.read(organizationSelectionRepositoryProvider);

  if (organizationSelections.isNotEmpty) {
    await ref.read(
        createFinancialClaimsInformationRequestsForSelectedOrganizationsHelperProvider
            .future);
  }
}

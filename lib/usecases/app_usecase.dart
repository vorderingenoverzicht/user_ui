// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';

abstract class IAppUsecase {
  Future<void> setHasCompletedOrganizationSelection(
      bool hasCompletedOrganizationSelection);
}

class AppUsecase implements IAppUsecase {
  final UserSettingsRepository userSettingsRepository;

  AppUsecase(
    this.userSettingsRepository,
  );

  @override
  Future<void> setHasCompletedOrganizationSelection(
      bool hasCompletedOrganizationSelection) async {
    await userSettingsRepository.setHasCompletedOrganizationSelection(
        hasCompletedOrganizationSelection);
  }
}

final appUsecaseProvider = Provider<IAppUsecase>((ref) {
  final userSettingsRepository =
      ref.read(userSettingsRepositoryProvider.notifier);

  return AppUsecase(
    userSettingsRepository,
  );
});

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL
import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/foundation.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/organization_selection.dart';
import 'package:user_ui/repositories/organization_selection/organization_selection_repository.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_organization_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';

abstract class IOrganizationSelectionUsecase {
  Future<void> syncSelectedOrganizationsWithScheme();
  Future<void> selectAllOrganizations();
  Future<void> deselectAllOrganizations();
  Future<void> setOrganizationSelection(String oin, bool selected);
  Future<void> setAutoSelectAllOrganizations(bool autoSelectAllOrganizations);
}

class OrganizationSelectionUsecase implements IOrganizationSelectionUsecase {
  final OrganizationSelectionRepository organizationSelectionRepository;
  final SchemeOrganizationRepository schemeOrganizationRepository;
  final UserSettingsRepository userSettingsRepository;

  OrganizationSelectionUsecase(
    this.organizationSelectionRepository,
    this.schemeOrganizationRepository,
    this.userSettingsRepository,
  );

  @override
  Future<void> syncSelectedOrganizationsWithScheme() async {
    final List<SchemeOrganization> schemeOrganizations =
        await schemeOrganizationRepository.organizations;
    final List<OrganizationSelection> organizationSelections =
        await organizationSelectionRepository.organizationSelections;

    // Add all new organizations
    for (final schemeOrg in schemeOrganizations) {
      debugPrint(schemeOrg.name);

      if (!organizationSelections
          .any((selection) => selection.oin == schemeOrg.oin)) {
        debugPrint("Added ${schemeOrg.oin}");

        await organizationSelectionRepository.add(
          oin: schemeOrg.oin,
        );
      } else {
        debugPrint("Did not add ${schemeOrg.oin}");
      }
    }
  }

  @override
  Future<void> selectAllOrganizations() async {
    final List<OrganizationSelection> organizationSelections =
        await organizationSelectionRepository.organizationSelections;

    for (final organizationSelection in organizationSelections) {
      debugPrint("Set ${organizationSelection.oin} selected");

      await organizationSelectionRepository.update(
        organizationSelection.copyWith(
          selected: true,
        ),
      );
    }
  }

  @override
  Future<void> deselectAllOrganizations() async {
    final List<OrganizationSelection> organizationSelections =
        await organizationSelectionRepository.organizationSelections;

    for (final organizationSelection in organizationSelections) {
      debugPrint("Set ${organizationSelection.oin} not selected");

      await organizationSelectionRepository.update(
        organizationSelection.copyWith(
          selected: false,
        ),
      );
    }
  }

  @override
  Future<void> setOrganizationSelection(String oin, bool selected) async {
    var organizationSelections =
        await organizationSelectionRepository.organizationSelections;

    var organizationSelection = organizationSelections.singleWhere(
      (organizationSelection) => organizationSelection.oin == oin,
    );

    await organizationSelectionRepository.update(
      organizationSelection.copyWith(
        selected: selected,
      ),
    );
  }

  @override
  Future<void> setAutoSelectAllOrganizations(
          bool autoSelectAllOrganizations) async =>
      await userSettingsRepository
          .setAutoSelectAllOrganizations(autoSelectAllOrganizations);
}

final organizationSelectionUsecaseProvider =
    Provider<IOrganizationSelectionUsecase>((ref) {
  final organizationSelectionRepository =
      ref.watch(organizationSelectionRepositoryProvider.notifier);

  final schemeOrganizationRepository =
      ref.watch(schemeOrganizationRepositoryProvider.notifier);

  final userSettingsRepository =
      ref.watch(userSettingsRepositoryProvider.notifier);

  return OrganizationSelectionUsecase(
    organizationSelectionRepository,
    schemeOrganizationRepository,
    userSettingsRepository,
  );
});

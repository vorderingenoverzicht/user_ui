import 'dart:convert';

import 'package:fcid_library/fcid_v4.dart';
import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/financial_claims_information_received/financial_claims_information_received_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/payment_plan/payment_plan_group_repository.dart';
import 'package:user_ui/repositories/payment_plan/payment_plan_rule_repository.dart';

part 'process_payment_plan_rules.g.dart';

@riverpod
Future<void> processPaymentPlanRules(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  debugPrint(
      "     ${financialClaimsInformationRequest.id} Step: 15 processPaymentPlanRules");

  final paymentPlanRulesList =
      await ref.read(paymentPlanRuleRepositoryProvider.notifier).rules;

  final paymentPlanGroupList =
      ref.read(paymentPlanGroupRepositoryProvider.notifier);

  final financialClaimsInformationReceivedList = await ref
      .read(financialClaimsInformationReceivedRepositoryProvider.future);

  final financialClaimsInformationReceived =
      financialClaimsInformationReceivedList.lastWhere(
    (financialClaimsInformationReceived) =>
        financialClaimsInformationReceived.oin ==
            financialClaimsInformationRequest.oin &&
        financialClaimsInformationReceived.financialClaimsInformationDocument !=
            null,
  );

  PaymentPlanGroup paymentPlanGroup = PaymentPlanGroup(
      zaakkenmerk: List.empty(), saldo: 0, termijnen: 0, betaalRegels: 0);

  bool planGroupEmpty = true;

  if (financialClaimsInformationReceived.financialClaimsInformationDocument !=
      null) {
    final financialClaimsInformationDocumentJson = json.decode(
        financialClaimsInformationReceived.financialClaimsInformationDocument!);
    final financialClaimsInformationDocument =
        FinancialClaimsInformationDocument.fromJson(
      financialClaimsInformationDocumentJson,
    );

    final org = financialClaimsInformationDocument.body.aangeleverdDoor;

    for (final rule in paymentPlanRulesList) {
      if (rule.organisaties.contains(org)) {
        for (final financialeZaak
            in financialClaimsInformationDocument.body.financieleZaken) {
          List<String> zaakkenmerken = [
            financialeZaak.zaakkenmerk,
            ...paymentPlanGroup.zaakkenmerk,
          ];

          final newPaymentPlanGroup = PaymentPlanGroup(
            zaakkenmerk: zaakkenmerken,
            saldo: paymentPlanGroup.saldo + financialeZaak.saldo,
            termijnen: 0,
            betaalRegels: 0,
          );
          paymentPlanGroup = newPaymentPlanGroup;
          planGroupEmpty = false;
        }
      }
    }
  }

  for (final rule in paymentPlanRulesList) {
    if (paymentPlanGroup.saldo >= rule.minimumBedrag &&
        paymentPlanGroup.saldo < rule.maximumBedrag) {
      final newPaymentPlanGroup = PaymentPlanGroup(
        zaakkenmerk: paymentPlanGroup.zaakkenmerk,
        saldo: paymentPlanGroup.saldo,
        termijnen: paymentPlanGroup.termijnen,
        betaalRegels: rule.id,
      );

      paymentPlanGroup = newPaymentPlanGroup;
    }
  }
  if (!planGroupEmpty) {
    switch (paymentPlanGroup.saldo) {
      case < 120:
        final newPaymentPlanGroup = PaymentPlanGroup(
            zaakkenmerk: paymentPlanGroup.zaakkenmerk,
            saldo: paymentPlanGroup.saldo,
            termijnen: 3,
            betaalRegels: paymentPlanGroup.betaalRegels);
        paymentPlanGroup = newPaymentPlanGroup;
        break;
      case < 300:
        final newPaymentPlanGroup = PaymentPlanGroup(
            zaakkenmerk: paymentPlanGroup.zaakkenmerk,
            saldo: paymentPlanGroup.saldo,
            termijnen: 6,
            betaalRegels: paymentPlanGroup.betaalRegels);
        paymentPlanGroup = newPaymentPlanGroup;
        break;
      case < 450:
        final newPaymentPlanGroup = PaymentPlanGroup(
            zaakkenmerk: paymentPlanGroup.zaakkenmerk,
            saldo: paymentPlanGroup.saldo,
            termijnen: 9,
            betaalRegels: paymentPlanGroup.betaalRegels);
        paymentPlanGroup = newPaymentPlanGroup;
        break;
      case < 1250:
        final newPaymentPlanGroup = PaymentPlanGroup(
            zaakkenmerk: paymentPlanGroup.zaakkenmerk,
            saldo: paymentPlanGroup.saldo,
            termijnen: 12,
            betaalRegels: paymentPlanGroup.betaalRegels);
        paymentPlanGroup = newPaymentPlanGroup;
        break;
      case < 2500:
        final newPaymentPlanGroup = PaymentPlanGroup(
            zaakkenmerk: paymentPlanGroup.zaakkenmerk,
            saldo: paymentPlanGroup.saldo,
            termijnen: 18,
            betaalRegels: paymentPlanGroup.betaalRegels);
        paymentPlanGroup = newPaymentPlanGroup;
        break;
      case < 3000:
        final newPaymentPlanGroup = PaymentPlanGroup(
            zaakkenmerk: paymentPlanGroup.zaakkenmerk,
            saldo: paymentPlanGroup.saldo,
            termijnen: 24,
            betaalRegels: paymentPlanGroup.betaalRegels);
        paymentPlanGroup = newPaymentPlanGroup;
        break;
      case < 4000:
        final newPaymentPlanGroup = PaymentPlanGroup(
            zaakkenmerk: paymentPlanGroup.zaakkenmerk,
            saldo: paymentPlanGroup.saldo,
            termijnen: 30,
            betaalRegels: paymentPlanGroup.betaalRegels);
        paymentPlanGroup = newPaymentPlanGroup;
        break;
      default:
        final newPaymentPlanGroup = PaymentPlanGroup(
            zaakkenmerk: paymentPlanGroup.zaakkenmerk,
            saldo: paymentPlanGroup.saldo,
            termijnen: 36,
            betaalRegels: paymentPlanRulesList.last.id);
        paymentPlanGroup = newPaymentPlanGroup;
        break;
    }

    paymentPlanGroupList.addGroup(paymentPlanGroup);
  }

  final financialClaimsInformationRequestUpdate =
      financialClaimsInformationRequest.copyWith(
    status: FinancialClaimsInformationRequestStatus.paymentPlanRulesProcessed,
  );

  await financialClaimsInformationRequestRepository.update(
    financialClaimsInformationRequestUpdate,
  );
}

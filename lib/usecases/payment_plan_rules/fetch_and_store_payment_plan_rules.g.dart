// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fetch_and_store_payment_plan_rules.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$fetchAndStorePaymentPlanRulesHash() =>
    r'c11e7f7dff1cf3123de6ef69f6bcb5bb5276795b';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [fetchAndStorePaymentPlanRules].
@ProviderFor(fetchAndStorePaymentPlanRules)
const fetchAndStorePaymentPlanRulesProvider =
    FetchAndStorePaymentPlanRulesFamily();

/// See also [fetchAndStorePaymentPlanRules].
class FetchAndStorePaymentPlanRulesFamily extends Family<AsyncValue<void>> {
  /// See also [fetchAndStorePaymentPlanRules].
  const FetchAndStorePaymentPlanRulesFamily();

  /// See also [fetchAndStorePaymentPlanRules].
  FetchAndStorePaymentPlanRulesProvider call(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) {
    return FetchAndStorePaymentPlanRulesProvider(
      financialClaimsInformationRequest,
    );
  }

  @override
  FetchAndStorePaymentPlanRulesProvider getProviderOverride(
    covariant FetchAndStorePaymentPlanRulesProvider provider,
  ) {
    return call(
      provider.financialClaimsInformationRequest,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'fetchAndStorePaymentPlanRulesProvider';
}

/// See also [fetchAndStorePaymentPlanRules].
class FetchAndStorePaymentPlanRulesProvider
    extends AutoDisposeFutureProvider<void> {
  /// See also [fetchAndStorePaymentPlanRules].
  FetchAndStorePaymentPlanRulesProvider(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) : this._internal(
          (ref) => fetchAndStorePaymentPlanRules(
            ref as FetchAndStorePaymentPlanRulesRef,
            financialClaimsInformationRequest,
          ),
          from: fetchAndStorePaymentPlanRulesProvider,
          name: r'fetchAndStorePaymentPlanRulesProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$fetchAndStorePaymentPlanRulesHash,
          dependencies: FetchAndStorePaymentPlanRulesFamily._dependencies,
          allTransitiveDependencies:
              FetchAndStorePaymentPlanRulesFamily._allTransitiveDependencies,
          financialClaimsInformationRequest: financialClaimsInformationRequest,
        );

  FetchAndStorePaymentPlanRulesProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.financialClaimsInformationRequest,
  }) : super.internal();

  final FinancialClaimsInformationRequest financialClaimsInformationRequest;

  @override
  Override overrideWith(
    FutureOr<void> Function(FetchAndStorePaymentPlanRulesRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FetchAndStorePaymentPlanRulesProvider._internal(
        (ref) => create(ref as FetchAndStorePaymentPlanRulesRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        financialClaimsInformationRequest: financialClaimsInformationRequest,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _FetchAndStorePaymentPlanRulesProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FetchAndStorePaymentPlanRulesProvider &&
        other.financialClaimsInformationRequest ==
            financialClaimsInformationRequest;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash =
        _SystemHash.combine(hash, financialClaimsInformationRequest.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FetchAndStorePaymentPlanRulesRef on AutoDisposeFutureProviderRef<void> {
  /// The parameter `financialClaimsInformationRequest` of this provider.
  FinancialClaimsInformationRequest get financialClaimsInformationRequest;
}

class _FetchAndStorePaymentPlanRulesProviderElement
    extends AutoDisposeFutureProviderElement<void>
    with FetchAndStorePaymentPlanRulesRef {
  _FetchAndStorePaymentPlanRulesProviderElement(super.provider);

  @override
  FinancialClaimsInformationRequest get financialClaimsInformationRequest =>
      (origin as FetchAndStorePaymentPlanRulesProvider)
          .financialClaimsInformationRequest;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

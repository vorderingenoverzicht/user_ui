import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/payment_plan_rules/payment_plan_rules_client_live.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/payment_plan/payment_plan_rule_repository.dart';

part 'fetch_and_store_payment_plan_rules.g.dart';

@riverpod
Future<void> fetchAndStorePaymentPlanRules(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  debugPrint(
      "     ${financialClaimsInformationRequest.id} Step: 14 fetchAndStorePaymentPlanRules");

  var rules = await ref.read(rulesClientProvider).fetchRules();

  var paymentPlanRules = rules
      .map(
        (rule) => PaymentPlanRule(
          id: rule.id,
          organisaties: rule.organisaties.join(","),
          minimumBedrag: rule.minBedrag,
          maximumBedrag: rule.maxBedrag,
          type: rule.type,
        ),
      )
      .toList();

  await ref
      .read(paymentPlanRuleRepositoryProvider.notifier)
      .addRules(paymentPlanRules);

  final financialClaimsInformationRequestUpdate =
      financialClaimsInformationRequest.copyWith(
    status: FinancialClaimsInformationRequestStatus
        .paymentPlanRulesFetchedAndStored,
  );

  await financialClaimsInformationRequestRepository.update(
    financialClaimsInformationRequestUpdate,
  );
}

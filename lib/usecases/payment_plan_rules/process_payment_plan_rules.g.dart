// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'process_payment_plan_rules.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$processPaymentPlanRulesHash() =>
    r'6df7d48a478babf53028d3713a4e6b37fbfd320d';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [processPaymentPlanRules].
@ProviderFor(processPaymentPlanRules)
const processPaymentPlanRulesProvider = ProcessPaymentPlanRulesFamily();

/// See also [processPaymentPlanRules].
class ProcessPaymentPlanRulesFamily extends Family<AsyncValue<void>> {
  /// See also [processPaymentPlanRules].
  const ProcessPaymentPlanRulesFamily();

  /// See also [processPaymentPlanRules].
  ProcessPaymentPlanRulesProvider call(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) {
    return ProcessPaymentPlanRulesProvider(
      financialClaimsInformationRequest,
    );
  }

  @override
  ProcessPaymentPlanRulesProvider getProviderOverride(
    covariant ProcessPaymentPlanRulesProvider provider,
  ) {
    return call(
      provider.financialClaimsInformationRequest,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'processPaymentPlanRulesProvider';
}

/// See also [processPaymentPlanRules].
class ProcessPaymentPlanRulesProvider extends AutoDisposeFutureProvider<void> {
  /// See also [processPaymentPlanRules].
  ProcessPaymentPlanRulesProvider(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) : this._internal(
          (ref) => processPaymentPlanRules(
            ref as ProcessPaymentPlanRulesRef,
            financialClaimsInformationRequest,
          ),
          from: processPaymentPlanRulesProvider,
          name: r'processPaymentPlanRulesProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$processPaymentPlanRulesHash,
          dependencies: ProcessPaymentPlanRulesFamily._dependencies,
          allTransitiveDependencies:
              ProcessPaymentPlanRulesFamily._allTransitiveDependencies,
          financialClaimsInformationRequest: financialClaimsInformationRequest,
        );

  ProcessPaymentPlanRulesProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.financialClaimsInformationRequest,
  }) : super.internal();

  final FinancialClaimsInformationRequest financialClaimsInformationRequest;

  @override
  Override overrideWith(
    FutureOr<void> Function(ProcessPaymentPlanRulesRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ProcessPaymentPlanRulesProvider._internal(
        (ref) => create(ref as ProcessPaymentPlanRulesRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        financialClaimsInformationRequest: financialClaimsInformationRequest,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _ProcessPaymentPlanRulesProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ProcessPaymentPlanRulesProvider &&
        other.financialClaimsInformationRequest ==
            financialClaimsInformationRequest;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash =
        _SystemHash.combine(hash, financialClaimsInformationRequest.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ProcessPaymentPlanRulesRef on AutoDisposeFutureProviderRef<void> {
  /// The parameter `financialClaimsInformationRequest` of this provider.
  FinancialClaimsInformationRequest get financialClaimsInformationRequest;
}

class _ProcessPaymentPlanRulesProviderElement
    extends AutoDisposeFutureProviderElement<void>
    with ProcessPaymentPlanRulesRef {
  _ProcessPaymentPlanRulesProviderElement(super.provider);

  @override
  FinancialClaimsInformationRequest get financialClaimsInformationRequest =>
      (origin as ProcessPaymentPlanRulesProvider)
          .financialClaimsInformationRequest;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

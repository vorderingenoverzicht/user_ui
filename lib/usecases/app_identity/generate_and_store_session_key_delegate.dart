import 'dart:typed_data';

import 'package:dart_connect/dart_connect.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/app_identity/app_session_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';

part 'generate_and_store_session_key_delegate.g.dart';

@riverpod
Future<void> Function(
  Ref ref,
  String oin,
) generateAndStoreSessionKeyDelegate(Ref _) {
  return (ref, oin) async {
    var userSettings = ref.read(userSettingsRepositoryProvider);
    if (userSettings.delay) {
      await Future.delayed(const Duration(seconds: 2));
    }

    final Uint8List key = Aes.generateKey();

    await ref.read(appSessionRepositoryProvider.notifier).add(
          key: Base64.parseFromBytes(key),
          oin: oin,
        );
  };
}

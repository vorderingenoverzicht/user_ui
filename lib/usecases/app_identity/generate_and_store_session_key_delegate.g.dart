// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'generate_and_store_session_key_delegate.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$generateAndStoreSessionKeyDelegateHash() =>
    r'163a1ce2891c09461c2f2f0b06896e15216d6b5a';

/// See also [generateAndStoreSessionKeyDelegate].
@ProviderFor(generateAndStoreSessionKeyDelegate)
final generateAndStoreSessionKeyDelegateProvider =
    AutoDisposeProvider<Future<void> Function(Ref ref, String oin)>.internal(
  generateAndStoreSessionKeyDelegate,
  name: r'generateAndStoreSessionKeyDelegateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$generateAndStoreSessionKeyDelegateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef GenerateAndStoreSessionKeyDelegateRef
    = AutoDisposeProviderRef<Future<void> Function(Ref ref, String oin)>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

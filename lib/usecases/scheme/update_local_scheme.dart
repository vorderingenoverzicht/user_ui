// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/scheme/scheme_client_live.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/scheme_app_manager/scheme_app_manager_repository.dart';
import 'package:user_ui/repositories/scheme_document_type/scheme_document_type_repository.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_organization_repository.dart';

part 'update_local_scheme.g.dart';

@riverpod
Future<void> updateLocalScheme(Ref ref) async {
  final schemeClient = ref.read(schemeClientProvider);
  final scheme = await schemeClient.fetchScheme();

  final schemeOrganizationRepository =
      ref.read(schemeOrganizationRepositoryProvider.notifier);

  final localSchemeOrganizations =
      await schemeOrganizationRepository.organizations;
  // Mark organizations, that are no longer in the scheme, unavailable
  for (final localSchemeOrganization in localSchemeOrganizations) {
    debugPrint(localSchemeOrganization.oin);

    if (!scheme.organizations.any((schemeOrganization) =>
        schemeOrganization.oin == localSchemeOrganization.oin)) {
      debugPrint("Set ${localSchemeOrganization.oin} unavailable");

      await schemeOrganizationRepository.update(
        localSchemeOrganization.copyWith(
          available: false,
        ),
      );
    } else {
      debugPrint(
          "Did not set ${localSchemeOrganization.oin} unavailable (still in scheme)");
    }
  }

  final loggingHelper = ref.watch(loggingProvider);
  // Make sure all organizations currently in the scheme are also (updated) in the local scheme
  for (final organization in scheme.organizations) {
    final localSchemeOrganizations =
        await schemeOrganizationRepository.organizations;

    final organizationExistsInLocalSchemeIterable = localSchemeOrganizations
        .where((SchemeOrganization schemeOrganization) =>
            schemeOrganization.oin == organization.oin);

    if (organizationExistsInLocalSchemeIterable.length > 1) {
      loggingHelper.addLog(DeviceEvent.uu_66,
          "multiple organizations with the same OIN in scheme organization repository");
      throw Exception(
          "multiple organizations with the same OIN in scheme organization repository");
    } else if (organizationExistsInLocalSchemeIterable.isEmpty) {
      await schemeOrganizationRepository.add(
        oin: organization.oin,
        name: organization.name,
        publicKey: organization.publicKey,
        discoveryUrl: organization.discoveryUrl,
        available: true,
      );

      debugPrint("Added new organization ${organization.oin}");
    } else if (organizationExistsInLocalSchemeIterable.length == 1) {
      final localSchemeOrganization =
          organizationExistsInLocalSchemeIterable.single;

      if (!localSchemeOrganization.available ||
          localSchemeOrganization.name != organization.name ||
          localSchemeOrganization.discoveryUrl != organization.discoveryUrl ||
          localSchemeOrganization.publicKey != organization.publicKey) {
        schemeOrganizationRepository.update(localSchemeOrganization.copyWith(
            name: organization.name,
            publicKey: organization.publicKey,
            discoveryUrl: organization.discoveryUrl,
            available: true));

        debugPrint("Updated ${localSchemeOrganization.oin}");
      }
    }
  }

  final schemeDocumentTypeRepository =
      ref.read(schemeDocumentTypeRepositoryProvider.notifier);
  // Mark documentTypes, that are no longer in the scheme, unavailable
  for (final localSchemeDocumentType
      in await schemeDocumentTypeRepository.documentTypes) {
    debugPrint(localSchemeDocumentType.name);

    if (!scheme.documentTypes.any((schemeDocumentType) =>
        schemeDocumentType.name == localSchemeDocumentType.name)) {
      debugPrint("Set ${localSchemeDocumentType.name} unavailable");

      await schemeDocumentTypeRepository.update(
        localSchemeDocumentType.copyWith(
          available: false,
        ),
      );
    } else {
      debugPrint(
          "Did not set ${localSchemeDocumentType.name} unavailable (still in scheme)");
    }
  }

  // Make sure all documentTypes currently in the scheme are also (updated) in the local scheme
  for (final documentType in scheme.documentTypes) {
    final localSchemeDocumentTypes =
        await schemeDocumentTypeRepository.documentTypes;

    final documentTypeExistsInLocalSchemeIterable =
        localSchemeDocumentTypes.where((schemeDocumentType) =>
            schemeDocumentType.name == documentType.name);

    if (documentTypeExistsInLocalSchemeIterable.length > 1) {
      loggingHelper.addLog(DeviceEvent.uu_67,
          "multiple documentTypes with the same name in scheme document type repository");
      throw Exception(
          "multiple documentTypes with the same name in scheme document type repository");
    } else if (documentTypeExistsInLocalSchemeIterable.isEmpty) {
      await schemeDocumentTypeRepository.add(
        name: documentType.name,
        available: true,
      );

      debugPrint("Added new document type ${documentType.name}");
    } else if (documentTypeExistsInLocalSchemeIterable.length == 1) {
      final localSchemeDocumentType =
          documentTypeExistsInLocalSchemeIterable.single;

      if (!localSchemeDocumentType.available ||
          localSchemeDocumentType.name != documentType.name) {
        schemeDocumentTypeRepository.update(
          localSchemeDocumentType.copyWith(
            name: documentType.name,
            available: true,
          ),
        );

        debugPrint("Updated ${localSchemeDocumentType.name}");
      }
    }
  }

  final schemeAppManagerRepository =
      ref.read(schemeAppManagerRepositoryProvider.notifier);
  // Mark appmanagers, that are no longer in the scheme, unavailable
  for (final localSchemeAppManager
      in await schemeAppManagerRepository.appManagers) {
    debugPrint(localSchemeAppManager.oin);

    if (!scheme.appManagers.any((schemeAppManager) =>
        schemeAppManager.oin == localSchemeAppManager.oin)) {
      debugPrint("Set ${localSchemeAppManager.oin} unavailable");

      await schemeAppManagerRepository.update(
        localSchemeAppManager.copyWith(
          available: false,
        ),
      );
    } else {
      debugPrint(
          "Did not set ${localSchemeAppManager.oin} unavailable (still in scheme)");
    }
  }

  // Make sure all appmanagers currently in the scheme are also (updated) in the local scheme
  for (final appmanager in scheme.appManagers) {
    final localSchemeAppManagers = await schemeAppManagerRepository.appManagers;

    final appmanagerExistsInLocalSchemeIterable = localSchemeAppManagers
        .where((schemeAppManager) => schemeAppManager.oin == appmanager.oin);

    if (appmanagerExistsInLocalSchemeIterable.length > 1) {
      loggingHelper.addLog(DeviceEvent.uu_68,
          "multiple appmanagers with the same OIN in scheme app manager repository");
      throw Exception(
          "multiple appmanagers with the same OIN in scheme app manager repository");
    } else if (appmanagerExistsInLocalSchemeIterable.isEmpty) {
      await schemeAppManagerRepository.add(
        oin: appmanager.oin,
        name: appmanager.name,
        publicKey: appmanager.publicKey,
        discoveryUrl: appmanager.discoveryUrl,
        available: true,
      );

      debugPrint("Added new appmanager ${appmanager.oin}");
    } else if (appmanagerExistsInLocalSchemeIterable.length == 1) {
      final localSchemeAppManager =
          appmanagerExistsInLocalSchemeIterable.single;

      if (!localSchemeAppManager.available ||
          localSchemeAppManager.name != appmanager.name ||
          localSchemeAppManager.discoveryUrl != appmanager.discoveryUrl ||
          localSchemeAppManager.publicKey != appmanager.publicKey) {
        schemeAppManagerRepository.update(localSchemeAppManager.copyWith(
            name: appmanager.name,
            publicKey: appmanager.publicKey,
            discoveryUrl: appmanager.discoveryUrl,
            available: true));

        debugPrint("Updated ${localSchemeAppManager.oin}");
      }
    }
  }
}

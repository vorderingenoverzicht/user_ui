// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL
import 'package:user_ui/app.dart';
import 'package:user_ui/worker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:user_ui/env_provider.dart';
import 'package:user_ui/setup_providers.dart';
import 'package:user_ui/theme/theme_manager.dart';
import 'package:user_ui/setup_mocked_providers.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';

/// Main entry point for the app, handling different flavors and environments.
///
/// To build for mobile devices with flavors:
/// ```sh
///   flutter build ios --flavor tst
///   flutter build android --flavor demo
///```
/// To build for web without flavors:
/// ```sh
///   flutter build web --pwa-strategy=none
///```
/// When running on web, ensure an env.json file exists in the assets folder
/// with the following structure:
/// ```json
/// {
///   "environmentType": "demo" | "tst",
///   "schemeUrl": "https://your-domain.com"
/// }
/// ```
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Future<ProviderContainer> setupProvidersPerEnvironment(
      EnvironmentType type, String schemeUrl) async {
    final environment = Environment(type: type, schemeUrl: schemeUrl);

    switch (type) {
      case EnvironmentType.demo:
      case EnvironmentType.test:
      case EnvironmentType.local:
        return await setupProviders(environment);
      case EnvironmentType.mock:
        return await setupMockedProviders(environment);
    }
  }

  ProviderContainer? container;
  // App flavor will be Null when not specified in the build command
  switch (appFlavor) {
    case ("demo"):
      container = await setupProvidersPerEnvironment(
        EnvironmentType.demo,
        'https://api.stelsel.vorijk-demo.blauweknop.app/v1',
      );
      break;
    case ("tst"):
      container = await setupProvidersPerEnvironment(
        EnvironmentType.test,
        'https://api.stelsel.vorijk-test.blauweknop.app/v1',
      );
      break;
    case ("local"):
      container = await setupProvidersPerEnvironment(
        EnvironmentType.local,
        'http://scheme-process.scheme-manager.vorderingenoverzicht.blauweknop.bk/v1',
      );
      break;
    case ("mock"):
      container = await setupProvidersPerEnvironment(
        EnvironmentType.mock,
        '',
      );
      break;
    default:
      final environment = ProviderContainer().read(environmentProvider);
      await environment.initialize();
      container = await setupProvidersPerEnvironment(
          environment.type, environment.schemeUrl);
      break;
  }

  await container.read(workerProvider.future);

  if (!kReleaseMode) {
    container
        .read(userSettingsRepositoryProvider.notifier)
        .setShowCriticalErrorAlerts(true);
  }

  ThemeManager.setBreakpoints();

  runApp(
    UncontrolledProviderScope(
      container: container,
      child: const App(),
    ),
  );
}

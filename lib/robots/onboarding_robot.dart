import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/features/activation/activation_info_screen.dart';
import 'package:user_ui/screens/features/activation/complete_activation_screen.dart';
import 'package:user_ui/screens/features/onboarding/components/data_use_text_row.dart';
import 'package:user_ui/screens/features/onboarding/components/proposition_text_row.dart';
import 'package:user_ui/screens/features/onboarding/data_use_screen.dart';
import 'package:user_ui/screens/features/onboarding/propositon_screen.dart';

class OnboardingRobot {
  OnboardingRobot(this.tester);

  WidgetTester tester;

  // Proposition screen
  Future<void> assertThatUserIsOnThePropositionScreen() async {
    expect(find.byType(ThemedButton), findsOneWidget);
    expect(find.byType(PropositionTextRow), findsNWidgets(3));
    expect(find.bySemanticsLabel('video placeholder'), findsOneWidget);
    expect(find.text('Dit heb je aan de app'), findsOneWidget);
  }

  Future<void> navigateToTheDataUseScreen() async {
    final nextButton =
        find.byKey(const Key(PropositionScreen.propositionNextButtonKey));
    await tester.ensureVisible(nextButton);
    await tester.tap(nextButton);
    await tester.pumpAndSettle(const Duration(seconds: 2));
  }

  // Data use screen
  Future<void> assertThatUserIsOnTheDataUseScreen() async {
    expect(find.byType(ThemedButton), findsOneWidget);
    expect(find.byType(DataUseTextRow), findsNWidgets(4));
    expect(find.text('Zo gebruikt de app je gegevens'), findsOneWidget);
  }

  Future<void> navigateToTheActivationInfoScreen() async {
    final nextButton =
        find.byKey(const Key(DataUseScreen.dataUseNextButtonKey));
    await tester.ensureVisible(nextButton);
    await tester.tap(nextButton);
    await tester.pumpAndSettle(const Duration(seconds: 2));
  }

  // Activation info screen
  Future<void> assertThatUserIsOnTheActivationInfoScreen() async {
    expect(find.byType(ThemedButton), findsNWidgets(2));
    expect(find.bySemanticsLabel('digid login image'), findsOneWidget);
    expect(find.bySemanticsLabel('digid icon'), findsOneWidget);
    expect(find.text('Je gaat de VO Rijk app activeren'), findsOneWidget);
  }

  Future<void> navigateToStartActivationScreen() async {
    final activateButton = find
        .byKey(const Key(ActivationInfoScreen.activationInfoActivateButtonKey));
    await tester.ensureVisible(activateButton);
    await tester.tap(activateButton);
    await tester.pump(const Duration(seconds: 2));
  }

  // Start activation screen
  Future<void> assertThatUserIsOnTheStartActivationInfoScreen() async {
    expect(find.text('App activeren voorbereiden...'), findsOneWidget);
  }

  Future<void> navigateToCompleteActivationScreen() async {
    final BuildContext context = tester.element(
      find.text('App activeren voorbereiden...'),
    );
    context.goNamed(CompleteActivationScreen.routeName);
    await tester.pumpAndSettle(const Duration(seconds: 2));
  }
}

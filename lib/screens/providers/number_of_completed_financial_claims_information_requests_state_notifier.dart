// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';

abstract class INumberOfCompletedFinancialClaimsInformationRequestsStateNotifier {
  int get numberOfCompletedFinancialClaimsInformationRequests;
}

class NumberOfCompletedFinancialClaimsInformationRequestsStateNotifier
    extends StateNotifier<int>
    implements
        INumberOfCompletedFinancialClaimsInformationRequestsStateNotifier {
  final List<FinancialClaimsInformationRequest>
      financialClaimsInformationRequests;

  NumberOfCompletedFinancialClaimsInformationRequestsStateNotifier(
      this.financialClaimsInformationRequests)
      : super(0) {
    state = numberOfCompletedFinancialClaimsInformationRequests;
  }

  @override
  int get numberOfCompletedFinancialClaimsInformationRequests {
    if (financialClaimsInformationRequests.isEmpty) {
      return 0;
    }

    var lastDatetimeRequested =
        financialClaimsInformationRequests.last.dateTimeRequested;

    return financialClaimsInformationRequests
        .where((request) =>
            request.dateTimeRequested == lastDatetimeRequested &&
            financialClaimsInformationRequestEndStatusList
                .contains(request.status))
        .length;
  }
}

final numberOfCompletedFinancialClaimsInformationRequestsStateNotifierProvider =
    StateNotifierProvider<
        NumberOfCompletedFinancialClaimsInformationRequestsStateNotifier,
        int>((ref) {
  final financialClaimsInformationRequests =
      ref.watch(financialClaimsInformationRequestRepositoryProvider);

  return NumberOfCompletedFinancialClaimsInformationRequestsStateNotifier(
      financialClaimsInformationRequests);
});

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/entities/log_record.dart';
import 'package:user_ui/screens/features/error/error_log_screen.dart';
import 'package:user_ui/theme/severity_extension.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

SnackBar createSnackBar(LogRecord log, BuildContext context, GoRouter goRouter,
    ScaffoldMessengerState? scaffold) {
  const double backgroundOpacity = 0.9;

  return SnackBar(
    backgroundColor: context.colorScheme.surfaceContainerHighest
        .withOpacity(backgroundOpacity),
    behavior: SnackBarBehavior.floating,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(RadiusSize.medium.value),
      ),
    ),
    content: Row(
      children: [
        Icon(
          log.severity.severityIcon,
          color: log.severity.getSeverityColor(context),
          size: IconSize.xlarge.value,
        ),
        addHorizontalSpace(Spacing.large.value),
        Flexible(
          child: Wrap(
            children: [
              RichText(
                text: TextSpan(
                  text: "${log.deviceEventClassId.toUpperCase()} - ",
                  style: context.textTheme.bodyLarge?.copyWith(
                      color: context.colorScheme.surface,
                      fontWeight: FontWeight.bold),
                  children: [
                    TextSpan(
                      text: log.name,
                      style: context.textTheme.bodyLarge?.copyWith(
                          color: context.colorScheme.onInverseSurface),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        TextButton(
          onPressed: () {
            goRouter.pushNamed(ErrorLogScreen.routeName,
                pathParameters: {'showBackButton': 'true'});
            scaffold!.hideCurrentSnackBar(reason: SnackBarClosedReason.action);
          },
          child: Text(
            'Toon logs',
            style: context.textTheme.bodyLarge?.copyWith(
              color: context.colorScheme.onInverseSurface,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    ),
  );
}

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';

class ThemedProgressIndicator extends StatelessWidget {
  const ThemedProgressIndicator({super.key, this.color, this.size = 24});
  final Color? color;
  final double size;
  @override
  Widget build(BuildContext context) {
    return SizedBox.square(
      dimension: size,
      child: CircularProgressIndicator(
        color: color,
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:user_ui/repositories/logging/event_severity_level.dart';
import 'package:user_ui/theme/theme_constants.dart';

class ThemedChipButton extends StatelessWidget {
  final EventSeverityLevel severity;
  final bool isSelected;
  final Function(EventSeverityLevel) onToggle;
  final Color color;

  const ThemedChipButton({
    super.key,
    required this.severity,
    required this.isSelected,
    required this.onToggle,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return FilterChip(
      selectedColor: isSelected ? color : Colors.grey.shade200,
      padding: EdgeInsets.symmetric(
          horizontal: Spacing.medium.value, vertical: Spacing.small.value),
      labelStyle: Theme.of(context)
          .textTheme
          .labelMedium
          ?.copyWith(color: Colors.white),
      label: Text(severity.toString().split('.').last),
      selected: isSelected,
      onSelected: (bool value) {
        onToggle(severity);
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/theme/responsive_grid.dart';

class DevelopmentOverlayWidget extends ConsumerWidget {
  const DevelopmentOverlayWidget({
    super.key,
    required this.children,
  });

  final List<Widget> children;

  String getScreenSize(BuildContext context, String size) =>
      '$size ${MediaQuery.of(context).size.width}';
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Stack(
      children: [
        ...children,
        Align(
          alignment: Alignment.bottomCenter,
          child: Text(
            responsiveValue(
              context,
              xs: getScreenSize(context, 'xsmall'),
              sm: getScreenSize(context, 'small'),
              md: getScreenSize(context, 'medium'),
              lg: getScreenSize(context, 'large'),
              xl: getScreenSize(context, 'xlarge'),
            ),
            style: Theme.of(context)
                .textTheme
                .bodySmall
                ?.copyWith(fontWeight: FontWeight.bold),
          ),
        ),
        ResponsiveGridRow(
          margin: 16,
          children: [
            for (int i = 0; i < 12; i++)
              ResponsiveGridCol(
                xs: 1,
                child: Container(
                  color: Colors.red.withOpacity(0.2),
                  height: MediaQuery.of(context).size.height,
                ),
              ),
          ],
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';

class ThemedDivider extends StatelessWidget {
  const ThemedDivider({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: context.colorScheme.outlineVariant,
      height: Spacing.tiny.value,
    );
  }
}

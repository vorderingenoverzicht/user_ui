// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:user_ui/screens/common/basic_dialog_scaffold.dart';
import 'package:user_ui/screens/common/basic_scaffold.dart';
import 'package:user_ui/screens/common/themed_progress_indicator.dart';
import 'package:user_ui/screens/features/home/components/deep_link.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class ThemedLoadingIndicator extends StatelessWidget {
  const ThemedLoadingIndicator({
    required this.text,
    this.onBackbuttonPress,
    this.showDrawer = false,
    this.addDeepLink = false,
    this.isDialog = false,
    super.key,
  });

  final void Function()? onBackbuttonPress;
  final bool showDrawer;
  final String text;
  final bool addDeepLink;
  final bool isDialog;

  @override
  Widget build(BuildContext context) {
    return isDialog
        ? BasicDialogScaffold(
            onBackbuttonPress: onBackbuttonPress,
            content: _Content(text: text, addDeepLink: addDeepLink),
          )
        : BasicScaffold(
            onBackbuttonPress: onBackbuttonPress,
            showDrawer: showDrawer,
            content: _Content(text: text, addDeepLink: addDeepLink),
          );
  }
}

class _Content extends StatelessWidget {
  const _Content({
    required this.text,
    required this.addDeepLink,
  });

  final String text;
  final bool addDeepLink;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          text,
          style: context.textTheme.displayMedium,
          textAlign: TextAlign.center,
        ),
        addVerticalSpace(Spacing.xlarge.value),
        ThemedProgressIndicator(size: IconSize.xlarge.value),
        if (addDeepLink) const DeepLink(),
      ],
    );
  }
}

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/features/activation/reactivation_screen.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';

class ReactivationBanner extends ConsumerWidget {
  const ReactivationBanner({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0.0,
          backgroundColor: context.colorScheme.error,
          minimumSize: Size(double.infinity, Spacing.xxlarge.value),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.zero,
          ),
        ),
        onPressed: (() async {
          context.pushNamed(ReactivationScreen.routeName);
        }),
        child: Text(
          "De app is niet meer geactiveerd.",
          style: context.textTheme.bodyLarge?.copyWith(
            fontWeight: FontWeight.bold,
            color: context.colorScheme.surfaceContainerLowest,
          ),
        ));
  }
}

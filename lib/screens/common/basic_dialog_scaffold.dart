// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:user_ui/screens/common/themed_back_button.dart';
import 'package:user_ui/screens/common/themed_close_button.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';

class BasicDialogScaffold extends StatelessWidget {
  const BasicDialogScaffold({
    super.key,
    this.title,
    this.onBackbuttonPress,
    this.alignment = Alignment.center,
    required this.content,
  });

  final String? title;
  final void Function()? onBackbuttonPress;
  final Widget content;
  final Alignment alignment;
  static const double backButtonLeadingWidth = 130;
  static const double toolbarHeight = 64;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: title != null ? Text(title!) : null,
        titleTextStyle: context.textTheme.displaySmall,
        leading: onBackbuttonPress != null
            ? ThemedBackButton(
                onBackbuttonPress: onBackbuttonPress,
              )
            : const SizedBox.shrink(),
        leadingWidth: onBackbuttonPress != null ? backButtonLeadingWidth : null,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(RadiusSize.medium.value),
            topRight: Radius.circular(RadiusSize.medium.value),
          ),
        ),
        backgroundColor: context.colorScheme.surface,
        foregroundColor: context.colorScheme.primary,
        toolbarHeight: toolbarHeight,
        scrolledUnderElevation: 2,
        elevation: 0.5,
        actions: [
          ThemedCloseButton(
            onPressed: () => Navigator.pop(context),
            color: context.colorScheme.primary,
          ),
        ],
      ),
      backgroundColor: Colors.transparent,
      body: Align(
        alignment: alignment,
        child: content,
      ),
    );
  }
}

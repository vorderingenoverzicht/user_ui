import 'package:flutter/material.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/theme/theme_constants.dart';

class ResponsiveDialog extends StatelessWidget {
  const ResponsiveDialog({super.key, required this.child});

  final Widget child;
  @override
  Widget build(BuildContext context) {
    return ResponsiveCenter(
      xs: 12,
      lg: 10,
      xl: 8,
      child: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: Dialog(
          insetPadding: const EdgeInsets.symmetric(horizontal: 0, vertical: 24),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(RadiusSize.medium.value),
          ),
          child: child,
        ),
      ),
    );
  }
}

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL
import 'package:flutter/material.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';

class ThemedCard extends StatelessWidget {
  const ThemedCard({
    super.key,
    this.child,
    this.width = double.infinity,
  });

  final Widget? child;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      decoration: BoxDecoration(
        color: context.colorScheme.surface,
        borderRadius: BorderRadius.circular(RadiusSize.large.value),
        boxShadow: [
          BoxShadow(
            color: context.colorScheme.onSurface.withOpacity(0.3),
            offset: const Offset(0, 1),
            blurRadius: RadiusSize.xlarge.value,
          )
        ],
      ),
      child: child,
    );
  }
}

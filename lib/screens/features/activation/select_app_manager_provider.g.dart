// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'select_app_manager_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$selectAppManagerHash() => r'01545b1390e66897ef5574aaab1d9b94b718e7ef';

/// See also [selectAppManager].
@ProviderFor(selectAppManager)
final selectAppManagerProvider = AutoDisposeFutureProvider<void>.internal(
  selectAppManager,
  name: r'selectAppManagerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$selectAppManagerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef SelectAppManagerRef = AutoDisposeFutureProviderRef<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

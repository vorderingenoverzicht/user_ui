import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/common/basic_scaffold.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/features/clean_up/remove_all_data_and_user_settings_screen.dart';
import 'package:user_ui/theme/responsive_grid.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class DeactivationScreen extends StatelessWidget {
  static const routeName = 'DeactivationScreen';

  const DeactivationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BasicScaffold(
      title: "Deactiveer de app",
      showDrawer: true,
      content: Padding(
        padding: EdgeInsets.only(bottom: Spacing.large.value),
        child: Column(
          children: [
            _Body(),
            if (responsiveValue(context, xs: true, md: false)) ...[
              Spacer(),
            ] else ...[
              addVerticalSpace(Spacing.xxlarge.value)
            ],
            _Footer(),
          ],
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body();

  @override
  Widget build(BuildContext context) {
    return ResponsiveCenter(
      xs: 12,
      md: 10,
      lg: 8,
      xl: 6,
      child: Text(
        'Gegevens gaan niet verloren als je ze wist in deze app. Je kunt altijd opnieuw gegevens opvragen bij de organisaties.',
        style: context.textTheme.bodyLarge,
      ),
    );
  }
}

class _Footer extends StatelessWidget {
  const _Footer();

  @override
  Widget build(BuildContext context) {
    return ResponsiveCenter(
      xs: 12,
      md: 10,
      lg: 8,
      xl: 6,
      child: Column(
        children: [
          ThemedButton(
            buttonText: "Deactiveer app",
            style: ThemedButtonStyle.primary,
            onPress: () {
              context.goNamed(RemoveAllDataAndUserSettingsScreen.routeName);
            },
          ),
        ],
      ),
    );
  }
}

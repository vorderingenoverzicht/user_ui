// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/screens/common/basic_scaffold.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/common/themed_loading_indicator.dart';
import 'package:user_ui/screens/features/activation/select_app_manager_provider.dart';
import 'package:user_ui/screens/features/activation/start_activation_screen.dart';
import 'package:user_ui/screens/features/error/themed_error_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/screens/features/onboarding/data_use_screen.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';
import 'package:user_ui/usecases/scheme/update_local_scheme.dart';

class ActivationInfoScreen extends ConsumerWidget {
  static const routeName = 'ActivationInfoScreen';
  static const String activationInfoActivateButtonKey =
      'activationInfoActivateButton';
  static const String activationInfoNoDigidButtonKey =
      'activationInfoNoDigidButton';
  const ActivationInfoScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final schemeUseCase = ref.watch(updateLocalSchemeProvider);

    return schemeUseCase.when(
      data: (data) => BasicScaffold(
        onBackbuttonPress: () {
          if (context.canPop()) {
            context.pop();
          } else {
            context.goNamed(DataUseScreen.routeName);
          }
        },
        content: Align(
          alignment: Alignment.topCenter,
          child: Scrollbar(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  addVerticalSpace(Spacing.large.value),
                  const _Body(),
                  const _Footer(
                    activationInfoActivateButtonKey,
                    activationInfoNoDigidButtonKey,
                  ),
                  addVerticalSpace(Spacing.large.value),
                ],
              ),
            ),
          ),
        ),
      ),
      error: (error, stackTrace) {
        context.goNamed(
          ThemedErrorScreen.routeName,
          pathParameters: {
            'isCritical': 'false',
            'redirectRoute': HomeScreen.routeName,
          },
        );
        return const SizedBox.shrink();
      },
      loading: () => ThemedLoadingIndicator(
          onBackbuttonPress: () => context.goNamed(DataUseScreen.routeName),
          text: "Bezig met het voorbereiden van de app..."),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body();

  static const double imageAspectRatio = 4 / 3;

  @override
  Widget build(BuildContext context) {
    return ResponsiveCenter(
      xs: 12,
      md: 10,
      lg: 8,
      xl: 6,
      alignment: Alignment.topCenter,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              "Activeer met DigiD",
              style: context.textTheme.displayLarge,
            ),
          ),
          addVerticalSpace(Spacing.xlarge.value),
          Text(
            "Activeer de app door eenmalig in te loggen met uw DigiD",
            style: Theme.of(context)
                .textTheme
                .titleLarge
                ?.copyWith(color: context.colorScheme.onSurface),
          ),
          addVerticalSpace(Spacing.large.value),
          Text(
            "Uw pincode van DigiD is een andere pincode dan uw pincode voor de app.",
            style: Theme.of(context)
                .textTheme
                .titleLarge
                ?.copyWith(color: context.colorScheme.onSurface),
          ),
          addVerticalSpace(Spacing.large.value),
          Text(
            "Na activatie staan uw naam, geboortedatum en burgerservice-nummer in de app.",
            style: Theme.of(context)
                .textTheme
                .titleLarge
                ?.copyWith(color: context.colorScheme.onSurface),
          ),
          addVerticalSpace(Spacing.large.value),
          AspectRatio(
            aspectRatio: imageAspectRatio,
            child: SvgPicture.asset(
              "assets/images/digid.svg",
              semanticsLabel: 'digid login image',
              fit: BoxFit.fitWidth,
              clipBehavior: Clip.hardEdge,
            ),
          ),
          addVerticalSpace(Spacing.xlarge.value),
        ],
      ),
    );
  }
}

class _Footer extends ConsumerWidget {
  const _Footer(this.activationInfoActivateButtonKey,
      this.activationInfoNoDigidButtonKey);
  final String activationInfoActivateButtonKey;
  final String activationInfoNoDigidButtonKey;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final asyncValue = ref.watch(selectAppManagerProvider);
    final loggingHelper = ref.read(loggingProvider);
    return ResponsiveCenter(
      xs: 12,
      md: 8,
      lg: 4,
      child: Column(
        children: [
          ThemedButton(
            key: Key(activationInfoActivateButtonKey),
            buttonText: "Activeer met DigiD",
            buttonIcon: SvgPicture.asset(
              width: IconSize.large.value,
              height: IconSize.large.value,
              "assets/images/digid_icon.svg",
              semanticsLabel: 'digid icon',
            ),
            style: ThemedButtonStyle.primary,
            onPress: () {
              asyncValue.when(
                skipLoadingOnRefresh: true,
                data: (_) {
                  context.goNamed(StartActivationScreen.routeName);
                },
                error: (error, stackTrace) => context.goNamed(
                  ThemedErrorScreen.routeName,
                  pathParameters: {
                    'isCritical': 'false',
                    'redirectRoute': ActivationInfoScreen.routeName,
                  },
                ),
                loading: () => {
                  debugPrint('activation info Loading'),
                },
              );
            },
          ),
          addVerticalSpace(Spacing.large.value),
          ThemedButton(
            key: Key(activationInfoNoDigidButtonKey),
            style: ThemedButtonStyle.tertiary,
            onPress: () async {
              final Uri uri = Uri.https('digid.nl', '/aanvragen');

              if (await canLaunchUrl(uri)) {
                launchUrl(uri);
              } else {
                loggingHelper.addLog(
                    DeviceEvent.uu_105, 'Failed to launch url ${uri.path}');
              }
            },
            buttonText: 'Ik heb geen DigiD',
          ),
        ],
      ),
    );
  }
}

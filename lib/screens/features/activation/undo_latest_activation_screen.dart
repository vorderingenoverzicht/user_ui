// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/common/themed_loading_indicator.dart';
import 'package:user_ui/screens/features/activation/activation_info_screen.dart';
import 'package:user_ui/usecases/registration/undo_latest_registration.dart';

class UndoLatestActivationScreen extends ConsumerWidget {
  static const routeName = 'UndoLatestActivationScreen';

  const UndoLatestActivationScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ref.watch(undoLatestRegistrationProvider).when(
          data: (_) {
            context.goNamed(ActivationInfoScreen.routeName);
            return const SizedBox.shrink();
          },
          error: (err, stack) => Text('Error: $err'),
          loading: () => ThemedLoadingIndicator(
            onBackbuttonPress: () =>
                context.goNamed(ActivationInfoScreen.routeName),
            text: "Laatste activatie ongedaan maken...",
            addDeepLink: true,
          ),
        );
  }
}

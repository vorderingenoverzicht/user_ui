// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/common/themed_loading_indicator.dart';
import 'package:user_ui/screens/features/activation/activation_info_screen.dart';
import 'package:user_ui/usecases/registration/start_reactivation.dart';

class ReactivationScreen extends ConsumerWidget {
  static const routeName = 'ReactivationScreen';

  const ReactivationScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ref.watch(startReactivationProvider).when(
          data: (_) {
            context.goNamed(ActivationInfoScreen.routeName);
            return const SizedBox.shrink();
          },
          error: (err, stack) {
            return Text('Error: $err');
          },
          loading: () => const ThemedLoadingIndicator(
            showDrawer: true,
            text: "Bezig met heractiveren van de app...",
          ),
        );
  }
}

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/common/themed_loading_indicator.dart';
import 'package:user_ui/screens/features/app_startup/startup_screen.dart';
import 'package:user_ui/screens/features/error/themed_error_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/usecases/app/remove_all_data_and_user_settings.dart';

class RemoveAllDataAndUserSettingsScreen extends ConsumerWidget {
  static const routeName = 'RemoveAllDataAndUserSettingsScreen';

  const RemoveAllDataAndUserSettingsScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ref.watch(removeAllDataAndUserSettingsProvider).when(
          data: (_) {
            context.goNamed(StartupScreen.routeName);
            return const SizedBox.shrink();
          },
          error: (err, stack) {
            context.goNamed(
              ThemedErrorScreen.routeName,
              pathParameters: {
                'isCritical': 'false',
                'redirectRoute': HomeScreen.routeName,
              },
            );
            return const SizedBox.shrink();
          },
          loading: () => ThemedLoadingIndicator(
            onBackbuttonPress: () => context.goNamed(StartupScreen.routeName),
            text: "App leegmaken...",
            addDeepLink: true,
          ),
        );
  }
}

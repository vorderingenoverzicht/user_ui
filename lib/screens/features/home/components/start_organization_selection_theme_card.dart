import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/common/themed_card.dart';
import 'package:user_ui/screens/features/organization_selection/organization_selection_screen.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class StartOrganizationSelectionThemeCard extends StatelessWidget {
  const StartOrganizationSelectionThemeCard({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ThemedCard(
      child: Padding(
        padding: EdgeInsets.all(Spacing.large.value),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Stel uw eigen overzicht van openstaande rekeningen bij publieke organisaties samen (bijvoorbeeld belastingen of een verkeersboete).',
              style: context.textTheme.bodyLarge,
            ),
            addVerticalSpace(Spacing.medium.value),
            ThemedButton(
              buttonText: 'Vraag uw gegevens op',
              style: ThemedButtonStyle.primary,
              onPress: () => context.goNamed(
                OrganizationSelectionScreen.routeName,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

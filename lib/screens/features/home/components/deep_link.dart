// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:app_links/app_links.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/screens/features/activation/complete_activation_screen.dart';

class DeepLink extends ConsumerStatefulWidget {
  const DeepLink({super.key});

  @override
  DeepLinkState createState() => DeepLinkState();
}

class DeepLinkState extends ConsumerState<DeepLink> {
  bool _initialURILinkHandled = false;
  StreamSubscription? _streamSubscription;
  final appLinks = AppLinks();

  @override
  void initState() {
    super.initState();

    _initURIHandler();
    _incomingLinkHandler();
  }

  @override
  Widget build(BuildContext context) {
    return const SizedBox.shrink();
  }

  Future<void> _initURIHandler() async {
    final loggingHelper = ref.read(loggingProvider);

    if (!_initialURILinkHandled) {
      _initialURILinkHandled = true;
      try {
        final initialURI = await appLinks.getInitialLinkString();
        // Use the initialURI and warn the user if it is not correct,
        // but keep in mind it could be `null`.
        if (initialURI != null) {
          debugPrint("Initial URI received $initialURI");
          if (!mounted) {
            return;
          }
        } else {
          debugPrint("Null Initial URI received");
        }
      } on PlatformException catch (error) {
        // Platform messages may fail, so we use a try/catch PlatformException.
        // Handle exception by warning the user their action did not succeed
        debugPrint("Failed to receive initial uri");
        loggingHelper.addLog(DeviceEvent.uu_91,
            'Failed to receive initial uri ${error.toString()}');
      } on FormatException catch (error) {
        debugPrint('Malformed Initial URI received');
        loggingHelper.addLog(DeviceEvent.uu_92,
            'Malformed Initial URI received ${error.toString()}');
        if (!mounted) {
          return;
        }
      }
    }
  }

  /// Handle incoming links - the ones that the app will receive from the OS
  /// while already started.
  void _incomingLinkHandler() {
    if (!kIsWeb) {
      // It will handle app links while the app is already started - be it in
      // the foreground or in the background.
      _streamSubscription = appLinks.uriLinkStream.listen((Uri? uri) {
        if (!mounted) {
          return;
        }
        debugPrint('Received URI: $uri');
        context.goNamed(CompleteActivationScreen.routeName);
      }, onError: (Object error) {
        final loggingHelper = ref.read(loggingProvider);
        debugPrint('Error occurred: $error');
        loggingHelper.addLog(
            DeviceEvent.uu_93, 'Error occured ${error.toString()}');
        if (!mounted) {
          return;
        }
      });
    }
  }

  @override
  void dispose() {
    _streamSubscription?.cancel();
    super.dispose();
  }
}

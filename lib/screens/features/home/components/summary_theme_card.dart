import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/common/themed_card.dart';
import 'package:user_ui/screens/features/loading/loading_screen.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class SummaryThemeCard extends StatelessWidget {
  const SummaryThemeCard({
    required this.numberOfSelectedOrganizations,
    required this.numberOfCompletedFinancialClaimsInformationRequests,
    required this.numberOfFinancialClaimsInformationStorages,
    super.key,
  });

  final int numberOfSelectedOrganizations;
  final int numberOfCompletedFinancialClaimsInformationRequests;
  final int numberOfFinancialClaimsInformationStorages;

  @override
  Widget build(BuildContext context) {
    return ThemedCard(
      child: Padding(
        padding: EdgeInsets.all(Spacing.large.value),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            addVerticalSpace(Spacing.large.value),
            Text(
              'Vorderingenoverzicht',
              style: context.textTheme.displayMedium,
            ),
            addVerticalSpace(Spacing.large.value),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  width: IconSize.xlarge.value,
                  height: IconSize.xlarge.value,
                  "assets/icons/selected.svg",
                ),
                addHorizontalSpace(Spacing.large.value),
                Flexible(
                  child: Text(
                    numberOfSelectedOrganizations == 1
                        ? '1 organisatie geselecteerd'
                        : '$numberOfSelectedOrganizations organisaties geselecteerd',
                    style: context.textTheme.bodyLarge,
                  ),
                ),
              ],
            ),
            if (numberOfSelectedOrganizations > 0)
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    width: IconSize.xlarge.value,
                    height: IconSize.xlarge.value,
                    "assets/icons/selected.svg",
                  ),
                  addHorizontalSpace(Spacing.large.value),
                  Flexible(
                    child: Text(
                      numberOfCompletedFinancialClaimsInformationRequests ==
                              numberOfSelectedOrganizations
                          ? (numberOfSelectedOrganizations == 1
                              ? 'De organisatie heeft gereageerd'
                              : 'Alle organisaties hebben gereageerd')
                          : '$numberOfCompletedFinancialClaimsInformationRequests van $numberOfSelectedOrganizations hebben gereageerd',
                      style: context.textTheme.bodyLarge,
                    ),
                  ),
                ],
              ),
            addVerticalSpace(Spacing.large.value),
            ResponsiveCenter(
              xs: 12,
              md: 8,
              child: ThemedButton(
                buttonText: 'Bekijk overzicht',
                style: ThemedButtonStyle.primary,
                onPress: numberOfFinancialClaimsInformationStorages > 0
                    ? () =>
                        context.goNamed(FinancialClaimsOverviewScreen.routeName)
                    : () => context.goNamed(LoadingScreen.routeName),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/common/basic_scaffold.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/features/activation/activation_info_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/screens/features/onboarding/components/data_use_text_row.dart';
import 'package:user_ui/screens/features/onboarding/propositon_screen.dart';
import 'package:user_ui/screens/features/pincode/pincode_screen.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class DataUseScreen extends StatelessWidget {
  static const routeName = 'DataUseScreen';
  static const String dataUseNextButtonKey = 'dataUseNextButton';

  const DataUseScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BasicScaffold(
      onBackbuttonPress: () => context.goNamed(PropositionScreen.routeName),
      content: Align(
        alignment: Alignment.topCenter,
        child: Scrollbar(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                addVerticalSpace(Spacing.large.value),
                const _Body(),
                const _Footer(dataUseNextButtonKey),
                addVerticalSpace(Spacing.large.value),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body();

  @override
  Widget build(BuildContext context) {
    return ResponsiveCenter(
      xs: 12,
      md: 10,
      lg: 8,
      xl: 6,
      alignment: Alignment.topCenter,
      child: Column(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              "Uw gegevens in de app",
              style: context.textTheme.displayLarge,
            ),
          ),
          addVerticalSpace(Spacing.xlarge.value),
          DataUseTextRow(
            text: TextSpan(
              style: context.textTheme.bodyLarge,
              children: const <TextSpan>[
                TextSpan(text: 'Uw gegevens worden versleuteld en beveiligd '),
              ],
            ),
          ),
          addVerticalSpace(Spacing.xlarge.value),
          DataUseTextRow(
            text: TextSpan(
              style: context.textTheme.bodyLarge,
              children: const <TextSpan>[
                TextSpan(text: 'Uw gegevens zijn alleen door u te zien'),
              ],
            ),
          ),
          addVerticalSpace(Spacing.xlarge.value),
          DataUseTextRow(
            text: TextSpan(
              style: context.textTheme.bodyLarge,
              children: const [
                TextSpan(text: 'U kiest zelf welke gegevens u bekijkt'),
              ],
            ),
          ),
          addVerticalSpace(Spacing.xlarge.value),
          DataUseTextRow(
            text: TextSpan(
              style: context.textTheme.bodyLarge,
              children: const <TextSpan>[
                TextSpan(text: 'U kunt het overzicht altijd weer weggooien'),
              ],
            ),
          ),
          addVerticalSpace(Spacing.xlarge.value),
        ],
      ),
    );
  }
}

class _Footer extends ConsumerWidget {
  const _Footer(this.dataUseNextButtonKey);
  final String dataUseNextButtonKey;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ResponsiveCenter(
      xs: 12,
      md: 8,
      lg: 4,
      child: ThemedButton(
        key: Key(dataUseNextButtonKey),
        buttonText: "Volgende",
        style: ThemedButtonStyle.primary,
        onPress: () {
          if (ref
              .read(userSettingsRepositoryProvider)
              .hasCompletedRegistration) {
            context.goNamed(HomeScreen.routeName);
          } else if (ref.read(userSettingsRepositoryProvider).pincode.isEmpty) {
            context.goNamed(PincodeScreen.routeName);
          } else {
            context.goNamed(ActivationInfoScreen.routeName);
          }
        },
      ),
    );
  }
}

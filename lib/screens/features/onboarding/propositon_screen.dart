// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/common/basic_scaffold.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/features/onboarding/components/proposition_text_row.dart';
import 'package:user_ui/screens/features/onboarding/data_use_screen.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class PropositionScreen extends StatelessWidget {
  static const routeName = 'PropositionScreen';
  static const String propositionNextButtonKey = 'propositionNextButton';

  const PropositionScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BasicScaffold(
      content: Align(
        alignment: Alignment.topCenter,
        child: Scrollbar(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                addVerticalSpace(Spacing.large.value),
                const _Body(),
                const _Footer(propositionNextButtonKey),
                addVerticalSpace(Spacing.large.value),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body();

  static const double imageAspectRatio = 4 / 3;

  @override
  Widget build(BuildContext context) {
    return ResponsiveCenter(
      xs: 12,
      md: 10,
      lg: 8,
      xl: 6,
      alignment: Alignment.topCenter,
      child: Column(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              "Wat heeft u aan de app?",
              style: context.textTheme.displayLarge,
            ),
          ),
          addVerticalSpace(Spacing.xlarge.value),
          PropositionTextRow(
            text: TextSpan(
              style: context.textTheme.bodyLarge,
              children: const [
                TextSpan(
                    text:
                        "Één overzicht van wat u nog moet betalen aan de overheid, bijvoorbeeld aan de Belastingdienst of het CJIB"),
              ],
            ),
          ),
          addVerticalSpace(Spacing.xlarge.value),
          PropositionTextRow(
            text: TextSpan(
              style: context.textTheme.bodyLarge,
              children: const <TextSpan>[
                TextSpan(text: 'Hulp bij vragen over wat u moet betalen'),
              ],
            ),
          ),
          addVerticalSpace(Spacing.xlarge.value),
          AspectRatio(
            aspectRatio: imageAspectRatio,
            child: SvgPicture.asset(
              "assets/images/video_placeholder_cropped.svg",
              semanticsLabel: 'video placeholder',
              fit: BoxFit.fitWidth,
              clipBehavior: Clip.hardEdge,
            ),
          ),
          addVerticalSpace(Spacing.xlarge.value),
        ],
      ),
    );
  }
}

class _Footer extends StatelessWidget {
  const _Footer(this.propositionNextButtonKey);
  final String propositionNextButtonKey;

  @override
  Widget build(BuildContext context) {
    return ResponsiveCenter(
      xs: 12,
      md: 8,
      lg: 4,
      child: ThemedButton(
        key: Key(propositionNextButtonKey),
        buttonText: "Volgende",
        style: ThemedButtonStyle.primary,
        onPress: () => context.goNamed(DataUseScreen.routeName),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/repositories/logging/event_severity_level.dart';
import 'package:user_ui/repositories/logging/logs_sorted_by_time_stamp_notifier.dart';
import 'package:user_ui/screens/common/basic_scaffold.dart';
import 'package:user_ui/screens/common/themed_chip_button.dart';
import 'package:user_ui/screens/common/themed_loading_indicator.dart';
import 'package:user_ui/screens/features/error/themed_error_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/theme/severity_extension.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class ErrorLogScreen extends ConsumerWidget {
  const ErrorLogScreen({required this.showBackButton, super.key});
  final bool showBackButton;
  static const routeName = 'ErrorLogScreen';

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final asyncValue = ref.watch(logsSortedByTimeStampProvider);

    return asyncValue.when(
      data: (logs) => BasicScaffold(
        title: 'Foutenlogboek',
        showDrawer: !showBackButton,
        onBackbuttonPress: showBackButton ? () => context.pop() : null,
        content: Column(
          children: [
            Wrap(
              spacing: Spacing.medium.value,
              runSpacing: Spacing.medium.value,
              children: EventSeverityLevel.values.map((severity) {
                return ThemedChipButton(
                  color: severity.toInt().getSeverityColor(context),
                  severity: severity,
                  isSelected: ref
                      .read(logsSortedByTimeStampProvider.notifier)
                      .filterEnabled(severity),
                  onToggle: (EventSeverityLevel selectedSeverity) {
                    ref
                        .read(logsSortedByTimeStampProvider.notifier)
                        .toggleFilter(selectedSeverity);
                  },
                );
              }).toList(),
            ),
            addVerticalSpace(Spacing.medium.value),
            Expanded(
              child: ListView.builder(
                itemCount: logs.length,
                itemBuilder: (context, i) {
                  return ListTile(
                    title: Row(
                      children: [
                        Container(
                          width: Spacing.small.value,
                          height: context.textTheme.bodyLarge?.fontSize,
                          color: logs[i].severity.getSeverityColor(context),
                        ),
                        addHorizontalSpace(Spacing.medium.value),
                        Flexible(
                          child: Text(
                              '${logs[i].timestamp.toIso8601String()} - ${logs[i].deviceEventClassId.toUpperCase()} ${logs[i].name}'),
                        ),
                      ],
                    ),
                    titleTextStyle: context.textTheme.bodyLarge,
                    subtitle: Text(logs[i].flexString1),
                    subtitleTextStyle: context.textTheme.bodyMedium,
                    tileColor: i % 2 == 0
                        ? context.colorScheme.surfaceContainerLowest
                        : context.colorScheme.surface,
                  );
                },
              ),
            ),
          ],
        ),
      ),
      error: (error, stackTrace) {
        context.goNamed(
          ThemedErrorScreen.routeName,
          pathParameters: {
            'isCritical': 'false',
            'redirectRoute': HomeScreen.routeName,
          },
        );
        return const SizedBox.shrink();
      },
      loading: () => ThemedLoadingIndicator(
          onBackbuttonPress: () => context.goNamed(HomeScreen.routeName),
          text: "Bezig met het ophalen van de logs..."),
    );
  }
}

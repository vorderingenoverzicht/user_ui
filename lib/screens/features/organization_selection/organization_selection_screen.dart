// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/repositories/organization_selection/organization_selection_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/common/basic_dialog_scaffold.dart';
import 'package:user_ui/screens/common/basic_scaffold.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/screens/common/themed_loading_indicator.dart';
import 'package:user_ui/screens/features/error/themed_error_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/screens/features/organization_selection/components/organization_list_view.dart';
import 'package:user_ui/screens/features/organization_selection/components/organization_overview_buttons.dart';
import 'package:user_ui/screens/features/organization_selection/components/organization_selection_buttons.dart';
import 'package:user_ui/screens/features/organization_selection/components/organization_selection_tip.dart';
import 'package:user_ui/screens/features/organization_selection/components/organization_status_buttons.dart';
import 'package:user_ui/screens/features/organization_selection/organization_selection_screen_state.dart';
import 'package:user_ui/screens/providers/number_of_selected_organizations_state_notifier.dart';
import 'package:user_ui/theme/responsive_grid.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';
import 'package:user_ui/usecases/organization_selection_usecase.dart';
import 'package:user_ui/usecases/scheme/update_local_scheme.dart';

class OrganizationSelectionScreen extends ConsumerWidget {
  static const routeName = 'AutoSelectAllOrganizationsScreen';

  const OrganizationSelectionScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final bool hasCompletedOrganizationSelection = ref
        .read(userSettingsRepositoryProvider)
        .hasCompletedOrganizationSelection;

    final schemeUseCase = ref.watch(updateLocalSchemeProvider);

    return schemeUseCase.when(
      data: (_) {
        if (ref.read(userSettingsRepositoryProvider).delay) {
          Future.delayed(const Duration(milliseconds: 2000));
        }

        final organizationSelectionUsecase =
            ref.read(organizationSelectionUsecaseProvider);

        organizationSelectionUsecase.syncSelectedOrganizationsWithScheme();

        return hasCompletedOrganizationSelection
            ? const _OrganizationSelectionDialogScaffold()
            : const _OrganizationSelectionScreenScaffold();
      },
      error: ((error, stackTrace) {
        context.goNamed(
          ThemedErrorScreen.routeName,
          pathParameters: {
            'isCritical': 'false',
            'redirectRoute': HomeScreen.routeName,
          },
        );
        return const SizedBox.shrink();
      }),
      loading: () => ThemedLoadingIndicator(
        onBackbuttonPress: hasCompletedOrganizationSelection
            ? null
            : () => context.goNamed(HomeScreen.routeName),
        text: "Uw gegevens worden opgehaald...",
        isDialog: hasCompletedOrganizationSelection,
      ),
    );
  }
}

class _OrganizationSelectionScreenScaffold extends StatelessWidget {
  const _OrganizationSelectionScreenScaffold();

  @override
  Widget build(BuildContext context) {
    return BasicScaffold(
      onBackbuttonPress: () => context.goNamed(HomeScreen.routeName),
      content: const Align(
        alignment: Alignment.topCenter,
        child: SingleChildScrollView(
          child: Column(
            children: [
              ResponsiveCenter(
                xs: 12,
                md: 10,
                lg: 8,
                xl: 6,
                child: _Body(),
              ),
              ResponsiveCenter(
                xs: 12,
                md: 8,
                lg: 4,
                child: _Footer(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _OrganizationSelectionDialogScaffold extends StatelessWidget {
  const _OrganizationSelectionDialogScaffold();

  @override
  Widget build(BuildContext context) {
    return BasicDialogScaffold(
      alignment: Alignment.topCenter,
      content: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            addVerticalSpace(Spacing.medium.value),
            ResponsiveCenter(
              xs: 12,
              md: 10,
              lg: 8,
              xl: 6,
              margin: responsiveValue(context, xs: Spacing.large.value, md: 0),
              segments: responsiveValue(context, xs: 12, lg: 10, xl: 8),
              child: const _Body(),
            ),
            ResponsiveCenter(
              xs: 12,
              md: 10,
              lg: 8,
              xl: 6,
              margin: responsiveValue(context, xs: Spacing.large.value, sm: 0),
              segments: responsiveValue(context, xs: 12, lg: 10, xl: 8),
              child: const _Footer(),
            ),
          ],
        ),
      ),
    );
  }
}

class _Body extends ConsumerWidget {
  const _Body();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var numberOfSelectedOrganizations =
        ref.watch(numberOfSelectedOrganizationsProvider);

    var numberOfAvailableOrganizations =
        ref.watch(organizationSelectionRepositoryProvider).length;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Welke organisaties wilt u in uw overzicht?",
          style: context.textTheme.displayLarge,
        ),
        if (ref
            .read(userSettingsRepositoryProvider)
            .hasCompletedOrganizationSelection) ...[
          addVerticalSpace(Spacing.large.value),
          Text(
            '$numberOfSelectedOrganizations van de $numberOfAvailableOrganizations organisaties ${numberOfSelectedOrganizations == 1 ? 'is' : 'zijn'} geselecteerd',
            style: context.textTheme.titleMedium,
          ),
        ],
        addVerticalSpace(Spacing.large.value),
        const OrganizationSelectionTip(),
        addVerticalSpace(Spacing.large.value),
        const OrganizationList(),
      ],
    );
  }
}

class _Footer extends ConsumerWidget {
  const _Footer();

  Widget getButtons(OrganizationSelectionScreenState state) {
    switch (state) {
      case OrganizationSelectionScreenState.overview:
        return OrganizationOverviewButtons(state);
      case OrganizationSelectionScreenState.status:
        return const OrganizationStatusButtons();
      case OrganizationSelectionScreenState.selection:
        return const OrganizationSelectionButtons();
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final screenState =
        ref.watch(organizationSelectionScreenStateNotifierProvider);
    return screenState.when(
      data: (state) => Align(
        alignment: Alignment.bottomCenter,
        child: getButtons(state),
      ),
      error: (error, stackTrace) {
        context.goNamed(
          ThemedErrorScreen.routeName,
          pathParameters: {
            'isCritical': 'false',
            'redirectRoute': HomeScreen.routeName,
          },
        );
        return const SizedBox.shrink();
      },
      loading: () => const Text('Loading'),
    );
  }
}

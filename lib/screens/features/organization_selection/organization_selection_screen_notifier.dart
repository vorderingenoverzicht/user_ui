// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:fcid_library/fcid_library.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/organization_selection/organization_selection_repository.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_organization_repository.dart';

part 'organization_selection_screen_notifier.g.dart';

class OrganizationSelectionViewModel {
  OrganizationSelectionViewModel({
    required this.oin,
    required this.name,
    required this.selected,
    required this.available,
  });

  final String oin;
  final String name;
  final bool selected;
  final bool available;
}

class OrganizationSelectionScreenViewModel {
  OrganizationSelectionScreenViewModel({
    required this.organizationSelectionViewModels,
    required this.hasSelectedOrganizations,
    required this.hasSelectedAllOrganizations,
  });

  final List<OrganizationSelectionViewModel> organizationSelectionViewModels;
  final bool hasSelectedOrganizations;
  final bool hasSelectedAllOrganizations;
}

@riverpod
class OrganizationSelectionScreenNotifier
    extends _$OrganizationSelectionScreenNotifier {
  @override
  Future<OrganizationSelectionScreenViewModel> build() async {
    List<OrganizationSelectionViewModel> organizationSelectionViewModels = [];

    final organizations = ref.watch(schemeOrganizationRepositoryProvider);

    final organizationSelections =
        ref.watch(organizationSelectionRepositoryProvider);

    for (final organizationSelection in organizationSelections) {
      final organization = organizations
          .where(
              (SchemeOrganization org) => org.oin == organizationSelection.oin)
          .first;

      if (organization.available ||
          !organization.available && organizationSelection.selected) {
        organizationSelectionViewModels.add(
          OrganizationSelectionViewModel(
            oin: organization.oin,
            name: organization.name,
            available: organization.available,
            selected: organizationSelection.selected,
          ),
        );
      }
    }

    var hasSelectedOrganizations =
        organizationSelections.where((o) => o.selected).isNotEmpty;

    var hasSelectedAllOrganizations =
        organizationSelections.every((e) => e.selected);

    organizationSelectionViewModels.sort(((a, b) => a.name.compareTo(b.name)));

    return OrganizationSelectionScreenViewModel(
      organizationSelectionViewModels: organizationSelectionViewModels,
      hasSelectedOrganizations: hasSelectedOrganizations,
      hasSelectedAllOrganizations: hasSelectedAllOrganizations,
    );
  }
}

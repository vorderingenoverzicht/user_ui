import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/organization_information/organization_information_notifier.dart';
import 'package:user_ui/screens/common/basic_dialog_scaffold.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/theme/responsive_grid.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class OrganizationInformationDialog extends ConsumerWidget {
  const OrganizationInformationDialog({
    super.key,
    required this.oin,
  });
  final String oin;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final organizationInformation =
        ref.watch(getOrganizationInformationProvider(oin: oin));

    return BasicDialogScaffold(
      alignment: Alignment.topCenter,
      content: organizationInformation.when(
        data: (information) => SingleChildScrollView(
          child: ResponsiveCenter(
            xs: 12,
            md: 10,
            lg: 8,
            xl: 6,
            margin: responsiveValue(context, xs: Spacing.large.value, md: 0),
            segments: responsiveValue(context, xs: 12, lg: 10, xl: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                addVerticalSpace(Spacing.large.value),
                Text(information.organization,
                    style: context.textTheme.displayLarge),
                addVerticalSpace(Spacing.large.value),
                Text(information.introText,
                    style: context.textTheme.titleLarge
                        ?.copyWith(color: context.colorScheme.onSurface)),
                addVerticalSpace(Spacing.large.value),
                Text(
                    'Vorderingen die u kunt opvragen bij ${information.organization}',
                    style: context.textTheme.titleLarge
                        ?.copyWith(color: context.colorScheme.onSurface)),
                addVerticalSpace(Spacing.medium.value),
                _ClaimsList(information.availableClaims),
                Text(
                    'Vorderingen die u nog niet kunt opvragen bij ${information.organization}',
                    style: context.textTheme.titleLarge
                        ?.copyWith(color: context.colorScheme.onSurface)),
                addVerticalSpace(Spacing.medium.value),
                _ClaimsList(information.nonAvailableClaims),
              ],
            ),
          ),
        ),
        error: (error, stackTrace) => Text('$error'),
        loading: () => const CircularProgressIndicator(),
      ),
    );
  }
}

class _ClaimsList extends StatelessWidget {
  const _ClaimsList(this.claims);
  final List<String> claims;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: Spacing.medium.value),
      child: Column(
        children: [
          for (final claim in claims) ...[
            Row(
              children: [
                CircleAvatar(
                  backgroundColor: context.colorScheme.tertiary,
                  radius: RadiusSize.small.value,
                ),
                addHorizontalSpace(Spacing.medium.value),
                Flexible(
                  child: Text(
                    claim,
                    style: context.textTheme.titleLarge,
                  ),
                ),
              ],
            ),
            addVerticalSpace(Spacing.medium.value),
          ],
        ],
      ),
    );
  }
}

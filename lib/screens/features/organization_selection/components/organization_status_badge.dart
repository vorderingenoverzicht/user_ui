import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/screens/features/organization_selection/components/status_not_responded_icon.dart';
import 'package:user_ui/screens/features/organization_selection/components/status_responded_icon.dart';
import 'package:user_ui/screens/features/organization_selection/organization_selection_screen_notifier.dart';

class OrganizationStatusBadge extends ConsumerWidget {
  const OrganizationStatusBadge(
      {super.key, required this.organizationSelection});

  final OrganizationSelectionViewModel organizationSelection;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final financialClaimsInformationRequests =
        ref.watch(financialClaimsInformationRequestRepositoryProvider);

    final lastDatetimeRequested =
        financialClaimsInformationRequests.last.dateTimeRequested;

    final organizationResponded = financialClaimsInformationRequests
        .where((request) =>
            request.oin == organizationSelection.oin &&
            request.dateTimeRequested == lastDatetimeRequested &&
            financialClaimsInformationRequestEndStatusList
                .contains(request.status))
        .isNotEmpty;

    return organizationSelection.selected
        ? organizationResponded
            ? const StatusRespondedIcon()
            : const StatusNotRespondedIcon()
        : const SizedBox.shrink();
  }
}

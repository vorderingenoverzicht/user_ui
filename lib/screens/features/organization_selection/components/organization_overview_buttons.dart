// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/repositories/organization_selection/organization_selection_repository.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/features/loading/loading_screen.dart';
import 'package:user_ui/screens/features/organization_selection/organization_selection_screen_state.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_spacing.dart';
import 'package:user_ui/usecases/organization_selection_usecase.dart';

class OrganizationOverviewButtons extends ConsumerStatefulWidget {
  const OrganizationOverviewButtons(this.state, {super.key});
  final OrganizationSelectionScreenState state;
  @override
  ConsumerState<OrganizationOverviewButtons> createState() =>
      ShowOrganizationButtonsState();
}

class ShowOrganizationButtonsState
    extends ConsumerState<OrganizationOverviewButtons> {
  @override
  Widget build(BuildContext context) {
    final organizationSelections =
        ref.watch(organizationSelectionRepositoryProvider);
    return Column(
      children: [
        addVerticalSpace(Spacing.large.value),
        ThemedButton(
          style: ThemedButtonStyle.primary,
          onPress: () => autoSelectAllOrganizations(
            onComplete: () {
              context.goNamed(LoadingScreen.routeName);
            },
          ),
          buttonText: organizationSelections.length > 1
              ? 'Vraag alle aangesloten organisaties'
              : 'Vraag aangesloten organisatie',
        ),
        addVerticalSpace(Spacing.large.value),
        ThemedButton(
          style: ThemedButtonStyle.quarternary,
          onPress: () => customSelectOrganizations(
            onComplete: () {
              ref
                  .read(
                      organizationSelectionScreenStateNotifierProvider.notifier)
                  .setState(OrganizationSelectionScreenState.selection);
            },
          ),
          buttonText: 'Ik wil zelf organisaties kiezen',
        ),
        addVerticalSpace(Spacing.medium.value),
      ],
    );
  }

  void customSelectOrganizations({required Function onComplete}) {
    ref
        .read(organizationSelectionUsecaseProvider)
        .setAutoSelectAllOrganizations(false)
        .then((_) => onComplete());
  }

  void autoSelectAllOrganizations({required Function onComplete}) {
    ref
        .read(organizationSelectionUsecaseProvider)
        .setAutoSelectAllOrganizations(true)
        .then((_) => ref
            .read(organizationSelectionUsecaseProvider)
            .selectAllOrganizations())
        .then((_) => onComplete());
  }
}

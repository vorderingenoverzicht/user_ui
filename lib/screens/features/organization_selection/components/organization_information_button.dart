import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:user_ui/screens/common/responsive_dialog.dart';
import 'package:user_ui/screens/features/organization_selection/components/information_button_icon.dart';
import 'package:user_ui/screens/features/organization_selection/components/organization_information_dialog.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';

class OrganizationInformationButton extends StatelessWidget {
  const OrganizationInformationButton({
    super.key,
    required this.oin,
  });
  final String oin;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        kIsWeb
            ? showDialog(
                context: context,
                useSafeArea: false,
                builder: (_) {
                  return ResponsiveDialog(
                    child: OrganizationInformationDialog(
                      oin: oin,
                    ),
                  );
                },
              )
            : showModalBottomSheet(
                context: context,
                isScrollControlled:
                    true, // Needed to make screen 90% of full screen height.
                clipBehavior: Clip
                    .antiAliasWithSaveLayer, // Needed to make borders round when isScrollControlled is true.
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(RadiusSize.medium.value),
                    topRight: Radius.circular(RadiusSize.medium.value),
                  ),
                ),
                backgroundColor: context.colorScheme.surface,
                builder: (_) {
                  return SizedBox(
                    height: MediaQuery.of(context).size.height * 0.9,
                    child: OrganizationInformationDialog(
                      oin: oin,
                    ),
                  );
                },
              );
      },
      child: const SizedBox.square(
        dimension: minTapSize,
        child: Align(
          alignment: Alignment.topRight,
          child: InformationButtonIcon(),
        ),
      ),
    );
  }
}

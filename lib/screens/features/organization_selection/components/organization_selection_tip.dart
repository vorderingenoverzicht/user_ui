// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extended_colors.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class OrganizationSelectionTip extends StatelessWidget {
  const OrganizationSelectionTip({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(RadiusSize.medium.value),
        ),
        color: ThemeExtendedColors.iconOrange,
      ),
      width: double.infinity,
      padding: EdgeInsets.all(Spacing.large.value),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(Spacing.small.value),
            decoration: BoxDecoration(
              color: ThemeExtendedColors.tipSurfaceContainer,
              borderRadius: BorderRadius.all(
                Radius.circular(RadiusSize.small.value),
              ),
            ),
            child: Text(
              'LET OP',
              textAlign: TextAlign.center,
              style: context.textTheme.labelLarge?.copyWith(
                color: context.colorScheme.onPrimary,
                fontWeight: FontWeight.w600,
                fontSize: 12,
              ),
            ),
          ),
          addVerticalSpace(Spacing.medium.value),
          Text(
            'Nog niet alle rekeningen en schulden worden getoond in uw overzicht. Bekijk per organisatie wat wél en niet beschikbaar is.',
            style: context.textTheme.bodyLarge,
          )
        ],
      ),
    );
  }
}

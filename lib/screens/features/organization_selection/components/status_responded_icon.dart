import 'package:flutter/material.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extended_colors.dart';
import 'package:user_ui/theme/theme_extensions.dart';

class StatusRespondedIcon extends StatelessWidget {
  const StatusRespondedIcon({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox.square(
      dimension: IconSize.medium.value,
      child: Center(
        child: CircleAvatar(
          backgroundColor: ThemeExtendedColors.iconGreen,
          child: Icon(
            Icons.check,
            color: context.colorScheme.surface,
            size: IconSize.small.value,
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/features/error/themed_error_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/screens/features/organization_selection/components/organization_list_tile.dart';
import 'package:user_ui/screens/features/organization_selection/organization_selection_screen_notifier.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class OrganizationList extends ConsumerWidget {
  const OrganizationList({super.key});
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final organizationSelectionScreenViewModelAsyncValue =
        ref.watch(organizationSelectionScreenNotifierProvider);
    return organizationSelectionScreenViewModelAsyncValue.when(
      data: (viewModel) {
        return Container(
          padding: EdgeInsets.symmetric(vertical: Spacing.medium.value),
          decoration: BoxDecoration(
            color: context.colorScheme.surfaceContainerLowest,
            borderRadius: BorderRadius.all(
              Radius.circular(RadiusSize.medium.value),
            ),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Spacing.large.value),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Momenteel aangesloten",
                  style: context.textTheme.headlineMedium,
                ),
                addVerticalSpace(Spacing.large.value),
                for (final organization
                    in viewModel.organizationSelectionViewModels) ...[
                  OrganizationListTile(
                    organizationSelection: organization,
                  ),
                  addVerticalSpace(Spacing.medium.value),
                ],
              ],
            ),
          ),
        );
      },
      skipLoadingOnReload: true,
      loading: () => const Center(child: CircularProgressIndicator()),
      error: (error, __) {
        context.goNamed(
          ThemedErrorScreen.routeName,
          pathParameters: {
            'isCritical': 'false',
            'redirectRoute': HomeScreen.routeName,
          },
        );
        return const SizedBox.shrink();
      },
    );
  }
}

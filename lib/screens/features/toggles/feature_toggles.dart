// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/common/basic_scaffold.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';

class FeatureToggleScreen extends ConsumerWidget {
  static const routeName = 'FeatureToggleScreen';

  const FeatureToggleScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final userSettings = ref.watch(userSettingsRepositoryProvider);

    return BasicScaffold(
      title: "Experimentals",
      onBackbuttonPress: () {
        if (context.canPop()) {
          context.pop();
        } else {
          context.goNamed(HomeScreen.routeName);
        }
      },
      content: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SwitchListTile(
              title: Text('Delay'),
              value: userSettings.delay,
              onChanged: (bool value) {
                ref
                    .read(userSettingsRepositoryProvider.notifier)
                    .setDelay(value);
              },
            ),
            SwitchListTile(
              title: Text('Betalingsregeling Rijk'),
              value: userSettings.betalingsregelingRijkFeature,
              onChanged: (bool value) {
                ref
                    .read(userSettingsRepositoryProvider.notifier)
                    .setBetalingsregelingRijkFeature(value);
              },
            ),
            SwitchListTile(
              title: Text('Development Overlay'),
              value: userSettings.showDevelopmentOverlay,
              onChanged: (bool value) {
                ref
                    .read(userSettingsRepositoryProvider.notifier)
                    .setShowDevelopmentOverlay(value);
              },
            ),
          ],
        ),
      ),
    );
  }
}

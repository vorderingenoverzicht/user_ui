import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/entities/pincode_error.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/common/basic_scaffold.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/screens/common/themed_progress_indicator.dart';
import 'package:user_ui/screens/features/activation/activation_info_screen.dart';
import 'package:user_ui/screens/features/error/themed_error_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/screens/features/onboarding/data_use_screen.dart';
import 'package:user_ui/screens/features/pincode/components/num_pad.dart';
import 'package:user_ui/screens/features/pincode/components/pincode.dart';
import 'package:user_ui/screens/features/pincode/components/pincode_warning.dart';
import 'package:user_ui/screens/features/pincode/forgot_pincode_screen.dart';
import 'package:user_ui/screens/features/pincode/pincode_notifier.dart';
import 'package:user_ui/screens/features/pincode/pincode_screen_state.dart';
import 'package:user_ui/theme/responsive_grid.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';
import 'package:user_ui/usecases/onboarding_usecase.dart';

// ignore: must_be_immutable
class PincodeScreen extends ConsumerStatefulWidget {
  static const routeName = 'PincodeScreen';
  final List<PincodeError> errors = [];
  String initialPincode = "";

  PincodeScreen({super.key});

  @override
  ConsumerState<PincodeScreen> createState() => _PincodeScreenState();
}

class _PincodeScreenState extends ConsumerState<PincodeScreen> {
  bool _isVerifying = false;

  final FocusNode _myFocusNode = FocusNode();

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    _myFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final screenStateProvider = ref.watch(pincodeScreenStateNotifierProvider);

    const String forgotPincodeButtonText = 'Pincode vergeten';

    return screenStateProvider.when(
      data: (state) {
        final pincode = ref.watch(pincodeNotifierProvider);

        if (pincode.length == 5) {
          _isVerifying = true;
          widget.errors.clear();

          Future.delayed(const Duration(milliseconds: 1000), () async {
            _isVerifying = false;
            switch (state) {
              case PincodeScreenState.choose:
                await validatePinSelection(pincode);
              case PincodeScreenState.confirm:
                await validatePinConfirmation(pincode);
              case PincodeScreenState.locked:
                await validatePin(pincode);
              case PincodeScreenState.unlocked:
            }
          });
        }
        return BasicScaffold(
          title:
              '', // Empty title ensures that the AppBar is always vissible. Even in locked state, when the back button is not visible.
          onBackbuttonPress: state == PincodeScreenState.locked
              ? null
              : () {
                  if (state == PincodeScreenState.confirm) {
                    ref
                        .read(pincodeScreenStateNotifierProvider.notifier)
                        .setState(PincodeScreenState.choose);
                  }
                  context.goNamed(DataUseScreen.routeName);
                },
          content: KeyboardListener(
            focusNode: _myFocusNode,
            autofocus: kIsWeb ? true : false,
            onKeyEvent: (event) {
              if (event is KeyDownEvent &&
                  event.logicalKey == LogicalKeyboardKey.backspace) {
                ref.read(pincodeNotifierProvider.notifier).removeDigit();
              } else {
                if (event.character != null) {
                  ref
                      .read(pincodeNotifierProvider.notifier)
                      .addDigit(event.character!);
                }
              }
            },
            child: Column(
              children: [
                _Header(state: state),
                _Warning(
                  isVerifying: _isVerifying,
                  widget: widget,
                ),
                addVerticalSpace(
                  responsiveValue(
                    context,
                    xs: Spacing.medium.value,
                    md: Spacing.large.value,
                  ),
                ),
                const Pincode(),
                addVerticalSpace(
                  responsiveValue(
                    context,
                    xs: Spacing.medium.value,
                    sm: Spacing.large.value,
                    md: Spacing.xlarge.value,
                  ),
                ),
                Visibility(
                  visible: state == PincodeScreenState.locked,
                  maintainState: true,
                  maintainAnimation: true,
                  maintainSize: true,
                  child: const _ForgotPincodeButton(
                    forgotPincodeButtonText: forgotPincodeButtonText,
                  ),
                ),
                Flexible(
                  child: Align(
                    alignment: responsiveValue(context,
                        xs: Alignment.bottomCenter, md: Alignment.center),
                    child: ConstrainedBox(
                      constraints:
                          const BoxConstraints(minHeight: 200, maxHeight: 350),
                      child: LayoutBuilder(
                        builder: (context, constraints) =>
                            NumPad(height: constraints.maxHeight),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
      error: (error, stackTrace) {
        context.goNamed(
          ThemedErrorScreen.routeName,
          pathParameters: {
            'isCritical': 'false',
            'redirectRoute': DataUseScreen.routeName,
          },
        );
        return const SizedBox.shrink();
      },
      loading: () => const Center(child: ThemedProgressIndicator()),
    );
  }

  Future<void> validatePinSelection(String pincode) async {
    final validationErrors =
        await ref.read(pincodeNotifierProvider.notifier).validate();

    widget.errors.addAll(validationErrors);

    if (widget.errors.isEmpty) {
      ref
          .read(pincodeScreenStateNotifierProvider.notifier)
          .setState(PincodeScreenState.confirm);

      widget.initialPincode = pincode;
    }
    ref.read(pincodeNotifierProvider.notifier).clear();
  }

  Future<void> validatePinConfirmation(String pincode) async {
    final compareErrors = await ref
        .read(pincodeNotifierProvider.notifier)
        .compare(widget.initialPincode);

    widget.errors.addAll(compareErrors);

    if (widget.errors.isEmpty) {
      await ref
          .read(userSettingsRepositoryProvider.notifier)
          .setPincode(pincode);
      await ref.read(onboardingUsecaseProvider).setHasSeenOnboarding(true);

      if (mounted) {
        ref
            .read(pincodeScreenStateNotifierProvider.notifier)
            .setState(PincodeScreenState.unlocked);
        context.goNamed(ActivationInfoScreen.routeName);
      }
    }
    ref.read(pincodeNotifierProvider.notifier).clear();
  }

  Future<void> validatePin(String pincode) async {
    final confirmedPincode = ref.read(userSettingsRepositoryProvider).pincode;

    final compareErrors = await ref
        .read(pincodeNotifierProvider.notifier)
        .compare(confirmedPincode);

    widget.errors.addAll(compareErrors);

    if (widget.errors.isEmpty) {
      if (mounted) {
        ref
            .read(pincodeScreenStateNotifierProvider.notifier)
            .setState(PincodeScreenState.unlocked);
        context.goNamed(HomeScreen.routeName);
      }
    }
    ref.read(pincodeNotifierProvider.notifier).clear();
  }
}

class _ForgotPincodeButton extends StatelessWidget {
  const _ForgotPincodeButton({
    required this.forgotPincodeButtonText,
  });

  final String forgotPincodeButtonText;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        context.goNamed(ForgotPincodeScreen.routeName);
      },
      child: Text(
        forgotPincodeButtonText,
        style: context.textTheme.headlineMedium?.copyWith(
          color: context.colorScheme.primary,
        ),
      ),
    );
  }
}

class _Warning extends StatelessWidget {
  const _Warning({
    required bool isVerifying,
    required this.widget,
  }) : _isVerifying = isVerifying;

  final bool _isVerifying;
  final PincodeScreen widget;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 72,
      child: _isVerifying
          ? Center(
              child: Padding(
                padding: EdgeInsets.all(Spacing.medium.value),
                child: const ThemedProgressIndicator(),
              ),
            )
          : PincodeWarning(
              errors: widget.errors,
            ),
    );
  }
}

class _Header extends StatelessWidget {
  final PincodeScreenState state;

  const _Header({required this.state});

  @override
  Widget build(BuildContext context) {
    String title = "";
    switch (state) {
      case PincodeScreenState.choose:
        title = "Bedenk uw pincode";
      case PincodeScreenState.confirm:
        title = "Herhaal uw pincode";
      case PincodeScreenState.unlocked:
      case PincodeScreenState.locked:
        title = "Voer uw pincode in";
    }

    return ResponsiveCenter(
      xs: 12,
      md: 10,
      lg: 8,
      xl: 6,
      child: Align(
        alignment: Alignment.topLeft,
        child: Text(
          title,
          style: context.textTheme.displayLarge,
        ),
      ),
    );
  }
}

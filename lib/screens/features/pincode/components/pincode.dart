import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/screens/features/pincode/components/pincode_digit.dart';
import 'package:user_ui/screens/features/pincode/pincode_notifier.dart';
import 'package:user_ui/theme/responsive_grid.dart';
import 'package:user_ui/theme/theme_constants.dart';

class Pincode extends ConsumerWidget {
  const Pincode({super.key});
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final pincode = ref.watch(pincodeNotifierProvider);

    return ResponsiveCenter(
      xs: 12,
      md: 10,
      lg: 8,
      xl: 6,
      child: Align(
        alignment: Alignment.topCenter,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: Spacing.medium.value),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              for (int i = 0; i < 5; i++)
                PincodeDigit(
                  dense: responsiveValue(context, xs: true, sm: false),
                  filled: pincode.length > i,
                  selected: pincode.length == i,
                ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:user_ui/entities/pincode_error.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/theme/responsive_grid.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';

class PincodeWarning extends ConsumerWidget {
  final List<PincodeError> errors;

  const PincodeWarning({
    super.key,
    required this.errors,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ResponsiveCenter(
      xs: 12,
      md: 10,
      lg: 8,
      xl: 6,
      child: Align(
        alignment: Alignment.topLeft,
        child: errors.isNotEmpty
            ? ListTile(
                minLeadingWidth: 0,
                horizontalTitleGap: Spacing.large.value,
                contentPadding: const EdgeInsets.all(0),
                dense: responsiveValue(context, xs: true, sm: false),
                titleTextStyle: Theme.of(context)
                    .textTheme
                    .titleLarge
                    ?.copyWith(
                        color: context.colorScheme.error,
                        fontWeight: FontWeight.w700),
                subtitleTextStyle: Theme.of(context)
                    .textTheme
                    .titleLarge
                    ?.copyWith(color: context.colorScheme.error),
                leading: FaIcon(
                  FontAwesomeIcons.circleExclamation,
                  size: IconSize.small.value,
                  color: context.colorScheme.error,
                ),
                title: Text(
                  errors.first.title,
                ),
                subtitle: Text(
                  errors.first.message,
                ),
              )
            : const SizedBox.shrink(),
      ),
    );
  }
}

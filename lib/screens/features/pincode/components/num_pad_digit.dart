import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:user_ui/screens/features/pincode/pincode_notifier.dart';
import 'package:user_ui/theme/theme_extensions.dart';

class NumPadDigit extends ConsumerWidget {
  final int index;
  const NumPadDigit(this.index, {super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    Text textDigit(String digit) => Text(
          digit,
          style: context.textTheme.headlineMedium?.copyWith(
            fontSize: 24,
          ),
        );

    final Widget digitWidget = index == 10
        ? textDigit("")
        : index == 11
            ? textDigit("0")
            : index == 12
                ? const FaIcon(
                    FontAwesomeIcons.deleteLeft,
                    color: Colors.black,
                  ) // TODO: Add the right icon
                : textDigit(index.toString());

    return TextButton(
      onPressed: () {
        if (index == 12) {
          ref.read(pincodeNotifierProvider.notifier).removeDigit();
        } else if (index == 10) {
          // TODO: Add biometrics button handler
        } else {
          final digit = index == 11 ? "0" : index.toString();
          ref.read(pincodeNotifierProvider.notifier).addDigit(digit);
        }
      },
      child: Center(
        child: digitWidget,
      ),
    );
  }
}

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/screens/features/pincode/components/num_pad_digit.dart';

class NumPad extends StatelessWidget {
  final double height;
  final int columns = 3;
  final int digits = 12;

  const NumPad({super.key, required this.height});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: kIsWeb ? Alignment.topCenter : Alignment.bottomCenter,
      child: SizedBox(
        height: height,
        child: ResponsiveCenter(
          xs: 12,
          md: 10,
          lg: 8,
          xl: 6,
          child: GridView.builder(
            itemCount: digits,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: columns,
              mainAxisExtent: height / (digits / columns),
            ),
            itemBuilder: (context, index) {
              return NumPadDigit(
                  index + 1); // + 1 so the first digit starts at 1
            },
          ),
        ),
      ),
    );
  }
}

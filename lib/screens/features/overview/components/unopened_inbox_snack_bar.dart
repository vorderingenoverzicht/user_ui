// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class UnopenedInboxSnackBar extends StatelessWidget {
  final VoidCallback onPressed;

  const UnopenedInboxSnackBar({super.key, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(Spacing.large.value),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(RadiusSize.medium.value),
        color: context.colorScheme.surfaceContainerHighest,
      ),
      child: Padding(
        padding: EdgeInsets.only(
          left: Spacing.large.value,
          top: Spacing.large.value,
          right: Spacing.large.value,
        ),
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Nieuwe gegevens ontvangen.",
                style: context.textTheme.titleMedium?.copyWith(
                  color: context.colorScheme.onPrimary,
                ),
              ),
            ),
            addVerticalSpace(Spacing.large.value),
            Align(
              alignment: Alignment.centerRight,
              child: TextButton(
                onPressed: onPressed,
                child: Text(
                  "Voeg toe aan overzicht",
                  style: context.textTheme.titleMedium
                      ?.copyWith(color: context.colorScheme.primaryContainer),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

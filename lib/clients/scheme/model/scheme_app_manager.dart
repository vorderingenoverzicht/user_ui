// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

class SchemeAppManager {
  String oin;
  String name;
  String discoveryUrl;
  String publicKey;

  SchemeAppManager({
    required this.oin,
    required this.name,
    required this.discoveryUrl,
    required this.publicKey,
  });

  factory SchemeAppManager.fromJson(Map<String, dynamic> jsonObject) {
    return SchemeAppManager(
      oin: jsonObject["oin"],
      name: jsonObject["name"],
      discoveryUrl: jsonObject["discoveryUrl"],
      publicKey: jsonObject["publicKey"],
    );
  }

  Map<String, dynamic> toJson() => {
        'oin': oin,
        'name': name,
        'discoveryUrl': discoveryUrl,
        'publicKey': publicKey,
      };

  @override
  bool operator ==(other) => other is SchemeAppManager && oin == other.oin;

  @override
  int get hashCode => oin.hashCode;
}

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:convert';

import 'package:dart_connect/dart_connect.dart';
import 'package:flutter/services.dart';
import 'package:user_ui/clients/citizen_financial_claim/citizen_financial_claim_client.dart';

class CitizenFinancialClaimClientMock extends CitizenFinancialClaimClient {
  @override
  Future<String> configureClaimsRequest(
    String serviceUrl,
    String sessionToken,
    Base64String encryptedEnvelope,
  ) async {
    final keys =
        jsonDecode(await rootBundle.loadString('assets/mock_app_keys.json'));
    final aesKey = keys["aesKey"];
    final encryptedMessage = Aes.encrypt(
      Base64.toBytes(aesKey),
      utf8.encode('1234'),
    );
    return Base64.parseFromBytes(encryptedMessage);
  }

  @override
  Future<String> requestFinancialClaimsInformation(
      String serviceUrl, String sessionToken, String configurationToken) async {
    var oin = "00000000000000000001";
    if (serviceUrl.contains("mock2")) {
      oin = "00000000000000000002";
    } else if (serviceUrl.contains("mock3")) {
      oin = "00000000000000000003";
    } else if (serviceUrl.contains("mock4")) {
      oin = "00000000000000000004";
    }

    var bsn = "814859094";

    Map<String, Object> jsonObject;
    jsonObject = {
      "type": "FINANCIAL_CLAIMS_INFORMATION_DOCUMENT_V3",
      "version": "3",
      "body": {
        "aangeleverd_door": oin,
        "document_datumtijd": "2023-05-08T16:59:42.892815882Z",
        "bsn": bsn,
        "financiele_zaken": [
          {
            "zaakkenmerk": "81029991234567891",
            "bsn": bsn,
            "totaal_opgelegd": 11300,
            "totaal_reeds_betaald": 0,
            "saldo": 11300,
            "saldo_datumtijd": "2023-04-01T00:00:00Z",
            "gebeurtenissen": [
              {
                "datumtijd_opgelegd": "2023-03-31T10:25:00Z",
                "zaakkenmerk": "81029991234567891",
                "beschikkingsnummer": "",
                "bsn": bsn,
                "primaire_verplichting": true,
                "type": "CJIB_WAHV",
                "categorie": "Algemeen",
                "bedrag": 10400,
                "omschrijving":
                    "Kenteken: a-000-aa; 11 km te hard buiten de bebouwde kom op 31 maart 2023",
                "juridische_grondslag_omschrijving":
                    "Boete voor te hard rijden Artikel 21 (RVV 1990)",
                "juridische_grondslag_bron":
                    "https://wetten.overheid.nl/jci1.3:c:BWBR0004825&hoofdstuk=II&paragraaf=8&artikel=21&z=2023-01-01&g=2023-01-01",
                "opgelegd_door": oin,
                "uitgevoerd_door": oin,
                "gebeurtenis_type": "FinancieleVerplichtingOpgelegd",
                "gebeurtenis_kenmerk": "81029991234567891-1",
                "datumtijd_gebeurtenis": "2023-03-31T10:25:00Z"
              },
              {
                "datumtijd_opgelegd": "2023-04-01T00:00:00Z",
                "zaakkenmerk": "81029991234567891",
                "beschikkingsnummer": "",
                "bsn": bsn,
                "primaire_verplichting": false,
                "type": "CJIB_ADMINISTRATIEKOSTEN",
                "categorie": "Administratiekosten",
                "bedrag": 900,
                "omschrijving": "Administratiekosten",
                "juridische_grondslag_omschrijving":
                    "CJIB mag op grond van Artikel 63d van het Organisatiebesluit Ministerie van Justitie en Veiligheid administratiekosten in rekening brengen",
                "juridische_grondslag_bron":
                    "https://wetten.overheid.nl/BWBR0045971/2021-12-03/0",
                "opgelegd_door": oin,
                "uitgevoerd_door": oin,
                "gebeurtenis_type": "FinancieleVerplichtingOpgelegd",
                "gebeurtenis_kenmerk": "81029991234567891-2",
                "datumtijd_gebeurtenis": "2023-04-01T00:00:00Z"
              },
              {
                "datumtijd_opgelegd": "2023-04-02T00:00:00Z",
                "zaakkenmerk": "81029991234567891",
                "bsn": bsn,
                "bedrag": 11300,
                "betaalwijze": "Handmatig",
                "te_betalen_aan": oin,
                "rekeningnummer": "GB33BUKB20201555555555",
                "rekeningnummer_tenaamstelling": "CJIB",
                "betalingskenmerk": "81029991234567891",
                "vervaldatum": "2023-05-02T00:00:00Z",
                "gebeurtenis_type": "BetalingsverplichtingOpgelegd",
                "gebeurtenis_kenmerk": "81029991234567891-3",
                "datumtijd_gebeurtenis": "2023-04-02T00:00:00Z"
              }
            ],
            "achterstanden": []
          }
        ]
      }
    };

    if (serviceUrl.contains("mock2")) {
      jsonObject = {
        "type": "FINANCIAL_CLAIMS_INFORMATION_DOCUMENT_V3",
        "version": "3",
        "body": {
          "aangeleverd_door": oin,
          "document_datumtijd": "2023-05-08T17:08:10.135410492Z",
          "bsn": bsn,
          "financiele_zaken": [
            {
              "zaakkenmerk": "81029991234567891",
              "bsn": bsn,
              "totaal_opgelegd": 11300,
              "totaal_reeds_betaald": 0,
              "saldo": 11300,
              "saldo_datumtijd": "2023-04-01T00:00:00Z",
              "gebeurtenissen": [
                {
                  "datumtijd_opgelegd": "2023-03-31T10:25:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "beschikkingsnummer": "",
                  "bsn": bsn,
                  "primaire_verplichting": true,
                  "type": "CJIB_WAHV",
                  "categorie": "Algemeen",
                  "bedrag": 10400,
                  "omschrijving":
                      "Kenteken: a-000-aa; 11 km te hard buiten de bebouwde kom op 31 maart 2023",
                  "juridische_grondslag_omschrijving":
                      "Boete voor te hard rijden Artikel 21 (RVV 1990)",
                  "juridische_grondslag_bron":
                      "https://wetten.overheid.nl/jci1.3:c:BWBR0004825&hoofdstuk=II&paragraaf=8&artikel=21&z=2023-01-01&g=2023-01-01",
                  "opgelegd_door": oin,
                  "uitgevoerd_door": oin,
                  "gebeurtenis_type": "FinancieleVerplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-1",
                  "datumtijd_gebeurtenis": "2023-03-31T10:25:00Z"
                },
                {
                  "datumtijd_opgelegd": "2023-04-01T00:00:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "beschikkingsnummer": "",
                  "bsn": bsn,
                  "primaire_verplichting": false,
                  "type": "CJIB_ADMINISTRATIEKOSTEN",
                  "categorie": "Administratiekosten",
                  "bedrag": 900,
                  "omschrijving": "Administratiekosten",
                  "juridische_grondslag_omschrijving":
                      "CJIB mag op grond van Artikel 63d van het Organisatiebesluit Ministerie van Justitie en Veiligheid administratiekosten in rekening brengen",
                  "juridische_grondslag_bron":
                      "https://wetten.overheid.nl/BWBR0045971/2021-12-03/0",
                  "opgelegd_door": oin,
                  "uitgevoerd_door": oin,
                  "gebeurtenis_type": "FinancieleVerplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-2",
                  "datumtijd_gebeurtenis": "2023-04-01T00:00:00Z"
                },
                {
                  "datumtijd_opgelegd": "2023-04-02T00:00:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "bsn": bsn,
                  "bedrag": 11300,
                  "betaalwijze": "Handmatig",
                  "te_betalen_aan": oin,
                  "rekeningnummer": "GB33BUKB20201555555555",
                  "rekeningnummer_tenaamstelling": "CJIB",
                  "betalingskenmerk": "81029991234567891",
                  "vervaldatum": "2023-05-02T00:00:00Z",
                  "gebeurtenis_type": "BetalingsverplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-3",
                  "datumtijd_gebeurtenis": "2023-04-02T00:00:00Z"
                }
              ],
              "achterstanden": ["81029991234567891-3"]
            }
          ]
        }
      };
    } else if (serviceUrl.contains("mock3")) {
      jsonObject = {
        "type": "FINANCIAL_CLAIMS_INFORMATION_DOCUMENT_V3",
        "version": "3",
        "body": {
          "aangeleverd_door": oin,
          "document_datumtijd": "2023-05-08T17:09:36.174281639Z",
          "bsn": bsn,
          "financiele_zaken": [
            {
              "zaakkenmerk": "81029991234567891",
              "bsn": bsn,
              "totaal_opgelegd": 16500,
              "totaal_reeds_betaald": 0,
              "saldo": 16500,
              "saldo_datumtijd": "2023-05-11T00:00:00Z",
              "gebeurtenissen": [
                {
                  "datumtijd_opgelegd": "2023-03-31T10:25:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "beschikkingsnummer": "",
                  "bsn": bsn,
                  "primaire_verplichting": true,
                  "type": "CJIB_WAHV",
                  "categorie": "Algemeen",
                  "bedrag": 10400,
                  "omschrijving":
                      "Kenteken: a-000-aa; 11 km te hard buiten de bebouwde kom op 31 maart 2023",
                  "juridische_grondslag_omschrijving":
                      "Boete voor te hard rijden Artikel 21 (RVV 1990)",
                  "juridische_grondslag_bron":
                      "https://wetten.overheid.nl/jci1.3:c:BWBR0004825&hoofdstuk=II&paragraaf=8&artikel=21&z=2023-01-01&g=2023-01-01",
                  "opgelegd_door": oin,
                  "uitgevoerd_door": oin,
                  "gebeurtenis_type": "FinancieleVerplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-1",
                  "datumtijd_gebeurtenis": "2023-03-31T10:25:00Z"
                },
                {
                  "datumtijd_opgelegd": "2023-04-01T00:00:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "beschikkingsnummer": "",
                  "bsn": bsn,
                  "primaire_verplichting": false,
                  "type": "CJIB_ADMINISTRATIEKOSTEN",
                  "categorie": "Administratiekosten",
                  "bedrag": 900,
                  "omschrijving": "Administratiekosten",
                  "juridische_grondslag_omschrijving":
                      "CJIB mag op grond van Artikel 63d van het Organisatiebesluit Ministerie van Justitie en Veiligheid administratiekosten in rekening brengen",
                  "juridische_grondslag_bron":
                      "https://wetten.overheid.nl/BWBR0045971/2021-12-03/0",
                  "opgelegd_door": oin,
                  "uitgevoerd_door": oin,
                  "gebeurtenis_type": "FinancieleVerplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-2",
                  "datumtijd_gebeurtenis": "2023-04-01T00:00:00Z"
                },
                {
                  "datumtijd_opgelegd": "2023-04-02T00:00:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "bsn": bsn,
                  "bedrag": 11300,
                  "betaalwijze": "Handmatig",
                  "te_betalen_aan": oin,
                  "rekeningnummer": "GB33BUKB20201555555555",
                  "rekeningnummer_tenaamstelling": "CJIB",
                  "betalingskenmerk": "81029991234567891",
                  "vervaldatum": "2023-05-02T00:00:00Z",
                  "gebeurtenis_type": "BetalingsverplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-3",
                  "datumtijd_gebeurtenis": "2023-04-02T00:00:00Z"
                },
                {
                  "ingetrokken_gebeurtenis_kenmerk": "81029991234567891-3",
                  "datumtijd_ingetrokken": "2023-05-05T00:00:00Z",
                  "gebeurtenis_type": "BetalingsverplichtingIngetrokken",
                  "gebeurtenis_kenmerk": "81029991234567891-5",
                  "datumtijd_gebeurtenis": "2023-05-05T00:00:00Z"
                },
                {
                  "datumtijd_opgelegd": "2023-05-11T00:00:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "beschikkingsnummer": "",
                  "bsn": bsn,
                  "primaire_verplichting": false,
                  "type": "CJIB_KOSTEN_EERSTE_AANMANING",
                  "categorie": "Kosten eerste aanmaning",
                  "bedrag": 5200,
                  "omschrijving": "Kosten eerste aanmaning",
                  "juridische_grondslag_omschrijving":
                      "CJIB mag op grond van Artikel 63d van het Organisatiebesluit Ministerie van Justitie en Veiligheid aanmaningskosten in rekening brengen",
                  "juridische_grondslag_bron":
                      "https://wetten.overheid.nl/BWBR0045971/2021-12-03/0",
                  "opgelegd_door": oin,
                  "uitgevoerd_door": oin,
                  "gebeurtenis_type": "FinancieleVerplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-4",
                  "datumtijd_gebeurtenis": "2023-05-11T00:00:00Z"
                },
                {
                  "datumtijd_opgelegd": "2023-05-11T00:00:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "bsn": bsn,
                  "bedrag": 16500,
                  "betaalwijze": "Handmatig",
                  "te_betalen_aan": oin,
                  "rekeningnummer": "GB33BUKB20201555555555",
                  "rekeningnummer_tenaamstelling": "CJIB",
                  "betalingskenmerk": "81029991234567891",
                  "vervaldatum": "2023-05-31T00:00:00Z",
                  "gebeurtenis_type": "BetalingsverplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-6",
                  "datumtijd_gebeurtenis": "2023-05-11T00:00:00Z"
                }
              ],
              "achterstanden": ["81029991234567891-6"]
            }
          ]
        }
      };
    } else if (serviceUrl.contains("mock4")) {
      jsonObject = {
        "type": "FINANCIAL_CLAIMS_INFORMATION_DOCUMENT_V3",
        "version": "3",
        "body": {
          "aangeleverd_door": oin,
          "document_datumtijd": "2023-05-08T17:10:32.056855524Z",
          "bsn": bsn,
          "financiele_zaken": [
            {
              "zaakkenmerk": "81029991234567891",
              "bsn": bsn,
              "totaal_opgelegd": 16500,
              "totaal_reeds_betaald": 16500,
              "saldo": 0,
              "saldo_datumtijd": "2023-05-15T00:00:00Z",
              "gebeurtenissen": [
                {
                  "datumtijd_opgelegd": "2023-03-31T10:25:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "beschikkingsnummer": "",
                  "bsn": bsn,
                  "primaire_verplichting": true,
                  "type": "CJIB_WAHV",
                  "categorie": "Algemeen",
                  "bedrag": 10400,
                  "omschrijving":
                      "Kenteken: a-000-aa; 11 km te hard buiten de bebouwde kom op 31 maart 2023",
                  "juridische_grondslag_omschrijving":
                      "Boete voor te hard rijden Artikel 21 (RVV 1990)",
                  "juridische_grondslag_bron":
                      "https://wetten.overheid.nl/jci1.3:c:BWBR0004825&hoofdstuk=II&paragraaf=8&artikel=21&z=2023-01-01&g=2023-01-01",
                  "opgelegd_door": oin,
                  "uitgevoerd_door": oin,
                  "gebeurtenis_type": "FinancieleVerplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-1",
                  "datumtijd_gebeurtenis": "2023-03-31T10:25:00Z"
                },
                {
                  "datumtijd_opgelegd": "2023-04-01T00:00:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "beschikkingsnummer": "",
                  "bsn": bsn,
                  "primaire_verplichting": false,
                  "type": "CJIB_ADMINISTRATIEKOSTEN",
                  "categorie": "Administratiekosten",
                  "bedrag": 900,
                  "omschrijving": "Administratiekosten",
                  "juridische_grondslag_omschrijving":
                      "CJIB mag op grond van Artikel 63d van het Organisatiebesluit Ministerie van Justitie en Veiligheid administratiekosten in rekening brengen",
                  "juridische_grondslag_bron":
                      "https://wetten.overheid.nl/BWBR0045971/2021-12-03/0",
                  "opgelegd_door": oin,
                  "uitgevoerd_door": oin,
                  "gebeurtenis_type": "FinancieleVerplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-2",
                  "datumtijd_gebeurtenis": "2023-04-01T00:00:00Z"
                },
                {
                  "datumtijd_opgelegd": "2023-04-02T00:00:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "bsn": bsn,
                  "bedrag": 11300,
                  "betaalwijze": "Handmatig",
                  "te_betalen_aan": oin,
                  "rekeningnummer": "GB33BUKB20201555555555",
                  "rekeningnummer_tenaamstelling": "CJIB",
                  "betalingskenmerk": "81029991234567891",
                  "vervaldatum": "2023-05-02T00:00:00Z",
                  "gebeurtenis_type": "BetalingsverplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-3",
                  "datumtijd_gebeurtenis": "2023-04-02T00:00:00Z"
                },
                {
                  "ingetrokken_gebeurtenis_kenmerk": "81029991234567891-3",
                  "datumtijd_ingetrokken": "2023-05-05T00:00:00Z",
                  "gebeurtenis_type": "BetalingsverplichtingIngetrokken",
                  "gebeurtenis_kenmerk": "81029991234567891-5",
                  "datumtijd_gebeurtenis": "2023-05-05T00:00:00Z"
                },
                {
                  "datumtijd_opgelegd": "2023-05-11T00:00:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "beschikkingsnummer": "",
                  "bsn": bsn,
                  "primaire_verplichting": false,
                  "type": "CJIB_KOSTEN_EERSTE_AANMANING",
                  "categorie": "Kosten eerste aanmaning",
                  "bedrag": 5200,
                  "omschrijving": "Kosten eerste aanmaning",
                  "juridische_grondslag_omschrijving":
                      "CJIB mag op grond van Artikel 63d van het Organisatiebesluit Ministerie van Justitie en Veiligheid aanmaningskosten in rekening brengen",
                  "juridische_grondslag_bron":
                      "https://wetten.overheid.nl/BWBR0045971/2021-12-03/0",
                  "opgelegd_door": oin,
                  "uitgevoerd_door": oin,
                  "gebeurtenis_type": "FinancieleVerplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-4",
                  "datumtijd_gebeurtenis": "2023-05-11T00:00:00Z"
                },
                {
                  "datumtijd_opgelegd": "2023-05-11T00:00:00Z",
                  "zaakkenmerk": "81029991234567891",
                  "bsn": bsn,
                  "bedrag": 16500,
                  "betaalwijze": "Handmatig",
                  "te_betalen_aan": oin,
                  "rekeningnummer": "GB33BUKB20201555555555",
                  "rekeningnummer_tenaamstelling": "CJIB",
                  "betalingskenmerk": "81029991234567891",
                  "vervaldatum": "2023-05-31T00:00:00Z",
                  "gebeurtenis_type": "BetalingsverplichtingOpgelegd",
                  "gebeurtenis_kenmerk": "81029991234567891-6",
                  "datumtijd_gebeurtenis": "2023-05-11T00:00:00Z"
                },
                {
                  "datumtijd_verwerkt": "2023-05-15T00:00:00Z",
                  "betalingskenmerk": "81029991234567891",
                  "datumtijd_ontvangen": "2023-05-14T00:00:00Z",
                  "ontvangen_door": oin,
                  "verwerkt_door": oin,
                  "bedrag": 16500,
                  "gebeurtenis_type": "BetalingVerwerkt",
                  "gebeurtenis_kenmerk": "81029991234567891-7",
                  "datumtijd_gebeurtenis": "2023-05-15T00:00:00Z"
                }
              ],
              "achterstanden": []
            }
          ]
        }
      };
    }

    //TODO: Fix this return value
    return json.encode(jsonObject);
  }
}

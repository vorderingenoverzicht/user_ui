// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:dart_connect/dart_connect.dart';

abstract class CitizenFinancialClaimClient {
  Future<String> configureClaimsRequest(
    String serviceUrl,
    String sessionToken,
    Base64String encryptedEnvelope,
  );

  Future<String> requestFinancialClaimsInformation(
    String serviceUrl,
    String sessionToken,
    String configurationToken,
  );
}

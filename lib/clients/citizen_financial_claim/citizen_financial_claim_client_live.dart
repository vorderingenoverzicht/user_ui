// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:async';
import 'dart:convert';

import 'package:dart_connect/dart_connect.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;
import 'package:user_ui/clients/citizen_financial_claim/citizen_financial_claim_client.dart';
import 'package:user_ui/clients/session_expired_exception.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

class CitizenFinancialClaimClientLive extends CitizenFinancialClaimClient {
  CitizenFinancialClaimClientLive(this.loggingHelper);

  final LoggingHelper loggingHelper;

  @override
  Future<String> configureClaimsRequest(
    String serviceUrl,
    String sessionToken,
    Base64String encryptedEnvelope,
  ) async {
    var uri = Uri.parse("$serviceUrl/configure_claims_request");

    final response = await http.post(
      uri,
      body: json.encode(encryptedEnvelope),
      headers: {
        "Authorization": sessionToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      if (response.body.contains("session expired")) {
        loggingHelper.addLog(DeviceEvent.uu_17,
            "failed to configure claims request - session expired");
        throw SessionExpiredException(
            'Failed to configure claims request - session expired');
      }
      loggingHelper.addLog(
          DeviceEvent.uu_16, "Failed to configure_claims_request");
      throw Exception('Failed to configure_claims_request');
    }
  }

  @override
  Future<String> requestFinancialClaimsInformation(
      String serviceUrl, String sessionToken, String configurationToken) async {
    var requestFinancialClaimsInformationUri =
        Uri.parse("$serviceUrl/request_financial_claims_information");

    var uri = requestFinancialClaimsInformationUri
        .replace(queryParameters: {"configurationToken": configurationToken});

    final response = await http.get(
      uri,
      headers: {
        "Authorization": sessionToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      if (response.body.contains("session expired")) {
        loggingHelper.addLog(DeviceEvent.uu_102,
            "failed to request financial claims information - session expired");
        throw SessionExpiredException(
            'Failed to request financial claims information - session expired');
      }
      loggingHelper.addLog(
          DeviceEvent.uu_19, "failed to requestFinancialClaimsInformation");
      throw Exception('Failed to requestFinancialClaimsInformation');
    }
  }
}

final citizenFinancialClaimClientProvider =
    Provider<CitizenFinancialClaimClient>((ref) {
  final logging = ref.watch(loggingProvider);
  return CitizenFinancialClaimClientLive(logging);
});

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:async';
import 'dart:convert';

import 'package:dart_connect/dart_connect.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;
import 'package:riverpod_annotation/riverpod_annotation.dart';

import 'package:user_ui/clients/session_expired_exception.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

part 'register_app_delegate.g.dart';

/// This delegate is needed because family providers are not mockable in Riverpod 2.0.
/// see: https://github.com/rrousselGit/riverpod/discussions/2510
/// and also: https://github.com/rrousselGit/riverpod/issues/3009
@riverpod
Future<String> Function(
  Ref ref,
  String serviceUrl,
  String sessionToken,
  Base64String encryptedAppIdentity,
) registerAppDelegate(Ref _) {
  return (
    ref,
    serviceUrl,
    sessionToken,
    encryptedAppIdentity,
  ) async {
    final loggingHelper = ref.read(loggingProvider);
    final uri = Uri.parse("$serviceUrl/register_app");

    final response = await http.post(
      uri,
      body: json.encode(encryptedAppIdentity),
      headers: {
        "Authorization": sessionToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      if (response.body.contains("session expired")) {
        loggingHelper.addLog(
            DeviceEvent.uu_5, "Failed to fetch certificate - session expired");
        throw SessionExpiredException(
            'Failed to fetch certificate - session expired');
      }

      loggingHelper.addLog(DeviceEvent.uu_6, "Failed to register app");
      throw Exception('Failed to register app');
    }
  };
}

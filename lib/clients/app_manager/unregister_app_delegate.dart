// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:async';
import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/app_manager/models/unregister_app_request.dart';
import 'package:user_ui/clients/session_expired_exception.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

part 'unregister_app_delegate.g.dart';

/// This delegate is needed because family providers are not mockable in Riverpod 2.0.
/// see: https://github.com/rrousselGit/riverpod/discussions/2510
/// and also: https://github.com/rrousselGit/riverpod/issues/3009
@riverpod
Future<void> Function(
  Ref ref,
  String serviceUrl,
  String sessionToken,
  UnregisterAppRequest unregisterAppRequest,
) unregisterAppDelegate(Ref _) {
  return (
    ref,
    serviceUrl,
    sessionToken,
    unregisterAppRequest,
  ) async {
    final loggingHelper = ref.read(loggingProvider);
    final uri = Uri.parse("$serviceUrl/unregister_app");

    final response = await http.post(
      uri,
      body: json.encode(unregisterAppRequest),
      headers: {
        "Authorization": sessionToken,
      },
    );

    if (response.statusCode == 200) {
      return;
    } else {
      if (response.body.contains("session expired")) {
        loggingHelper.addLog(
            DeviceEvent.uu_7, "Failed to unregister app - session expired");
        throw SessionExpiredException(
            'Failed to unregister app - session expired');
      }
      loggingHelper.addLog(DeviceEvent.uu_8, "Failed to unregister app");
      throw Exception('Failed to unregister app');
    }
  };
}

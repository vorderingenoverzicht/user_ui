// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'unregister_app_request.freezed.dart';
part 'unregister_app_request.g.dart';

@freezed
class UnregisterAppRequest with _$UnregisterAppRequest {
  const factory UnregisterAppRequest({
    required String registrationToken,
    required String signature,
  }) = _UnregisterAppRequest;

  factory UnregisterAppRequest.fromJson(Map<String, Object?> json) =>
      _$UnregisterAppRequestFromJson(json);
}

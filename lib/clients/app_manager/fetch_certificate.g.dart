// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fetch_certificate.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$fetchCertificateHash() => r'6e0e36a1d70f97941aaf7913ec2ec80d7759e619';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [fetchCertificate].
@ProviderFor(fetchCertificate)
const fetchCertificateProvider = FetchCertificateFamily();

/// See also [fetchCertificate].
class FetchCertificateFamily extends Family<AsyncValue<String>> {
  /// See also [fetchCertificate].
  const FetchCertificateFamily();

  /// See also [fetchCertificate].
  FetchCertificateProvider call({
    required String serviceUrl,
    required String sessionToken,
    required String registrationToken,
  }) {
    return FetchCertificateProvider(
      serviceUrl: serviceUrl,
      sessionToken: sessionToken,
      registrationToken: registrationToken,
    );
  }

  @override
  FetchCertificateProvider getProviderOverride(
    covariant FetchCertificateProvider provider,
  ) {
    return call(
      serviceUrl: provider.serviceUrl,
      sessionToken: provider.sessionToken,
      registrationToken: provider.registrationToken,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'fetchCertificateProvider';
}

/// See also [fetchCertificate].
class FetchCertificateProvider extends AutoDisposeFutureProvider<String> {
  /// See also [fetchCertificate].
  FetchCertificateProvider({
    required String serviceUrl,
    required String sessionToken,
    required String registrationToken,
  }) : this._internal(
          (ref) => fetchCertificate(
            ref as FetchCertificateRef,
            serviceUrl: serviceUrl,
            sessionToken: sessionToken,
            registrationToken: registrationToken,
          ),
          from: fetchCertificateProvider,
          name: r'fetchCertificateProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$fetchCertificateHash,
          dependencies: FetchCertificateFamily._dependencies,
          allTransitiveDependencies:
              FetchCertificateFamily._allTransitiveDependencies,
          serviceUrl: serviceUrl,
          sessionToken: sessionToken,
          registrationToken: registrationToken,
        );

  FetchCertificateProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.serviceUrl,
    required this.sessionToken,
    required this.registrationToken,
  }) : super.internal();

  final String serviceUrl;
  final String sessionToken;
  final String registrationToken;

  @override
  Override overrideWith(
    FutureOr<String> Function(FetchCertificateRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FetchCertificateProvider._internal(
        (ref) => create(ref as FetchCertificateRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        serviceUrl: serviceUrl,
        sessionToken: sessionToken,
        registrationToken: registrationToken,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<String> createElement() {
    return _FetchCertificateProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FetchCertificateProvider &&
        other.serviceUrl == serviceUrl &&
        other.sessionToken == sessionToken &&
        other.registrationToken == registrationToken;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, serviceUrl.hashCode);
    hash = _SystemHash.combine(hash, sessionToken.hashCode);
    hash = _SystemHash.combine(hash, registrationToken.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FetchCertificateRef on AutoDisposeFutureProviderRef<String> {
  /// The parameter `serviceUrl` of this provider.
  String get serviceUrl;

  /// The parameter `sessionToken` of this provider.
  String get sessionToken;

  /// The parameter `registrationToken` of this provider.
  String get registrationToken;
}

class _FetchCertificateProviderElement
    extends AutoDisposeFutureProviderElement<String> with FetchCertificateRef {
  _FetchCertificateProviderElement(super.provider);

  @override
  String get serviceUrl => (origin as FetchCertificateProvider).serviceUrl;
  @override
  String get sessionToken => (origin as FetchCertificateProvider).sessionToken;
  @override
  String get registrationToken =>
      (origin as FetchCertificateProvider).registrationToken;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

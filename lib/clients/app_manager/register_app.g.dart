// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_app.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$registerAppHash() => r'13754db6092d1e846f338788aba1268b6b2aebae';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [registerApp].
@ProviderFor(registerApp)
const registerAppProvider = RegisterAppFamily();

/// See also [registerApp].
class RegisterAppFamily extends Family<AsyncValue<String>> {
  /// See also [registerApp].
  const RegisterAppFamily();

  /// See also [registerApp].
  RegisterAppProvider call({
    required String serviceUrl,
    required String sessionToken,
    required String encryptedAppIdentity,
  }) {
    return RegisterAppProvider(
      serviceUrl: serviceUrl,
      sessionToken: sessionToken,
      encryptedAppIdentity: encryptedAppIdentity,
    );
  }

  @override
  RegisterAppProvider getProviderOverride(
    covariant RegisterAppProvider provider,
  ) {
    return call(
      serviceUrl: provider.serviceUrl,
      sessionToken: provider.sessionToken,
      encryptedAppIdentity: provider.encryptedAppIdentity,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'registerAppProvider';
}

/// See also [registerApp].
class RegisterAppProvider extends AutoDisposeFutureProvider<String> {
  /// See also [registerApp].
  RegisterAppProvider({
    required String serviceUrl,
    required String sessionToken,
    required String encryptedAppIdentity,
  }) : this._internal(
          (ref) => registerApp(
            ref as RegisterAppRef,
            serviceUrl: serviceUrl,
            sessionToken: sessionToken,
            encryptedAppIdentity: encryptedAppIdentity,
          ),
          from: registerAppProvider,
          name: r'registerAppProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$registerAppHash,
          dependencies: RegisterAppFamily._dependencies,
          allTransitiveDependencies:
              RegisterAppFamily._allTransitiveDependencies,
          serviceUrl: serviceUrl,
          sessionToken: sessionToken,
          encryptedAppIdentity: encryptedAppIdentity,
        );

  RegisterAppProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.serviceUrl,
    required this.sessionToken,
    required this.encryptedAppIdentity,
  }) : super.internal();

  final String serviceUrl;
  final String sessionToken;
  final String encryptedAppIdentity;

  @override
  Override overrideWith(
    FutureOr<String> Function(RegisterAppRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: RegisterAppProvider._internal(
        (ref) => create(ref as RegisterAppRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        serviceUrl: serviceUrl,
        sessionToken: sessionToken,
        encryptedAppIdentity: encryptedAppIdentity,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<String> createElement() {
    return _RegisterAppProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is RegisterAppProvider &&
        other.serviceUrl == serviceUrl &&
        other.sessionToken == sessionToken &&
        other.encryptedAppIdentity == encryptedAppIdentity;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, serviceUrl.hashCode);
    hash = _SystemHash.combine(hash, sessionToken.hashCode);
    hash = _SystemHash.combine(hash, encryptedAppIdentity.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin RegisterAppRef on AutoDisposeFutureProviderRef<String> {
  /// The parameter `serviceUrl` of this provider.
  String get serviceUrl;

  /// The parameter `sessionToken` of this provider.
  String get sessionToken;

  /// The parameter `encryptedAppIdentity` of this provider.
  String get encryptedAppIdentity;
}

class _RegisterAppProviderElement
    extends AutoDisposeFutureProviderElement<String> with RegisterAppRef {
  _RegisterAppProviderElement(super.provider);

  @override
  String get serviceUrl => (origin as RegisterAppProvider).serviceUrl;
  @override
  String get sessionToken => (origin as RegisterAppProvider).sessionToken;
  @override
  String get encryptedAppIdentity =>
      (origin as RegisterAppProvider).encryptedAppIdentity;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package

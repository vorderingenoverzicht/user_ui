// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:user_ui/clients/service_discovery/models/discovered_services.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client.dart';

class ServiceDiscoveryClientMock extends ServiceDiscoveryClient {
  @override
  Future<DiscoveredServices> fetchDiscoveredServices(
    String endpoint,
  ) async {
    return DiscoveredServices(
      sessionApi: SessionApi(v1: "http://session-api.external/v1"),
      financialClaimRequestApi: FinancialClaimRequestApi(
          v3: "http://financial-claim-request-api.external/v1"),
      registrationApi: RegistrationApi(v1: "http://session-api.external/v1"),
      appManagerUi: "app://vorijk.nl/activatie-voltooien",
    );
  }
}

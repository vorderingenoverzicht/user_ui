// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:user_ui/clients/service_discovery/models/discovered_services.dart';

abstract class ServiceDiscoveryClient {
  Future<DiscoveredServices> fetchDiscoveredServices(
    String endpoint,
  );
}

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

class DiscoveredServices {
  SessionApi sessionApi;
  FinancialClaimRequestApi? financialClaimRequestApi;
  RegistrationApi? registrationApi;
  String? appManagerUi;

  DiscoveredServices({
    required this.sessionApi,
    this.financialClaimRequestApi,
    this.registrationApi,
    this.appManagerUi,
  });

  String getFinancialClaimRequestApiV3(LoggingHelper loggingHelper) {
    if (financialClaimRequestApi == null ||
        financialClaimRequestApi!.v3.isEmpty) {
      loggingHelper.addLog(DeviceEvent.uu_34,
          "error: financial claim request api url not provided");
      throw Exception("error: financial claim request api url not provided");
    }

    return financialClaimRequestApi!.v3;
  }

  String getSessionApiV1(LoggingHelper loggingHelper) {
    if (sessionApi.v1.isEmpty) {
      loggingHelper.addLog(
          DeviceEvent.uu_35, "error: session api url not provided");
      throw Exception("error: session api url not provided");
    }

    return sessionApi.v1;
  }

  String getRegistrationApiV1(LoggingHelper loggingHelper) {
    if (registrationApi == null || registrationApi!.v1.isEmpty) {
      loggingHelper.addLog(
          DeviceEvent.uu_36, "error: registration api url not provided");
      throw Exception("error: registration api url not provided");
    }

    return registrationApi!.v1;
  }

  String getAppManagerUi(LoggingHelper loggingHelper) {
    if (appManagerUi == null || appManagerUi!.isEmpty) {
      loggingHelper.addLog(
          DeviceEvent.uu_48, "error: app manager ui url not provided");
      throw Exception("error: app manager ui url not provided");
    }

    return appManagerUi!;
  }

  factory DiscoveredServices.fromJson(Map<String, dynamic> jsonObject) {
    return DiscoveredServices(
      sessionApi: SessionApi.fromJson(jsonObject["sessionApi"]),
      financialClaimRequestApi:
          jsonObject.containsKey("financialClaimRequestApi")
              ? FinancialClaimRequestApi.fromJson(
                  jsonObject["financialClaimRequestApi"])
              : null,
      registrationApi: jsonObject.containsKey("registrationApi")
          ? RegistrationApi.fromJson(jsonObject["registrationApi"])
          : null,
      appManagerUi: jsonObject.containsKey("appManagerUi")
          ? jsonObject["appManagerUi"]
          : null,
    );
  }

  Map<String, dynamic> toJson() => {
        'sessionApi': sessionApi,
        'financialClaimRequestApi': financialClaimRequestApi,
        'registrationApi': registrationApi,
        'appManagerUi': appManagerUi,
      };
}

class FinancialClaimRequestApi {
  String v3;

  FinancialClaimRequestApi({required this.v3});

  factory FinancialClaimRequestApi.fromJson(Map<String, dynamic> jsonObject) {
    return FinancialClaimRequestApi(
      v3: jsonObject["v3"],
    );
  }

  Map<String, dynamic> toJson() => {
        'v3': v3,
      };
}

class SessionApi {
  String v1;

  SessionApi({required this.v1});

  factory SessionApi.fromJson(Map<String, dynamic> jsonObject) {
    return SessionApi(
      v1: jsonObject["v1"],
    );
  }

  Map<String, dynamic> toJson() => {
        'v1': v1,
      };
}

class RegistrationApi {
  String v1;

  RegistrationApi({required this.v1});

  factory RegistrationApi.fromJson(Map<String, dynamic> jsonObject) {
    return RegistrationApi(
      v1: jsonObject["v1"],
    );
  }

  Map<String, dynamic> toJson() => {
        'v1': v1,
      };
}

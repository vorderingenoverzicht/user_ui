// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'model/rule.dart';

abstract class PaymentPlanRulesClient {
  Future<List<Rule>> fetchRules();
}

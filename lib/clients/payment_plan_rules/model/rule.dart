// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

class Rule {
  int id;
  List<String> organisaties;
  int minBedrag;
  int maxBedrag;
  String type;

  Rule({
    required this.id,
    required this.organisaties,
    required this.minBedrag,
    required this.maxBedrag,
    required this.type,
  });

  factory Rule.fromJson(Map<String, dynamic> json) {
    var id = (json["id"] ?? []) as int;
    var organisaties = List<String>.from((json["organisaties"] ?? []) as List);
    var minBedrag = (json["minBedrag"] ?? []) as int;
    var maxBedrag = (json["maxBedrag"] ?? []) as int;
    var type = (json["type"] ?? []) as String;

    return Rule(
      id: id,
      organisaties: organisaties,
      minBedrag: minBedrag,
      maxBedrag: maxBedrag,
      type: type,
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'organisaties': organisaties,
        'minBedrag': minBedrag,
        'maxBedrag': maxBedrag,
        'type': type,
      };
}

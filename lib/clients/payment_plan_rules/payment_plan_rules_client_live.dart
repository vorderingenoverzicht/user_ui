// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;
import 'package:user_ui/clients/payment_plan_rules/payment_plan_rules_client.dart';
import 'package:user_ui/env_provider.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

import 'model/rule.dart';

class PaymentPlanRulesClientLive implements PaymentPlanRulesClient {
  PaymentPlanRulesClientLive(this.env, this.loggingHelper);
  final LoggingHelper loggingHelper;
  final Environment env;

  @override
  Future<List<Rule>> fetchRules() async {
    final Uri fetchRulesURI = Uri.parse(
        'https://field-lab-mocks.cjib.vorijk-test.blauweknop.app/regels'); //TODO: use env var for base url ticket https://gitlab.com/blauwe-knop/vorderingenoverzicht/issues/-/issues/812
    final response = await http.get(fetchRulesURI);

    if (response.statusCode == 200) {
      var responseJson = json.decode(response.body);

      var result = responseJson as List;
      var rules = result.map((i) => Rule.fromJson(i)).toList();

      return Future.value(rules);
    } else {
      loggingHelper.addLog(DeviceEvent.uu_121, "Failed to fetch rules");
      throw Exception('Failed to fetch rules');
    }
  }
}

final rulesClientProvider = Provider<PaymentPlanRulesClient>((ref) {
  final env = ref.watch(environmentProvider);
  final logging = ref.watch(loggingProvider);
  return PaymentPlanRulesClientLive(env, logging);
});

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class Challenge {
  String nonce;
  String organizationNonceSignature;
  String ephemeralOrganizationEcPublicKey;

  Challenge({
    required this.nonce,
    required this.organizationNonceSignature,
    required this.ephemeralOrganizationEcPublicKey,
  });

  factory Challenge.fromJson(Map<String, dynamic> jsonObject) {
    return Challenge(
      nonce: jsonObject["nonce"],
      organizationNonceSignature: jsonObject["organizationNonceSignature"],
      ephemeralOrganizationEcPublicKey:
          jsonObject["ephemeralOrganizationEcPublicKey"],
    );
  }

  Map<String, dynamic> toJson() => {
        'nonce': nonce,
        'organizationNonceSignature': organizationNonceSignature,
        'ephemeralOrganizationEcPublicKey': ephemeralOrganizationEcPublicKey,
      };
}

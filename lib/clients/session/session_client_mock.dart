// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:convert';

import 'package:dart_connect/dart_connect.dart';
import 'package:flutter/services.dart';
import 'package:user_ui/clients/session/models/challenge.dart';
import 'package:user_ui/clients/session/models/challenge_response.dart';
import 'package:user_ui/clients/session/session_client.dart';

class SessionClientMock implements SessionClient {
  SessionClientMock();

  @override
  Future<Challenge> requestChallenge(String serviceUrl) async {
    final appManagerKeyJson = jsonDecode(
        await rootBundle.loadString('assets/mock_app_manager_keys.json'));

    final privateKey =
        // ignore: deprecated_member_use
        Ec.parsePrivateKeyFromPem(utf8.encode(appManagerKeyJson["privateKey"]));

    // ignore: deprecated_member_use
    final signature = Ecdsa.sign(
      privateKey,
      utf8.encode("8883776d-4b2c-4019-902f-166eb02bfa0s"),
    );

    return Challenge(
      nonce: "8883776d-4b2c-4019-902f-166eb02bfa0s",
      organizationNonceSignature: Base64.parseFromBytes(signature),
      ephemeralOrganizationEcPublicKey: appManagerKeyJson["publicKey"],
    );
  }

  @override
  Future<String> completeChallenge(
      String serviceUrl, ChallengeResponse challengeResponse) async {
    final appManagerKeyJson = jsonDecode(
        await rootBundle.loadString('assets/mock_app_manager_keys.json'));
    final aesKeyBytes = Base64.toBytes(appManagerKeyJson["aesKey"]);

    final encryptedToken = Base64.parseFromBytes(
      Aes.encrypt(
        aesKeyBytes,
        utf8.encode('token'),
      ),
    );

    return encryptedToken;
  }
}

// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:user_ui/clients/session/models/challenge.dart';
import 'package:user_ui/clients/session/models/challenge_response.dart';

abstract class SessionClient {
  Future<Challenge> requestChallenge(
    String serviceUrl,
  );
  Future<String> completeChallenge(
    String serviceUrl,
    ChallengeResponse challengeResponse,
  );
}

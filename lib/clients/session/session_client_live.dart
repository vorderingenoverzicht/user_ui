// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:async';
import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;
import 'package:user_ui/clients/session/models/challenge.dart';
import 'package:user_ui/clients/session/models/challenge_response.dart';
import 'package:user_ui/clients/session/session_client.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

class SessionClientLive implements SessionClient {
  SessionClientLive(this.loggingHelper);

  final LoggingHelper loggingHelper;

  @override
  Future<Challenge> requestChallenge(
    String serviceUrl,
  ) async {
    var uri = Uri.parse("$serviceUrl/challenge");

    final response = await http.post(uri);

    if (response.statusCode == 200) {
      var responseJson = json.decode(response.body);

      var result = Challenge.fromJson(responseJson);

      return result;
    } else {
      loggingHelper.addLog(DeviceEvent.uu_22, "Failed to create challenge");
      throw Exception('Failed to create challenge');
    }
  }

  @override
  Future<String> completeChallenge(
    String serviceUrl,
    ChallengeResponse challengeResponse,
  ) async {
    var uri = Uri.parse("$serviceUrl/challenge/complete");

    final response = await http.post(
      uri,
      body: json.encode(challengeResponse),
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      loggingHelper.addLog(DeviceEvent.uu_23, "Failed to complete challenge.");
      throw Exception('Failed to complete challenge');
    }
  }
}

final sessionClientProvider = Provider<SessionClient>((ref) {
  final logging = ref.watch(loggingProvider);
  return SessionClientLive(logging);
});

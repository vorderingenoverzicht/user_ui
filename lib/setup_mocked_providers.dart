// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:user_ui/env_provider.dart';
import 'package:dart_connect/dart_connect.dart';
import 'package:fcid_library/fcid_library.dart';
import 'package:user_ui/entities/app_session.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/clients/app_manager/mock_overrides.dart';
import 'package:user_ui/clients/scheme/scheme_client_live.dart';
import 'package:user_ui/clients/session/session_client_live.dart';
import 'package:user_ui/clients/session/session_client_mock.dart';
import 'package:user_ui/clients/scheme/mocks/scheme_client_mock.dart';
import 'package:user_ui/repositories/app_identity/latest_app_session_delegate.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_mock.dart';
import 'package:user_ui/clients/citizen_financial_claim/citizen_financial_claim_client_live.dart';
import 'package:user_ui/clients/citizen_financial_claim/mocks/citizen_financial_claim_client_mock.dart';
import 'package:user_ui/repositories/payment_plan/mocks/payment_plan_group_repository_mock.dart';
import 'package:user_ui/repositories/payment_plan/mocks/payment_plan_rule_repository_mock.dart';
import 'package:user_ui/repositories/scheme_organization/mocks/scheme_organization_repository_mock.dart';
import 'package:user_ui/repositories/financial_claims_information_storage/mocks/financial_claims_information_storage_repository_mock.dart';
import 'package:user_ui/utils/shared_preferences_provider.dart';

Future<ProviderContainer> setupMockedProviders(Environment environment) async {
  final sharedPreferences = await SharedPreferences.getInstance();

  return ProviderContainer(
    overrides: [
      sharedPreferencesProvider.overrideWithValue(sharedPreferences),
      loggingDelegateProvider.overrideWith(
        (ref) => (ref, eventClassId, eventName, severityLevel, message) async {
          debugPrint('fcid_widgets error');
        },
      ),
      isPaymentPlanEnabledProvider.overrideWith(
        (ref) => false,
      ),
      getPaymentPlanGroupsProvider.overrideWith(
        (ref) => ref.watch(paymentPlanGroupRepositoryMockProvider),
      ),
      getPaymentPlanRulesProvider.overrideWith(
        (ref) => ref.watch(paymentPlanRuleRepositoryMockProvider),
      ),
      getFinancialClaimInformationDocumentListProvider.overrideWith(
        (ref) {
          return ref
              .watch(financialClaimsInformationStorageRepositoryMockProvider);
        },
      ),
      getSchemeOrganizationsProvider.overrideWith(
        (ref) => ref.watch(schemeOrganizationRepositoryMockProvider),
      ),
      getFinancialClaimInformationDocumentDelegateProvider.overrideWith(
        (ref) => (ref, id) async {
          final financialClaimsInformationStorageRepository = ref.watch(
            financialClaimsInformationStorageRepositoryMockProvider.notifier,
          );

          var financialClaimsInformationStorage =
              await financialClaimsInformationStorageRepository.getById(id);

          return financialClaimsInformationStorage;
        },
      ),
      environmentProvider.overrideWithValue(environment),
      schemeClientProvider.overrideWith((ref) {
        return SchemeClientMock();
      }),
      sessionClientProvider.overrideWith((ref) {
        return SessionClientMock();
      }),
      serviceDiscoveryClientProvider.overrideWith((ref) {
        return ServiceDiscoveryClientMock();
      }),
      citizenFinancialClaimClientProvider.overrideWith((ref) {
        return CitizenFinancialClaimClientMock();
      }),
      latestAppSessionDelegateProvider.overrideWith(
        (ref) => (
          ref,
          oin,
        ) async {
          final keys = jsonDecode(
              await rootBundle.loadString('assets/mock_app_keys.json'));
          final aesKey = keys["aesKey"];
          final aesKeyBytes = Base64.toBytes(aesKey);

          final encryptedToken = Base64.parseFromBytes(
            Aes.encrypt(
              aesKeyBytes,
              utf8.encode('token'),
            ),
          );
          return AppSession(
              id: 1, key: aesKey, token: encryptedToken, oin: oin);
        },
      ),
      environmentProvider.overrideWith((ref) => environment),
      ...await appManagerClientMockOverrides()
    ],
  );
}
